﻿using FitTracker.WebTest.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FitTracker.WebTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<UserModel> model = GetUserModels();

            List<UserViewModel> result = model
                .ToList()
                .Select(x => new UserViewModel()
                {
                    UniqueId = x.UniqueId,
                    Token = x.Token,
                    ShortToken = x.Token != null && x.Token.Length > 27 ? 
                        x.Token.Substring(0, 27) + "..." : x.Token,
                    Username = x.Username,
                    Password = x.Password
                })
                .ToList();

            return View(result);
        }

        [HttpGet]
        public ActionResult NewUser()
        {
            UserModel model = new UserModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult NewUser(UserModel model)
        {
            if (ModelState.IsValid)
            {
                model.UniqueId = Guid.NewGuid();

                JavaScriptSerializer jss = new JavaScriptSerializer();

                String jsonData = System.IO.File.ReadAllText(pathToUsers);

                List<UserModel> data = GetUserModels();
                data.Add(model);

                jsonData = jss.Serialize(data);

                System.IO.File.WriteAllText(pathToUsers, jsonData);

                return RedirectToAction("Index");
            }

            return View(model);
        }
        
        public ActionResult GetToken(Guid guid)
        {
            var host = _host;

            var request = (HttpWebRequest)WebRequest.Create(String.Format(@"{0}/client/register?userExternalId={1}&consumerKey={2}&consumerSecret={3}", host, guid.ToString(), ConfigurationManager.AppSettings["ConsumerKey"], ConfigurationManager.AppSettings["ConsumerSecret"]));

            var data = Encoding.ASCII.GetBytes("");

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            ResponseTokenModel tokenModel = jss.Deserialize<ResponseTokenModel>(responseString);

            List<UserModel> jsonData = GetUserModels();
            foreach (var item in jsonData)
            {
                if (item.UniqueId != guid) continue;
                item.Token = tokenModel.access_token;
            }

            SetUserModels(jsonData);

            return RedirectToAction("Index");
        }

        public ActionResult ConnectTrackers(String token)
        {
            return Redirect(_webhost + "/Connectors/Index?accessToken=" + token);
        }

        public ActionResult DeleteUser(Guid userId)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();

            String jsonData = System.IO.File.ReadAllText(pathToUsers);

            List<UserModel> data = GetUserModels();
            data = data.Where(x => x.UniqueId != userId).ToList();
            
            jsonData = jss.Serialize(data);

            System.IO.File.WriteAllText(pathToUsers, jsonData);
            
            return RedirectToAction("Index");
        }

        public ActionResult GetNutrition(String token)
        {
            var host = _host;

            var request = (HttpWebRequest)WebRequest.Create(String.Format(@"{0}/fitTracker/getNutrition", host));

            var data = Encoding.ASCII.GetBytes("");

            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            request.Headers.Add("Authorization", "Bearer " + token);
            
            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            List<NutritionModel> nutritionModel = jss.Deserialize<List<NutritionModel>>(responseString);

            return View(nutritionModel);
        }

        public ActionResult GetWeight(String token)
        {
            var host = _host;

            var request = (HttpWebRequest)WebRequest.Create(String.Format(@"{0}/fitTracker/getWeight", host));

            var data = Encoding.ASCII.GetBytes("");

            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            request.Headers.Add("Authorization", "Bearer " + token);

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            List<WeightModel> weightModel = jss.Deserialize<List<WeightModel>>(responseString);

            return View(weightModel);
        }

        public ActionResult GetRoutine(String token)
        {
            var host = _host;
         
            var request = (HttpWebRequest)WebRequest.Create(String.Format(@"{0}/fitTracker/getRoutine", host));

            var data = Encoding.ASCII.GetBytes("");

            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            request.Headers.Add("Authorization", "Bearer " + token);

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            List<RoutineModel> routineModel = jss.Deserialize<List<RoutineModel>>(responseString);

            return View(routineModel);
        }

        public ActionResult GetFitness(String token)
        {
            var host = _host;
         
            var request = (HttpWebRequest)WebRequest.Create(String.Format(@"{0}/fitTracker/getFitness", host));

            var data = Encoding.ASCII.GetBytes("");

            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            request.Headers.Add("Authorization", "Bearer " + token);

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            List<FitnessModel> fitnessModel = jss.Deserialize<List<FitnessModel>>(responseString);

            return View(fitnessModel);
        }

        public ActionResult GetPulse(String token)
        {
            var host = _host;
         
            var request = (HttpWebRequest)WebRequest.Create(String.Format(@"{0}/fitTracker/getPulse", host));

            var data = Encoding.ASCII.GetBytes("");

            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            request.Headers.Add("Authorization", "Bearer " + token);

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            List<PulseModel> pulseModel = jss.Deserialize<List<PulseModel>>(responseString);

            return View(pulseModel);
        }

        public ActionResult GetSleep(String token)
        {
            var host = _host;

            var request = (HttpWebRequest)WebRequest.Create(String.Format(@"{0}/fitTracker/getSleep", host));

            var data = Encoding.ASCII.GetBytes("");

            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            request.Headers.Add("Authorization", "Bearer " + token);

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            List<SleepModel> sleepModel = jss.Deserialize<List<SleepModel>>(responseString);

            return View(sleepModel);
        }

        private readonly String pathToUsers = AppDomain.CurrentDomain.BaseDirectory + @"\users.json";

        private List<UserModel> GetUserModels()
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String jsonData = System.IO.File.ReadAllText(pathToUsers);
            List<UserModel> data = new List<UserModel>();


            if (!String.IsNullOrEmpty(jsonData))
            {
                data = jss.Deserialize<List<UserModel>>(jsonData);
            }

            return data;
        }

        private void SetUserModels(List<UserModel> data)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String jsonData = jss.Serialize(data);
            System.IO.File.WriteAllText(pathToUsers, jsonData);
        }

        private readonly String _host = ConfigurationManager.AppSettings["host"];
        private readonly String _webhost = ConfigurationManager.AppSettings["webhost"];
    }
}