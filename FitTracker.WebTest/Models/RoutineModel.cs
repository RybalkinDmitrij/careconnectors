﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.WebTest.Models
{
    public class RoutineModel
    {
        public String _userId { get; set; }

        public String timestamp { get; set; }

        public String utc_offset { get; set; }

        public Int32 steps { get; set; }

        public Decimal distance { get; set; }

        public Int32 floors { get; set; }

        public Decimal elevation { get; set; }

        public Decimal calories_burned { get; set; }

        public String source { get; set; }
    }
}