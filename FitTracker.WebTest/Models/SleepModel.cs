﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.WebTest.Models
{
    public class SleepModel
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String _userId { get; set; }

        /// <summary>
        /// Timestamp for the measurement set
        /// </summary>
        public String timestamp { get; set; }

        /// <summary>
        /// Timezone information for the measurement set
        /// </summary>
        public String utc_offset { get; set; }

        /// <summary>
        /// The value of the measured quantity in seconds
        /// </summary>
        public Int32 total_sleep { get; set; }

        /// <summary>
        /// The value of the measured quantity in seconds
        /// </summary>
        public Int32 awake { get; set; }

        /// <summary>
        /// The value of the measured quantity in seconds
        /// </summary>
        public Int32 deep { get; set; }

        /// <summary>
        /// The value of the measured quantity in seconds
        /// </summary>
        public Int32 light { get; set; }

        /// <summary>
        /// The value of the measured quantity in seconds
        /// </summary>
        public Int32 rem { get; set; }

        /// <summary>
        /// The value of the measured quantity
        /// </summary>
        public Int32 times_woken { get; set; }

        /// <summary>
        /// The short name of the application that recorded the activity
        /// </summary>
        public String source { get; set; }
    }
}