﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.WebTest.Models
{
    public class FitnessModel
    {
        public String _userId { get; set; }

        public String timestamp { get; set; }

        public String utc_offset { get; set; }

        public String type { get; set; }

        public String intensity { get; set; }

        public String start_time { get; set; }

        public Decimal distance { get; set; }

        public Decimal duration { get; set; }

        public Decimal calories { get; set; }

        public String source { get; set; }
    }
}