﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.WebTest.Models
{
    public class NutritionModel
    {
        public String _userId { get; set; }

        public String timestamp { get; set; }

        public String utc_offset { get; set; }

        public Decimal calories { get; set; }

        public Decimal carbohydrates { get; set; }

        public Decimal fat { get; set; }

        public Decimal fiber { get; set; }

        public Decimal protein { get; set; }

        public Decimal sodium { get; set; }

        public Decimal water { get; set; }

        public String meal { get; set; }

        public String source { get; set; }
    }
}