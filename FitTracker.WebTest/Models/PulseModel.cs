﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.WebTest.Models
{
    public class PulseModel
    {
        public String _userId { get; set; }

        public String timestamp { get; set; }

        public String utc_offset { get; set; }

        public Decimal diastolic_blood_pressure { get; set; }

        public Decimal systolic_blood_pressure { get; set; }

        public Decimal heart_pulse { get; set; }

        public String source { get; set; }
    }
}