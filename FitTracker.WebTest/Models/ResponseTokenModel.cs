﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.WebTest.Models
{
    public class ResponseTokenModel
    {
        public String access_token { get; set; }
    }
}