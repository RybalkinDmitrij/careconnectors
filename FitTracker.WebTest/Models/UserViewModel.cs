﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.WebTest.Models
{
    public class UserViewModel
    {
        public Guid UniqueId { get; set; }

        public String Token { get; set; }

        public String ShortToken { get; set; }

        public String Username { get; set; }

        public String Password { get; set; }
    }
}