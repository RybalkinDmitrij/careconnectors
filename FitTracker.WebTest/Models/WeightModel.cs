﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.WebTest.Models
{
    public class WeightModel
    {
        public String _userId { get; set; }

        public String timestamp { get; set; }

        public String utc_offset { get; set; }

        public Decimal weight { get; set; }

        public Decimal height { get; set; }

        public Decimal free_mass { get; set; }

        public Decimal fat_percent { get; set; }

        public Decimal mass_weight { get; set; }

        public Decimal bmi { get; set; }

        public String source { get; set; }
    }
}