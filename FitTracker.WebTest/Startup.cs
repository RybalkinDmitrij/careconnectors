﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FitTracker.WebTest.Startup))]
namespace FitTracker.WebTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
