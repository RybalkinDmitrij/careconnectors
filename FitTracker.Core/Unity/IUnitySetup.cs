﻿using Microsoft.Practices.Unity;

namespace FitTracker.Core.Unity
{
    public interface IUnitySetup
    {
        IUnityContainer RegisterTypes(IUnityContainer container);
    }
}
