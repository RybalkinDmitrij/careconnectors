﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Enums.Strava
{
    public enum BikeType
    {
        /// <summary>
        /// The bike is a mountain bike.
        /// </summary>
        Mountain,
        /// <summary>
        /// The bike is a cross bike.
        /// </summary>
        Cross,
        /// <summary>
        /// The bike is a road bike.
        /// </summary>
        Road,
        /// <summary>
        /// The bike is a time trial bike.
        /// </summary>
        Timetrial
    }
}
