﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Enums.Strava
{
    public enum ActivityType
    {
        /// <summary>
        /// Ride
        /// </summary>
        Ride,
        /// <summary>
        /// Run
        /// </summary>
        Run,
        /// <summary>
        /// Swim
        /// </summary>
        Swim,
        /// <summary>
        /// Hike
        /// </summary>
        Hike,
        /// <summary>
        /// Walk
        /// </summary>
        Walk,
        /// <summary>
        /// Nordic Ski
        /// </summary>
        NordicSki,
        /// <summary>
        /// Alpine Ski
        /// </summary>
        AlpineSki,
        /// <summary>
        /// Backcountry Ski
        /// </summary>
        BackcountrySki,
        /// <summary>
        /// Ice Skate
        /// </summary>
        IceSkate,
        /// <summary>
        /// Inline Skate
        /// </summary>
        InlineSkate,
        /// <summary>
        /// Kite Surf
        /// </summary>
        Kitesurf,
        /// <summary>
        /// Roller Ski
        /// </summary>
        RollerSki,
        /// <summary>
        /// Windsurf
        /// </summary>
        Windsurf,
        /// <summary>
        /// Workout
        /// </summary>
        Workout,
        /// <summary>
        /// Snowboard
        /// </summary>
        Snowboard,
        /// <summary>
        /// Snowshoe
        /// </summary>
        Snowshoe
    }
}
