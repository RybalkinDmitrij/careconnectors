﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Enums.Strava
{
    /// <summary>
    /// Used by the Club class.
    /// </summary>
    public enum SportType
    {
        /// <summary>
        /// The club is for cyclists.
        /// </summary>
        Cycling,
        /// <summary>
        /// The club is for runners.
        /// </summary>
        Running,
        /// <summary>
        /// The club is for triathletes.
        /// </summary>
        Triathlon,
        /// <summary>
        /// Other club.
        /// </summary>
        Other
    }
}
