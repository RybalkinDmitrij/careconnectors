﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Enums.Fitbit
{
    public enum DeviceType
    {
        Tracker = 1,
        Scale = 2
    }
}
