﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Enums.Fitbit
{
    public enum APICollectionType
    {
        activities,
        foods,
        meals,
        sleep,
        body,
        user,
        weight    
    }
}
