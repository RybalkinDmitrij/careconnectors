﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Enums.Fitbit
{
    public enum Gender
    {
        NA,
        MALE,
        FEMALE
    }
}
