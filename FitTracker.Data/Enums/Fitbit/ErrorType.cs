﻿using FitTracker.Data.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Enums.Fitbit
{
    public enum ErrorType
    {
        [StringValue("validation")]
        Validation,
        [StringValue("oauth")]
        OAuth,
        [StringValue("request")]
        Request,
        [StringValue("not_found")]
        NotFound,
        [StringValue("system")]
        System
    }
}
