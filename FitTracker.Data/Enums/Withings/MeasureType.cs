﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Enums.Withings
{
    public enum MeasureType
    {
        BodyScale = 1,
        HeartRate = 2
    }
}
