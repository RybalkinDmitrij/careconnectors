﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Enums.Auth
{
    public enum TypeTracker
    {
        All = -1,
        Fitbit = 0,
        Withings = 1,
        Jawbone = 2,
        Runkeeper = 3
    }
}
