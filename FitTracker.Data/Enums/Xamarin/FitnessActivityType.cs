﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Enums.Xamarin
{
    public enum FitnessActivityType
    {
        Bicycle,
        WalkRun,
        Floors,
        Training,
        Workout,
        Steps,
        ActivityEnergy,
        RestEnergy,
        NikeFuel
    }
}
