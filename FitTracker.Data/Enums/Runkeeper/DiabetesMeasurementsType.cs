﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Enums.Runkeeper
{
    public enum DiabetesMeasurementsType
    {
        CPeptide,
        FastingPlasmaGlucoseTest,
        HemoglobinA1c ,
        Insulin,
        OralGlucoseToleranceTest,
        RandomPlasmaGlucoseTest,
        Triglyceride
    }
}
