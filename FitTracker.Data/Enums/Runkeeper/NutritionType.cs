﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Enums.Runkeeper
{
    public enum NutritionType
    {
        Calories,
        Carbohydrates,
        Fat,
        Fiber,
        Protein,
        Sodium,
        Water
    }
}
