﻿using System.Data.Entity;
using FitTracker.Data.Migrations;
using Microsoft.Practices.Unity;

using FitTracker.Core.Unity;

using IFitbit = FitTracker.Data.Repo.Fitbit;
using ImplFitbit = FitTracker.Data.Repo.Fitbit.Impl;

using ILog = FitTracker.Data.Repo.Log;
using ImplLog = FitTracker.Data.Repo.Log.Impl;

using IAuth = FitTracker.Data.Repo.Auth;
using ImplAuth = FitTracker.Data.Repo.Auth.Impl;

using IWithings = FitTracker.Data.Repo.Withings;
using ImplWithings = FitTracker.Data.Repo.Withings.Impl;

using IJawbone = FitTracker.Data.Repo.Jawbone;
using ImplJawbone = FitTracker.Data.Repo.Jawbone.Impl;

using IRunkeeper = FitTracker.Data.Repo.Runkeeper;
using ImplRunkeeper = FitTracker.Data.Repo.Runkeeper.Impl;

using IStrava = FitTracker.Data.Repo.Strava;
using ImplStrava = FitTracker.Data.Repo.Strava.Impl;

using IMisfit = FitTracker.Data.Repo.Misfit;
using ImplMisfit = FitTracker.Data.Repo.Misfit.Impl;

using IHealhKit = FitTracker.Data.Repo.Xamarin;
using ImplHealhKit = FitTracker.Data.Repo.Xamarin.Impl;

namespace FitTracker.Data
{
    public class UnitySetup : IUnitySetup
    {
        //public IUnityContainer Initialise()
        //{
        //    var container = new UnityContainer();

        //    RegisterTypes(container);

        //    var serviceProvider = new UnityServiceLocator(container);
        //    ServiceLocator.SetLocatorProvider(() => serviceProvider);

        //    return container;
        //}

        public IUnityContainer RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IUnitOfWork, UnitOfWork<FitTrackerContext>>();

            container.RegisterType<IAuth.IClientRepo, ImplAuth.ClientRepo>();
            container.RegisterType<IAuth.IApplicantRepo, ImplAuth.ApplicantRepo>();
            container.RegisterType<IAuth.IDbUserRepo, ImplAuth.DbUserRepo>();

            container.RegisterType<ILog.ILogErrorRepo, ImplLog.LogErrorRepo>();
            container.RegisterType<ILog.ILogActionRepo, ImplLog.LogActionRepo>();

            container.RegisterType<IFitbit.IUserRepo, ImplFitbit.UserRepo>();
            container.RegisterType<IFitbit.IUserProfileRepo, ImplFitbit.UserProfileRepo>();
            container.RegisterType<IFitbit.IActivityDistanceRepo, ImplFitbit.ActivityDistanceRepo>();
            container.RegisterType<IFitbit.IActivityGoalsRepo, ImplFitbit.ActivityGoalsRepo>();
            container.RegisterType<IFitbit.IActivityLogRepo, ImplFitbit.ActivityLogRepo>();
            container.RegisterType<IFitbit.IActivitySummaryRepo, ImplFitbit.ActivitySummaryRepo>();
            container.RegisterType<IFitbit.IBodyRepo, ImplFitbit.BodyRepo>();
            container.RegisterType<IFitbit.IBodyGoalsRepo, ImplFitbit.BodyGoalsRepo>();
            container.RegisterType<IFitbit.IDeviceRepo, ImplFitbit.DeviceRepo>();
            container.RegisterType<IFitbit.IFatLogRepo, ImplFitbit.FatLogRepo>();
            container.RegisterType<IFitbit.IFoodGoalsRepo, ImplFitbit.FoodGoalsRepo>();
            container.RegisterType<IFitbit.IFoodLogRepo, ImplFitbit.FoodLogRepo>();
            container.RegisterType<IFitbit.IFoodLogUnitRepo, ImplFitbit.FoodLogUnitRepo>();
            container.RegisterType<IFitbit.IFoodSummaryRepo, ImplFitbit.FoodSummaryRepo>();
            container.RegisterType<IFitbit.ILoggedFoodRepo, ImplFitbit.LoggedFoodRepo>();
            container.RegisterType<IFitbit.IMinuteDataRepo, ImplFitbit.MinuteDataRepo>();
            container.RegisterType<IFitbit.INutritionalValuesRepo, ImplFitbit.NutritionalValuesRepo>();
            container.RegisterType<IFitbit.ISleepLogRepo, ImplFitbit.SleepLogRepo>();
            container.RegisterType<IFitbit.ISleepSummaryRepo, ImplFitbit.SleepSummaryRepo>();
            container.RegisterType<IFitbit.IWaterRepo, ImplFitbit.WaterRepo>();
            container.RegisterType<IFitbit.IWaterGoalRepo, ImplFitbit.WaterGoalRepo>();
            container.RegisterType<IFitbit.IWeightLogRepo, ImplFitbit.WeightLogRepo>();

            container.RegisterType<IWithings.IActivityMeasureRepo, ImplWithings.ActivityMeasureRepo>();
            container.RegisterType<IWithings.IBodyMeasureRepo, ImplWithings.BodyMeasureRepo>();
            container.RegisterType<IWithings.IBodyMeasureItemRepo, ImplWithings.BodyMeasureItemRepo>();
            container.RegisterType<IWithings.IIntradayActivityRepo, ImplWithings.IntradayActivityRepo>();
            container.RegisterType<IWithings.ISleepMeasureRepo, ImplWithings.SleepMeasureRepo>();
            container.RegisterType<IWithings.ISleepSummaryRepo, ImplWithings.SleepSummaryRepo>();

            container.RegisterType<IJawbone.IAccountInfoRepo, ImplJawbone.AccountInfoRepo>();
            container.RegisterType<IJawbone.IBodyEventRepo, ImplJawbone.BodyEventRepo>();
            container.RegisterType<IJawbone.ICustomRepo, ImplJawbone.CustomRepo>();
            container.RegisterType<IJawbone.IGoalsRepo, ImplJawbone.GoalsRepo>();
            container.RegisterType<IJawbone.IHeartRateRepo, ImplJawbone.HeartRateRepo>();
            container.RegisterType<IJawbone.IHourlyTotalRepo, ImplJawbone.HourlyTotalRepo>();
            container.RegisterType<IJawbone.IMealsRepo, ImplJawbone.MealsRepo>();
            container.RegisterType<IJawbone.IMoodRepo, ImplJawbone.MoodRepo>();
            container.RegisterType<IJawbone.IMovesRepo, ImplJawbone.MovesRepo>();
            container.RegisterType<IJawbone.IMovesTickRepo, ImplJawbone.MovesTickRepo>();
            container.RegisterType<IJawbone.ISettingsRepo, ImplJawbone.SettingsRepo>();
            container.RegisterType<IJawbone.ISleepsRepo, ImplJawbone.SleepsRepo>();
            container.RegisterType<IJawbone.ITimezoneRepo, ImplJawbone.TimezoneRepo>();
            container.RegisterType<IJawbone.IWorkoutsRepo, ImplJawbone.WorkoutsRepo>();

            container.RegisterType<IRunkeeper.ICaloriesRepo, ImplRunkeeper.CaloriesRepo>();
            container.RegisterType<IRunkeeper.IDistancesRepo, ImplRunkeeper.DistancesRepo>();
            container.RegisterType<IRunkeeper.IFitnessActivitiesRepo, ImplRunkeeper.FitnessActivitiesRepo>();
            container.RegisterType<IRunkeeper.IHeartRatesRepo, ImplRunkeeper.HeartRatesRepo>();
            container.RegisterType<IRunkeeper.IImageRepo, ImplRunkeeper.ImageRepo>();
            container.RegisterType<IRunkeeper.IPathRepo, ImplRunkeeper.PathRepo>();
            container.RegisterType<IRunkeeper.IBackgroundActivitiesRepo, ImplRunkeeper.BackgroundActivitiesRepo>();
            container.RegisterType<IRunkeeper.IDiabetesMeasurementsRepo, ImplRunkeeper.DiabetesMeasurementsRepo>();
            container.RegisterType<IRunkeeper.IExercisesRepo, ImplRunkeeper.ExercisesRepo>();
            container.RegisterType<IRunkeeper.ISetsRepo, ImplRunkeeper.SetsRepo>();
            container.RegisterType<IRunkeeper.IStrengthTrainingRepo, ImplRunkeeper.StrengthTrainingRepo>();
            container.RegisterType<IRunkeeper.INutritionRepo, ImplRunkeeper.NutritionRepo>();
            container.RegisterType<IRunkeeper.IGeneralMeasurementsRepo, ImplRunkeeper.GeneralMeasurementsRepo>();
            container.RegisterType<IRunkeeper.IProfileRepo, ImplRunkeeper.ProfileRepo>();
            container.RegisterType<IRunkeeper.ISleepRepo, ImplRunkeeper.SleepRepo>();
            container.RegisterType<IRunkeeper.IWeightRepo, ImplRunkeeper.WeightRepo>();

            container.RegisterType<IStrava.IAthleteRepo, ImplStrava.AthleteRepo>();
            container.RegisterType<IStrava.IBikeRepo, ImplStrava.BikeRepo>();
            container.RegisterType<IStrava.IClubRepo, ImplStrava.ClubRepo>();
            container.RegisterType<IStrava.IGearRepo, ImplStrava.GearRepo>();
            container.RegisterType<IStrava.IShoesRepo, ImplStrava.ShoesRepo>();
            container.RegisterType<IStrava.IActivityRepo, ImplStrava.ActivityRepo>();
            container.RegisterType<IStrava.IMapRepo, ImplStrava.MapRepo>();
            container.RegisterType<IStrava.IActivityLapRepo, ImplStrava.ActivityLapRepo>();
            container.RegisterType<IStrava.ICommentRepo, ImplStrava.CommentRepo>();
            container.RegisterType<IStrava.IPhotoRepo, ImplStrava.PhotoRepo>();

            container.RegisterType<IMisfit.IDeviceRepo, ImplMisfit.DeviceRepo>();
            container.RegisterType<IMisfit.IProfileInfoRepo, ImplMisfit.ProfileInfoRepo>();
            container.RegisterType<IMisfit.IGoalRepo, ImplMisfit.GoalRepo>();
            container.RegisterType<IMisfit.ISessionRepo, ImplMisfit.SessionRepo>();
            container.RegisterType<IMisfit.ISleepRepo, ImplMisfit.SleepRepo>();
            container.RegisterType<IMisfit.ISummaryRepo, ImplMisfit.SummaryRepo>();
            container.RegisterType<IMisfit.ISleepDetailRepo, ImplMisfit.SleepDetailRepo>();

            container.RegisterType<IHealhKit.IBaseDataRepo, ImplHealhKit.BaseDataRepo>();
            container.RegisterType<IHealhKit.IBodyMeasurementRepo, ImplHealhKit.BodyMeasurementRepo>();
            container.RegisterType<IHealhKit.IFitnessActivityRepo, ImplHealhKit.FitnessActivityRepo>();
            container.RegisterType<IHealhKit.ISleepRepo, ImplHealhKit.SleepRepo>();
            
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<FitTrackerContext, Configuration>());

            return container;
        }
    }
}
