﻿using System;

namespace FitTracker.Data
{
    public interface IUnitOfWork : IDisposable
    {
        Int32 Commit();

        TRepo GetRepo<TRepo>();
    }
}
