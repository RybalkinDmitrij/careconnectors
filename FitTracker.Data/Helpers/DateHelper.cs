﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Helpers
{
    public static class DateHelper
    {
        public static DateTime FromUnixTime(long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        public static long ToUnixTime(DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date - epoch).TotalSeconds);
        }

        public static DateTime FromFormatTime(long time, FormatDateHelper format)
        {
            if (time != 0)
            {
                if (format == FormatDateHelper.ddMMyyyy)
                {
                    var strTime = time.ToString();

                    return new DateTime(Convert.ToInt32(strTime.Substring(4, 4)), Convert.ToInt32(strTime.Substring(2, 2)), Convert.ToInt32(strTime.Substring(0, 2)));
                }
                else if (format == FormatDateHelper.yyyyMMdd)
                {
                    var strTime = time.ToString();

                    return new DateTime(Convert.ToInt32(strTime.Substring(0, 4)), Convert.ToInt32(strTime.Substring(4, 2)), Convert.ToInt32(strTime.Substring(6, 2)));
                }
            }

            return DateTime.MinValue;
        }
    }

    public enum FormatDateHelper
    {
        ddMMyyyy = 0,
        yyyyMMdd = 1
    }
}
