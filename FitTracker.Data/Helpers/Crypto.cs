﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Helpers
{
    public static class Crypto
    {
        public static String DoMd5Hash(String source)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                return GetMd5Hash(md5Hash, source);
            }
        }

        public static Boolean VerifyMd5Hash(String input, String hash)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                // Hash the input.
                String hashOfInput = GetMd5Hash(md5Hash, input);

                // Create a StringComparer an compare the hashes.
                StringComparer comparer = StringComparer.OrdinalIgnoreCase;

                if (0 == comparer.Compare(hashOfInput, hash))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static String DoConsumerKey()
        {
            using (MD5 md5Hash = MD5.Create())
            {
                String source = Guid.NewGuid().ToString() + DateHelper.ToUnixTime(DateTime.Now) + Guid.NewGuid().ToString();
                return GetMd5Hash(md5Hash, source);
            }
        }

        public static String DoConsumerSecret()
        {
            using (MD5 md5Hash = MD5.Create())
            {
                String source = DateHelper.ToUnixTime(DateTime.Now) + Guid.NewGuid().ToString() + "FITtracker Project" + DateTime.UtcNow.ToLongTimeString();
                return GetMd5Hash(md5Hash, source);
            }
        }

        public static String DoAspNetUserName()
        {
            using (MD5 md5Hash = MD5.Create())
            {
                String source = Guid.NewGuid().ToString();
                return GetMd5Hash(md5Hash, source);
            }
        }

        public static String DoAspNetEmail()
        {
            using (MD5 md5Hash = MD5.Create())
            {
                String source = Guid.NewGuid().ToString();
                return "fake" + GetMd5Hash(md5Hash, source) + "@fake.com";
            }
        }

        public static String DoAspNetUserPassword()
        {
            using (MD5 md5Hash = MD5.Create())
            {
                String source = Guid.NewGuid().ToString();
                return GetMd5Hash(md5Hash, source) + "123dF3!nud";
            }
        }

        private static String GetMd5Hash(MD5 md5Hash, string input)
        {
            // Convert the input string to a byte array and compute the hash.
            Byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (Int32 i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
    }
}
