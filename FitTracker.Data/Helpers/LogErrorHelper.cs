﻿using FitTracker.Core.Unity;
using FitTracker.Data.Repo.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Helpers
{
    public static class LogErrorHelper
    {
        public static void SaveError(String message, Int64 userId = 0)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repoLogError = uow.GetRepo<ILogErrorRepo>();

                repoLogError.Save(message);

                uow.Commit();
            }
        }
    }
}
