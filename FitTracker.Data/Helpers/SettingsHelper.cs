﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Data.Helpers
{
    public class SettingsHelper
    {
        private static string _fitBitConsumerKey;
        private static string _fitBitConsumerSecret;

        private static string _withingsConsumerKey;
        private static string _withingsConsumerSecret;

        private static string _jawboneConsumerKey;
        private static string _jawboneConsumerSecret;

        private static string _runkeeperConsumerKey;
        private static string _runkeeperConsumerSecret;

        private static string _stravaConsumerKey;
        private static string _stravaConsumerSecret;

        private static string _misfitConsumerKey;
        private static string _misfitConsumerSecret;

        /// <summary>
        /// Initailize settings from Web.Config that will be used throughout the application.
        /// </summary>
        public static void Initialize(String fitBitConsumerKey = "", String fitBitConsumerSecret = "",
                                        String withingsConsumerKey = "", String withingsConsumerSecret = "",
                                        String jawboneConsumerKey = "", String jawboneConsumerSecret = "",
                                        String runkeeperConsumerKey = "", String runkeeperConsumerSecret = "",
                                        String stravaConsumerKey = "", String stravaConsumerSecret = "",
                                        String misfitConsumerKey = "", String misfitConsumerSecret = "")
        {
            try
            {
                if (String.IsNullOrEmpty(fitBitConsumerKey))
                {
                    throw new Exception("INVALID_FITBITCONSUMERKEY");
                }
                else if (String.IsNullOrEmpty(fitBitConsumerSecret))
                {
                    throw new Exception("INVALID_FITBITCONSUMERSECRET");
                }
                else if (String.IsNullOrEmpty(withingsConsumerKey))
                {
                    throw new Exception("INVALID_WITHINGSCONSUMERKEY");
                }
                else if (String.IsNullOrEmpty(withingsConsumerSecret))
                {
                    throw new Exception("INVALID_WITHINGSCONSUMERSECRET");
                }
                else if (String.IsNullOrEmpty(jawboneConsumerKey))
                {
                    throw new Exception("INVALID_JAWBONECONSUMERKEY");
                }
                else if (String.IsNullOrEmpty(jawboneConsumerSecret))
                {
                    throw new Exception("INVALID_JAWBONECONSUMERSECRET");
                }
                else if (String.IsNullOrEmpty(runkeeperConsumerKey))
                {
                    throw new Exception("INVALID_RUNKEEPERCONSUMERKEY");
                }
                else if (String.IsNullOrEmpty(runkeeperConsumerSecret))
                {
                    throw new Exception("INVALID_RUNKEEPERCONSUMERSECRET");
                }
                else if (String.IsNullOrEmpty(stravaConsumerKey))
                {
                    throw new Exception("INVALID_STRAVACONSUMERKEY");
                }
                else if (String.IsNullOrEmpty(stravaConsumerSecret))
                {
                    throw new Exception("INVALID_STRAVACONSUMERSECRET");
                }
                else if (String.IsNullOrEmpty(misfitConsumerKey))
                {
                    throw new Exception("INVALID_MISFITCONSUMERKEY");
                }
                else if (String.IsNullOrEmpty(misfitConsumerSecret))
                {
                    throw new Exception("INVALID_MISFITCONSUMERSECRET");
                }

                _fitBitConsumerKey = fitBitConsumerKey;
                _fitBitConsumerSecret = fitBitConsumerSecret;
                _withingsConsumerKey = withingsConsumerKey;
                _withingsConsumerSecret = withingsConsumerSecret;
                _jawboneConsumerKey = jawboneConsumerKey;
                _jawboneConsumerSecret = jawboneConsumerSecret;
                _runkeeperConsumerKey = runkeeperConsumerKey;
                _runkeeperConsumerSecret = runkeeperConsumerSecret;
                _stravaConsumerKey = stravaConsumerKey;
                _stravaConsumerSecret = stravaConsumerSecret;
                _misfitConsumerKey = misfitConsumerKey;
                _misfitConsumerSecret = misfitConsumerSecret;
            }
            catch (Exception ex)
            {
                LogErrorHelper.SaveError(ex.Message);
            }
        }

        public static string FitBitConsumerKey
        {
            get
            {
                return _fitBitConsumerKey;
            }
        }

        public static string FitBitConsumerSecret
        {
            get
            {
                return _fitBitConsumerSecret;
            }
        }

        public static string WithingsConsumerKey
        {
            get
            {
                return _withingsConsumerKey;
            }
        }

        public static string WithingsConsumerSecret
        {
            get
            {
                return _withingsConsumerSecret;
            }
        }

        public static string JawboneConsumerKey
        {
            get
            {
                return _jawboneConsumerKey;
            }
        }

        public static string JawboneConsumerSecret
        {
            get
            {
                return _jawboneConsumerSecret;
            }
        }

        public static string RunkeeperConsumerKey
        {
            get
            {
                return _runkeeperConsumerKey;
            }
        }

        public static string RunkeeperConsumerSecret
        {
            get
            {
                return _runkeeperConsumerSecret;
            }
        }

        public static string StravaConsumerKey
        {
            get
            {
                return _stravaConsumerKey;
            }
        }

        public static string StravaConsumerSecret
        {
            get
            {
                return _stravaConsumerSecret;
            }
        }

        public static string MisfitConsumerKey
        {
            get
            {
                return _misfitConsumerKey;
            }
        }

        public static string MisfitConsumerSecret
        {
            get
            {
                return _misfitConsumerSecret;
            }
        }
        
        /// <summary>
        /// Available fitness tracker device types
        /// </summary>
        public enum FitnessTrackers
        {
            FitBit,
            Withings,
            Jawbone,
            Runkeeper,
            Strava,
            Misfit
        }
    }
}