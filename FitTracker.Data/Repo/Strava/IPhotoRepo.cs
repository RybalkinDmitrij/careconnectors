﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Strava;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Strava.Api.Activities;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Strava
{
    public interface IPhotoRepo : IRepoBase<Photo>
    {
        /// <summary>
        /// List of Photo received by athleteId
        /// </summary>
        /// <param name="athleteId">Strava UserID</param>
        List<Photo> GetByAthleteId(Int64 athleteId);

        /// <summary>
        /// List of Photo received by ActivityId
        /// </summary>
        /// <param name="activityId">Activity Id</param>
        List<Photo> GetByActivityId(Int64 activityId);

        /// <summary>
        /// Save Photo
        /// </summary>
        /// <param name="activityId">Activity Id</param>
        /// <param name="data">Comments from Activity</param>
        void SavePhotos(Int64 activityId, List<ApiModels.Photo> data);
    }
}
