﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Strava;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Strava.Api.Activities;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Strava
{
    public interface IActivityRepo : IRepoBase<Activity>
    {
        /// <summary>
        /// List of Activity received by athlet eId
        /// </summary>
        /// <param name="athleteId">Strava UserID</param>
        List<Activity> GetByAthleteId(Int64 athleteId);

        /// <summary>
        /// Activity received by Activity id
        /// </summary>
        /// <param name="id">Activity ID</param>
        Activity GetByActivityId(Int64 id);

        /// <summary>
        /// Save Activity
        /// </summary>
        /// <param name="athleteId">Athlete Id</param>
        /// <param name="data">data from Strava app</param>
        Activity SaveActivity(Int64 athleteId, ApiModels.Activity data);
    }
}
