﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Strava;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Strava.Api.Athletes;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Strava
{
    public interface IAthleteRepo : IRepoBase<Athlete>
    {
        /// <summary>
        /// Profile received by UserId
        /// </summary>
        /// <param name="userId">Strava UserID</param>
        Athlete GetByUserId(Int64 userId);

        /// <summary>
        /// Save Profile
        /// </summary>
        /// <param name="data">Data from Strava app</param>
        Athlete SaveAthlete(ApiModels.Athlete data);
    }
}
