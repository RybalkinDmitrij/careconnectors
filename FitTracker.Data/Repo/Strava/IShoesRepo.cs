﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Strava;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Strava.Api.Gear;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Strava
{
    public interface IShoesRepo : IRepoBaseWithoutPkId<Shoes>
    {
        /// <summary>
        /// List of Shoes received by AthleteId
        /// </summary>
        /// <param name="athleteId">Strava AthleteID</param>
        List<Shoes> GetByAthleteId(Int64 athleteId);

        /// <summary>
        /// Shoes received by GearId
        /// </summary>
        /// <param name="gearId">Gear PkID</param>
        Shoes GetByGearId(Int64 gearId);

        /// <summary>
        /// Save Shoes
        /// </summary>
        /// <param name="athleteId">Athlete Id</param>
        /// <param name="data">Shoes</param>
        void SaveShoes(Int64 athleteId, ApiModels.Bike data);

        /// <summary>
        /// Save entity
        /// </summary>
        /// <param name="entity">Bike entity</param>
        /// <param name="save">Save or Update</param>
        /// <param name="currentUserId">Current User Id</param>
        Shoes Save(Shoes entity, Boolean save, Int64 currentUserId = 0);
    }
}
