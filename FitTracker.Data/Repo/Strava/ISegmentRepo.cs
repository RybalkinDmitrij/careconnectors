﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Strava;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Strava.Api.Segments;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Strava
{
    public interface ISegmentRepo : IRepoBase<Segment>
    {
        /// <summary>
        /// Segment received by Segment Id
        /// </summary>
        /// <param name="id">Segment Id</param>
        Segment GetBySegmentId(Int32 id);

        /// <summary>
        /// Save Segment
        /// </summary>
        /// <param name="data">data from Strava app</param>
        Segment SaveSegment(ApiModels.Segment data);
    }
}
