﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Strava;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Strava.Api.Activities;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Strava
{
    public interface IMapRepo : IRepoBase<Map>
    {
        /// <summary>
        /// Map received by Map id
        /// </summary>
        /// <param name="id">Map ID</param>
        Map GetByMapId(String id);

        /// <summary>
        /// Save Map
        /// </summary>
        /// <param name="data">data from Strava app</param>
        Map SaveMap(ApiModels.Map data);
    }
}
