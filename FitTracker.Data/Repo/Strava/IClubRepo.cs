﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Strava;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Strava.Api.Clubs;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Strava
{
    public interface IClubRepo : IRepoBase<Club>
    {
        /// <summary>
        /// Club received by UserId
        /// </summary>
        /// <param name="athleteId">Ahlete ID</param>
        Club GetByAthleteId(Int64 athleteId);

        /// <summary>
        /// Save Club
        /// </summary>
        /// <param name="userId">Ahlete ID</param>
        /// <param name="data">Data from Strava app</param>
        Club SaveClub(Int64 userId, ApiModels.Club data);
    }
}
