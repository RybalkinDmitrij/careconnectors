﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Strava;

using ApiModels = Strava.Api.Segments;
using System.Collections.Generic;
using ST = FitTracker.Data.Enums.Strava;

namespace FitTracker.Data.Repo.Strava.Impl
{
    internal class SegmentRepo : RepoBase<Segment, FitTrackerContext>, ISegmentRepo
    {
        public SegmentRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods


        /// <summary>
        /// Segment received by Segment Id
        /// </summary>
        /// <param name="id">Segment Id</param>
        public Segment GetBySegmentId(Int32 id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Save Segment
        /// </summary>
        /// <param name="data">data from Strava app</param>
        public Segment SaveSegment(ApiModels.Segment data)
        {
            Map map = new Map();
            var repoMap = base.UnitOfWork.GetRepo<IMapRepo>();
            map = repoMap.SaveMap(data.Map);

            Segment entity = GetBySegmentId(data.Id);

            if (entity == null) entity = new Segment();

            entity.Id = data.Id;
            entity.ActivityType = data.ActivityType;
            entity.AthleteCount = data.AthleteCount;
            entity.AverageGrade = Convert.ToDecimal(data.AverageGrade);
            entity.City = data.City;
            entity.ClimbCategory = (ST.ClimbCategory)entity.ClimbCategory;
            entity.Country = data.Country;
            entity.CreatedAt = data.CreatedAt;
            entity.Distance = Convert.ToDecimal(data.Distance);
            entity.EffortCount = data.EffortCount;
            entity.Id = data.Id;
            entity.IsHazardous = data.IsHazardous;
            entity.IsPrivate = data.IsPrivate;
            entity.IsStarred = data.IsStarred;
            entity.MapId = map.PkID;
            entity.MaxElevation = Convert.ToDecimal(data.MaxElevation);
            entity.MaxGrade = Convert.ToDecimal(data.MaxGrade);
            entity.MinElevation = Convert.ToDecimal(data.MinElevation);
            entity.Name = (data.Name != null) ? data.Name : "";
            entity.PersonalRecordDistance = Convert.ToDecimal(data.PersonalRecordDistance);
            entity.PersonalRecordTime = data.PersonalRecordTime;
            entity.ResourceState = data.ResourceState;
            entity.StarCount = data.StarCount;
            entity.State = data.State;
            entity.TotalElevationGain = Convert.ToDecimal(data.TotalElevationGain);
            entity.UpdatedAt = data.UpdatedAt;

            entity = Save(entity);

            Commit();

            return entity;
        }
    }
}
