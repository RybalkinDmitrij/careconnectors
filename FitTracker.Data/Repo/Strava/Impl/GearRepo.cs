﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Strava;

using ApiModels = Strava.Api.Gear;
using System.Collections.Generic;
using ST = FitTracker.Data.Enums.Strava;

namespace FitTracker.Data.Repo.Strava.Impl
{
    internal class GearRepo : RepoBase<Gear, FitTrackerContext>, IGearRepo
    {
        public GearRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Gears received by athlete Id
        /// </summary>
        /// <param name="athleteId">athlete Id</param>
        public List<Gear> GetByAthleteId(Int64 athleteId)
        {
            return GetAll().Where(x => x.Bike.AthleteId == athleteId || x.Shoes.AthleteId == athleteId).ToList();
        }

        /// <summary>
        /// Gear received by Gear Id deom Strava
        /// </summary>
        /// <param name="Id">Gear ID</param>
        public Gear GetByGearId(String id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Save Gear for Bike or Shoes
        /// </summary>
        /// <param name="data">data from Strava app</param>
        public Gear SaveGear(ApiModels.GearSummary data)
        {
            Gear entity = GetByGearId(data.Id);

            if (entity == null) entity = new Gear();

            entity.Id = data.Id;
            entity.Distance = Convert.ToDecimal(data.Distance);
            entity.IsPrimary = data.IsPrimary;
            entity.Name = (data.Name != null) ? data.Name : "";
            entity.ResourceState = entity.ResourceState;

            entity = Save(entity);

            Commit();

            return entity;
        }
    }
}
