﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Strava;

using ApiModels = Strava.Api.Activities;
using System.Collections.Generic;
using ST = FitTracker.Data.Enums.Strava;

namespace FitTracker.Data.Repo.Strava.Impl
{
    internal class PhotoRepo : RepoBase<Photo, FitTrackerContext>, IPhotoRepo
    {
        public PhotoRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Photo received by athleteId
        /// </summary>
        /// <param name="athleteId">Strava UserID</param>
        public List<Photo> GetByAthleteId(Int64 athleteId)
        {
            return GetAll().Where(x => x.Activity.AthleteId == athleteId).ToList();
        }

        /// <summary>
        /// List of Photo received by ActivityId
        /// </summary>
        /// <param name="activityId">Activity Id</param>
        public List<Photo> GetByActivityId(Int64 activityId)
        {
            return GetAll().Where(x => x.ActivityId == activityId).ToList();
        }

        /// <summary>
        /// Save Photo
        /// </summary>
        /// <param name="activityId">Activity Id</param>
        /// <param name="data">Comments from Activity</param>
        public void SavePhotos(Int64 activityId, List<ApiModels.Photo> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[str_Photo] WHERE ActivityId = '{0}'", activityId));

            foreach (var item in data)
            {
                var entity = new Photo();

                entity.ActivityId = activityId;
                entity.Caption = item.Caption;
                entity.CreatedAt = item.CreatedAt;
                entity.ExternalUid = item.ExternalUid;
                entity.Id = item.Id;
                entity.ImageUrl = item.ImageUrl;
                entity.Latitude = Convert.ToDecimal(item.Location[0]);
                entity.Longitude = Convert.ToDecimal(item.Location[1]);
                entity.ResourceState = item.ResourceState;
                entity.Type = item.Type;
                entity.UploadedAt = item.UploadedAt;

                Save(entity);
            }

            Commit();
        }
    }
}
