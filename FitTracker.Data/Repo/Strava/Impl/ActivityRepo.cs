﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Strava;

using ApiModels = Strava.Api.Activities;
using ApiModelsGear = Strava.Api.Gear;
using System.Collections.Generic;
using ST = FitTracker.Data.Enums.Strava;

namespace FitTracker.Data.Repo.Strava.Impl
{
    internal class ActivityRepo : RepoBase<Activity, FitTrackerContext>, IActivityRepo
    {
        public ActivityRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Activity received by athlet eId
        /// </summary>
        /// <param name="athleteId">Strava UserID</param>
        public List<Activity> GetByAthleteId(Int64 athleteId)
        {
            return GetAll().Where(x => x.AthleteId == athleteId).ToList();
        }

        /// <summary>
        /// Activity received by Activity id
        /// </summary>
        /// <param name="id">Activity ID</param>
        public Activity GetByActivityId(Int64 id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Save Activity
        /// </summary>
        /// <param name="athleteId">Athlete Id</param>
        /// <param name="data">data from Strava app</param>
        public Activity SaveActivity(Int64 athleteId, ApiModels.Activity data)
        {
            Gear gear = new Gear();
            var repoGear = base.UnitOfWork.GetRepo<IGearRepo>();
            gear = repoGear.SaveGear(data.Gear);

            Map map = new Map();
            var repoMap = base.UnitOfWork.GetRepo<IMapRepo>();
            map = repoMap.SaveMap(data.Map);

            Activity entity = GetByActivityId(data.Id);

            if (entity == null) entity = new Activity();

            entity.Id = data.Id;
            entity.AchievementCount = data.AchievementCount;
            entity.AthleteCount = data.AthleteCount;
            entity.AthleteId = athleteId;
            entity.AverageCadence = entity.AverageCadence;
            entity.AverageHeartrate = Convert.ToDecimal(data.AverageHeartrate);
            entity.AveragePower = Convert.ToDecimal(data.AveragePower);
            entity.AverageSpeed = Convert.ToDecimal(data.AverageSpeed);
            entity.AverageTemperature = Convert.ToDecimal(data.AverageTemperature);
            entity.Calories = Convert.ToDecimal(data.Calories);
            entity.City = (data.City != null) ? data.City : "";
            entity.CommentCount = Convert.ToInt32(data.CommentCount);
            entity.Country = (data.Country != null) ? data.Country : "";
            entity.DateTimeStart = data.DateTimeStart;
            entity.DateTimeStartLocal = data.DateTimeStartLocal;
            entity.Description = (data.Description != null) ? data.Description : "";
            entity.Distance = Convert.ToDecimal(data.Distance);
            entity.ElapsedTime = data.ElapsedTime;
            entity.ElevationGain = Convert.ToDecimal(data.ElevationGain);
            entity.EndLatitude = Convert.ToDecimal(data.EndLatitude);
            entity.EndLongitude = Convert.ToDecimal(data.EndLongitude);
            entity.ExternalId = (data.ExternalId != null) ? data.ExternalId : "";
            entity.GearId = gear.PkID;
            entity.HasKudoed = data.HasKudoed;
            entity.HasPowerMeter = data.HasPowerMeter;
            entity.IsCommute = data.IsCommute;
            entity.IsFlagged = data.IsFlagged;
            entity.IsManual = data.IsManual;
            entity.IsPrivate = data.IsPrivate;
            entity.IsTrainer = data.IsTrainer;
            entity.Kilojoules = Convert.ToDecimal(data.Kilojoules);
            entity.KudosCount = data.KudosCount;
            entity.MapId = map.PkID;
            entity.MaxHeartrate = Convert.ToDecimal(data.MaxHeartrate);
            entity.MaxSpeed = Convert.ToDecimal(data.MaxSpeed);
            entity.MovingTime = data.MovingTime;
            entity.Name = data.Name;
            entity.PhotoCount = data.PhotoCount;
            entity.StartLatitude = Convert.ToDecimal(data.StartLatitude);
            entity.StartLongitude = Convert.ToDecimal(data.StartLongitude);
            entity.State = (data.State != null) ? data.State : "";
            entity.TimeZone = data.TimeZone;
            entity.Truncated = (data.Truncated != null) ? data.Truncated : 0;
            entity.Type = (ST.ActivityType)data.Type;
            entity.WeightedAverageWatts = data.WeightedAverageWatts;

            entity = Save(entity);

            Commit();

            return entity;
        }
    }
}
