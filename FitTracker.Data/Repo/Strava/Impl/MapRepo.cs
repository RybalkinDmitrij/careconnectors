﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Strava;

using ApiModels = Strava.Api.Activities;
using System.Collections.Generic;
using ST = FitTracker.Data.Enums.Strava;

namespace FitTracker.Data.Repo.Strava.Impl
{
    internal class MapRepo : RepoBase<Map, FitTrackerContext>, IMapRepo
    {
        public MapRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// Map received by Map id
        /// </summary>
        /// <param name="id">Map ID</param>
        public Map GetByMapId(String id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Save Map
        /// </summary>
        /// <param name="data">data from Strava app</param>
        public Map SaveMap(ApiModels.Map data)
        {
            Map entity = GetByMapId(data.Id);

            if (entity == null) entity = new Map();

            entity.Id = data.Id;
            entity.Polyline = (data.Polyline != null) ? data.Polyline : "";
            entity.ResourceState = data.ResourceState;
            entity.SummaryPolyline = (data.SummaryPolyline != null) ? data.SummaryPolyline : "";

            entity = Save(entity);

            Commit();

            return entity;
        }
    }
}
