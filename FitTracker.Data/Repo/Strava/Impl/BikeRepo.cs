﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Strava;

using ApiModels = Strava.Api.Gear;
using System.Collections.Generic;
using ST = FitTracker.Data.Enums.Strava;
using System.Data.Entity;

namespace FitTracker.Data.Repo.Strava.Impl
{
    internal class BikeRepo : RepoBaseWithoutPkId<Bike, FitTrackerContext>, IBikeRepo
    {
        public BikeRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Bikes received by AthleteId
        /// </summary>
        /// <param name="userId">Strava AthleteId</param>
        public List<Bike> GetByAthleteId(Int64 athleteId)
        {
            return GetAll().Where(x => x.AthleteId == athleteId).ToList();
        }

        /// <summary>
        /// Bike received by GearId
        /// </summary>
        /// <param name="gearId">Gear PkID</param>
        public Bike GetByGearId(Int64 gearId)
        {
            return GetAll().FirstOrDefault(x => x.GearId == gearId);
        }

        /// <summary>
        /// Save Bike
        /// </summary>
        /// <param name="athleteId">Athlete Id</param>
        /// <param name="data">Bike</param>
        public void SaveBike(Int64 athleteId, ApiModels.Bike data)
        {
            Gear gear = new Gear();
            var repoGear = base.UnitOfWork.GetRepo<IGearRepo>();
            gear = repoGear.SaveGear((ApiModels.GearSummary)data);

            //ExecuteSql(String.Format(@"DELETE FROM [dbo].[str_Bike] WHERE GearId = '{0}'", gear.PkID));

            Bike entity = GetByGearId(gear.PkID);
            Boolean save = false;

            if (entity == null)
            {
                entity = new Bike();
                save = true;
            }

            entity.AthleteId = athleteId;
            entity.BikeType = (ST.BikeType)data.FrameType;
            entity.Brand = (data.Brand != null) ? data.Brand : "";
            entity.Description = (data.Description != null) ? data.Description : "";
            entity.Model = (data.Model != null) ? data.Model : "";
            entity.GearId = gear.PkID;

            entity = Save(entity, save);

            Commit();
        }

        /// <summary>
        /// Save entity
        /// </summary>
        /// <param name="entity">Bike entity</param>
        /// <param name="currentUserId">Current User Id</param>
        public Bike Save(Bike entity, Boolean save, Int64 currentUserId = 0)
        {
            if (save)
            {
                entity.IsDeleted = false;

                entity.DateCreate = DateTime.Now;
                entity.DateUpdate = DateTime.Now;
                entity.DateDelete = null;

                entity.UserCreateId = currentUserId;
                entity.UserUpdateId = currentUserId;
                entity.UserDeleteId = 0;

                return DbSet.Add(entity);
            }
            else
            {
                entity.IsDeleted = false;
                entity.DateUpdate = DateTime.Now;
                entity.UserUpdateId = currentUserId;

                entity = DbSet.Attach(entity);
                Context.Entry(entity).State = EntityState.Modified;
                return entity;
            }
        }
    }
}
