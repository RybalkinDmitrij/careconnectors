﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Strava;

using ApiModels = Strava.Api.Activities;
using System.Collections.Generic;
using ST = FitTracker.Data.Enums.Strava;

namespace FitTracker.Data.Repo.Strava.Impl
{
    internal class ActivityLapRepo : RepoBase<ActivityLap, FitTrackerContext>, IActivityLapRepo
    {
        public ActivityLapRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of ActivityLap received by athleteId
        /// </summary>
        /// <param name="athleteId">Strava UserID</param>
        public List<ActivityLap> GetByAthleteId(Int64 athleteId)
        {
            return GetAll().Where(x => x.Activity.AthleteId == athleteId).ToList();
        }

        /// <summary>
        /// List of ActivityLap received by ActivityId
        /// </summary>
        /// <param name="activityId">Activity Id</param>
        public List<ActivityLap> GetByActivityId(Int64 activityId)
        {
            return GetAll().Where(x => x.ActivityId == activityId).ToList();
        }

        /// <summary>
        /// Save ActivityLap
        /// </summary>
        /// <param name="activityId">Activity Id</param>
        /// <param name="data">Data from Strava</param>
        public void SaveActivityLaps(Int64 activityId, List<ApiModels.ActivityLap> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[str_ActivityLap] WHERE ActivityId = '{0}'", activityId));

            foreach (var item in data)
            {
                var entity = new ActivityLap();

                entity.ActivityId = activityId;
                entity.AverageCadence = Convert.ToDecimal(item.AverageCadence);
                entity.AverageHeartrate = Convert.ToDecimal(item.AverageHeartrate);
                entity.AveragePower = Convert.ToDecimal(item.AveragePower);
                entity.AverageSpeed = Convert.ToDecimal(item.AverageSpeed);
                entity.Distance = Convert.ToDecimal(item.Distance);
                entity.ElapsedTime = item.ElapsedTime;
                entity.EndIndex = item.EndIndex;
                entity.Id = item.Id;
                entity.LapIndex = item.LapIndex;
                entity.MaxHeartrate = Convert.ToDecimal(item.MaxHeartrate);
                entity.MaxSpeed = Convert.ToDecimal(item.MaxSpeed);
                entity.MovingTime = item.MovingTime;
                entity.Name = item.Name;
                entity.ResourceState = item.ResourceState;
                entity.Start = item.Start;
                entity.StartIndex = item.StartIndex;
                entity.StartLocal = (item.StartLocal == new DateTime(1, 1, 1)) ? new DateTime(1900, 1, 1) : item.StartLocal;
                entity.TotalElevationGain = Convert.ToDecimal(item.TotalElevationGain);

                Save(entity);
            }

            Commit();
        }
    }
}
