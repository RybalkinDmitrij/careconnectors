﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Strava;

using ApiModels = Strava.Api.Athletes;
using System.Collections.Generic;
using ST = FitTracker.Data.Enums.Strava;

namespace FitTracker.Data.Repo.Strava.Impl
{
    internal class AthleteRepo : RepoBase<Athlete, FitTrackerContext>, IAthleteRepo
    {
        public AthleteRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// Ahlete received by UserId
        /// </summary>
        /// <param name="userId">Ahlete UserID</param>
        public Athlete GetByUserId(Int64 userId)
        {
            return GetAll().FirstOrDefault(x => x.Id == userId);
        }

        /// <summary>
        /// Save Ahlete
        /// </summary>
        /// <param name="data">Data from Strava app</param>
        public Athlete SaveAthlete(ApiModels.Athlete data)
        {
            Athlete entity = GetByUserId(data.Id);

            if (entity == null) entity = new Athlete();

            entity.Id = data.Id;
            entity.ApproveFollowers = data.ApproveFollowers;
            entity.City = (data.City != null) ? data.City : "";
            entity.Country = (data.Country != null) ? data.Country : "";
            entity.CreatedAt = data.CreatedAt;
            entity.DatePreference = data.DatePreference;
            entity.Email = (data.Email != null) ? data.Email : "";
            entity.FirstName = (data.FirstName != null) ? data.FirstName : "";
            entity.Follower = (data.Follower != null) ? data.Follower : "None";
            entity.FollowerCount = data.FollowerCount;
            entity.Friend = (data.Friend != null) ? data.Friend : "None";
            entity.FriendCount = data.FriendCount;
            entity.Ftp = (data.Ftp != null) ? data.Ftp : 0;
            entity.IsPremium = data.IsPremium;
            entity.LastName = (data.LastName != null) ? data.LastName : "";
            entity.MeasurementPreference = data.MeasurementPreference;
            entity.MutualFriendCount = data.MutualFriendCount;
            entity.Profile = (data.Profile != null) ? data.Profile : ""; 
            entity.ProfileMedium = (data.ProfileMedium != null) ? data.ProfileMedium : "";
            entity.Sex = (data.Sex != null) ? data.Sex : "";
            entity.ResourceState = data.ResourceState;
            entity.State = (data.State != null) ? data.State : "";
            entity.UpdatedAt = data.UpdatedAt;

            entity = Save(entity);

            Commit();

            return entity;
        }
    }
}
