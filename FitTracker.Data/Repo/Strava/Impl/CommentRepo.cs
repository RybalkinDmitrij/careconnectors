﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Strava;

using ApiModels = Strava.Api.Activities;
using System.Collections.Generic;
using ST = FitTracker.Data.Enums.Strava;

namespace FitTracker.Data.Repo.Strava.Impl
{
    internal class CommentRepo : RepoBase<Comment, FitTrackerContext>, ICommentRepo
    {
        public CommentRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Comment received by athleteId
        /// </summary>
        /// <param name="athleteId">Runkeeper UserID</param>
        public List<Comment> GetByAthleteId(Int64 athleteId)
        {
            return GetAll().Where(x => x.Activity.AthleteId == athleteId).ToList();
        }

        /// <summary>
        /// List of Comment received by ActivityId
        /// </summary>
        /// <param name="activityId">Activity Id</param>
        public List<Comment> GetByActivityId(Int64 activityId)
        {
            return GetAll().Where(x => x.ActivityId == activityId).ToList();
        }

        /// <summary>
        /// Save Comment
        /// </summary>
        /// <param name="activityId">Activity Id</param>
        /// <param name="data">Comments from Activity</param>
        public void SaveComments(Int64 activityId, List<ApiModels.Comment> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[str_Comment] WHERE ActivityId = '{0}'", activityId));

            foreach (var item in data)
            {
                var entity = new Comment();

                entity.ActivityId = activityId;
                entity.Author = item.Athlete.FirstName + " " + item.Athlete.LastName;
                entity.Id = item.Id;
                entity.ResourceState = item.ResourceState;
                entity.Text = item.Text;
                entity.TimeCreated = item.TimeCreated;

                Save(entity);
            }

            Commit();
        }
    }
}
