﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Strava;

using ApiModels = Strava.Api.Clubs;
using System.Collections.Generic;
using ST = FitTracker.Data.Enums.Strava;

namespace FitTracker.Data.Repo.Strava.Impl
{
    internal class ClubRepo : RepoBase<Club, FitTrackerContext>, IClubRepo
    {
        public ClubRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// Club received by athlete Id
        /// </summary>
        /// <param name="athleteId">athlete Id</param>
        public Club GetByAthleteId(Int64 athleteId)
        {
            return GetAll().FirstOrDefault(x => x.AthleteId == athleteId);
        }

        /// <summary>
        /// Save Club
        /// </summary>
        /// <param name="athleteId">athlete Id</param>
        /// <param name="data">Data from Strava app</param>
        public Club SaveClub(Int64 athleteId, ApiModels.Club data)
        {
            Club entity = GetByAthleteId(athleteId);

            if (entity == null) entity = new Club();

            entity.Id = data.Id;
            entity.AthleteId = athleteId;
            entity.City = (data.City != null) ? data.City : "";
            entity.ClubType = (ST.ClubType)data.ClubType;
            entity.Country = (data.Country != null) ? data.Country : "";
            entity.Description = (data.Description != null) ? data.Description : "";
            entity.IsPrivate = data.IsPrivate;
            entity.MemberCount = (data.MemberCount != null) ? data.MemberCount : 0; 
            entity.Name = (data.Name != null) ? data.Name : "";
            entity.ProfileMedium = (data.ProfileMedium != null) ? data.ProfileMedium : "";
            entity.ResourceState = data.ResourceState;
            entity.SportType = (ST.SportType)data.SportType;
            entity.State = (data.State != null) ? data.State : "";

            entity = Save(entity);

            Commit();

            return entity;
        }
    }
}
