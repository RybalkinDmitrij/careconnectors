﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Strava;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Strava.Api.Gear;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Strava
{
    public interface IGearRepo : IRepoBase<Gear>
    {
        /// <summary>
        /// List of Gears received by athlet eId
        /// </summary>
        /// <param name="athleteId">Strava UserID</param>
        List<Gear> GetByAthleteId(Int64 athleteId);

        /// <summary>
        /// Gear received by Gear PKid
        /// </summary>
        /// <param name="id">Gear ID</param>
        Gear GetByGearId(String id);

        /// <summary>
        /// Save Gear for Bike or Shoes
        /// </summary>
        /// <param name="data">data from Strava app</param>
        Gear SaveGear(ApiModels.GearSummary data);
    }
}
