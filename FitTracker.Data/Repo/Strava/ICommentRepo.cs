﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Strava;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Strava.Api.Activities;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Strava
{
    public interface ICommentRepo : IRepoBase<Comment>
    {
        /// <summary>
        /// List of Comment received by athleteId
        /// </summary>
        /// <param name="athleteId">Runkeeper UserID</param>
        List<Comment> GetByAthleteId(Int64 athleteId);

        /// <summary>
        /// List of Comment received by ActivityId
        /// </summary>
        /// <param name="activityId">Activity Id</param>
        List<Comment> GetByActivityId(Int64 activityId);

        /// <summary>
        /// Save Comment
        /// </summary>
        /// <param name="activityId">Activity Id</param>
        /// <param name="data">Comments from Activity</param>
        void SaveComments(Int64 activityId, List<ApiModels.Comment> data);
    }
}
