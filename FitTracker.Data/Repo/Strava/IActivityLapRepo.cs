﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Strava;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Strava.Api.Activities;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Strava
{
    public interface IActivityLapRepo : IRepoBase<ActivityLap>
    {
        /// <summary>
        /// List of ActivityLap received by athleteId
        /// </summary>
        /// <param name="athleteId">Strava UserID</param>
        List<ActivityLap> GetByAthleteId(Int64 athleteId);

        /// <summary>
        /// List of ActivityLap received by ActivityId
        /// </summary>
        /// <param name="activityId">Activity Id</param>
        List<ActivityLap> GetByActivityId(Int64 activityId);

        /// <summary>
        /// Save ActivityLap
        /// </summary>
        /// <param name="activityId">Activity Id</param>
        /// <param name="data">Data from Strava</param>
        void SaveActivityLaps(Int64 activityId, List<ApiModels.ActivityLap> data);
    }
}
