﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Strava;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Strava.Api.Gear;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Strava
{
    public interface IBikeRepo : IRepoBaseWithoutPkId<Bike>
    {
        /// <summary>
        /// List of Bikes received by athlete Id
        /// </summary>
        /// <param name="athleteId">athlete Id</param>
        List<Bike> GetByAthleteId(Int64 athleteId);

        /// <summary>
        /// Bike received by UserId and GearId
        /// </summary>
        /// <param name="gearId">Gear PKID</param>
        Bike GetByGearId(Int64 gearId);

        /// <summary>
        /// Save Bike
        /// </summary>
        /// <param name="athleteId">Athlete Id</param>
        /// <param name="data">Bike</param>
        void SaveBike(Int64 athleteId, ApiModels.Bike data);

        /// <summary>
        /// Save entity
        /// </summary>
        /// <param name="entity">Bike entity</param>
        /// <param name="save">Save or Update</param>
        /// <param name="currentUserId">Current User Id</param>
        Bike Save(Bike entity, Boolean save, Int64 currentUserId = 0);
    }
}
