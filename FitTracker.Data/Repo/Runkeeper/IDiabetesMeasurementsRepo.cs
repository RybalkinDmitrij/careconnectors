﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface IDiabetesMeasurementsRepo : IRepoBase<DiabetesMeasurements>
    {
        /// <summary>
        /// List of Diabetes Measurements received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<DiabetesMeasurements> GetByUserId(String userId);

        /// <summary>
        /// Diabetes Measurement received by UserId and the time when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Diabetes Measurement happened</param>
        DiabetesMeasurements GetByUserId(String userId, DateTime date);

        /// <summary>
        /// List of Diabetes Measurements received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Diabetes Measurements</param>
        /// <param name="endDate">End interval for Diabetes Measurements</param>
        List<DiabetesMeasurements> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Save Diabetes Measurement
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Diabete sMeasurement happened</param>
        /// <param name="data">Data from Runkeeper app</param>
        DiabetesMeasurements SaveDiabetesMeasurement(String userId, DateTime date, ApiModels.DiabetesMeasurementsPastModel data);
    }
}
