﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface IBackgroundActivitiesRepo : IRepoBase<BackgroundActivities>
    {
        /// <summary>
        /// List of Background Activities received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<BackgroundActivities> GetByUserId(String userId);

        /// <summary>
        /// Background Activitie received by UserId and the time when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Background Activitie happened</param>
        BackgroundActivities GetByUserId(String userId, DateTime date);

        /// <summary>
        /// List of Background Activities received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Background Activities</param>
        /// <param name="endDate">End interval for Background Activities</param>
        List<BackgroundActivities> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Save Background Activitie 
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Background Activitie happened</param>
        /// <param name="data">Data from Runkeeper app</param>
        BackgroundActivities SaveBackgroundActivitie(String userId, DateTime date, ApiModels.BackgroundActivitiesPastModel data);
    }
}
