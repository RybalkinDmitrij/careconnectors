﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface IExercisesRepo : IRepoBase<Exercises>
    {
        /// <summary>
        /// List of Exercises received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<Exercises> GetByUserId(String userId);

        /// <summary>
        /// List of Exercises received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Exercises</param>
        /// <param name="endDate">End interval for Exercises</param>
        List<Exercises> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// List of Exercises received by strengthTrainingId
        /// </summary>
        /// <param name="strengthTrainingId">Strength Training Id</param>
        List<Exercises> GetByStrengthTrainingId(Int64 strengthTrainingId);

        /// <summary>
        /// Save Exercise
        /// </summary>
        /// <param name="strengthTrainingId">Strength Training Id</param>
        /// <param name="data">Exercises from Strength Training</param>
        void SaveExercise(Int64 strengthTrainingId, List<ApiModels.ExercisesModel> data);
    }
}
