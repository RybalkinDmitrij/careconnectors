﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface IDistancesRepo : IRepoBase<Distances>
    {
        /// <summary>
        /// List of Distances received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<Distances> GetByUserId(String userId);

        /// <summary>
        /// List of Distances received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Distances</param>
        /// <param name="endDate">End interval for Distances</param>
        List<Distances> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// List of Distances received by fitnessActivityId
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        List<Distances> GetByFitnessActivitiesId(Int64 fitnessActivityId);

        /// <summary>
        /// Save Distances
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        /// <param name="data">Distances from Fitness Activitie</param>
        void SaveDistance(Int64 fitnessActivityId, List<ApiModels.DistanceModel> data);
    }
}
