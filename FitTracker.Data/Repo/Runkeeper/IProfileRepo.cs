﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface IProfileRepo : IRepoBase<Profile>
    {
        /// <summary>
        /// Profile received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        Profile GetByUserId(String userId);

        /// <summary>
        /// Save Profile
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="data">Data from Runkeeper app</param>
        Profile SaveProfile(String userId, ApiModels.ProfileModel data);
    }
}
