﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;
using FitTracker.Data.Enums.Runkeeper;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface INutritionRepo : IRepoBase<Nutrition>
    {
        /// <summary>
        /// List of Nutrition received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<Nutrition> GetByUserId(String userId);

        /// <summary>
        /// Nutrition received by UserId and the time when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Nutrition happened</param>
        /// <param name="type">Type of Nutrition</param>
        Nutrition GetByUserId(String userId, DateTime date, NutritionType type);

        /// <summary>
        /// List of Nutrition received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Nutrition</param>
        /// <param name="endDate">End interval for Nutrition</param>
        List<Nutrition> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Save Nutrition
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Nutrition happened</param>
        /// <param name="data">Data from Runkeeper app</param>
        Nutrition SaveNutrition(String userId, DateTime date, ApiModels.NutritionPastModel data);
    }
}
