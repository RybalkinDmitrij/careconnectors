﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface IStrengthTrainingRepo : IRepoBase<StrengthTraining>
    {
        /// <summary>
        /// List of Strength Training received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<StrengthTraining> GetByUserId(String userId);

        /// <summary>
        /// Strength Training received by UserId and the time when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Strength Training happened</param>
        StrengthTraining GetByUserId(String userId, DateTime date);

        /// <summary>
        /// List of Strength Training received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Strength Training</param>
        /// <param name="endDate">End interval for Strength Training</param>
        List<StrengthTraining> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Save Strength Training
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Strength Training happened</param>
        /// <param name="data">Data from Runkeeper app</param>
        StrengthTraining SaveStrengthTraining(String userId, DateTime date, ApiModels.StrengthTrainingActivitiesPastModel data);
    }
}
