﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;
using FitTracker.Data.Enums.Runkeeper;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface IWeightRepo : IRepoBase<Weight>
    {
        /// <summary>
        /// List of Weight received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<Weight> GetByUserId(String userId);

        /// <summary>
        /// Weight received by UserId and the time when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Weight happened</param>
        /// <param name="type">Type of Weight</param>
        Weight GetByUserId(String userId, DateTime date, WeightType type);

        /// <summary>
        /// List of Weight received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Weight</param>
        /// <param name="endDate">End interval for Weight</param>
        List<Weight> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Save Weight
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Weight happened</param>
        /// <param name="data">Data from Runkeeper app</param>
        Weight SaveWeight(String userId, DateTime date, ApiModels.WeightPastModel data);
    }
}
