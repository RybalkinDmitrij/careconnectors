﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface IGeneralMeasurementsRepo : IRepoBase<GeneralMeasurements>
    {
        /// <summary>
        /// List of General Measurements received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<GeneralMeasurements> GetByUserId(String userId);

        /// <summary>
        /// General Measurement received by UserId and the time when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when General Measurement happened</param>
        GeneralMeasurements GetByUserId(String userId, DateTime date);

        /// <summary>
        /// List of General Measurements received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for General Measurements</param>
        /// <param name="endDate">End interval for General Measurements</param>
        List<GeneralMeasurements> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Save General Measurement
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when General Measurement happened</param>
        /// <param name="data">Data from Runkeeper app</param>
        GeneralMeasurements SaveGeneralMeasurement(String userId, DateTime date, ApiModels.GeneralMeasurementsPastModel data);
    }
}
