﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;
using FitTracker.Data.Enums.Runkeeper;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface ISleepRepo : IRepoBase<Sleep>
    {
        /// <summary>
        /// List of Sleep Activities received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<Sleep> GetByUserId(String userId);

        /// <summary>
        /// Sleep received by UserId and the time when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Sleep happened</param>
        /// <param name="type">Type of Sleep</param>
        Sleep GetByUserId(String userId, DateTime date, SleepType type);

        /// <summary>
        /// List of Sleep received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Sleep</param>
        /// <param name="endDate">End interval for Sleep</param>
        List<Sleep> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Save Sleep
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Sleep happened</param>
        /// <param name="data">Data from Runkeeper app</param>
        Sleep SaveSleep(String userId, DateTime date, ApiModels.SleepPastModel data);
    }
}
