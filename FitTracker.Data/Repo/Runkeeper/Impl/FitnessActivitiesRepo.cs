﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Runkeeper;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper.Impl
{
    internal class FitnessActivitiesRepo : RepoBase<FitnessActivities, FitTrackerContext>, IFitnessActivitiesRepo
    {
        public FitnessActivitiesRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Fitness Activities received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        public List<FitnessActivities> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        /// <summary>
        /// Fitness Activitie received by UserId and the time when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Fitness Activitie happened</param>
        public FitnessActivities GetByUserId(String userId, DateTime date)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId && x.StartTime == date);
        }

        /// <summary>
        /// List of Fitness Activities received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Fitness Activities</param>
        /// <param name="endDate">End interval for Fitness Activities</param>
        public List<FitnessActivities> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.StartTime >= startDate && x.StartTime <= endDate).ToList();
        }

        /// <summary>
        /// Save Fitness Activitie
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Fitness Activitie happened</param>
        /// <param name="data">Data from Runkeeper app</param>
        public FitnessActivities SaveActivitySummary(String userId, DateTime date, ApiModels.FitnessActivitiesPastModel data)
        {
            FitnessActivities entity = GetByUserId(userId, date);

            if (entity == null) entity = new FitnessActivities();

            entity.UserId = userId;
            entity.AverageHeartRate = (data.AverageHeartRate != null) ? data.AverageHeartRate : 0;
            entity.Climb = Convert.ToDecimal(data.Climb);
            entity.Duration = Convert.ToDecimal(data.Duration);
            entity.Equipment = data.Equipment;
            entity.IsLive = data.IsLive;
            entity.Notes = (data.Notes != null) ? data.Notes : "";
            entity.SecondaryType = (data.SecondaryType != null) ? data.SecondaryType : "";
            entity.StartTime = data.StartTime;
            entity.TotalCalories = Convert.ToDecimal(data.TotalCalories);
            entity.TotalDistance = Convert.ToDecimal(data.TotalDistance);
            entity.Type = data.Type;
            entity.Source = (data.Source != null) ? data.Source : "";

            entity = Save(entity);

            Commit();

            var repoDistance = base.UnitOfWork.GetRepo<IDistancesRepo>();
            repoDistance.SaveDistance(entity.PkID, data.Distance);

            Commit();

            var repoCalories = base.UnitOfWork.GetRepo<ICaloriesRepo>();
            repoCalories.SaveCalories(entity.PkID, data.Calories);

            Commit();

            var repoHeartRate = base.UnitOfWork.GetRepo<IHeartRatesRepo>();
            repoHeartRate.SaveHeartRates(entity.PkID, data.HeartRate);

            Commit();

            var repoImage = base.UnitOfWork.GetRepo<IImageRepo>();
            repoImage.SaveImage(entity.PkID, data.Images);

            Commit();

            var repoPath = base.UnitOfWork.GetRepo<IPathRepo>();
            repoPath.SavePath(entity.PkID, data.Path);

            Commit();

            return entity;
        }
    }
}
