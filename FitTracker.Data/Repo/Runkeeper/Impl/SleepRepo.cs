﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Runkeeper;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;
using RE = FitTracker.Data.Enums.Runkeeper;

namespace FitTracker.Data.Repo.Runkeeper.Impl
{
    internal class SleepRepo : RepoBase<Sleep, FitTrackerContext>, ISleepRepo
    {
        public SleepRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Sleep Activities received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        public List<Sleep> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        /// <summary>
        /// Sleep received by UserId and the time when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Sleep happened</param>
        public Sleep GetByUserId(String userId, DateTime date, RE.SleepType type)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId && x.Timestamp == date && x.Type == type);
        }

        /// <summary>
        /// List of Sleep received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Sleep</param>
        /// <param name="endDate">End interval for Sleep</param>
        public List<Sleep> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Timestamp >= startDate && x.Timestamp <= endDate).ToList();
        }

        /// <summary>
        /// Save Sleep
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Sleep happened</param>
        /// <param name="data">Data from Runkeeper app</param>
        public Sleep SaveSleep(String userId, DateTime date, ApiModels.SleepPastModel data)
        {
            Sleep entity = GetByUserId(userId, date, (RE.SleepType)data.MeasurementType);

            if (entity == null) entity = new Sleep();

            entity.UserId = userId;
            entity.Timestamp = data.Timestamp;
            entity.Type = (RE.SleepType)data.MeasurementType;
            entity.Measurement = (data.Measurement != null) ? Convert.ToDecimal(data.Measurement) : 0;
            entity.Source = (data.Source != null) ? data.Source : "";

            Save(entity);

            Commit();

            return entity;
        }
    }
}
