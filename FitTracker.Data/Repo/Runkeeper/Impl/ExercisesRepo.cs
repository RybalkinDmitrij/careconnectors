﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Runkeeper;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper.Impl
{
    internal class ExercisesRepo : RepoBase<Exercises, FitTrackerContext>, IExercisesRepo
    {
        public ExercisesRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Exercises received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        public List<Exercises> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.StrengthTraining.UserId == userId).ToList();
        }

        /// <summary>
        /// List of Exercises received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Exercises</param>
        /// <param name="endDate">End interval for Exercises</param>
        public List<Exercises> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.StrengthTraining.UserId == userId && x.StrengthTraining.StartTime >= startDate && x.StrengthTraining.StartTime <= endDate).ToList();
        }

        /// <summary>
        /// List of Exercises received by strengthTrainingId
        /// </summary>
        /// <param name="strengthTrainingId">Strength Training Id</param>
        public List<Exercises> GetByStrengthTrainingId(Int64 strengthTrainingId)
        {
            return GetAll().Where(x => x.StrengthTrainingId == strengthTrainingId).ToList();
        }

        /// <summary>
        /// Save Exercise
        /// </summary>
        /// <param name="strengthTrainingId">Strength Training Id</param>
        /// <param name="data">Exercises from Strength Training</param>
        public void SaveExercise(Int64 strengthTrainingId, List<ApiModels.ExercisesModel> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[rk_Exercises] WHERE StrengthTrainingId = '{0}'", strengthTrainingId));

            foreach (var item in data)
            {
                var entity = new Exercises();

                entity.StrengthTrainingId = strengthTrainingId;
                entity.PrimaryType = (item.PrimaryType != null) ? item.PrimaryType : "";
                entity.SecondaryType = (item.SecondaryType != null) ? item.SecondaryType : "";
                entity.PrimaryMuscleGroup = (item.SecondaryMuscleGroup != null) ? item.SecondaryMuscleGroup : "";
                entity.SecondaryMuscleGroup = (item.SecondaryMuscleGroup != null) ? item.SecondaryMuscleGroup : "";
                entity.Notes = (item.Notes != null) ? item.Notes : "";
                entity.Routine = (item.Routine != null) ? item.Routine : "";

                Save(entity);

                Commit();

                var repoSets = base.UnitOfWork.GetRepo<ISetsRepo>();
                repoSets.SaveSet(entity.PkID, item.Sets);

                Commit();
            }

            
        }
    }
}
