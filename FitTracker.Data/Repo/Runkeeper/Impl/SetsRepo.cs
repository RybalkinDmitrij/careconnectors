﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Runkeeper;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper.Impl
{
    internal class SetsRepo : RepoBase<Sets, FitTrackerContext>, ISetsRepo
    {
        public SetsRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Sets received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        public List<Sets> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.Exercise.StrengthTraining.UserId == userId).ToList();
        }

        /// <summary>
        /// List of Sets received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Sets</param>
        /// <param name="endDate">End interval for Sets</param>
        public List<Sets> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.Exercise.StrengthTraining.UserId == userId && x.Exercise.StrengthTraining.StartTime >= startDate && x.Exercise.StrengthTraining.StartTime <= endDate).ToList();
        }

        /// <summary>
        /// List of Sets received by exerciseId
        /// </summary>
        /// <param name="exerciseId">Exercise Id</param>
        public List<Sets> GetByExerciseId(Int64 exerciseId)
        {
            return GetAll().Where(x => x.ExerciseId == exerciseId).ToList();
        }

        /// <summary>
        /// List of Sets received by strengthTrainingId
        /// </summary>
        /// <param name="strengthTrainingId">Strength Training Id</param>
        public List<Sets> GetByStrengthTrainingId(Int64 strengthTrainingId)
        {
            return GetAll().Where(x => x.Exercise.StrengthTrainingId == strengthTrainingId).ToList();
        }

        /// <summary>
        /// Save Sets
        /// </summary>
        /// <param name="exerciseId">Exercises Id</param>
        /// <param name="data">Sets from Exercises<</param>
        public void SaveSet(Int64 exerciseId, List<ApiModels.SetsModel> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[rk_Sets] WHERE ExerciseId = '{0}'", exerciseId));

            foreach (var item in data)
            {
                var entity = new Sets();

                entity.ExerciseId = exerciseId;
                entity.Weight = (item.Weight != null) ? Convert.ToDecimal(item.Weight) : 0;
                entity.Repetitions = (item.Repetitions != null) ? Convert.ToInt32(item.Repetitions) : 0;
                entity.Notes = (item.Notes != null) ? item.Notes : "";

                Save(entity);
            }

            Commit();
        }
    }
}
