﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Runkeeper;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper.Impl
{
    internal class DistancesRepo : RepoBase<Distances, FitTrackerContext>, IDistancesRepo
    {
        public DistancesRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Distances received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        public List<Distances> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.FitnessActivity.UserId == userId).ToList();
        }

        /// <summary>
        /// List of Distances received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Distances</param>
        /// <param name="endDate">End interval for Distances</param>
        public List<Distances> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.FitnessActivity.UserId == userId && x.FitnessActivity.StartTime >= startDate && x.FitnessActivity.StartTime <= endDate).ToList();
        }

        /// <summary>
        /// List of Distances received by fitnessActivityId
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        public List<Distances> GetByFitnessActivitiesId(Int64 fitnessActivityId)
        {
            return GetAll().Where(x => x.FitnessActivityId == fitnessActivityId).ToList();
        }

        /// <summary>
        /// Save Distances
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        /// <param name="data">Distances from Fitness Activitie</param>
        public void SaveDistance(Int64 fitnessActivityId, List<ApiModels.DistanceModel> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[rk_Distance] WHERE FitnessActivityId = '{0}'", fitnessActivityId));

            foreach (var item in data)
            {
                var entity = new Distances();

                entity.FitnessActivityId = fitnessActivityId;
                entity.Distance = Convert.ToDecimal(item.Distance);
                entity.Timestamp = Convert.ToDecimal(item.Timestamp);

                Save(entity);
            }

            Commit();
        }
    }
}
