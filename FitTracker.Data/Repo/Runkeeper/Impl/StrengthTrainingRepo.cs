﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Runkeeper;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper.Impl
{
    internal class StrengthTrainingRepo : RepoBase<StrengthTraining, FitTrackerContext>, IStrengthTrainingRepo
    {
        public StrengthTrainingRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Strength Training received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        public List<StrengthTraining> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        /// <summary>
        /// Strength Training received by UserId and the time when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Strength Training happened</param>
        public StrengthTraining GetByUserId(String userId, DateTime date)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId && x.StartTime == date);
        }

        /// <summary>
        /// List of Strength Training received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Strength Training</param>
        /// <param name="endDate">End interval for Strength Training</param>
        public List<StrengthTraining> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.StartTime >= startDate && x.StartTime <= endDate).ToList();
        }

        /// <summary>
        /// Save Strength Training
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Strength Training happened</param>
        /// <param name="data">Data from Runkeeper app</param>
        public StrengthTraining SaveStrengthTraining(String userId, DateTime date, ApiModels.StrengthTrainingActivitiesPastModel data)
        {
            StrengthTraining entity = GetByUserId(userId, date);

            if (entity == null) entity = new StrengthTraining();

            entity.UserId = userId;
            entity.StartTime = data.StartTime;
            entity.TotalCalories = Convert.ToDecimal(data.TotalCalories);
            entity.Notes = (data.Notes != null) ? data.Notes : "";
            entity.Source = (data.Source != null) ? data.Source : "";

            entity = Save(entity);

            Commit();

            var repoExercises = base.UnitOfWork.GetRepo<IExercisesRepo>();
            repoExercises.SaveExercise(entity.PkID, data.Exercises);

            Commit();

            return entity;
        }
    }
}
