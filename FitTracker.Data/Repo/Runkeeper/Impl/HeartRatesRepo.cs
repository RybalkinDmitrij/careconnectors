﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Runkeeper;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper.Impl
{
    internal class HeartRatesRepo : RepoBase<HeartRates, FitTrackerContext>, IHeartRatesRepo
    {
        public HeartRatesRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Heart Rates received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        public List<HeartRates> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.FitnessActivity.UserId == userId).ToList();
        }

        /// <summary>
        /// List of Heart Rates received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Heart Rates</param>
        /// <param name="endDate">End interval for Heart Rates</param>
        public List<HeartRates> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.FitnessActivity.UserId == userId && x.FitnessActivity.StartTime >= startDate && x.FitnessActivity.StartTime <= endDate).ToList();
        }

        /// <summary>
        /// List of HeartRates received by fitnessActivityId
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        public List<HeartRates> GetByFitnessActivityId(Int64 fitnessActivityId)
        {
            return GetAll().Where(x => x.FitnessActivityId == fitnessActivityId).ToList();
        }

        /// <summary>
        /// Save Heart Rates
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        /// <param name="data">Heart Rates from Fitness Activitie</param>
        public void SaveHeartRates(Int64 fitnessActivityId, List<ApiModels.HeartRateModel> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[rk_HeartRate] WHERE FitnessActivityId = '{0}'", fitnessActivityId));

            foreach (var item in data)
            {
                var entity = new HeartRates();

                entity.FitnessActivityId = fitnessActivityId;
                entity.HeartRate = Convert.ToDecimal(item.HeartRate);
                entity.Timestamp = Convert.ToDecimal(item.Timestamp);

                Save(entity);
            }

            Commit();
        }
    }
}
