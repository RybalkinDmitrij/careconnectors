﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Runkeeper;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;
using RE = FitTracker.Data.Enums.Runkeeper;

namespace FitTracker.Data.Repo.Runkeeper.Impl
{
    internal class GeneralMeasurementsRepo : RepoBase<GeneralMeasurements, FitTrackerContext>, IGeneralMeasurementsRepo
    {
        public GeneralMeasurementsRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of General Measurements received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        public List<GeneralMeasurements> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        /// <summary>
        /// General Measurement received by UserId and the time when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when General Measurement happened</param>
        public GeneralMeasurements GetByUserId(String userId, DateTime date)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId && x.Timestamp == date);
        }

        /// <summary>
        /// List of General Measurements received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for General Measurements</param>
        /// <param name="endDate">End interval for General Measurements</param>
        public List<GeneralMeasurements> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Timestamp >= startDate && x.Timestamp <= endDate).ToList();
        }

        /// <summary>
        /// Save General Measurement
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when General Measurement happened</param>
        /// <param name="data">Data from Runkeeper app</param>
        public GeneralMeasurements SaveGeneralMeasurement(String userId, DateTime date, ApiModels.GeneralMeasurementsPastModel data)
        {
            GeneralMeasurements entity = GetByUserId(userId, date);

            if (entity == null) entity = new GeneralMeasurements();

            entity.UserId = userId;
            entity.Timestamp = data.Timestamp;
            entity.Type = (RE.GeneralMeasurementsType)data.MeasurementType;
            entity.Measurement = (data.Measurement != null) ? Convert.ToDecimal(data.Measurement) : 0;
            entity.Source = (data.Source != null) ? data.Source : "";

            Commit();

            return entity;
        }
    }
}
