﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Runkeeper;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper.Impl
{
    internal class ProfileRepo : RepoBase<Profile, FitTrackerContext>, IProfileRepo
    {
        public ProfileRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods


        /// <summary>
        /// Profile received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        public Profile GetByUserId(String userId)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId);
        }

        /// <summary>
        /// Save Profile
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="data">Data from Runkeeper app</param>
        public Profile SaveProfile(String userId, ApiModels.ProfileModel data)
        {
            Profile entity = GetByUserId(userId);

            if (entity == null) entity = new Profile();

            entity.UserId = userId;
            entity.AthleteType = (data.AthleteType != null) ? data.AthleteType : "None";
            entity.Birthday = data.Birthday;
            entity.Elite = data.Elite;
            entity.Gender = data.Gender;
            entity.Location = (data.Location != null) ? data.Location : "None";
            entity.Name = data.Name;

            entity = Save(entity);

            Commit();

            return entity;
        }
    }
}
