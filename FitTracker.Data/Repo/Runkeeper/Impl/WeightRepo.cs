﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Runkeeper;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;
using RE = FitTracker.Data.Enums.Runkeeper;

namespace FitTracker.Data.Repo.Runkeeper.Impl
{
    internal class WeightRepo : RepoBase<Weight, FitTrackerContext>, IWeightRepo
    {
        public WeightRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Weight received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        public List<Weight> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        /// <summary>
        /// Weight received by UserId and the time when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Weight happened</param>
        public Weight GetByUserId(String userId, DateTime date, RE.WeightType type)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId && x.Timestamp == date && x.Type == type);
        }

        /// <summary>
        /// List of Weight received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Weight</param>
        /// <param name="endDate">End interval for Weight</param>
        public List<Weight> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Timestamp >= startDate && x.Timestamp <= endDate).ToList();
        }

        /// <summary>
        /// Save Weight
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Weight happened</param>
        /// <param name="data">Data from Runkeeper app</param>
        public Weight SaveWeight(String userId, DateTime date, ApiModels.WeightPastModel data)
        {
            Weight entity = GetByUserId(userId, date, (RE.WeightType)data.MeasurementType);

            if (entity == null) entity = new Weight();

            entity.UserId = userId;
            entity.Timestamp = data.Timestamp;
            entity.Type = (RE.WeightType)data.MeasurementType;
            entity.Measurement = (data.Measurement != null) ? Convert.ToDecimal(data.Measurement) : 0;
            entity.Source = (data.Source != null) ? data.Source : "";

            Save(entity);

            Commit();

            return entity;
        }
    }
}
