﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Runkeeper;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;
using RE = FitTracker.Data.Enums.Runkeeper;

namespace FitTracker.Data.Repo.Runkeeper.Impl
{
    internal class NutritionRepo : RepoBase<Nutrition, FitTrackerContext>, INutritionRepo
    {
        public NutritionRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Nutrition received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        public List<Nutrition> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        /// <summary>
        /// Nutrition received by UserId and the time when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Nutrition happened</param>
        public Nutrition GetByUserId(String userId, DateTime date, RE.NutritionType type)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId && x.Timestamp == date && x.Type == type);
        }

        /// <summary>
        /// List of Nutrition received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Nutrition</param>
        /// <param name="endDate">End interval for Nutrition</param>
        public List<Nutrition> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Timestamp >= startDate && x.Timestamp <= endDate).ToList();
        }

        /// <summary>
        /// Save Nutrition
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Nutrition happened</param>
        /// <param name="data">Data from Runkeeper app</param>
        public Nutrition SaveNutrition(String userId, DateTime date, ApiModels.NutritionPastModel data)
        {
            Nutrition entity = GetByUserId(userId, date, (RE.NutritionType)data.MeasurementType);

            if (entity == null) entity = new Nutrition();

            entity.UserId = userId;
            entity.Timestamp = data.Timestamp;
            entity.Type = (RE.NutritionType)data.MeasurementType;
            entity.Measurement = (data.Measurement != null) ? Convert.ToDecimal(data.Measurement) : 0;
            entity.Source = (data.Source != null) ? data.Source : "";

            Save(entity);

            Commit();

            return entity;
        }
    }
}
