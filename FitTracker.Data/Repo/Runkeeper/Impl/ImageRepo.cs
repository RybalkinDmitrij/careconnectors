﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Runkeeper;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper.Impl
{
    internal class ImageRepo : RepoBase<Image, FitTrackerContext>, IImageRepo
    {
        public ImageRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Images received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        public List<Image> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.FitnessActivity.UserId == userId).ToList();
        }

        /// <summary>
        /// List of Images received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Images</param>
        /// <param name="endDate">End interval for Images</param>
        public List<Image> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.FitnessActivity.UserId == userId && x.FitnessActivity.StartTime >= startDate && x.FitnessActivity.StartTime <= endDate).ToList();
        }

        /// <summary>
        /// List of Images received by fitnessActivityId
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        public List<Image> GetByFitnessActivityId(Int64 fitnessActivityId)
        {
            return GetAll().Where(x => x.FitnessActivityId == fitnessActivityId).ToList();
        }

        /// <summary>
        /// Save Image
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        /// <param name="data">Calories from Fitness Activitie</param>
        public void SaveImage(Int64 fitnessActivityId, List<ApiModels.ImageModel> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[rk_Image] WHERE FitnessActivityId = '{0}'", fitnessActivityId));

            foreach (var item in data)
            {
                var entity = new Image();

                entity.FitnessActivityId = fitnessActivityId;
                entity.Latitude = Convert.ToDecimal(item.Latitude);
                entity.Longitude = Convert.ToDecimal(item.Longitude);
                entity.Timestamp = Convert.ToDecimal(item.Timestamp);
                entity.URI = item.Uri;

                Save(entity);
            }

            Commit();
        }
    }
}
