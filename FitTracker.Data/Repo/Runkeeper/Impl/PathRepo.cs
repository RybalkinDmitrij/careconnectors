﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Runkeeper;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper.Impl
{
    internal class PathRepo : RepoBase<Path, FitTrackerContext>, IPathRepo
    {
        public PathRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Path received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        public List<Path> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.FitnessActivity.UserId == userId).ToList();
        }

        /// <summary>
        /// List of Path received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Path</param>
        /// <param name="endDate">End interval for Path</param>
        public List<Path> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.FitnessActivity.UserId == userId && x.FitnessActivity.StartTime >= startDate && x.FitnessActivity.StartTime <= endDate).ToList();
        }

        /// <summary>
        /// List of Path received by fitnessActivityId
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        public List<Path> GetByFitnessActivityId(Int64 fitnessActivityId)
        {
            return GetAll().Where(x => x.FitnessActivityId == fitnessActivityId).ToList();
        }

        /// <summary>
        /// Save Path
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activity Id</param>
        /// <param name="data">Calories from Fitness Activity</param>
        public void SavePath(Int64 fitnessActivityId, List<ApiModels.PathModel> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[rk_Path] WHERE FitnessActivityId = '{0}'", fitnessActivityId));

            foreach (var item in data)
            {
                var entity = new Path();

                entity.FitnessActivityId = fitnessActivityId;
                entity.Latitude = Convert.ToDecimal(item.Latitude);
                entity.Longitude = Convert.ToDecimal(item.Longitude);
                entity.Altitude = Convert.ToDecimal(item.Altitude);
                entity.Timestamp = Convert.ToDecimal(item.Timestamp);
                entity.Type = item.Type;

                Save(entity);
            }

            Commit();
        }
    }
}
