﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Runkeeper;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper.Impl
{
    internal class CaloriesRepo : RepoBase<CaloriesMod, FitTrackerContext>, ICaloriesRepo
    {
        public CaloriesRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Calories received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        public List<CaloriesMod> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.FitnessActivity.UserId == userId).ToList();
        }

        /// <summary>
        /// List of Calories received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Calories</param>
        /// <param name="endDate">End interval for Calories</param>
        public List<CaloriesMod> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.FitnessActivity.UserId == userId && x.FitnessActivity.StartTime >= startDate && x.FitnessActivity.StartTime <= endDate).ToList();
        }

        /// <summary>
        /// List of Calories received by fitnessActivityId
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        public List<CaloriesMod> GetByFitnessActivityId(Int64 fitnessActivityId)
        {
            return GetAll().Where(x => x.FitnessActivityId == fitnessActivityId).ToList();
        }

        /// <summary>
        /// Save Calories
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        /// <param name="data">Calories from Fitness Activitie</param>
        public void SaveCalories(Int64 fitnessActivityId, List<ApiModels.CaloriesModel> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[rk_Calories] WHERE FitnessActivityId = '{0}'", fitnessActivityId));

            foreach (var item in data)
            {
                var entity = new CaloriesMod();

                entity.FitnessActivityId = fitnessActivityId;
                entity.Calories = Convert.ToDecimal(item.Calories);
                entity.Timestamp = Convert.ToDecimal(item.Timestamp);

                Save(entity);
            }

            Commit();
        }
    }
}
