﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface ISetsRepo : IRepoBase<Sets>
    {
        /// <summary>
        /// List of Sets received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<Sets> GetByUserId(String userId);

        /// <summary>
        /// List of Sets received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Sets</param>
        /// <param name="endDate">End interval for Sets</param>
        List<Sets> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// List of Sets received by exerciseId
        /// </summary>
        /// <param name="exerciseId">Exercise Id</param>
        List<Sets> GetByExerciseId(Int64 exerciseId);

        /// <summary>
        /// List of Sets received by strengthTrainingId
        /// </summary>
        /// <param name="strengthTrainingId">Strength Training Id</param>
        List<Sets> GetByStrengthTrainingId(Int64 strengthTrainingId);

        /// <summary>
        /// Save Sets
        /// </summary>
        /// <param name="exerciseId">Exercise Id</param>
        /// <param name="data">Sets from Exercises<</param>
        void SaveSet(Int64 exerciseId, List<ApiModels.SetsModel> data);
    }
}
