﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface IFitnessActivitiesRepo : IRepoBase<FitnessActivities>
    {
        /// <summary>
        /// List of Fitness Activities received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<FitnessActivities> GetByUserId(String userId);

        /// <summary>
        /// Fitness Activitie received by UserId and the time when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Fitness Activitie happened</param>
        FitnessActivities GetByUserId(String userId, DateTime date);

        /// <summary>
        /// List of Fitness Activities received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Fitness Activities</param>
        /// <param name="endDate">End interval for Fitness Activities</param>
        List<FitnessActivities> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Save Fitness Activitie
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="date">The time when Fitness Activitie happened</param>
        /// <param name="data">Data from Runkeeper app</param>
        FitnessActivities SaveActivitySummary(String userId, DateTime date, ApiModels.FitnessActivitiesPastModel data);
    }
}
