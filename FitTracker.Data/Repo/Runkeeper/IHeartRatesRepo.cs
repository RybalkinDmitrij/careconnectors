﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface IHeartRatesRepo : IRepoBase<HeartRates>
    {
        /// <summary>
        /// List of Heart Rates received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<HeartRates> GetByUserId(String userId);

        /// <summary>
        /// List of Heart Rates received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Heart Rates</param>
        /// <param name="endDate">End interval for Heart Rates</param>
        List<HeartRates> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// List of HeartRates received by fitnessActivityId
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        List<HeartRates> GetByFitnessActivityId(Int64 fitnessActivityId);

        /// <summary>
        /// Save Heart Rates
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        /// <param name="data">Heart Rates from Fitness Activitie</param>
        void SaveHeartRates(Int64 fitnessActivityId, List<ApiModels.HeartRateModel> data);
    }
}
