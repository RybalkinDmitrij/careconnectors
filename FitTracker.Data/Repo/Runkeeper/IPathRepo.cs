﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface IPathRepo : IRepoBase<Path>
    {
        /// <summary>
        /// List of Path received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<Path> GetByUserId(String userId);

        /// <summary>
        /// List of Path received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Path</param>
        /// <param name="endDate">End interval for Path</param>
        List<Path> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// List of Path received by fitnessActivityId
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        List<Path> GetByFitnessActivityId(Int64 fitnessActivityId);

        /// <summary>
        /// Save Path
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        /// <param name="data">Calories from Fitness Activitie</param>
        void SavePath(Int64 fitnessActivityId, List<ApiModels.PathModel> data);
    }
}
