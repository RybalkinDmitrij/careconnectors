﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface ICaloriesRepo : IRepoBase<CaloriesMod>
    {
        /// <summary>
        /// List of Calories received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<CaloriesMod> GetByUserId(String userId);

        /// <summary>
        /// List of Calories received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Caloris</param>
        /// <param name="endDate">End interval for Caloris</param>
        List<CaloriesMod> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// List of Calories received by fitnessActivitiesId
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        List<CaloriesMod> GetByFitnessActivityId(Int64 fitnessActivityId);

        /// <summary>
        /// Save Calories
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activitie Id</param>
        /// <param name="data">Calories from Fitness Activitie</param>
        void SaveCalories(Int64 fitnessActivityId, List<ApiModels.CaloriesModel> data);
    }
}
