﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Runkeeper.API.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Runkeeper
{
    public interface IImageRepo : IRepoBase<Image>
    {
        /// <summary>
        /// List of Images received by UserId
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        List<Image> GetByUserId(String userId);

        /// <summary>
        /// List of Images received by UserId and time interval when it happened
        /// </summary>
        /// <param name="userId">Runkeeper UserID</param>
        /// <param name="startDate">Start interval for Images</param>
        /// <param name="endDate">End interval for Images</param>
        List<Image> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// List of Images received by fitnessActivityId
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activity Id</param>
        List<Image> GetByFitnessActivityId(Int64 fitnessActivityId);

        /// <summary>
        /// Save Image
        /// </summary>
        /// <param name="fitnessActivityId">Fitness Activity Id</param>
        /// <param name="data">Calories from Fitness Activity</param>
        void SaveImage(Int64 fitnessActivityId, List<ApiModels.ImageModel> data);
    }
}
