﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Jawbone;
using FitTracker.Data.Repo.Abstract;

namespace FitTracker.Data.Repo.Jawbone
{
    public interface ISettingsRepo : IRepoBase<Settings>
    {
        List<Settings> GetByUserId(String userId);
    }
}
