﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Jawbone;
using FitTracker.Data.Repo.Abstract;

namespace FitTracker.Data.Repo.Jawbone
{
    public interface IHeartRateRepo : IRepoBase<HeartRate>
    {
        List<HeartRate> GetByUserId(String userId);

        List<HeartRate> GetByUserId(String userId, DateTime startDate, DateTime endDate);
    }
}
