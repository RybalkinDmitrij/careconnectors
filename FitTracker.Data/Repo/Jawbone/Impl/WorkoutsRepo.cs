﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Jawbone;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Jawbone;

using JAM = Jawbone.Api.Models;
using FitTracker.Data.Helpers;

namespace FitTracker.Data.Repo.Jawbone.Impl
{
    internal class WorkoutsRepo : RepoBase<Workouts, FitTrackerContext>, IWorkoutsRepo
    {
        public WorkoutsRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<Workouts> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public List<Workouts> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Date >= startDate && x.Date <= endDate).ToList();
        }

        public void SaveData(String userId, DateTime date, JAM.Workouts data)
        {
            ExecuteSql(String.Format(@"UPDATE [dbo].[jwb_Workouts]
                                        SET IsDeleted = 1,
	                                        DateDelete = GETDATE()
                                        WHERE PkID in ( 
                                            SELECT nv.PkID FROM [dbo].[jwb_Workouts] nv
                                                WHERE nv.UserId = '{0}' AND nv.Date = '{1}'
                                        )", userId, date.ToString("MM/dd/yyyy")));

            ExecuteSql(String.Format(@"DELETE [dbo].[jwb_Workouts] WHERE IsDeleted = 1"));

            if (data == null) return;

            foreach (var item in data.data.items)
            {
                Decimal resPlaceLat = 0;
                Decimal resPlaceLon = 0;

                var entity = new Workouts();

                entity.UserId = userId;

                entity.Xid = item.xid;
                entity.Title = item.title;
                entity.Type = item.type != null ? item.type : String.Empty;
                entity.SubType = item.sub_type;
                entity.TimeCreated = DateHelper.FromUnixTime(item.time_created);
                entity.TimeUpdated = DateHelper.FromUnixTime(item.time_updated);
                entity.TimeCompleted = DateHelper.FromUnixTime(item.time_completed);
                entity.Date = DateHelper.FromFormatTime(item.date, FormatDateHelper.yyyyMMdd);
                entity.PlaceLat = Decimal.TryParse(item.place_lat, out resPlaceLat) ? resPlaceLat : 0;
                entity.PlaceLon = Decimal.TryParse(item.place_lon, out resPlaceLon) ? resPlaceLon : 0;
                entity.PlaceAcc = item.place_acc;
                entity.PlaceName = item.place_name;
                entity.Route = item.route;
                entity.Image = item.image;
                entity.SnapshotImage = item.snapshot_image;

                if (item.details != null)
                {
                    entity.Steps = item.details.steps;
                    entity.Time = item.details.time;
                    entity.BgActiveTime = item.details.bg_active_time;
                    entity.Meters = item.details.meters;
                    entity.Km = item.details.km;
                    entity.Intensity = item.details.intensity;
                    entity.Calories = item.details.calories;
                    entity.Bmr = item.details.bmr;
                    entity.BgCalories = item.details.bg_calories;
                    entity.BmrCalories = item.details.bmr_calories;
                    entity.Timezone = item.details.tz;
                }

                Save(entity);

                Commit();
            }
        }
    }
}
