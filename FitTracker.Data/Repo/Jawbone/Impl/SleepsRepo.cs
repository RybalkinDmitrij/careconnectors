﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Jawbone;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Jawbone;

using JAM = Jawbone.Api.Models;
using FitTracker.Data.Helpers;

namespace FitTracker.Data.Repo.Jawbone.Impl
{
    internal class SleepsRepo : RepoBase<Sleeps, FitTrackerContext>, ISleepsRepo
    {
        public SleepsRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<Sleeps> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public List<Sleeps> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Date >= startDate && x.Date <= endDate).ToList();
        }

        public void SaveData(String userId, DateTime date, JAM.Sleeps data)
        {
            ExecuteSql(String.Format(@"UPDATE [dbo].[jwb_Sleeps]
                                        SET IsDeleted = 1,
	                                        DateDelete = GETDATE()
                                        WHERE PkID in ( 
                                            SELECT nv.PkID FROM [dbo].[jwb_Sleeps] nv
                                                WHERE nv.UserId = '{0}' AND nv.Date = '{1}'
                                        )", userId, date.ToString("MM/dd/yyyy")));

            ExecuteSql(String.Format(@"DELETE [dbo].[jwb_Sleeps] WHERE IsDeleted = 1"));

            if (data == null) return;

            foreach (var item in data.data.items)
            {
                var entity = new Sleeps();

                entity.UserId = userId;
                
                entity.Xid = item.xid;
                entity.Title = item.title;
                entity.Type = "";
                entity.SubType = item.sub_type;
                entity.TimeCreated = DateHelper.FromUnixTime(item.time_created);
                entity.TimeCompleted = DateHelper.FromUnixTime(item.time_completed);
                entity.Date = DateHelper.FromFormatTime(item.date, FormatDateHelper.yyyyMMdd);
                entity.PlaceLat = item.place_lat;
                entity.PlaceLon = item.place_lon;
                entity.PlaceAcc = item.place_acc;
                entity.PlaceName = item.place_name;
                entity.SnapshotImage = item.snapshot_image;

                if (item.details != null)
                {
                    entity.SmartAlarmFire = DateHelper.FromUnixTime(item.details.smart_alarm_fire);
                    entity.AwakeTime = DateHelper.FromUnixTime(item.details.awake_time);
                    entity.AsleepTime = DateHelper.FromUnixTime(item.details.asleep_time);
                    entity.Awakenings = item.details.awakenings;
                    entity.Rem = item.details.rem;
                    entity.Light = item.details.light;
                    entity.Sound = item.details.sound;
                    entity.Awake = item.details.awake;
                    entity.Duration = item.details.duration;
                    entity.Quality = item.details.quality;
                    entity.Timezone = item.details.tz;
                }

                Save(entity);

                Commit();
            }
        }
    }
}
