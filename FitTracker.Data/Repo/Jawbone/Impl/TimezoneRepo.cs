﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Jawbone;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Jawbone;

namespace FitTracker.Data.Repo.Jawbone.Impl
{
    internal class TimezoneRepo : RepoBase<Timezone, FitTrackerContext>, ITimezoneRepo
    {
        public TimezoneRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<Timezone> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }
    }
}
