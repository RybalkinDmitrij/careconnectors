﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Jawbone;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Jawbone;

using JAM = Jawbone.Api.Models;
using FitTracker.Data.Helpers;

namespace FitTracker.Data.Repo.Jawbone.Impl
{
    internal class MoodRepo : RepoBase<Mood, FitTrackerContext>, IMoodRepo
    {
        public MoodRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<Mood> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public void SaveData(String userId, DateTime date, JAM.Mood data)
        {
            ExecuteSql(String.Format(@"UPDATE [dbo].[jwb_Mood]
                                        SET IsDeleted = 1,
	                                        DateDelete = GETDATE()
                                        WHERE PkID in ( 
                                            SELECT nv.PkID FROM [dbo].[jwb_Mood] nv
                                                WHERE nv.UserId = '{0}' AND nv.Date = '{1}'
                                        )", userId, date.ToString("MM/dd/yyyy")));

            ExecuteSql(String.Format(@"DELETE [dbo].[jwb_Mood] WHERE IsDeleted = 1"));

            if (data == null) return;

            var dataMood = data.data;

            if (dataMood.xid != null)
            {
                Decimal resPlaceLat = 0;
                Decimal resPlaceLon = 0;

                var entity = new Mood();

                entity.UserId = userId;

                entity.Xid = dataMood.xid;
                entity.Title = dataMood.title;
                entity.TimeCreated = DateHelper.FromUnixTime(dataMood.time_created);
                entity.TimeUpdated = DateHelper.FromUnixTime(dataMood.time_updated);
                entity.Date = DateHelper.FromFormatTime(dataMood.date, FormatDateHelper.yyyyMMdd);
                entity.Type = dataMood.type != null ? dataMood.type : String.Empty;
                entity.SubType = dataMood.sub_type;
                entity.PlaceLat = Decimal.TryParse(dataMood.place_lat, out resPlaceLat) ? resPlaceLat : 0;
                entity.PlaceLon = Decimal.TryParse(dataMood.place_lon, out resPlaceLon) ? resPlaceLon : 0;
                entity.PlaceAcc = dataMood.place_acc;
                entity.PlaceName = dataMood.place_name;
                entity.Timezone = dataMood.details != null ? dataMood.details.tz : String.Empty;

                Save(entity);

                Commit();
            }
        }
    }
}
