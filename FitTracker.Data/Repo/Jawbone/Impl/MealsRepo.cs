﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Jawbone;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Jawbone;

using JAM = Jawbone.Api.Models;
using FitTracker.Data.Helpers;

namespace FitTracker.Data.Repo.Jawbone.Impl
{
    internal class MealsRepo : RepoBase<Meals, FitTrackerContext>, IMealsRepo
    {
        public MealsRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<Meals> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public List<Meals> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Date >= startDate && x.Date <= endDate).ToList();
        }

        public void SaveData(String userId, DateTime date, JAM.Meals data)
        {
            ExecuteSql(String.Format(@"UPDATE [dbo].[jwb_Meals]
                                        SET IsDeleted = 1,
	                                        DateDelete = GETDATE()
                                        WHERE PkID in ( 
                                            SELECT nv.PkID FROM [dbo].[jwb_Meals] nv
                                                WHERE nv.UserId = '{0}' AND nv.Date = '{1}'
                                        )", userId, date.ToString("MM/dd/yyyy")));

            ExecuteSql(String.Format(@"DELETE [dbo].[jwb_Meals] WHERE IsDeleted = 1"));

            if (data == null) return;

            foreach (var item in data.data.items)
            {
                Decimal resPlaceLat = 0;
                Decimal resPlaceLon = 0;

                var entity = new Meals();

                entity.UserId = userId;

                entity.Xid = item.xid;
                entity.Title = item.title;
                entity.Note = item.note;
                entity.Type = item.type != null ? item.type : "";
                entity.SubType = item.sub_type;
                entity.TimeCreated = DateHelper.FromUnixTime(item.time_created);
                entity.TimeUpdated = DateHelper.FromUnixTime(item.time_updated);
                entity.TimeCompleted = DateHelper.FromUnixTime(item.time_completed);
                entity.Date = DateHelper.FromFormatTime(item.date, FormatDateHelper.yyyyMMdd);
                entity.PlaceLat = Decimal.TryParse(item.place_lat, out resPlaceLat) ? resPlaceLat : 0;
                entity.PlaceLon = Decimal.TryParse(item.place_lon, out resPlaceLon) ? resPlaceLon : 0;
                entity.PlaceAcc = item.place_acc;
                entity.PlaceName = item.place_name;

                if (item.details != null)
                {
                    entity.NumDrinks = item.details.num_drinks;
                    entity.NumWater = item.details.num_water;
                    entity.NumFoods = item.details.num_foods;
                    entity.OnlyWaters = item.details.only_waters;
                    entity.NumMealitemsGreen = item.details.num_mealitems_green;
                    entity.NumMealitemsYellow = item.details.num_mealitems_yellow;
                    entity.NumMealitemsRed = item.details.num_mealitems_red;
                    entity.NumMealitemsWithScore = item.details.num_mealitems_with_score;
                    entity.Fiber = item.details.fiber;
                    entity.PolyunsaturatedFat = item.details.polyunsaturated_fat;
                    entity.Potassium = item.details.potassium;
                    entity.Fat = item.details.fat;
                    entity.Carbohydrate = item.details.carbohydrate;
                    entity.SaturatedFat = item.details.saturated_fat;
                    entity.Protein = item.details.protein;
                    entity.MonounsaturatedFat = item.details.monounsaturated_fat;
                    entity.Sodium = item.details.sodium;
                    entity.VitaminC = item.details.vitamin_c;
                    entity.VitaminA = item.details.vitamin_a;
                    entity.Calories = item.details.calories;
                    entity.UnsaturatedFat = item.details.unsaturated_fat;
                    entity.Sugar = item.details.sugar;
                    entity.Calcium = item.details.calcium;
                    entity.Iron = item.details.iron;
                    entity.Cholesterol = item.details.cholesterol;
                    entity.Caffeine = item.details.caffeine;
                    entity.Accuracy = item.details.accuracy;
                    entity.Timezone = item.details.tz;
                }

                Save(entity);

                Commit();
            }
        }
    }
}
