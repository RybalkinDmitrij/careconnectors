﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Jawbone;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Jawbone;

using JAM = Jawbone.Api.Models;
using FitTracker.Data.Helpers;

namespace FitTracker.Data.Repo.Jawbone.Impl
{
    internal class BodyEventRepo : RepoBase<BodyEvent, FitTrackerContext>, IBodyEventRepo
    {
        public BodyEventRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<BodyEvent> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public List<BodyEvent> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Date >= startDate && x.Date <= endDate).ToList();
        }

        public void SaveData(String userId, DateTime date, JAM.BodyEvent data)
        {
            ExecuteSql(String.Format(@"UPDATE [dbo].[jwb_BodyEvent]
                                        SET IsDeleted = 1,
	                                        DateDelete = GETDATE()
                                        WHERE PkID in ( 
                                            SELECT nv.PkID FROM [dbo].[jwb_BodyEvent] nv
                                                WHERE nv.UserId = '{0}' AND nv.Date = '{1}'
                                        )", userId, date.ToString("MM/dd/yyyy")));

            ExecuteSql(String.Format(@"DELETE [dbo].[jwb_BodyEvent] WHERE IsDeleted = 1"));

            if (data == null) return;

            foreach (var item in data.data.items)
            {
                Decimal resPlaceLat = 0;
                Decimal resPlaceLon = 0;

                var entity = new BodyEvent();

                entity.UserId = userId;

                entity.Xid = item.xid;
                entity.Title = item.title != null ? item.title : String.Empty;
                entity.Type = item.type != null ? item.type : String.Empty;
                entity.TimeCreated = DateHelper.FromUnixTime(item.time_created);
                entity.TimeUpdated = DateHelper.FromUnixTime(item.time_updated);
                entity.Date = DateHelper.FromFormatTime(item.date, FormatDateHelper.yyyyMMdd);
                entity.PlaceLat = Decimal.TryParse(item.place_lat, out resPlaceLat) ? resPlaceLat : 0;
                entity.PlaceLon = Decimal.TryParse(item.place_lon, out resPlaceLon) ? resPlaceLon : 0;
                entity.PlaceAcc = item.place_acc;
                entity.PlaceName = item.place_name;
                entity.Note = item.note != null ? item.note : String.Empty;
                entity.LeanMass = item.lean_mass;
                entity.Weight = item.weight;
                entity.BodyFat = item.body_fat;
                entity.BMI = item.bmi;
                entity.Timezone = item.details != null ? item.details.tz : String.Empty;

                Save(entity);

                Commit();
            }
        }
    }
}
