﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Jawbone;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Jawbone;

namespace FitTracker.Data.Repo.Jawbone.Impl
{
    internal class HourlyTotalRepo : RepoBase<HourlyTotal, FitTrackerContext>, IHourlyTotalRepo
    {
        public HourlyTotalRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<HourlyTotal> GetByMovesId(Int64 movesId)
        {
            return GetAll().Where(x => x.MovesId == movesId).ToList();
        }
    }
}
