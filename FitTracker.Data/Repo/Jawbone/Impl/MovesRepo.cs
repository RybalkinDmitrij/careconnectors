﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Jawbone;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Jawbone;

using JAM = Jawbone.Api.Models;
using FitTracker.Data.Helpers;

namespace FitTracker.Data.Repo.Jawbone.Impl
{
    internal class MovesRepo : RepoBase<Moves, FitTrackerContext>, IMovesRepo
    {
        public MovesRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<Moves> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public List<Moves> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Date >= startDate && x.Date <= endDate).ToList();
        }

        public void SaveData(String userId, DateTime date, JAM.Moves data)
        {
            ExecuteSql(String.Format(@"UPDATE [dbo].[jwb_HourlyTotal]
                                        SET IsDeleted = 1,
	                                        DateDelete = GETDATE()
                                        WHERE PkID in ( 
                                            SELECT nv.PkID FROM [dbo].[jwb_HourlyTotal] nv
                                                INNER JOIN [dbo].[jwb_Moves] fl ON fl.PkID = nv.MovesId
                                                WHERE fl.UserId = '{0}' AND fl.Date = '{1}'
                                        )", userId, date.ToString("MM/dd/yyyy")));

            ExecuteSql(String.Format(@"UPDATE [dbo].[jwb_Moves]
                                        SET IsDeleted = 1,
	                                        DateDelete = GETDATE()
                                        WHERE PkID in ( 
                                            SELECT nv.PkID FROM [dbo].[jwb_Moves] nv
                                                WHERE nv.UserId = '{0}' AND nv.Date = '{1}'
                                        )", userId, date.ToString("MM/dd/yyyy")));

            ExecuteSql(String.Format(@"DELETE [dbo].[jwb_HourlyTotal] WHERE IsDeleted = 1"));
            ExecuteSql(String.Format(@"DELETE [dbo].[jwb_Moves] WHERE IsDeleted = 1"));

            if (data == null) return;

            var repoHourlyTotal = UnitOfWork.GetRepo<IHourlyTotalRepo>();

            foreach (var item in data.data.items)
            {
                var entity = new Moves();

                entity.UserId = userId;
                entity.Xid = item.xid;
                entity.Title = item.title;
                entity.Type = item.type;
                entity.TimeCreated = DateHelper.FromUnixTime(item.time_created);
                entity.TimeUpdated = DateHelper.FromUnixTime(item.time_updated);
                entity.TimeCompleted = DateHelper.FromUnixTime(item.time_completed);
                entity.Date = DateHelper.FromFormatTime(item.date, FormatDateHelper.yyyyMMdd);
                entity.SnapshotImage = item.snapshot_image;

                if (item.details != null)
                {
                    entity.Distance = item.details.distance;
                    entity.Km = item.details.km;
                    entity.Steps = item.details.steps;
                    entity.ActiveTime = item.details.active_time;
                    entity.LongestActive = item.details.longest_active;
                    entity.InactiveTime = item.details.inactive_time;
                    entity.LongestIdle = item.details.longest_idle;
                    entity.Calories = item.details.calories;
                    entity.BmrDay = item.details.bmr_day;
                    entity.Bmr = item.details.bmr;
                    entity.BgCalories = item.details.bg_calories;
                    entity.WoCalories = item.details.wo_calories;
                    entity.WoTime = item.details.wo_time;
                    entity.WoActiveTime = item.details.wo_active_time;
                    entity.WoCount = item.details.wo_count;
                    entity.WoLongest = item.details.wo_longest;
                    entity.Sunrise = item.details.sunrise;
                    entity.Sunset = item.details.sunset;
                    entity.Timezone = item.details.tz;

                    entity = Save(entity);

                    Commit();

                    if (item.details.hourly_totals != null)
                    {
                        foreach (var item2 in item.details.hourly_totals)
                        {
                            var entityHourlyTotal = new HourlyTotal();

                            entityHourlyTotal.MovesId = entity.PkID;
                            entityHourlyTotal.Distance = entity.Distance;
                            entityHourlyTotal.Calories = entity.Calories;
                            entityHourlyTotal.Steps = entity.Steps;
                            entityHourlyTotal.ActiveTime = entity.ActiveTime;
                            entityHourlyTotal.InactiveTime = entity.InactiveTime;
                            entityHourlyTotal.LongestActiveTime = entity.LongestActive;
                            entityHourlyTotal.LongestIdleTime = entity.LongestIdle;

                            repoHourlyTotal.Save(entityHourlyTotal);

                            Commit();
                        }
                    }
                }                
            }
        }
    }
}
