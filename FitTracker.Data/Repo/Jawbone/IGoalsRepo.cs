﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Jawbone;
using FitTracker.Data.Repo.Abstract;

namespace FitTracker.Data.Repo.Jawbone
{
    public interface IGoalsRepo : IRepoBase<Goals>
    {
        List<Goals> GetByUserId(String userId);
    }
}
