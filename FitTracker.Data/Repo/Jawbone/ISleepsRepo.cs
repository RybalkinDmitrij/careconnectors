﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Jawbone;
using FitTracker.Data.Repo.Abstract;

using JAM = Jawbone.Api.Models;

namespace FitTracker.Data.Repo.Jawbone
{
    public interface ISleepsRepo : IRepoBase<Sleeps>
    {
        List<Sleeps> GetByUserId(String userId);

        List<Sleeps> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        void SaveData(String userId, DateTime date, JAM.Sleeps data);
    }
}
