﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Jawbone;
using FitTracker.Data.Repo.Abstract;

namespace FitTracker.Data.Repo.Jawbone
{
    public interface IMovesTickRepo : IRepoBase<MovesTick>
    {
        List<MovesTick> GetByUserId(String userId);
    }
}
