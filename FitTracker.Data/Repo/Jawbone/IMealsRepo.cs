﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Jawbone;
using FitTracker.Data.Repo.Abstract;

using JAM = Jawbone.Api.Models;

namespace FitTracker.Data.Repo.Jawbone
{
    public interface IMealsRepo : IRepoBase<Meals>
    {
        List<Meals> GetByUserId(String userId);

        List<Meals> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        void SaveData(String userId, DateTime date, JAM.Meals data);
    }
}
