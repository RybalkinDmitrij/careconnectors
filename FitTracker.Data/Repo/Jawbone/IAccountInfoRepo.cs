﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Jawbone;
using FitTracker.Data.Repo.Abstract;

namespace FitTracker.Data.Repo.Jawbone
{
    public interface IAccountInfoRepo : IRepoBase<AccountInfo>
    {
        List<AccountInfo> GetByUserId(String userId);
    }
}
