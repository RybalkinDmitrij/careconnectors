﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Jawbone;
using FitTracker.Data.Repo.Abstract;

using JAM = Jawbone.Api.Models;

namespace FitTracker.Data.Repo.Jawbone
{
    public interface IMoodRepo : IRepoBase<Mood>
    {
        List<Mood> GetByUserId(String userId);

        void SaveData(String userId, DateTime date, JAM.Mood data);
    }
}
