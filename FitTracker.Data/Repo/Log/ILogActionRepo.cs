﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Log;
using FitTracker.Data.Repo.Abstract;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Log
{
    public interface ILogActionRepo
    {
        void ExecuteSql(String sql);

        LogAction Get(Guid Id);

        IQueryable<LogAction> GetAll();

        LogAction Add(LogAction entity);

        LogAction Update(LogAction entity);

        void Remove(Guid Id);

        void Remove(LogAction entity);

        void Remove(IEnumerable<LogAction> entities);
    }
}
