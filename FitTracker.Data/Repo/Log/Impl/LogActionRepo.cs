﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Log;
using System.Collections.Generic;
using System.Data.Entity;

namespace FitTracker.Data.Repo.Log.Impl
{
    internal class LogActionRepo : ILogActionRepo
    {
        public LogActionRepo(UnitOfWork<FitTrackerContext> unitOfWork)
        {
            UnitOfWork = unitOfWork;
            Context = UnitOfWork.Context;
            DbSet = Context.Set<LogAction>();
        }

        public void ExecuteSql(String sql)
        {
            Context.Database.ExecuteSqlCommand(sql);
        }

        public LogAction Get(Guid Id)
        {
            return DbSet.Find(Id);
        }
        
        public IQueryable<LogAction> GetAll()
        {
            return DbSet;
        }

        public LogAction Add(LogAction entity)
        {
            return DbSet.Add(entity);
        }

        public LogAction Update(LogAction entity)
        {
            entity = DbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public void Remove(Guid Id)
        {
            var entity = Get(Id);
            if (entity != null) Remove(entity);
        }

        public void Remove(LogAction entity)
        {
            DbSet.Remove(entity);
        }

        public void Remove(IEnumerable<LogAction> entities)
        {
            foreach (var entity in entities)
            {
                Remove(entity);
            }
        }

        #region protected methods
        
        protected String GetSimpleKey()
        {
            return Guid.NewGuid().ToString();
        }

        #endregion protected methods

        #region protected properties

        protected UnitOfWork<FitTrackerContext> UnitOfWork { get; private set; }
        protected FitTrackerContext Context { get; private set; }
        protected IDbSet<LogAction> DbSet { get; private set; }

        #endregion protected properties
    }
}
