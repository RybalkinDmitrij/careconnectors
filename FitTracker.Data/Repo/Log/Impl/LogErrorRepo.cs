﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Log;
using System.Data.Entity;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Log.Impl
{
    internal class LogErrorRepo : ILogErrorRepo
    {
        public LogErrorRepo(UnitOfWork<FitTrackerContext> unitOfWork)
        {
            UnitOfWork = unitOfWork;
            Context = UnitOfWork.Context;
            DbSet = Context.Set<LogError>();
        }

        public void ExecuteSql(String sql)
        {
            Context.Database.ExecuteSqlCommand(sql);
        }

        public LogError Get(Guid Id)
        {
            return DbSet.Find(Id);
        }

        public IQueryable<LogError> GetAll()
        {
            return DbSet;
        }

        public LogError Add(LogError entity)
        {
            return DbSet.Add(entity);
        }

        public LogError Update(LogError entity)
        {
            entity = DbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public void Remove(Guid Id)
        {
            var entity = Get(Id);
            if (entity != null) Remove(entity);
        }

        public void Remove(LogError entity)
        {
            DbSet.Remove(entity);
        }

        public void Remove(IEnumerable<LogError> entities)
        {
            foreach (var entity in entities)
            {
                Remove(entity);
            }
        }

        public LogError Save(String message, Int64 userId = 0)
        {
            var entity = new LogError();

            entity.Id = Guid.NewGuid();
            entity.Date = DateTime.Now;
            entity.Text = message;
            entity.UserId = userId;

            entity = Add(entity);
            
            return entity;
        }

        #region protected methods
        
        protected String GetSimpleKey()
        {
            return Guid.NewGuid().ToString();
        }

        #endregion protected methods

        #region protected properties

        protected UnitOfWork<FitTrackerContext> UnitOfWork { get; private set; }
        protected FitTrackerContext Context { get; private set; }
        protected IDbSet<LogError> DbSet { get; private set; }

        #endregion protected properties
    }
}
