﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Log;
using FitTracker.Data.Repo.Abstract;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Log
{
    public interface ILogErrorRepo
    {
        void ExecuteSql(String sql);

        LogError Get(Guid Id);

        IQueryable<LogError> GetAll();

        LogError Add(LogError entity);

        LogError Update(LogError entity);

        void Remove(Guid Id);

        void Remove(LogError entity);

        void Remove(IEnumerable<LogError> entities);

        LogError Save(String message, Int64 userId = 0);
    }
}
