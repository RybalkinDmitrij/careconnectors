﻿using FitTracker.Data.Entities.Auth;
using FitTracker.Data.Helpers;
using FitTracker.Data.Repo.Abstract.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Repo.Auth.Impl
{
    internal class DbUserRepo : RepoBase<DbUser, FitTrackerContext>, IDbUserRepo
    {
        public DbUserRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public DbUser GetByLogin(String login)
        {
            return GetAll().FirstOrDefault(x => x.Login == login);
        }

        public DbUser GetByLoginAndPassword(String login, String password)
        {
            String passwordHash = Crypto.DoMd5Hash(password);
            return GetAll().FirstOrDefault(x => x.Login == login && x.PasswordHash == passwordHash);
        }
    }
}
