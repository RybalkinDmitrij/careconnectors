﻿using FitTracker.Data.Entities.Auth;
using FitTracker.Data.Repo.Abstract.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Repo.Auth.Impl
{
    internal class ApplicantRepo : RepoBase<Applicant, FitTrackerContext>, IApplicantRepo
    {
        public ApplicantRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public IQueryable<Applicant> GetByUserId(Int64 userId)
        {
            return GetAll().Where(x => x.UserId == userId);
        }

        public Applicant GetByKeyAndSecret(String consumerKey, String consumerSecret)
        {
            return GetAll().SingleOrDefault(x => x.ConsumerKey == consumerKey && x.ConsumerSecret == consumerSecret);
        }
    }
}
