﻿using FitTracker.Data.Entities.Auth;
using FitTracker.Data.Repo.Abstract.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Repo.Auth.Impl
{
    internal class ClientRepo : RepoBase<Client, FitTrackerContext>, IClientRepo
    {
        public ClientRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<Client> GetByUserId(Int64 userId)
        {
            return GetAll().Where(x => x.Applicant.UserId == userId).ToList();
        }

        public Client GetByAspNetUserName(String userName)
        {
            return GetAll().SingleOrDefault(x => x.AspNetUserName == userName);
        }

        public Client GetByExternalId(String externalId, Int64 applicantId)
        {
            return GetAll().SingleOrDefault(x => x.ExternalId == externalId && x.ApplicantId == applicantId);
        }

        public Client GetByAccessToken(String accessToken)
        {
            return GetAll().SingleOrDefault(x => x.AccessToken == accessToken);
        }

        public Client GetByFitbitId(String fitbitId)
        {
            return GetAll().FirstOrDefault(x => x.FitbitId == fitbitId);
        }

        public Client GetByWithingsId(String withingsId)
        {
            return GetAll().FirstOrDefault(x => x.WithingsId == withingsId);
        }

        public Client GetByJawboneId(String jawboneId)
        {
            return GetAll().FirstOrDefault(x => x.JawboneId == jawboneId);
        }

        public Client GetByMisfitId(String misfitId)
        {
            return GetAll().FirstOrDefault(x => x.MisfitId == misfitId);
        }

        public Client GetByRunkeeperId(String runkeeperId)
        {
            return GetAll().FirstOrDefault(x => x.RunkeeperId == runkeeperId);
        }

        public Client GetByStravaId(String stravaId)
        {
            return GetAll().FirstOrDefault(x => x.StravaId == stravaId);
        }

        public void ClearFitbit(Int64 userId)
        {
            var client = Get(userId);

            if (client == null) throw new Exception("Client must be not null");

            client.FitbitId = String.Empty;
            client.FitbitAccessToken = String.Empty;
            client.FitbitSecret = String.Empty;

            Commit();
        }

        public void ClearWithings(Int64 userId)
        {
            var client = Get(userId);

            if (client == null) throw new Exception("Client must be not null");

            client.WithingsId = String.Empty;
            client.WithingsAccessToken = String.Empty;
            client.WithingsSecret = String.Empty;

            Commit();
        }

        public void ClearJawbone(Int64 userId)
        {
            var client = Get(userId);

            if (client == null) throw new Exception("Client must be not null");

            client.JawboneId = String.Empty;
            client.JawboneAccessToken = String.Empty;
            client.JawboneRefreshToken = String.Empty;

            Commit();
        }

        public void ClearRunkeeper(Int64 userId)
        {
            var client = Get(userId);

            if (client == null) throw new Exception("Client must be not null");

            client.RunkeeperId = String.Empty;
            client.RunkeeperAccessToken = String.Empty;
            client.RunkeeperRefreshToken = String.Empty;

            Commit();
        }

        public void ClearStrava(Int64 userId)
        {
            var client = Get(userId);

            if (client == null) throw new Exception("Client must be not null");

            client.StravaId = String.Empty;
            client.StravaAccessToken = String.Empty;
            client.StravaRefreshToken = String.Empty;

            Commit();
        }

        public void ClearMisfit(Int64 userId)
        {
            var client = Get(userId);

            if (client == null) throw new Exception("Client must be not null");

            client.MisfitId = String.Empty;
            client.MisfitAccessToken = String.Empty;
            client.MisfitRefreshToken = String.Empty;

            Commit();
        }
    }
}
