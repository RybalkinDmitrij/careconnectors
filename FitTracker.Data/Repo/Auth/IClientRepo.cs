﻿using System;
using System.Linq;

using FitTracker.Data.Repo.Abstract;
using FitTracker.Data.Entities.Auth;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Auth
{
    public interface IClientRepo : IRepoBase<Client>
    {
        List<Client> GetByUserId(Int64 userId);

        Client GetByAspNetUserName(String userName);

        Client GetByExternalId(String externalId, Int64 applicantId);

        Client GetByAccessToken(String accessToken);

        Client GetByFitbitId(String fitbitId);

        Client GetByWithingsId(String withingsId);

        Client GetByJawboneId(String jawboneId);

        Client GetByMisfitId(String jawboneId);

        Client GetByRunkeeperId(String runkeeperId);

        Client GetByStravaId(String stravaId);

        void ClearFitbit(Int64 userId);

        void ClearWithings(Int64 userId);

        void ClearJawbone(Int64 userId);

        void ClearRunkeeper(Int64 userId);

        void ClearStrava(Int64 userId);

        void ClearMisfit(Int64 userId);
    }
}
