﻿using System;
using System.Linq;

using FitTracker.Data.Repo.Abstract;
using FitTracker.Data.Entities.Auth;

namespace FitTracker.Data.Repo.Auth
{
    public interface IDbUserRepo : IRepoBase<DbUser>
    {
        DbUser GetByLogin(String login);

        DbUser GetByLoginAndPassword(String login, String password);
    }
}
