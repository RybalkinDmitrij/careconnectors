﻿using System;
using System.Linq;

using FitTracker.Data.Repo.Abstract;
using FitTracker.Data.Entities.Auth;

namespace FitTracker.Data.Repo.Auth
{
    public interface IApplicantRepo : IRepoBase<Applicant>
    {
        IQueryable<Applicant> GetByUserId(Int64 userId);

        Applicant GetByKeyAndSecret(String consumerKey, String consumerSecret);
    }
}
