﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Fitbit.Api.Models;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface IDeviceRepo : IRepoBase<Device>
    {
        List<Device> GetByUserId(String userId);

        void SaveDevices(String userId, List<ApiModels.Device> data);
    }
}
