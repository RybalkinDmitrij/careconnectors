﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Fitbit.Api.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface IBodyRepo : IRepoBase<Body>
    {
        List<Body> GetByUserId(String userId);

        Body GetByUserId(String userId, DateTime date);

        Body SaveBody(String userId, DateTime date, ApiModels.Body data);
    }
}
