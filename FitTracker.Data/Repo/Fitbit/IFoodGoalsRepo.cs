﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;

using FAM = Fitbit.Api.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface IFoodGoalsRepo : IRepoBase<FoodGoals>
    {
        List<FoodGoals> GetByUserId(String userId);

        FoodGoals GetByUserId(String userId, DateTime date);

        void SaveData(String userId, DateTime date, FAM.FoodGoals data);
    }
}
