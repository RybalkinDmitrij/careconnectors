﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface IWeightLogRepo : IRepoBase<WeightLog>
    {
        List<WeightLog> GetByUserId(String userId, DateTime startDate, DateTime endDate);
    }
}
