﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface IWaterRepo : IRepoBase<Water>
    {
    }
}
