﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Fitbit.Api.Models;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface IActivityDistanceRepo : IRepoBase<ActivityDistance>
    {
        List<ActivityDistance> GetByUserId(String userId);

        List<ActivityDistance> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        List<ActivityDistance> GetByActivitySummaryId(Int64 activitySummaryId);

        void SaveActivitiesDistance(Int64 activitySummaryId, List<ApiModels.ActivityDistance> data);
    }
}
