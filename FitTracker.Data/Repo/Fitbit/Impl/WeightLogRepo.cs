﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class WeightLogRepo : RepoBase<WeightLog, FitTrackerContext>, IWeightLogRepo
    {
        public WeightLogRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<WeightLog> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Date >= startDate && x.Date <= endDate).ToList();
        }
    }
}
