﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

using ApiModels = Fitbit.Api.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class ActivitySummaryRepo : RepoBase<ActivitySummary, FitTrackerContext>, IActivitySummaryRepo
    {
        public ActivitySummaryRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<ActivitySummary> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public ActivitySummary GetByUserId(String userId, DateTime date)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId && x.Date == date);
        }

        public List<ActivitySummary> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Date >= startDate && x.Date <= endDate).ToList();
        }

        public ActivitySummary SaveActivitySummary(String userId, DateTime date, ApiModels.ActivitySummary data)
        {
            ActivitySummary entity = GetByUserId(userId, date);

            if (entity == null) entity = new ActivitySummary();

            entity.UserId = userId;
            entity.Date = date;
            entity.ActivityCalories = data.ActivityCalories;
            entity.CaloriesBMR = data.CaloriesBMR;
            entity.Elevation = Convert.ToDecimal(data.Elevation);
            entity.Floors = data.Floors;
            entity.MarginalCalories = data.MarginalCalories;
            entity.VeryActiveMinutes = data.VeryActiveMinutes;
            entity.CaloriesOut = data.CaloriesOut;
            entity.FairlyActiveMinutes = Convert.ToDecimal(data.FairlyActiveMinutes);
            entity.LightlyActiveMinutes = Convert.ToDecimal(data.LightlyActiveMinutes);
            entity.SedentaryMinutes = Convert.ToDecimal(data.SedentaryMinutes);
            entity.Steps = data.Steps;

            entity = Save(entity);

            Commit();

            var repoActivityDistance = base.UnitOfWork.GetRepo<IActivityDistanceRepo>();

            repoActivityDistance.SaveActivitiesDistance(entity.PkID, data.Distances);

            Commit();

            return entity;
        }
    }
}
