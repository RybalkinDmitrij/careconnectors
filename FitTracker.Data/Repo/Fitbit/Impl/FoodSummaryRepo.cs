﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

using FAM = Fitbit.Api.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class FoodSummaryRepo : RepoBase<FoodSummary, FitTrackerContext>, IFoodSummaryRepo
    {
        public FoodSummaryRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public FoodSummary GetByUserId(String userId)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId);
        }

        public List<FoodSummary> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Date >= startDate && x.Date <= endDate).ToList();
        }

        public void SaveData(String userId, DateTime date, FAM.FoodSummary data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[fbt_FoodSummary] WHERE UserId = '{0}' AND Date = '{1}'", userId, date.ToString("MM/dd/yyyy")));

            var entity = new FoodSummary();

            entity.UserId = userId;
            entity.Date = date;
            entity.Calories = Convert.ToDecimal(data.Calories);
            entity.Carbs = Convert.ToDecimal(data.Carbs);
            entity.Fat = Convert.ToDecimal(data.Fat);
            entity.Fiber = Convert.ToDecimal(data.Fiber);
            entity.Protein = Convert.ToDecimal(data.Protein);
            entity.Sodium = Convert.ToDecimal(data.Sodium);
            entity.Water = Convert.ToDecimal(data.Water);

            Save(entity);

            Commit();
        }
    }
}
