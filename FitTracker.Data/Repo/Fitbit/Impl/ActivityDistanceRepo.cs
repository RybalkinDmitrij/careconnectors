﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

using ApiModels = Fitbit.Api.Models;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class ActivityDistanceRepo : RepoBase<ActivityDistance, FitTrackerContext>, IActivityDistanceRepo
    {
        public ActivityDistanceRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<ActivityDistance> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.ActivitySummary.UserId == userId).ToList();
        }

        public List<ActivityDistance> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.ActivitySummary.UserId == userId && x.ActivitySummary.Date >= startDate && x.ActivitySummary.Date <= endDate).ToList();
        }

        public List<ActivityDistance> GetByActivitySummaryId(Int64 activitySummaryId)
        {
            return GetAll().Where(x => x.ActivitySummaryId == activitySummaryId).ToList();
        }

        public void SaveActivitiesDistance(Int64 activitySummaryId, List<ApiModels.ActivityDistance> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[fbt_ActivityDistance] WHERE ActivitySummaryId = '{0}'", activitySummaryId));

            foreach (var item in data)
            {
                var entity = new ActivityDistance();

                entity.ActivitySummaryId = activitySummaryId;
                entity.Activity = !String.IsNullOrEmpty(item.Activity) ? item.Activity : String.Empty;
                entity.Distance = Convert.ToDecimal(item.Distance);

                Save(entity);
            }

            Commit();

            //var existData = GetByActivitySummaryId(activitySummaryId);

            //// for update
            //var dataUpdate = existData.Where(x => data.Select(t => t.Activity).Contains(x.Activity)).ToList();
            //foreach (var item in dataUpdate)
            //{
            //    var item2 = data.FirstOrDefault(x => x.Activity == item.Activity);

            //    var entity = item;

            //    entity.Distance = Convert.ToDecimal(item2.Distance);

            //    Save(entity);
            //}

            //// for add
            //var dataAdd = data.Where(x => !existData.Select(t => t.Activity).Contains(x.Activity)).ToList();
            //foreach (var item in dataAdd)
            //{
            //    var entity = new ActivityDistance();

            //    entity.Distance = Convert.ToDecimal(item.Distance);

            //    Save(entity);
            //}

            //// for remove
            //var dataRemove = existData.Where(x => !data.Select(t => t.Activity).Contains(x.Activity)).ToList();
            //foreach (var item in dataRemove)
            //{
            //    Remove(item);
            //}

            //Commit();
        }
    }
}
