﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

using ApiModels = Fitbit.Api.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class BodyGoalsRepo : RepoBase<BodyGoals, FitTrackerContext>, IBodyGoalsRepo
    {
        public BodyGoalsRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<BodyGoals> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public BodyGoals GetByUserId(String userId, DateTime date)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId && x.Date == date);
        }

        public BodyGoals SaveBodyGoals(String userId, DateTime date, ApiModels.BodyGoals data)
        {
            var entity = GetByUserId(userId, date);

            if (entity == null) entity = new BodyGoals();

            entity.UserId = userId;
            entity.Weight = Convert.ToDecimal(data.Weight);
            entity.Date = date;

            entity = Save(entity);

            Commit();

            return entity;
        }
    }
}
