﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

using ApiModels = Fitbit.Api.Models;
using FitTracker.Data.Repo.Log;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class ActivityLogRepo : RepoBase<ActivityLog, FitTrackerContext>, IActivityLogRepo
    {
        public ActivityLogRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<ActivityLog> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public List<ActivityLog> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Date >= startDate && x.Date <= endDate).ToList();
        }

        public void SaveActivitiesLog(String userId, DateTime date, List<ApiModels.ActivityLog> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[fbt_ActivityLog] WHERE UserId = '{0}' AND Date = '{1}'", userId, date.ToString("MM/dd/yyyy")));

            foreach (var item in data)
            {
                var entity = new ActivityLog();

                entity.UserId = userId;
                entity.Date = date;
                entity.ActivityId = item.ActivityId;
                entity.ActivityParentId = item.ActivityParentId;
                entity.Calories = item.Calories;
                entity.Description = !String.IsNullOrEmpty(item.Description) ? item.Description : String.Empty;
                entity.Distance = Convert.ToDecimal(item.Distance);
                entity.Duration = item.Duration;
                entity.HasStartTime = item.HasStartTime;
                entity.IsFavorite = item.IsFavorite;
                entity.LogId = item.LogId;
                entity.Name = !String.IsNullOrEmpty(item.Name) ? item.Name : String.Empty;
                entity.StartTime = !String.IsNullOrEmpty(item.StartTime) ? item.StartTime : String.Empty;
                entity.Steps = item.Steps;

                Save(entity);
            }

            Commit();
        }
    }
}
