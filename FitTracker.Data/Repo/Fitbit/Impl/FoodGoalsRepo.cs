﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

using FAM = Fitbit.Api.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class FoodGoalsRepo : RepoBase<FoodGoals, FitTrackerContext>, IFoodGoalsRepo
    {
        public FoodGoalsRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<FoodGoals> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public FoodGoals GetByUserId(String userId, DateTime date)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId && x.Date == date);
        }

        public void SaveData(String userId, DateTime date, FAM.FoodGoals data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[fbt_FoodGoals] WHERE UserId = '{0}' AND Date = '{1}'", userId, date.ToString("MM/dd/yyyy")));

            var entity = new FoodGoals();

            entity.UserId = userId;
            entity.Date = date;
            entity.Calories = data.Calories;

            Save(entity);

            Commit();
        }
    }
}
