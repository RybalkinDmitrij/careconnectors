﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

using FAM = Fitbit.Api.Models;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class NutritionalValuesRepo : RepoBase<NutritionalValues, FitTrackerContext>, INutritionalValuesRepo
    {
        public NutritionalValuesRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public NutritionalValues SaveData(FAM.NutritionalValues data)
        {
            if (data == null) return null;

            var entity = new NutritionalValues();

            entity.Calories = Convert.ToDecimal(data.Calories);
            entity.Carbs = Convert.ToDecimal(data.Carbs);
            entity.Fat = Convert.ToDecimal(data.Fat);
            entity.Fiber = Convert.ToDecimal(data.Fiber);
            entity.Protein = Convert.ToDecimal(data.Protein);
            entity.Sodium = Convert.ToDecimal(data.Sodium);

            Save(entity);

            Commit();

            return entity;
        }
    }
}
