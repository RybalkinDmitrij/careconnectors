﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;
using System.Collections.Generic;

using ApiModels = Fitbit.Api.Models;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class SleepLogRepo : RepoBase<SleepLog, FitTrackerContext>, ISleepLogRepo
    {
        public SleepLogRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<SleepLog> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public List<SleepLog> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Date >= startDate && x.Date <= endDate).ToList();
        }

        public void SaveData(String userId, DateTime date, List<ApiModels.SleepLog> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[fbt_SleepLog] WHERE UserId = '{0}' AND Date = '{1}'", userId, date.ToString("MM/dd/yyyy")));

            foreach (var item in data)
            {
                var entity = new SleepLog();

                entity.UserId = userId;
                entity.Date = date;
                entity.AwakeningsCount = item.AwakeningsCount;
                entity.Duration = item.Duration;
                entity.Efficiency = item.Efficiency;
                entity.IsMainSleep = item.IsMainSleep;
                entity.LogId = item.LogId;
                entity.MinutesAfterWakeup = item.MinutesAfterWakeup;
                entity.MinutesAsleep = item.MinutesAsleep;
                entity.MinutesAwake = item.MinutesAwake;
                entity.MinutesToFallAsleep = item.MinutesToFallAsleep;
                entity.StartTime = item.StartTime;
                entity.TimeInBed = item.TimeInBed;
                entity.AwakeCount = item.AwakeCount;
                entity.AwakeDuration = item.AwakeDuration;
                entity.RestlessCount = item.RestlessCount;
                entity.RestlessDuration = item.RestlessDuration;

                Save(entity);
            }

            Commit();
        }
    }
}
