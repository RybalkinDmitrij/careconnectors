﻿using System;
using System.Linq;
using System.Data.Entity;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

using FAM = Fitbit.Api.Models;
using System.Collections.Generic;
using FitTracker.Data.Helpers;
using System.Web.Script.Serialization;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class FoodLogRepo : RepoBase<FoodLog, FitTrackerContext>, IFoodLogRepo
    {
        public FoodLogRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<FoodLog> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId)
                .Include(x => x.LoggedFood)
                .Include(x => x.LoggedFood.Unit)
                .Include(x => x.NutritionalValues)
                .ToList();
        }

        public List<FoodLog> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId &&
                                       x.LogDate >= startDate &&
                                       x.LogDate <= endDate)
                .Include(x => x.LoggedFood)
                .Include(x => x.LoggedFood.Unit)
                .Include(x => x.NutritionalValues)
                .ToList();
        }

        public void SaveData(String userId, DateTime date, List<FAM.FoodLog> data)
        {
            var repoLoggedFood = UnitOfWork.GetRepo<ILoggedFoodRepo>();
            var repoNutritionalValues = UnitOfWork.GetRepo<INutritionalValuesRepo>();

            ExecuteSql(String.Format(@"UPDATE [dbo].[fbt_NutritionalValues]
                                        SET IsDeleted = 1,
	                                        DateDelete = GETDATE()
                                        WHERE PkID in ( 
                                            SELECT nv.PkID FROM [dbo].[fbt_NutritionalValues] nv 
                                                INNER JOIN [dbo].[fbt_FoodLog] fl ON nv.PkID = fl.NutritionalValuesId
                                                WHERE fl.UserId = '{0}' AND fl.LogDate = '{1}'
                                        )", userId, date.ToString("MM/dd/yyyy")));

            ExecuteSql(String.Format(@"UPDATE [dbo].[fbt_FoodLogUnit]
                                        SET IsDeleted = 1,
	                                        DateDelete = GETDATE()
                                        WHERE PkID in ( 
                                            SELECT flu.PkID FROM [dbo].[fbt_FoodLogUnit] flu 
                                                INNER JOIN [dbo].[fbt_LoggedFood] lf ON flu.PkID = lf.UnitId
                                                INNER JOIN [dbo].[fbt_FoodLog] fl ON lf.PkID = fl.LoggedFoodId
                                                WHERE fl.UserId = '{0}' AND fl.LogDate = '{1}'
                                        )", userId, date.ToString("MM/dd/yyyy")));

            ExecuteSql(String.Format(@"UPDATE [dbo].[fbt_LoggedFood]
                                        SET IsDeleted = 1,
	                                        DateDelete = GETDATE()
                                        WHERE PkID in ( 
                                            SELECT lf.PkID FROM [dbo].[fbt_LoggedFood] lf
                                               INNER JOIN [dbo].[fbt_FoodLog] fl ON lf.PkID = fl.LoggedFoodId
                                               WHERE fl.UserId = '{0}' AND fl.LogDate = '{1}'
                                        )", userId, date.ToString("MM/dd/yyyy")));

            ExecuteSql(String.Format(@"UPDATE [dbo].[fbt_FoodLog]
                                        SET IsDeleted = 1,
	                                        DateDelete = GETDATE()
                                        WHERE PkID in ( 
                                            SELECT PkID FROM [dbo].[fbt_FoodLog] WHERE UserId = '{0}' AND LogDate = '{1}'
                                        )", userId, date.ToString("MM/dd/yyyy")));

            ExecuteSql(String.Format(@"DELETE [dbo].[fbt_FoodLog] WHERE IsDeleted = 1"));
            ExecuteSql(String.Format(@"DELETE [dbo].[fbt_NutritionalValues] WHERE IsDeleted = 1"));
            ExecuteSql(String.Format(@"DELETE [dbo].[fbt_LoggedFood] WHERE IsDeleted = 1"));
            ExecuteSql(String.Format(@"DELETE [dbo].[fbt_FoodLogUnit] WHERE IsDeleted = 1"));

            foreach (var item in data)
            {
                var entityLoggedFood = repoLoggedFood.SaveData(item.LoggedFood);
                
                var entityNutritionalValues = repoNutritionalValues.SaveData(item.NutritionalValues);

                var entity = new FoodLog();

                entity.UserId = userId;
                entity.IsFavorite = item.IsFavorite;
                entity.LogDate = item.LogDate;
                entity.LogId = item.LogId;
                entity.LoggedFoodId = entityLoggedFood != null ? (Int64?)entityLoggedFood.PkID : null;
                entity.NutritionalValuesId = entityNutritionalValues != null ? (Int64?)entityNutritionalValues.PkID : null;

                Save(entity);

                Commit();
            }
        }
    }
}
