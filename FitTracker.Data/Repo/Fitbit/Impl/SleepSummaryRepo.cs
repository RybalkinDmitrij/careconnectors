﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

using ApiModels = Fitbit.Api.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class SleepSummaryRepo : RepoBase<SleepSummaryF, FitTrackerContext>, ISleepSummaryRepo
    {
        public SleepSummaryRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<SleepSummaryF> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public SleepSummaryF GetByUserId(String userId, DateTime date)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId && x.Date == date);
        }

        public List<SleepSummaryF> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Date >= startDate && x.Date <= endDate).ToList();
        }

        public SleepSummaryF SaveData(String userId, DateTime date, ApiModels.SleepSummary data)
        {
            SleepSummaryF entity = GetByUserId(userId, date);

            if (entity == null) entity = new SleepSummaryF();

            entity.UserId = userId;
            entity.Date = date;
            entity.TotalSleepRecords = data.TotalSleepRecords;
            entity.TotalMinutesAsleep = data.TotalMinutesAsleep;
            entity.TotalTimeInBed = data.TotalTimeInBed;

            entity = Save(entity);

            Commit();

            return entity;
        }
    }
}
