﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

using FAM = Fitbit.Api.Models;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class FoodLogUnitRepo : RepoBase<FoodLogUnit, FitTrackerContext>, IFoodLogUnitRepo
    {
        public FoodLogUnitRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public FoodLogUnit SaveData(FAM.FoodLogUnit data)
        {
            if (data == null) return null;

            var entity = new FoodLogUnit();

            entity.Id = data.Id;
            entity.Name = data.Name;
            entity.Plural = data.Plural;

            Save(entity);

            Commit();

            return entity;
        }
    }
}
