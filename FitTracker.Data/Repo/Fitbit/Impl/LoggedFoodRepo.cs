﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

using FAM = Fitbit.Api.Models;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class LoggedFoodRepo : RepoBase<LoggedFood, FitTrackerContext>, ILoggedFoodRepo
    {
        public LoggedFoodRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public LoggedFood SaveData(FAM.LoggedFood data)
        {
            var repoLogUnit = UnitOfWork.GetRepo<IFoodLogUnitRepo>();

            var unit = repoLogUnit.SaveData(data.Unit);

            var entity = new LoggedFood();

            entity.AccessLevel = !String.IsNullOrEmpty(data.AccessLevel) ? data.AccessLevel : String.Empty;
            entity.Amount = Convert.ToDecimal(data.Amount);
            entity.Brand = !String.IsNullOrEmpty(data.Brand) ? data.Brand : String.Empty;
            entity.Calories = Convert.ToDecimal(data.Calories);
            entity.FoodId = data.FoodId;
            entity.MealTypeId = data.MealTypeId;
            entity.Locale = !String.IsNullOrEmpty(data.Locale) ? data.Locale : String.Empty;
            entity.Name = !String.IsNullOrEmpty(data.Name) ? data.Name : String.Empty;
            entity.UnitId = unit != null ? (Int64?)unit.PkID : null;

            Save(entity);

            Commit();

            return entity;
        }
    }
}
