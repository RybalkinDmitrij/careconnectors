﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class UserProfileRepo : RepoBase<UserProfile, FitTrackerContext>, IUserProfileRepo
    {
        public UserProfileRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods
        
        #endregion override methods
    }
}
