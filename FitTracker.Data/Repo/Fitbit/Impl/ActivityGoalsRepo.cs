﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

using ApiModels = Fitbit.Api.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class ActivityGoalsRepo : RepoBase<ActivityGoals, FitTrackerContext>, IActivityGoalsRepo
    {
        public ActivityGoalsRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<ActivityGoals> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public ActivityGoals GetByUserId(String userId, DateTime date)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId && x.Date == date);
        }

        public ActivityGoals SaveActivityGoals(String userId, DateTime date, ApiModels.ActivityGoals data)
        {
            ActivityGoals entity = GetByUserId(userId, date);

            if (entity == null) entity = new ActivityGoals();

            entity.UserId = userId;
            entity.Date = date;
            entity.CaloriesOut = data.CaloriesOut;
            entity.Distance = data.Distance;
            entity.Floors = data.Floors;
            entity.Steps = data.Steps;

            entity = Save(entity);

            Commit();

            return entity;
        }
    }
}
