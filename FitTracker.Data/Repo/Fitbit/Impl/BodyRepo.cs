﻿using System;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

using ApiModels = Fitbit.Api.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class BodyRepo : RepoBase<Body, FitTrackerContext>, IBodyRepo
    {
        public BodyRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<Body> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public Body GetByUserId(String userId, DateTime date)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId && x.Date == date);
        }

        public Body SaveBody(String userId, DateTime date, ApiModels.Body data)
        {
            var entity = GetByUserId(userId, date);

            if (entity == null) entity = new Body();

            entity.UserId = userId;
            entity.Date = date;
            entity.Bicep = Convert.ToDecimal(data.Bicep);
            entity.BMI = Convert.ToDecimal(data.BMI);
            entity.Calf = Convert.ToDecimal(data.Calf);
            entity.Chest = Convert.ToDecimal(data.Chest);
            entity.Fat = Convert.ToDecimal(data.Fat);
            entity.Forearm = Convert.ToDecimal(data.Forearm);
            entity.Hips = Convert.ToDecimal(data.Hips);
            entity.Neck = Convert.ToDecimal(data.Neck);
            entity.Thigh = Convert.ToDecimal(data.Thigh);
            entity.Waist = Convert.ToDecimal(data.Waist);
            entity.Weight = Convert.ToDecimal(data.Weight);

            entity = Save(entity);

            Commit();

            return entity;
        }
    }
}
