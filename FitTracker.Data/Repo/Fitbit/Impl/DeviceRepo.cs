﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Fitbit;

using ApiModels = Fitbit.Api.Models;
using FitTracker.Data.Enums.Fitbit;

namespace FitTracker.Data.Repo.Fitbit.Impl
{
    internal class DeviceRepo : RepoBase<Device, FitTrackerContext>, IDeviceRepo
    {
        public DeviceRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<Device> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public void SaveDevices(String userId, List<ApiModels.Device> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[fbt_Device] WHERE UserId = '{0}'", userId));

            if (data == null) return;

            foreach (var item in data)
            {
                var entity = new Device();

                entity.UserId = userId;
                entity.Id = !String.IsNullOrEmpty(item.Id) ? item.Id : String.Empty;
                entity.Battery = !String.IsNullOrEmpty(item.Battery) ? item.Battery : String.Empty;
                entity.DeviceVersion = !String.IsNullOrEmpty(item.DeviceVersion) ? item.DeviceVersion : String.Empty;
                entity.LastSyncTime = item.LastSyncTime;
                entity.Type = (DeviceType)item.Type;
                
                Save(entity);
            }

            Commit();
        }
    }
}
