﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Fitbit.Api.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface IBodyGoalsRepo : IRepoBase<BodyGoals>
    {
        List<BodyGoals> GetByUserId(String userId);

        BodyGoals GetByUserId(String userId, DateTime date);

        BodyGoals SaveBodyGoals(String userId, DateTime date, ApiModels.BodyGoals data);
    }
}
