﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;

using FAM = Fitbit.Api.Models;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface ILoggedFoodRepo : IRepoBase<LoggedFood>
    {
        LoggedFood SaveData(FAM.LoggedFood data);
    }
}
