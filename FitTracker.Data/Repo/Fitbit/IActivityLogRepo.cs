﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Fitbit.Api.Models;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface IActivityLogRepo : IRepoBase<ActivityLog>
    {
        List<ActivityLog> GetByUserId(String userId);

        List<ActivityLog> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        void SaveActivitiesLog(String userId, DateTime date, List<ApiModels.ActivityLog> data);
    }
}
