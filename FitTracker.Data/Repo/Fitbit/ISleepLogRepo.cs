﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;
using System.Collections.Generic;

using ApiModels = Fitbit.Api.Models;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface ISleepLogRepo : IRepoBase<SleepLog>
    {
        List<SleepLog> GetByUserId(String userId);

        List<SleepLog> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        void SaveData(String userId, DateTime date, List<ApiModels.SleepLog> data);
    }
}
