﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;

using FAM = Fitbit.Api.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface IFoodSummaryRepo : IRepoBase<FoodSummary>
    {
        FoodSummary GetByUserId(String userId);

        List<FoodSummary> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        void SaveData(String userId, DateTime date, FAM.FoodSummary data);
    }
}
