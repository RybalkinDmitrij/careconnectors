﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Fitbit.Api.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface IActivitySummaryRepo : IRepoBase<ActivitySummary>
    {
        List<ActivitySummary> GetByUserId(String userId);

        ActivitySummary GetByUserId(String userId, DateTime date);

        List<ActivitySummary> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        ActivitySummary SaveActivitySummary(String userId, DateTime date, ApiModels.ActivitySummary data);
    }
}
