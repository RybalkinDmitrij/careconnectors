﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;

using FAM = Fitbit.Api.Models;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface INutritionalValuesRepo : IRepoBase<NutritionalValues>
    {
        NutritionalValues SaveData(FAM.NutritionalValues data);
    }
}
