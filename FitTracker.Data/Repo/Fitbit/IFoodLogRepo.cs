﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;
using System.Collections.Generic;

using FAM = Fitbit.Api.Models;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface IFoodLogRepo : IRepoBase<FoodLog>
    {
        List<FoodLog> GetByUserId(String userId);

        List<FoodLog> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        void SaveData(String userId, DateTime date, List<FAM.FoodLog> data);
    }
}
