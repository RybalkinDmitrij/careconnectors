﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;
using System.Collections.Generic;

using ApiModels = Fitbit.Api.Models;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface ISleepSummaryRepo : IRepoBase<SleepSummaryF>
    {
        List<SleepSummaryF> GetByUserId(String userId);

        SleepSummaryF GetByUserId(String userId, DateTime date);

        List<SleepSummaryF> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        SleepSummaryF SaveData(String userId, DateTime date, ApiModels.SleepSummary data);
    }
}
