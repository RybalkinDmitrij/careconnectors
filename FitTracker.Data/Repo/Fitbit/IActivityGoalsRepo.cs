﻿using System;
using System.Linq;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Repo.Abstract;

using ApiModels = Fitbit.Api.Models;
using System.Collections.Generic;

namespace FitTracker.Data.Repo.Fitbit
{
    public interface IActivityGoalsRepo : IRepoBase<ActivityGoals>
    {
        List<ActivityGoals> GetByUserId(String userId);

        ActivityGoals GetByUserId(String userId, DateTime date);

        ActivityGoals SaveActivityGoals(String userId, DateTime date, ApiModels.ActivityGoals data);
    }
}
