﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Misfit;
using FitTracker.Data.Repo.Abstract;
using ApiModels = Misfit.Api.Models;

namespace FitTracker.Data.Repo.Misfit
{
    public interface ISessionRepo : IRepoBase<Session>
    {
        /// <summary>
        /// List of Session received by UserId
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        List<Session> GetByUserId(String userId);

        /// <summary>
        /// Session received by Id
        /// </summary>
        /// <param name="id">Session ID</param>
        Session GetById(String id);

        /// <summary>
        /// Save Session
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="data">Session from Misfit</param>
        void SaveSession(String userId, ApiModels.Sessions data);
    }
}
