﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Misfit;
using FitTracker.Data.Repo.Abstract;
using ApiModels = Misfit.Api.Models;

namespace FitTracker.Data.Repo.Misfit
{
    public interface IDeviceRepo : IRepoBase<DeviceMisfit>
    {
        /// <summary>
        /// List of Devices received by UserId
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        List<DeviceMisfit> GetByUserId(String userId);

        /// <summary>
        /// Device received by Id
        /// </summary>
        /// <param name="id">Device ID</param>
        DeviceMisfit GetById(String id);

        /// <summary>
        /// Save Device
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="data">Device from Misfit</param>
        void SaveDevice(String userId, ApiModels.Device data);
    }
}
