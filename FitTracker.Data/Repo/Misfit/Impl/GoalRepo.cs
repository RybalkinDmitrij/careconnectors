﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Misfit;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Misfit;
using ApiModels = Misfit.Api.Models;

namespace FitTracker.Data.Repo.Misfit.Impl
{
    internal class GoalRepo : RepoBase<Goal, FitTrackerContext>, IGoalRepo
    {
        public GoalRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Devices received by UserId
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        public List<Goal> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.ProfileInfo.UserId == userId).ToList();
        }

        /// <summary>
        /// Device received by Id
        /// </summary>
        /// <param name="Id">Device ID</param>
        public Goal GetById(String id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Save Device
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="data">Device from Misfit</param>
        public void SaveGoal(String userId, ApiModels.Goals data)
        {
            var repoProfile = base.UnitOfWork.GetRepo<IProfileInfoRepo>();
            var profile = repoProfile.GetByUserId(userId);
            for (int i = 0; i < data.goals.Count; i++)
            {
                Goal entity = GetById(data.goals[i].id);

                if (entity == null) entity = new Goal();

                entity.Date = data.goals[i].date;
                entity.Id = data.goals[i].id;
                entity.Points = data.goals[i].points;
                entity.ProfileInfoId = profile.PkID;
                entity.TargetPoints = data.goals[i].targetPoints;

                entity = Save(entity);
            }

            Commit();
        }
    }
}
