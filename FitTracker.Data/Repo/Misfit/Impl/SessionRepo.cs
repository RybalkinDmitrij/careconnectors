﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Misfit;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Misfit;
using ApiModels = Misfit.Api.Models;

namespace FitTracker.Data.Repo.Misfit.Impl
{
    internal class SessionRepo : RepoBase<Session, FitTrackerContext>, ISessionRepo
    {
        public SessionRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Session received by UserId
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        public List<Session> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.ProfileInfo.UserId == userId).ToList();
        }

        /// <summary>
        /// Session received by Id
        /// </summary>
        /// <param name="Id">Session ID</param>
        public Session GetById(String id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Save Session
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="data">Session from Misfit</param>
        public void SaveSession(String userId, ApiModels.Sessions data)
        {
            var repoProfile = base.UnitOfWork.GetRepo<IProfileInfoRepo>();
            var profile = repoProfile.GetByUserId(userId);
            for (int i = 0; i < data.sessions.Count; i++)
            {
                Session entity = GetById(data.sessions[i].id);

                if (entity == null) entity = new Session();

                entity.ActivityType = data.sessions[i].activityType;
                entity.Calories = data.sessions[i].calories;
                entity.Distance = data.sessions[i].distance;
                entity.Duration = data.sessions[i].duration;
                entity.Id = data.sessions[i].id;
                entity.Points = data.sessions[i].points;
                entity.StartTime = data.sessions[i].startTime;
                entity.ProfileInfoId = profile.PkID;
                entity.Steps = data.sessions[i].steps;

                entity = Save(entity);
            }

            Commit();
        }
    }
}
