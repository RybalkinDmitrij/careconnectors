﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Misfit;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Misfit;
using ApiModels = Misfit.Api.Models;

namespace FitTracker.Data.Repo.Misfit.Impl
{
    internal class SleepDetailRepo : RepoBase<SleepDetail, FitTrackerContext>, ISleepDetailRepo
    {
        public SleepDetailRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Sleep Detail received by UserId
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        public List<SleepDetail> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.Sleep.ProfileInfo.UserId == userId).ToList();
        }

        /// <summary>
        /// List of Sleep Detail received by Sleep Id
        /// </summary>
        /// <param name="id">Sleep ID</param>
        public List<SleepDetail> GetBySleepId(String id)
        {
            return GetAll().Where(x => x.Sleep.Id == id).ToList();
        }

        /// <summary>
        /// Save Sleep Detail
        /// </summary>
        /// <param name="sleepId">Sleep Id</param>
        /// <param name="data">Sleep from Misfit</param>
        public void SaveSleepDetail(Int64 sleepId, List<ApiModels.SleepDetails> data)
        {
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[mft_SleepDetail] WHERE SleepId = '{0}'", sleepId));

            foreach (var item in data)
            {
                var entity = new SleepDetail();

                entity.SleepId = sleepId;
                entity.Datetime = item.datetime;
                entity.Value = item.value;

                Save(entity);
            }

            Commit();
            
        }
    }
}
