﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Misfit;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Misfit;
using ApiModels = Misfit.Api.Models;

namespace FitTracker.Data.Repo.Misfit.Impl
{
    internal class ProfileInfoRepo : RepoBase<ProfileInfo, FitTrackerContext>, IProfileInfoRepo
    {
        public ProfileInfoRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// Profile received by UserId
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        public ProfileInfo GetByUserId(String userId)
        {
            return GetAll().FirstOrDefault(x => x.UserId == userId);
        }

        /// <summary>
        /// Save Profile
        /// </summary>
        /// <param name="data">Data from Misfit app</param>
        public ProfileInfo SaveProfile(ApiModels.ProfileInfo data)
        {
            ProfileInfo entity = GetByUserId(data.userId);

            if (entity == null) entity = new ProfileInfo();

            entity.UserId = data.userId;
            entity.Birthday = data.birthday;
            entity.Email = (data.email != null) ? data.email : "None";
            entity.Gender = (data.gender != null) ? data.gender : "";
            entity.Name = (data.name != null) ? data.name : "None";

            entity = Save(entity);

            Commit();

            return entity;
        }
    }
}
