﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Misfit;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Misfit;
using ApiModels = Misfit.Api.Models;

namespace FitTracker.Data.Repo.Misfit.Impl
{
    internal class DeviceRepo : RepoBase<DeviceMisfit, FitTrackerContext>, IDeviceRepo
    {
        public DeviceRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Devices received by UserId
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        public List<DeviceMisfit> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.ProfileInfo.UserId == userId).ToList();
        }

        /// <summary>
        /// Device received by Id
        /// </summary>
        /// <param name="Id">Device ID</param>
        public DeviceMisfit GetById(String id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Save Device
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="data">Device from Misfit</param>
        public void SaveDevice(String userId, ApiModels.Device data)
        {
            var repoProfile = base.UnitOfWork.GetRepo<IProfileInfoRepo>();
            var profile = repoProfile.GetByUserId(userId);

            DeviceMisfit entity = GetById(data.id);

            if (entity == null) entity = new DeviceMisfit();

            entity.BatteryLevel = data.batteryLevel;
            entity.DeviceType = data.deviceType;
            entity.FirmwareVersion = data.firmwareVersion;
            entity.Id = data.id;
            entity.LastSyncTime = data.lastSyncTime;
            entity.ProfileInfoId = profile.PkID;
            entity.SerialNumber = data.serialNumber;

            entity = Save(entity);

            Commit();
        }
    }
}
