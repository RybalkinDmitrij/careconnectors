﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Misfit;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Misfit;
using ApiModels = Misfit.Api.Models;

namespace FitTracker.Data.Repo.Misfit.Impl
{
    internal class SummaryRepo : RepoBase<Summary, FitTrackerContext>, ISummaryRepo
    {
        public SummaryRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Summaries received by UserId
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        public List<Summary> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.ProfileInfo.UserId == userId).ToList();
        }

        /// <summary>
        /// Summary received by Id
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        /// <param name="date">Date of summary</param>
        public Summary GetByUserId(String userId, DateTime date)
        {
            return GetAll().FirstOrDefault(x => x.ProfileInfo.UserId == userId && x.Date == date);
        }

        /// <summary>
        /// Save Summary
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="data">Session from Misfit</param>
        public void SaveSummary(String userId, ApiModels.Summaries data)
        {
            var repoProfile = base.UnitOfWork.GetRepo<IProfileInfoRepo>();
            var profile = repoProfile.GetByUserId(userId);
            for (int i = 0; i < data.summary.Count; i++)
            {
                Summary entity = GetByUserId(userId, data.summary[i].date);

                if (entity == null) entity = new Summary();

                entity.ActivityCalories = data.summary[i].activityCalories;
                entity.Calories = data.summary[i].calories;
                entity.Date = data.summary[i].date;
                entity.Distance = data.summary[i].distance;
                entity.Points = data.summary[i].points;
                entity.ProfileInfoId = profile.PkID;
                entity.Steps = data.summary[i].steps;

                entity = Save(entity);
            }

            Commit();
        }
    }
}
