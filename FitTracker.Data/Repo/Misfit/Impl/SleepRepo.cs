﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Misfit;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Misfit;
using ApiModels = Misfit.Api.Models;

namespace FitTracker.Data.Repo.Misfit.Impl
{
    internal class SleepRepo : RepoBase<SleepMisfit, FitTrackerContext>, ISleepRepo
    {
        public SleepRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// List of Sleeps received by UserId
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        public List<SleepMisfit> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.ProfileInfo.UserId == userId).ToList();
        }

        /// <summary>
        /// Sleep received by Id
        /// </summary>
        /// <param name="Id">Sleep ID</param>
        public SleepMisfit GetById(String id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Save Sleep
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="data">Sleep from Misfit</param>
        public void SaveSleep(String userId, ApiModels.Sleeps data)
        {
            var repoProfile = base.UnitOfWork.GetRepo<IProfileInfoRepo>();
            var profile = repoProfile.GetByUserId(userId);
            for (int i = 0; i < data.sleeps.Count; i++)
            {
                SleepMisfit entity = GetById(data.sleeps[i].id);

                if (entity == null) entity = new SleepMisfit();

                entity.AutoDetected = data.sleeps[i].autoDetected;
                entity.Duration = data.sleeps[i].duration;
                entity.Id = data.sleeps[i].id;
                entity.StartTime = data.sleeps[i].startTime;
                entity.ProfileInfoId = profile.PkID;

                entity = Save(entity);

                var repoSleepDetail = base.UnitOfWork.GetRepo<ISleepDetailRepo>();
                repoSleepDetail.SaveSleepDetail(entity.PkID, data.sleeps[i].sleepDetails);
            }

            Commit();
        }
    }
}
