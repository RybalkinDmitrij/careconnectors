﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Misfit;
using FitTracker.Data.Repo.Abstract;
using ApiModels = Misfit.Api.Models;

namespace FitTracker.Data.Repo.Misfit
{
    public interface ISleepRepo : IRepoBase<SleepMisfit>
    {
        /// <summary>
        /// List of Sleeps received by UserId
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        List<SleepMisfit> GetByUserId(String userId);

        /// <summary>
        /// Sleep received by Id
        /// </summary>
        /// <param name="id">Sleep ID</param>
        SleepMisfit GetById(String id);

        /// <summary>
        /// Save Sleep
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="data">Sleep from Misfit</param>
        void SaveSleep(String userId, ApiModels.Sleeps data);
    }
}
