﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Misfit;
using FitTracker.Data.Repo.Abstract;
using ApiModels = Misfit.Api.Models;

namespace FitTracker.Data.Repo.Misfit
{
    public interface ISleepDetailRepo : IRepoBase<SleepDetail>
    {
        /// <summary>
        /// List of Sleep Detail received by UserId
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        List<SleepDetail> GetByUserId(String userId);

        /// <summary>
        /// List of Sleep Detail received by Sleep Id
        /// </summary>
        /// <param name="id">Sleep ID</param>
        List<SleepDetail> GetBySleepId(String id);

        /// <summary>
        /// Save Sleep Detail
        /// </summary>
        /// <param name="sleepId">Sleep Id</param>
        /// <param name="data">Sleep from Misfit</param>
        void SaveSleepDetail(Int64 sleepId, List<ApiModels.SleepDetails> data);
    }
}
