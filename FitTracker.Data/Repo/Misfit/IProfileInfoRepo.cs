﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Misfit;
using FitTracker.Data.Repo.Abstract;
using ApiModels = Misfit.Api.Models;

namespace FitTracker.Data.Repo.Misfit
{
    public interface IProfileInfoRepo : IRepoBase<ProfileInfo>
    {
        /// <summary>
        /// Profile received by UserId
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        ProfileInfo GetByUserId(String userId);

        /// <summary>
        /// Save Profile
        /// </summary>
        /// <param name="data">Data from Misfit app</param>
        ProfileInfo SaveProfile(ApiModels.ProfileInfo data);
    }
}
