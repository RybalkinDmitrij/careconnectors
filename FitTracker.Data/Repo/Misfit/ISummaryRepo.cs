﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Misfit;
using FitTracker.Data.Repo.Abstract;
using ApiModels = Misfit.Api.Models;

namespace FitTracker.Data.Repo.Misfit
{
    public interface ISummaryRepo : IRepoBase<Summary>
    {
        /// <summary>
        /// List of Summaries received by UserId
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        List<Summary> GetByUserId(String userId);

        /// <summary>
        /// Summary received by Id
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        /// <param name="date">Date of summary</param>
        Summary GetByUserId(String userId, DateTime date);

        /// <summary>
        /// Save Summary
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="data">Summary from Misfit</param>
        void SaveSummary(String userId, ApiModels.Summaries data);
    }
}
