﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Misfit;
using FitTracker.Data.Repo.Abstract;
using ApiModels = Misfit.Api.Models;

namespace FitTracker.Data.Repo.Misfit
{
    public interface IGoalRepo : IRepoBase<Goal>
    {
        /// <summary>
        /// List of Devices received by UserId
        /// </summary>
        /// <param name="userId">Misfit UserID</param>
        List<Goal> GetByUserId(String userId);

        /// <summary>
        /// Goal received by Id
        /// </summary>
        /// <param name="id">Goal ID</param>
        Goal GetById(String id);

        /// <summary>
        /// Save Goal
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="data">Goal from Misfit</param>
        void SaveGoal(String userId, ApiModels.Goals data);
    }
}
