﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Xamarin;
using FitTracker.Data.Repo.Abstract;

namespace FitTracker.Data.Repo.Xamarin
{
    public interface IBodyMeasurementRepo : IRepoBase<BodyMeasurement>
    {
        /// <summary>
        /// Save BodyMeasurement
        /// </summary>
        /// <param name="data">Device from Misfit</param>
        void SaveBodyMeasurement(BodyMeasurement data);
    }
}
