﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Xamarin;
using FitTracker.Data.Repo.Abstract;

namespace FitTracker.Data.Repo.Xamarin
{
    public interface IBaseDataRepo : IRepoBase<BaseData>
    {
        /// <summary>
        /// Save BaseData
        /// </summary>
        /// <param name="data">Device from Misfit</param>
        void SaveBaseData(BaseData data);
    }
}
