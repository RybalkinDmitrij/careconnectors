﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;
using FitTracker.Data.Entities.Misfit;
using FitTracker.Data.Repo.Xamarin;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Xamarin;

namespace FitTracker.Data.Repo.Xamarin.Impl
{
    internal class FitnessActivityRepo : RepoBase<FitnessActivity, FitTrackerContext>, IFitnessActivityRepo
    {
        public FitnessActivityRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        /// <summary>
        /// Save Device
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="data">Device from Misfit</param>
        public void SaveFitnessActivity(FitnessActivity data)
        {
            FitnessActivity entity = new FitnessActivity();

            entity.Timestamp = data.Timestamp;
            entity.Measurement = data.Measurement;
            entity.Source = data.Source;
            entity.Type = data.Type;
            entity.UserId = data.UserId;

            entity = Save(entity);

            Commit();
        }
    }
}
