﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Xamarin;
using FitTracker.Data.Repo.Abstract;

namespace FitTracker.Data.Repo.Xamarin
{
    public interface ISleepRepo : IRepoBase<HealhKitSleep>
    {
        /// <summary>
        /// Save Sleep
        /// </summary>
        /// <param name="data">Device from Misfit</param>
        void SaveSleep(HealhKitSleep data);
    }
}
