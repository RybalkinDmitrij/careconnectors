﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Xamarin;
using FitTracker.Data.Repo.Abstract;

namespace FitTracker.Data.Repo.Xamarin
{
    public interface IFitnessActivityRepo : IRepoBase<FitnessActivity>
    {
        /// <summary>
        /// Save FitnessActivity
        /// </summary>
        /// <param name="data">Device from Misfit</param>
        void SaveFitnessActivity(FitnessActivity data);
    }
}
