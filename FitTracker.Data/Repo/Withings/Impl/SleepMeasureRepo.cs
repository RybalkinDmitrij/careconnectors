﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Withings;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Withings;

using WAM = Withings.API.Models;
using FitTracker.Data.Helpers;

namespace FitTracker.Data.Repo.Withings.Impl
{
    internal class SleepMeasureRepo : RepoBase<SleepMeasure, FitTrackerContext>, ISleepMeasureRepo
    {
        public SleepMeasureRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<SleepMeasure> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public List<SleepMeasure> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.StartDate >= startDate && x.StartDate <= endDate).ToList();
        }

        public void SaveData(String userId, List<WAM.Sleep> data, DateTime startDate, DateTime endDate)
        {
            var startDateBeginDay = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
            var endDateEndDay = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 0);

            ExecuteSql(String.Format(@"DELETE FROM [dbo].[wth_SleepMeasure] WHERE UserId = '{0}' AND StartDate BETWEEN '{1}' AND '{2}'", userId, startDateBeginDay.ToString("yyyy-MM-dd"), endDateEndDay.ToString("yyyy-MM-dd")));

            foreach (var item in data)
            {
                var entity = new SleepMeasure();

                entity.UserId = userId;
                entity.StartDate = DateHelper.FromUnixTime(item.StartDate);
                entity.EndDate = DateHelper.FromUnixTime(item.EndDate);
                entity.State = item.State;

                entity = Save(entity);

                Commit();
            }
        }
    }
}
