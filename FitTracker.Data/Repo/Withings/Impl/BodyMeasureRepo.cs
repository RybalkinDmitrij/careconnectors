﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Withings;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Withings;
using Withings.API.Models;
using WE = FitTracker.Data.Enums.Withings;

namespace FitTracker.Data.Repo.Withings.Impl
{
    internal class BodyMeasureRepo : RepoBase<BodyMeasure, FitTrackerContext>, IBodyMeasureRepo
    {
        public BodyMeasureRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<BodyMeasure> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public List<BodyMeasure> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Date >= startDate && x.Date <= endDate).ToList();
        }

        public void SaveDataBodyScale(String userId, List<MeasureGroup> data, DateTime startDate, DateTime endDate)
        {
            var measureTypes = new[] { MeasureType.FateFreeMass, MeasureType.FatMassWeight, MeasureType.FatRatio, MeasureType.Height, MeasureType.Weight };
            data.ForEach(x =>
            {
                x.Measures = x.Measures.Where(t => measureTypes.Contains(t.MeasureType)).ToList();
            });

            data = data.Where(x => x.Measures.Count != 0).ToList();

            var startDateBeginDay = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
            var endDateEndDay = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 0);

            ExecuteSql(String.Format(@"DELETE item FROM [dbo].[wth_BodyMeasureItem] item INNER JOIN [dbo].[wth_BodyMeasure] gen ON item.BodyMeasureId = gen.PkId WHERE gen.UserId = '{0}' AND gen.Date BETWEEN '{1}' AND '{2}'  AND gen.Type = {3}", userId, startDateBeginDay.ToString("yyyy-MM-dd"), endDateEndDay.ToString("yyyy-MM-dd"), (Int32)WE.MeasureType.BodyScale));
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[wth_BodyMeasure] WHERE UserId = '{0}' AND Date BETWEEN '{1}' AND '{2}' AND Type = {3}", userId, startDateBeginDay.ToString("yyyy-MM-dd"), endDateEndDay.ToString("yyyy-MM-dd"), (Int32)WE.MeasureType.BodyScale));

            this.SaveData(userId, data, WE.MeasureType.BodyScale);
        }

        public void SaveDataHeartRate(String userId, List<MeasureGroup> data, DateTime startDate, DateTime endDate)
        {
            var measureTypes = new[] { MeasureType.DiastolicBloodPressure, MeasureType.HeartPulse, MeasureType.SystolicBloodPressure };
            data.ForEach(x =>
            {
                x.Measures = x.Measures.Where(t => measureTypes.Contains(t.MeasureType)).ToList();
            });

            data = data.Where(x => x.Measures.Count != 0).ToList();

            var startDateBeginDay = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
            var endDateEndDay = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 0);

            ExecuteSql(String.Format(@"DELETE item FROM [dbo].[wth_BodyMeasureItem] item INNER JOIN [dbo].[wth_BodyMeasure] gen ON item.BodyMeasureId = gen.PkId WHERE gen.UserId = '{0}' AND gen.Date BETWEEN '{1}' AND '{2}'  AND gen.Type = {3}", userId, startDateBeginDay.ToString("yyyy-MM-dd"), endDateEndDay.ToString("yyyy-MM-dd"), (Int32)WE.MeasureType.HeartRate));
            ExecuteSql(String.Format(@"DELETE FROM [dbo].[wth_BodyMeasure] WHERE UserId = '{0}' AND Date BETWEEN '{1}' AND '{2}' AND Type = {3}", userId, startDateBeginDay.ToString("yyyy-MM-dd"), endDateEndDay.ToString("yyyy-MM-dd"), (Int32)WE.MeasureType.HeartRate));

            this.SaveData(userId, data, WE.MeasureType.HeartRate);
        }

        private void SaveData(String userId, List<MeasureGroup> data, WE.MeasureType type)
        {
            foreach (var item in data)
            {
                var entity = new BodyMeasure();

                entity.UserId = userId;
                entity.GrpId = item.Id;
                entity.Attrib = (Int32)item.Attribution;
                entity.Date = item.Date;
                entity.Category = (Int32)item.Category;
                entity.Type = type;

                entity = Save(entity);

                Commit();

                UnitOfWork.GetRepo<IBodyMeasureItemRepo>().SaveData(entity.PkID, item.Measures);
            }
        }
    }
}
