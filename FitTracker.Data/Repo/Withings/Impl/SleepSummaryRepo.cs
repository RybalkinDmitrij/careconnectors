﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Withings;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Withings;

using WAM = Withings.API.Models;
using FitTracker.Data.Helpers;

namespace FitTracker.Data.Repo.Withings.Impl
{
    internal class SleepSummaryRepo : RepoBase<SleepSummaryW, FitTrackerContext>, ISleepSummaryRepo
    {
        public SleepSummaryRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<SleepSummaryW> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public List<SleepSummaryW> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Date >= startDate && x.Date <= endDate).ToList();
        }

        public void SaveData(String userId, List<WAM.SleepSummary> data, DateTime startDate, DateTime endDate)
        {
            var startDateBeginDay = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
            var endDateEndDay = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 0);

            ExecuteSql(String.Format(@"DELETE FROM [dbo].[wth_SleepSummary] WHERE UserId = '{0}' AND StartDate BETWEEN '{1}' AND '{2}'", userId, startDateBeginDay.ToString("yyyy-MM-dd"), endDateEndDay.ToString("yyyy-MM-dd")));

            foreach (var item in data)
            {
                var entity = new SleepSummaryW();

                entity.UserId = userId;
                entity.Timezone = item.Timezone;
                entity.Model = item.Model;
                entity.StartDate = DateHelper.FromUnixTime(item.StartDate);
                entity.EndDate = DateHelper.FromUnixTime(item.EndDate);
                entity.Modified = DateHelper.FromUnixTime(item.Modified);
                entity.Date = item.Date;

                if (item.Data != null)
                {
                    entity.WakeUpDuration = item.Data.WakeUpDuration;
                    entity.LightSleepDuration = item.Data.LightSleepDuration;
                    entity.DeepSleepDuration = item.Data.LightSleepDuration;
                    entity.RemSleepDuration = item.Data.RemSleepDuration;
                    entity.DurationToSleep = item.Data.DurationToSleep;
                    entity.DurationToWakeup = item.Data.DurationToWakeup;
                    entity.WakeupCount = item.Data.WakeupCount;
                }

                entity = Save(entity);

                Commit();
            }
        }
    }
}
