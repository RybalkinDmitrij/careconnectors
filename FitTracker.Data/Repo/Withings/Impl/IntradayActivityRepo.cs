﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Withings;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Withings;

namespace FitTracker.Data.Repo.Withings.Impl
{
    internal class IntradayActivityRepo : RepoBase<IntradayActivity, FitTrackerContext>, IIntradayActivityRepo
    {
        public IntradayActivityRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<IntradayActivity> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }
    }
}
