﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Withings;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Withings;
using Withings.API.Models;

namespace FitTracker.Data.Repo.Withings.Impl
{
    internal class ActivityMeasureRepo : RepoBase<ActivityMeasure, FitTrackerContext>, IActivityMeasureRepo
    {
        public ActivityMeasureRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<ActivityMeasure> GetByUserId(String userId)
        {
            return GetAll().Where(x => x.UserId == userId).ToList();
        }

        public List<ActivityMeasure> GetByUserId(String userId, DateTime startDate, DateTime endDate)
        {
            return GetAll().Where(x => x.UserId == userId && x.Date >= startDate && x.Date <= endDate).ToList();
        }

        public void SaveData(String userId, List<Activity> data, DateTime startDate, DateTime endDate)
        {
            var startDateBeginDay = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
            var endDateEndDay = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 0);

            ExecuteSql(String.Format(@"DELETE FROM [dbo].[wth_ActivityMeasure] WHERE UserId = '{0}' AND Date BETWEEN '{1}' AND '{2}'", userId, startDateBeginDay.ToString("yyyy-MM-dd"), endDateEndDay.ToString("yyyy-MM-dd")));

            foreach (var item in data)
            {
                var entity = new ActivityMeasure();

                entity.UserId = userId;
                entity.Date = item.Date;
                entity.Steps = item.Steps;
                entity.Distance = (Int32)item.Distance;
                entity.Calories = item.Calories;
                entity.TotalCalories = item.TotalCalories;
                entity.Elevation = item.Elevation;
                entity.Soft = item.Soft;
                entity.Moderate = item.Moderate;
                entity.Intense = item.Intense;
                entity.Timezone = item.Timezone;

                entity = Save(entity);

                Commit();
            }
        }
    }
}
