﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities;
using FitTracker.Data.Exceptions;

using FitTracker.Core.Extensions;

using FitTracker.Data.Repo.Withings;
using FitTracker.Data.Repo.Abstract.Impl;
using FitTracker.Data.Entities.Withings;
using Withings.API.Models;

namespace FitTracker.Data.Repo.Withings.Impl
{
    internal class BodyMeasureItemRepo : RepoBase<BodyMeasureItem, FitTrackerContext>, IBodyMeasureItemRepo
    {
        public BodyMeasureItemRepo(UnitOfWork<FitTrackerContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        #endregion override methods

        public List<BodyMeasureItem> GetByMeasureIds(List<Int64> ids)
        {
            return GetAll().Where(x => ids.Contains(x.BodyMeasureId)).ToList();
        }

        public void SaveData(Int64 bodyMeasureId, List<Measure> data)
        {
            foreach (var item in data)
            {
                var entity = new BodyMeasureItem();

                entity.BodyMeasureId = bodyMeasureId;
                entity.Value = item.Value;
                entity.Type = (Int32)item.MeasureType;
                entity.Unit = item.Unit;

                entity = Save(entity);

                Commit();
            }
        }
    }
}
