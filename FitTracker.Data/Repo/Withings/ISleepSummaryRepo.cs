﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Withings;
using FitTracker.Data.Repo.Abstract;

using WAM = Withings.API.Models;

namespace FitTracker.Data.Repo.Withings
{
    public interface ISleepSummaryRepo : IRepoBase<SleepSummaryW>
    {
        List<SleepSummaryW> GetByUserId(String userId);

        List<SleepSummaryW> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        void SaveData(String userId, List<WAM.SleepSummary> data, DateTime startDate, DateTime endDate);
    }
}
