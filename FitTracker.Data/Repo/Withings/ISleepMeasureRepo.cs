﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Withings;
using FitTracker.Data.Repo.Abstract;

using WAM = Withings.API.Models;

namespace FitTracker.Data.Repo.Withings
{
    public interface ISleepMeasureRepo : IRepoBase<SleepMeasure>
    {
        List<SleepMeasure> GetByUserId(String userId);

        List<SleepMeasure> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        void SaveData(String userId, List<WAM.Sleep> data, DateTime startDate, DateTime endDate);
    }
}
