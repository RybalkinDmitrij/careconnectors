﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Withings;
using FitTracker.Data.Repo.Abstract;
using WAM = Withings.API.Models;

namespace FitTracker.Data.Repo.Withings
{
    public interface IBodyMeasureItemRepo : IRepoBase<BodyMeasureItem>
    {
        List<BodyMeasureItem> GetByMeasureIds(List<Int64> ids);

        void SaveData(Int64 bodyMeasureId, List<WAM.Measure> data);
    }
}
