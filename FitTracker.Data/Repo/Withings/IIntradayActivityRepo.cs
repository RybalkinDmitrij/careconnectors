﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Withings;
using FitTracker.Data.Repo.Abstract;

namespace FitTracker.Data.Repo.Withings
{
    public interface IIntradayActivityRepo : IRepoBase<IntradayActivity>
    {
        List<IntradayActivity> GetByUserId(String userId);
    }
}
