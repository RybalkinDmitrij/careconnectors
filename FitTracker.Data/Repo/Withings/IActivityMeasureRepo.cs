﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Withings;
using FitTracker.Data.Repo.Abstract;
using Withings.API.Models;

namespace FitTracker.Data.Repo.Withings
{
    public interface IActivityMeasureRepo : IRepoBase<ActivityMeasure>
    {
        List<ActivityMeasure> GetByUserId(String userId);

        List<ActivityMeasure> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        void SaveData(String userId, List<Activity> data, DateTime startDate, DateTime endDate);
    }
}
