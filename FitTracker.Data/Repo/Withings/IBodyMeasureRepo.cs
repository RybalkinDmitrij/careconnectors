﻿using System;
using System.Collections.Generic;
using System.Linq;

using FitTracker.Data.Entities.Withings;
using FitTracker.Data.Repo.Abstract;
using Withings.API.Models;

namespace FitTracker.Data.Repo.Withings
{
    public interface IBodyMeasureRepo : IRepoBase<BodyMeasure>
    {
        List<BodyMeasure> GetByUserId(String userId);

        List<BodyMeasure> GetByUserId(String userId, DateTime startDate, DateTime endDate);

        void SaveDataBodyScale(String userId, List<MeasureGroup> data, DateTime startDate, DateTime endDate);

        void SaveDataHeartRate(String userId, List<MeasureGroup> data, DateTime startDate, DateTime endDate);
    }
}