﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace FitTracker.Data.Repo.Abstract.Impl
{
    internal abstract class RepoBase<TEntity, TDBContext> : IRepoBase<TEntity>
        where TDBContext : DbContext, new()
        where TEntity : AbstractEntity
    {
        #region public constructors

        public RepoBase(UnitOfWork<TDBContext> unitOfWork)
        {
            UnitOfWork = unitOfWork;
            Context = UnitOfWork.Context;
            DbSet = Context.Set<TEntity>();
        }

        #endregion public constructors

        #region public methods

        public virtual void ExecuteSql(String sql)
        {
            Context.Database.ExecuteSqlCommand(sql);
        }

        public virtual TEntity Get(Int64 Id)
        {
            return DbSet.Find(Id);
        }

        public virtual IQueryable<TEntity> Get(Expression<Func<TEntity, Boolean>> expression)
        {
            return GetAll().Where(expression);
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return DbSet.Where(x => !x.IsDeleted);
        }

        public virtual TEntity Create()
        {
            return DbSet.Create<TEntity>();
        }

        public virtual TEntity Save(TEntity entity, Int64 currentUserId = 0)
        {
            if (entity.PkID == 0)
            {
                entity.IsDeleted = false;

                entity.DateCreate = DateTime.Now;
                entity.DateUpdate = DateTime.Now;
                entity.DateDelete = null;

                entity.UserCreateId = currentUserId;
                entity.UserUpdateId = currentUserId;
                entity.UserDeleteId = 0;

                return DbSet.Add(entity);
            }
            else
            {
                entity.IsDeleted = false;
                entity.DateUpdate = DateTime.Now;
                entity.UserUpdateId = currentUserId;

                entity = DbSet.Attach(entity);
                Context.Entry(entity).State = EntityState.Modified;
                return entity;
            }
        }
        
        public virtual void Remove(Int64 Id, Int64 currentUserId = 0)
        {
            var entity = Get(Id);
            if (entity != null) Remove(entity, currentUserId);
        }

        public virtual void Remove(TEntity entity, Int64 currentUserId = 0)
        {
            entity.IsDeleted = true;
            entity.DateDelete = DateTime.Now;
            entity.UserDeleteId = currentUserId;

            entity = DbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;

            //DbSet.Remove(entity);
        }

        public virtual void Remove(IEnumerable<TEntity> entities, Int64 currentUserId = 0)
        {
            foreach (var entity in entities)
            {
                Remove(entity, currentUserId);
            }
        }

        public virtual Boolean IsExist(Expression<Func<TEntity, Boolean>> expression)
        {
            return GetAll().Any(expression);
        }

        public virtual Boolean IsUpdated(TEntity entity)
        {
            return Context.Entry(entity).State == EntityState.Modified;
        }

        #endregion public methods

        #region protected methods
        
        protected String GetSimpleKey()
        {
            return Guid.NewGuid().ToString();
        }

        protected void Commit()
        {
            UnitOfWork.Commit();
        }

        #endregion protected methods

        #region protected properties

        protected UnitOfWork<TDBContext> UnitOfWork { get; private set; }
        protected TDBContext Context { get; private set; }
        protected IDbSet<TEntity> DbSet { get; private set; }

        #endregion protected properties
    }
}
