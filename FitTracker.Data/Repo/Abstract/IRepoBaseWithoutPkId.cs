﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FitTracker.Data.Repo.Abstract
{
    public interface IRepoBaseWithoutPkId<TEntity>
        where TEntity : AbstractEntityWithoutPkId
    {
        void ExecuteSql(String sql);

        TEntity Get(Int64 Id);

        IQueryable<TEntity> Get(Expression<Func<TEntity, Boolean>> expression);

        IQueryable<TEntity> GetAll();

        TEntity Create();
        
        void Remove(Int64 Id, Int64 currentUserId = 0);

        void Remove(TEntity entity, Int64 currentUserId = 0);

        void Remove(IEnumerable<TEntity> entities, Int64 currentUserId = 0);

        Boolean IsExist(Expression<Func<TEntity, Boolean>> expression);

        Boolean IsUpdated(TEntity entity);
    }
}
