﻿using System.Data.Entity;

using CnfgAuth = FitTracker.Data.Cofigs.Auth;
using CnfgLog = FitTracker.Data.Cofigs.Log;
using CnfgFitbit = FitTracker.Data.Cofigs.Fitbit;
using CnfgWithings = FitTracker.Data.Cofigs.Withings;
using CnfgJawbone = FitTracker.Data.Cofigs.Jawbone;
using CnfgRunkeeper = FitTracker.Data.Cofigs.Runkeeper;
using CnfgStrava = FitTracker.Data.Cofigs.Strava;
using CnfgMisfit = FitTracker.Data.Cofigs.Misfit;
using CnfgHealthKit = FitTracker.Data.Cofigs.Xamarin;

namespace FitTracker.Data
{
    public class FitTrackerContext : DbContext
    {
        public FitTrackerContext()
            : base("ConnectionString")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CnfgAuth.ClientConfig());
            modelBuilder.Configurations.Add(new CnfgAuth.ApplicantConfig());
            modelBuilder.Configurations.Add(new CnfgAuth.DbUserConfig());

            modelBuilder.Configurations.Add(new CnfgLog.LogErrorConfig());
            modelBuilder.Configurations.Add(new CnfgLog.LogActionConfig());

            modelBuilder.Configurations.Add(new CnfgFitbit.UserConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.UserProfileConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.ActivityDistanceConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.ActivityGoalsConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.ActivityLogConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.ActivitySummaryConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.BodyConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.BodyGoalsConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.DeviceConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.FatLogConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.FoodGoalsConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.FoodLogConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.FoodLogUnitConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.FoodSummaryConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.LoggedFoodConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.MinuteDataConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.NutritionalValuesConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.SleepLogConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.SleepSummaryFConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.WaterConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.WaterGoalConfig());
            modelBuilder.Configurations.Add(new CnfgFitbit.WeightLogConfig());

            modelBuilder.Configurations.Add(new CnfgWithings.ActivityMeasureConfig());
            modelBuilder.Configurations.Add(new CnfgWithings.BodyMeasureConfig());
            modelBuilder.Configurations.Add(new CnfgWithings.BodyMeasureItemConfig());
            modelBuilder.Configurations.Add(new CnfgWithings.IntradayActivityConfig());
            modelBuilder.Configurations.Add(new CnfgWithings.SleepMeasureConfig());
            modelBuilder.Configurations.Add(new CnfgWithings.SleepSummaryWConfig());

            modelBuilder.Configurations.Add(new CnfgJawbone.AccountInfoConfig());
            modelBuilder.Configurations.Add(new CnfgJawbone.BodyEventConfig());
            modelBuilder.Configurations.Add(new CnfgJawbone.CustomConfig());
            modelBuilder.Configurations.Add(new CnfgJawbone.GoalsConfig());
            modelBuilder.Configurations.Add(new CnfgJawbone.HeartRateConfig());
            modelBuilder.Configurations.Add(new CnfgJawbone.HourlyTotalConfig());
            modelBuilder.Configurations.Add(new CnfgJawbone.MealsConfig());
            modelBuilder.Configurations.Add(new CnfgJawbone.MoodConfig());
            modelBuilder.Configurations.Add(new CnfgJawbone.MovesConfig());
            modelBuilder.Configurations.Add(new CnfgJawbone.MovesTickConfig());
            modelBuilder.Configurations.Add(new CnfgJawbone.SettingsConfig());
            modelBuilder.Configurations.Add(new CnfgJawbone.SleepsConfig());
            modelBuilder.Configurations.Add(new CnfgJawbone.TimezoneConfig());
            modelBuilder.Configurations.Add(new CnfgJawbone.WorkoutsConfig());

            modelBuilder.Configurations.Add(new CnfgRunkeeper.BackgroundActivitiesConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.CaloriesConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.DiabetesMeasurementsConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.DistanceConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.ExercisesConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.FitnessActivitiesConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.GeneralMeasurementsConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.HeartRateConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.ImageConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.NutritionConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.PathConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.ProfileConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.SetsConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.SleepConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.StrengthTrainingConfig());
            modelBuilder.Configurations.Add(new CnfgRunkeeper.WeightConfig());

            modelBuilder.Configurations.Add(new CnfgStrava.ActivityConfig());
            modelBuilder.Configurations.Add(new CnfgStrava.ActivityLapConfig());
            modelBuilder.Configurations.Add(new CnfgStrava.AthleteConfig());
            modelBuilder.Configurations.Add(new CnfgStrava.BikeConfig());
            modelBuilder.Configurations.Add(new CnfgStrava.ClubConfig());
            modelBuilder.Configurations.Add(new CnfgStrava.CommentConfig());
            modelBuilder.Configurations.Add(new CnfgStrava.GearConfig());
            modelBuilder.Configurations.Add(new CnfgStrava.MapConfig());
            modelBuilder.Configurations.Add(new CnfgStrava.PhotoConfig());
            modelBuilder.Configurations.Add(new CnfgStrava.SegmentConfig());
            modelBuilder.Configurations.Add(new CnfgStrava.SegmentEffortConfig());
            modelBuilder.Configurations.Add(new CnfgStrava.ShoesConfig());

            modelBuilder.Configurations.Add(new CnfgMisfit.DeviceMisfitConfig());
            modelBuilder.Configurations.Add(new CnfgMisfit.GoalConfig());
            modelBuilder.Configurations.Add(new CnfgMisfit.ProfileInfoConfig());
            modelBuilder.Configurations.Add(new CnfgMisfit.SessionConfig());
            modelBuilder.Configurations.Add(new CnfgMisfit.SleepMisfitConfig());
            modelBuilder.Configurations.Add(new CnfgMisfit.SummaryConfig());
            modelBuilder.Configurations.Add(new CnfgMisfit.SleepDetailConfig());

            modelBuilder.Configurations.Add(new CnfgHealthKit.BaseDataConfig());
            modelBuilder.Configurations.Add(new CnfgHealthKit.BodyMeasurementConfig());
            modelBuilder.Configurations.Add(new CnfgHealthKit.FitnessActivityConfig());
            modelBuilder.Configurations.Add(new CnfgHealthKit.HealhKitSleepConfig());
        }
    }
}