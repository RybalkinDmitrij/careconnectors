﻿using FitbitApi = Fitbit.Api;
using WithingsApi = Withings.API;
using JawboneApi = Jawbone.Api;
using StravaApi = Strava.Api;
using MisfitApi = Misfit.Api;
using FitTracker.Core.Unity;
using FitTracker.Data.Entities.Auth;
using FitTracker.Data.Repo.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitTracker.Data.Helpers;
using Strava.Api.Authentication;

namespace FitTracker.Data
{
    public static class FitTrackerClients
    {
        public static FitbitApi.FitbitClient GetFitbitClient(String userId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                Client clientDB = repo.GetByFitbitId(userId);

                if (clientDB == null) return null;

                return new FitbitApi.FitbitClient(SettingsHelper.FitBitConsumerKey, SettingsHelper.FitBitConsumerSecret, clientDB.FitbitAccessToken, clientDB.FitbitSecret);
            }
        }

        public static WithingsApi.WithingsClient GetWithingsClient(String userId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                Client clientDB = repo.GetByWithingsId(userId);

                if (clientDB == null) return null;

                return new WithingsApi.WithingsClient(SettingsHelper.WithingsConsumerKey, SettingsHelper.WithingsConsumerSecret, clientDB.WithingsAccessToken, clientDB.WithingsSecret);
            }
        }

        public static JawboneApi.JawboneClient GetJawboneClientByUserId(String userId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                Client clientDB = repo.GetByJawboneId(userId);

                if (clientDB == null) return null;

                return new JawboneApi.JawboneClient(clientDB.JawboneAccessToken);
            }
        }

        public static MisfitApi.MisfitClient GetMisfitClientByUserId(String userId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                Client clientDB = repo.GetByMisfitId(userId);

                if (clientDB == null) return null;

                return new MisfitApi.MisfitClient(clientDB.MisfitAccessToken);
            }
        }

        public static StravaApi.Clients.StravaClient GetStravaClientByUserId(String userId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                Client clientDB = repo.GetByStravaId(userId);

                if (clientDB == null) return null;

                StaticAuthentication auth = new StaticAuthentication(clientDB.StravaAccessToken);

                return new StravaApi.Clients.StravaClient(auth);
            }
        }

        public static JawboneApi.JawboneClient GetJawboneClientByToken(String token)
        {
            return new JawboneApi.JawboneClient(token);
        }

        public static StravaApi.Clients.StravaClient GetStravaClientByToken(String token)
        {
            StaticAuthentication auth = new StaticAuthentication(token);
            return new StravaApi.Clients.StravaClient(auth);
        }

        public static MisfitApi.MisfitClient GetMisfitClientByToken(String token)
        {
            return new MisfitApi.MisfitClient(token);
        }
    }
}
