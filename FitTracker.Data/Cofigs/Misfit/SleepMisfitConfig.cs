﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Misfit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Misfit
{
    internal class SleepMisfitConfig : AbstractEntityConfig<SleepMisfit>
    {
        public SleepMisfitConfig()
            : base("mft", "Sleep")
        {
            Property(x => x.Id).IsRequired();
            Property(x => x.AutoDetected).IsRequired();
            Property(x => x.StartTime).IsRequired();
            Property(x => x.Duration).IsRequired();

            HasRequired(x => x.ProfileInfo).WithMany(x => x.Sleeps).HasForeignKey(x => x.ProfileInfoId);
        }
    }
}
