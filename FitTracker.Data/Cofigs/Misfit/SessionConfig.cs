﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Misfit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Misfit
{
    internal class SessionConfig : AbstractEntityConfig<Session>
    {
        public SessionConfig()
            : base("mft", "Session")
        {
            Property(x => x.Id).IsRequired();
            Property(x => x.ActivityType).IsRequired();
            Property(x => x.StartTime).IsRequired();
            Property(x => x.Duration).IsRequired();
            Property(x => x.Points).IsRequired();
            Property(x => x.Steps).IsRequired();
            Property(x => x.Calories).IsRequired();
            Property(x => x.Distance).IsRequired();

            HasRequired(x => x.ProfileInfo).WithMany(x => x.Sessions).HasForeignKey(x => x.ProfileInfoId);
        }
    }
}
