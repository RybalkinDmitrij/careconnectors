﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Misfit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Misfit
{
    internal class SummaryConfig : AbstractEntityConfig<Summary>
    {
        public SummaryConfig()
            : base("mft", "Summary")
        {
            Property(x => x.Date).IsRequired();
            Property(x => x.Points).IsRequired();
            Property(x => x.Steps).IsRequired();
            Property(x => x.Calories).IsRequired();
            Property(x => x.Distance).IsRequired();
            Property(x => x.ActivityCalories).IsRequired();

            HasRequired(x => x.ProfileInfo).WithMany(x => x.Summaries).HasForeignKey(x => x.ProfileInfoId);
        }
    }
}
