﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Misfit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Misfit
{
    internal class ProfileInfoConfig : AbstractEntityConfig<ProfileInfo>
    {
        public ProfileInfoConfig()
            : base("mft", "Profile")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Name).IsRequired();
            Property(x => x.Birthday).IsRequired();
            Property(x => x.Gender).IsRequired();
            Property(x => x.Email).IsRequired();
        }
    }
}
