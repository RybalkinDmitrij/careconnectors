﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Misfit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Misfit
{
    internal class SleepDetailConfig : AbstractEntityConfig<SleepDetail>
    {
        public SleepDetailConfig()
            : base("mft", "SleepDetail")
        {
            Property(x => x.Datetime).IsRequired();
            Property(x => x.Value).IsRequired();

            HasRequired(x => x.Sleep).WithMany(x => x.SleepDetail).HasForeignKey(x => x.SleepId);
        }
    }
}
