﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Misfit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Misfit
{
    internal class DeviceMisfitConfig : AbstractEntityConfig<DeviceMisfit>
    {
        public DeviceMisfitConfig()
            : base("mft", "Device")
        {
            Property(x => x.Id).IsRequired();
            Property(x => x.DeviceType).IsRequired();
            Property(x => x.SerialNumber).IsRequired();
            Property(x => x.FirmwareVersion).IsRequired();
            Property(x => x.BatteryLevel).IsRequired();
            Property(x => x.LastSyncTime).IsRequired();

            HasRequired(x => x.ProfileInfo).WithMany(x => x.Devices).HasForeignKey(x => x.ProfileInfoId);
        }
    }
}
