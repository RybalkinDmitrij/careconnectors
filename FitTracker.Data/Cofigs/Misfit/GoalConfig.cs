﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Misfit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Misfit
{
    internal class GoalConfig : AbstractEntityConfig<Goal>
    {
        public GoalConfig()
            : base("mft", "Goal")
        {
            Property(x => x.Id).IsRequired();
            Property(x => x.Date).IsRequired();
            Property(x => x.Points).IsRequired();
            Property(x => x.TargetPoints).IsRequired();

            HasRequired(x => x.ProfileInfo).WithMany(x => x.Goals).HasForeignKey(x => x.ProfileInfoId);
        }
    }
}
