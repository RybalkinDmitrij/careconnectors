﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Xamarin;

namespace FitTracker.Data.Cofigs.Xamarin
{
    internal class BodyMeasurementConfig : AbstractEntityConfig<BodyMeasurement>
    {
        public BodyMeasurementConfig()
            : base("hlk", "BodyMeasurement")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Measurement).IsRequired();
            Property(x => x.Type).IsRequired();
            Property(x => x.Timestamp).IsRequired();
            Property(x => x.Source);
        }
    }
}
