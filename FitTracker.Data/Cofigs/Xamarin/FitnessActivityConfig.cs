﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Xamarin;

namespace FitTracker.Data.Cofigs.Xamarin
{
    internal class FitnessActivityConfig : AbstractEntityConfig<FitnessActivity>
    {
        public FitnessActivityConfig()
            : base("hlk", "FitnessActivity")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Measurement).IsRequired();
            Property(x => x.Type).IsRequired();
            Property(x => x.Timestamp).IsRequired();
            Property(x => x.Source);
        }
    }
}
