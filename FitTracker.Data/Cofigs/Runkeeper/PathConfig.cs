﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Runkeeper
{
    internal class PathConfig : AbstractEntityConfig<Path>
    {
        public PathConfig()
            : base("rk", "Path")
        {
            Property(x => x.Latitude).IsRequired();
            Property(x => x.Longitude).IsRequired();
            Property(x => x.Altitude).IsRequired();
            Property(x => x.Timestamp).IsRequired();
            Property(x => x.Type).IsRequired();

            HasRequired(x => x.FitnessActivity).WithMany(x => x.Path).HasForeignKey(x => x.FitnessActivityId);
        }
    }
}
