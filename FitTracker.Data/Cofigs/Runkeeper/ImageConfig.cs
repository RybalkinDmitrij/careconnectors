﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Runkeeper
{
    internal class ImageConfig : AbstractEntityConfig<Image>
    {
        public ImageConfig()
            : base("rk", "Image")
        {
            Property(x => x.Latitude).IsRequired();
            Property(x => x.Longitude).IsRequired();
            Property(x => x.Timestamp).IsRequired();

            HasRequired(x => x.FitnessActivity).WithMany(x => x.Images).HasForeignKey(x => x.FitnessActivityId);
        }
    }
}
