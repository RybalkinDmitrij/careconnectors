﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Runkeeper
{
    internal class SetsConfig : AbstractEntityConfig<Sets>
    {
        public SetsConfig()
            : base("rk", "Sets")
        {
            Property(x => x.Weight).IsRequired();
            Property(x => x.Repetitions).IsRequired();
            Property(x => x.Notes).IsRequired();

            HasRequired(x => x.Exercise).WithMany(x => x.Sets).HasForeignKey(x => x.ExerciseId);
        }
    }
}
