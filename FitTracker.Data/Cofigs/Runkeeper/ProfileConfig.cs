﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Runkeeper
{
    internal class ProfileConfig : AbstractEntityConfig<Profile>
    {
        public ProfileConfig()
            : base("rk", "Profile")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Name).IsRequired();
            Property(x => x.Location).IsRequired();
            Property(x => x.AthleteType).IsRequired();
            Property(x => x.Gender).IsRequired();
            Property(x => x.Birthday).IsRequired();
            Property(x => x.Elite).IsRequired();
        }
    }
}
