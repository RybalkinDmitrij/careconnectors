﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Runkeeper
{
    internal class StrengthTrainingConfig : AbstractEntityConfig<StrengthTraining>
    {
        public StrengthTrainingConfig()
            : base("rk", "StrengthTraining")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.StartTime).IsRequired();
            Property(x => x.TotalCalories).IsRequired();
            Property(x => x.Notes).IsRequired();
            Property(x => x.Source).IsRequired();
        }
    }
}
