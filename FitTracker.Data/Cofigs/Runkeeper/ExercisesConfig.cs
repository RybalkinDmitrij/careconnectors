﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Runkeeper
{
    internal class ExercisesConfig : AbstractEntityConfig<Exercises>
    {
        public ExercisesConfig()
            : base("rk", "Exercises")
        {
            Property(x => x.PrimaryType).IsRequired();
            Property(x => x.SecondaryType).IsRequired();
            Property(x => x.PrimaryMuscleGroup).IsRequired();
            Property(x => x.SecondaryMuscleGroup).IsRequired();
            Property(x => x.Routine).IsRequired();
            Property(x => x.Notes).IsRequired();

            HasRequired(x => x.StrengthTraining).WithMany(x => x.Exercises).HasForeignKey(x => x.StrengthTrainingId);
        }
    }
}
