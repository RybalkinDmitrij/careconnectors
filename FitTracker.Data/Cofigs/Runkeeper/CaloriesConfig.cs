﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Runkeeper
{
    internal class CaloriesConfig : AbstractEntityConfig<CaloriesMod>
    {
        public CaloriesConfig()
            : base("rk", "Calories")
        {
            Property(x => x.Calories).IsRequired();
            Property(x => x.Timestamp).IsRequired();

            HasRequired(x => x.FitnessActivity).WithMany(x => x.Calories).HasForeignKey(x => x.FitnessActivityId);
        }
    }
}
