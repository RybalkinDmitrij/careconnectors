﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Runkeeper
{
    internal class FitnessActivitiesConfig : AbstractEntityConfig<FitnessActivities>
    {
        public FitnessActivitiesConfig()
            : base("rk", "FitnessActivities")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Type).IsRequired();
            Property(x => x.SecondaryType).IsRequired();
            Property(x => x.Equipment).IsRequired();
            Property(x => x.StartTime).IsRequired();
            Property(x => x.TotalDistance).IsRequired();
            Property(x => x.Duration).IsRequired();
            Property(x => x.AverageHeartRate).IsRequired();
            Property(x => x.TotalCalories).IsRequired();
            Property(x => x.Climb).IsRequired();
            Property(x => x.Notes).IsRequired();
            Property(x => x.IsLive).IsRequired();
            Property(x => x.Source).IsRequired();
        }
    }
}
