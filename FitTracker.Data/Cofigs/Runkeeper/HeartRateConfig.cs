﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Runkeeper
{
    internal class HeartRateConfig : AbstractEntityConfig<HeartRates>
    {
        public HeartRateConfig()
            : base("rk", "HeartRate")
        {
            Property(x => x.HeartRate).IsRequired();
            Property(x => x.Timestamp).IsRequired();

            HasRequired(x => x.FitnessActivity).WithMany(x => x.HeartRate).HasForeignKey(x => x.FitnessActivityId);
        }
    }
}
