﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Runkeeper
{
    internal class DistanceConfig : AbstractEntityConfig<Distances>
    {
        public DistanceConfig()
            : base("rk", "Distance")
        {
            Property(x => x.Distance).IsRequired();
            Property(x => x.Timestamp).IsRequired();

            HasRequired(x => x.FitnessActivity).WithMany(x => x.Distances).HasForeignKey(x => x.FitnessActivityId);
        }
    }
}
