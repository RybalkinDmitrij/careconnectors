﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Runkeeper
{
    internal class GeneralMeasurementsConfig : AbstractEntityConfig<GeneralMeasurements>
    {
        public GeneralMeasurementsConfig()
            : base("rk", "GeneralMeasurements")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Measurement).IsRequired();
            Property(x => x.Timestamp).IsRequired();
            Property(x => x.Type).IsRequired();
            Property(x => x.Source).IsRequired();
        }
    }
}
