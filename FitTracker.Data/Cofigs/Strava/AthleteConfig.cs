﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Strava
{
    internal class AthleteConfig : AbstractEntityConfig<Athlete>
    {
        public AthleteConfig()
            : base("str", "Athlete")
        {
            Property(x => x.Id).IsRequired();
            Property(x => x.ResourceState).IsRequired();
            Property(x => x.FirstName).IsRequired();
            Property(x => x.LastName).IsRequired();
            Property(x => x.ProfileMedium).IsRequired();
            Property(x => x.City).IsRequired();
            Property(x => x.State).IsRequired();
            Property(x => x.Country).IsRequired();
            Property(x => x.Sex).IsRequired();
            Property(x => x.Friend).IsRequired();
            Property(x => x.Follower).IsRequired();
            Property(x => x.IsPremium).IsRequired();
            Property(x => x.CreatedAt).IsRequired();
            Property(x => x.UpdatedAt).IsRequired();
            Property(x => x.ApproveFollowers).IsRequired();
            Property(x => x.FollowerCount).IsRequired();
            Property(x => x.FriendCount).IsRequired();
            Property(x => x.MutualFriendCount).IsRequired();
            Property(x => x.DatePreference).IsRequired();
            Property(x => x.MeasurementPreference).IsRequired();
            Property(x => x.Email).IsRequired();
            Property(x => x.Ftp).IsRequired();
        }
    }
}
