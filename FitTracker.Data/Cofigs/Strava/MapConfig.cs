﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Strava
{
    internal class MapConfig : AbstractEntityConfig<Map>
    {
        public MapConfig()
            : base("str", "Map")
        {
            Property(x => x.Id).IsRequired();
            Property(x => x.Polyline).IsRequired();
            Property(x => x.SummaryPolyline).IsRequired();
            Property(x => x.ResourceState).IsRequired();
        }
    }
}
