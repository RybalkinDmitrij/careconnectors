﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Strava
{
    internal class ClubConfig : AbstractEntityConfig<Club>
    {
        public ClubConfig()
            : base("str", "Club")
        {
            Property(x => x.Id).IsRequired();
            Property(x => x.ResourceState).IsRequired();
            Property(x => x.Name).IsRequired();
            Property(x => x.ProfileMedium).IsRequired();
            Property(x => x.Description).IsRequired();
            Property(x => x.ClubType).IsRequired();
            Property(x => x.SportType).IsRequired();
            Property(x => x.City).IsRequired();
            Property(x => x.State).IsRequired();
            Property(x => x.Country).IsRequired();
            Property(x => x.IsPrivate).IsRequired();
            Property(x => x.MemberCount).IsRequired();

            HasRequired(x => x.Athlete).WithMany(x => x.Clubs).HasForeignKey(x => x.AthleteId);
        }
    }
}
