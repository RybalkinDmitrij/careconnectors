﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Strava
{
    internal class ShoesConfig : AbstractEntityWithoutPkIdConfig<Shoes>
    {
        public ShoesConfig()
            : base("str", "Shoes")
        {
            HasKey(x => x.GearId);

            Property(x => x.Brand).IsRequired();
            Property(x => x.Model).IsRequired();
            Property(x => x.Description).IsRequired();

            HasRequired(x => x.Athlete).WithMany(x => x.Shoes).HasForeignKey(x => x.AthleteId);
        }
    }
}
