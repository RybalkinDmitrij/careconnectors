﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Strava
{
    internal class CommentConfig : AbstractEntityConfig<Comment>
    {
        public CommentConfig()
            : base("str", "Comment")
        {
            Property(x => x.Id).IsRequired();
            Property(x => x.ResourceState).IsRequired();
            Property(x => x.Text).IsRequired();
            Property(x => x.Author).IsRequired();
            Property(x => x.TimeCreated).IsRequired();

            HasRequired(x => x.Activity).WithMany(x => x.Comments).HasForeignKey(x => x.ActivityId);
        }
    }
}
