﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Strava
{
    internal class ActivityConfig : AbstractEntityConfig<Activity>
    {
        public ActivityConfig()
            : base("str", "Activity")
        {
            Property(x => x.AthleteId).IsRequired();
            Property(x => x.Id).IsRequired();
            Property(x => x.Name).IsRequired();
            Property(x => x.ExternalId).IsRequired();
            Property(x => x.Type).IsRequired();
            Property(x => x.Distance).IsRequired();
            Property(x => x.MovingTime).IsRequired();
            Property(x => x.ElapsedTime).IsRequired();
            Property(x => x.ElevationGain).IsRequired();
            Property(x => x.HasKudoed).IsRequired();
            Property(x => x.AverageHeartrate).IsRequired();
            Property(x => x.MaxHeartrate).IsRequired();
            Property(x => x.Truncated).IsRequired();
            Property(x => x.City).IsRequired();
            Property(x => x.State).IsRequired();
            Property(x => x.Country).IsRequired();
            Property(x => x.AverageSpeed).IsRequired();
            Property(x => x.MaxSpeed).IsRequired();
            Property(x => x.AverageCadence).IsRequired();
            Property(x => x.AverageTemperature).IsRequired();
            Property(x => x.AveragePower).IsRequired();
            Property(x => x.Kilojoules).IsRequired();
            Property(x => x.IsCommute).IsRequired();
            Property(x => x.IsDeleted).IsRequired();
            Property(x => x.IsFlagged).IsRequired();
            Property(x => x.IsManual).IsRequired();
            Property(x => x.IsPrivate).IsRequired();
            Property(x => x.IsTrainer).IsRequired();
            Property(x => x.AchievementCount).IsRequired();
            Property(x => x.KudosCount).IsRequired();
            Property(x => x.CommentCount).IsRequired();
            Property(x => x.AthleteCount).IsRequired();
            Property(x => x.PhotoCount).IsRequired();
            Property(x => x.DateTimeStart).IsRequired();
            Property(x => x.DateTimeStartLocal).IsRequired();
            Property(x => x.WeightedAverageWatts).IsRequired();
            Property(x => x.TimeZone).IsRequired();
            Property(x => x.StartLatitude).IsRequired();
            Property(x => x.StartLongitude).IsRequired();
            Property(x => x.EndLatitude).IsRequired();
            Property(x => x.EndLongitude).IsRequired();
            Property(x => x.HasPowerMeter).IsRequired();  
            Property(x => x.Calories).IsRequired();
            Property(x => x.Description).IsRequired();

            HasRequired(x => x.Map).WithMany(x => x.Activities).HasForeignKey(x => x.MapId);
            HasRequired(x => x.Gear).WithMany(x => x.Activities).HasForeignKey(x => x.GearId);
            HasRequired(x => x.Athlete).WithMany(x => x.Activities).HasForeignKey(x => x.AthleteId);
        }
    }
}
