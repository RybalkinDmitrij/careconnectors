﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Strava
{
    internal class SegmentEffortConfig : AbstractEntityConfig<SegmentEffort>
    {
        public SegmentEffortConfig()
            : base("str", "SegmentEffort")
        {
            Property(x => x.Id).IsRequired();
            Property(x => x.AverageCadence).IsRequired();
            Property(x => x.AveragePower).IsRequired();
            Property(x => x.AverageHeartrate).IsRequired();
            Property(x => x.MaxHeartrate).IsRequired();
            Property(x => x.ResourceState).IsRequired();
            Property(x => x.Name).IsRequired();
            Property(x => x.UserId).IsRequired();
            Property(x => x.KingOfMountainRank).IsRequired();
            Property(x => x.PersonalRecordRank).IsRequired();
            Property(x => x.MovingTime).IsRequired();
            Property(x => x.ElapsedTime).IsRequired();
            Property(x => x.DateTimeStart).IsRequired();
            Property(x => x.SegmentDistance).IsRequired();
            Property(x => x.StartIndex).IsRequired();
            Property(x => x.EndIndex).IsRequired();

            HasRequired(x => x.Activity).WithMany(x => x.SegmentEfforts).HasForeignKey(x => x.ActivityId);
            HasRequired(x => x.Segment).WithMany(x => x.SegmentEfforts).HasForeignKey(x => x.SegmentId);
        }
    }
}
