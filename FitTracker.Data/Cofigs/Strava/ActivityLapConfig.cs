﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Strava
{
    internal class ActivityLapConfig : AbstractEntityConfig<ActivityLap>
    {
        public ActivityLapConfig()
            : base("str", "ActivityLap")
        {
            Property(x => x.Id).IsRequired();
            Property(x => x.ResourceState).IsRequired();
            Property(x => x.Name).IsRequired();
            Property(x => x.ElapsedTime).IsRequired();
            Property(x => x.MovingTime).IsRequired();
            Property(x => x.Start).IsRequired();
            Property(x => x.StartLocal).IsRequired();
            Property(x => x.Distance).IsRequired();
            Property(x => x.StartIndex).IsRequired();
            Property(x => x.EndIndex).IsRequired();
            Property(x => x.TotalElevationGain).IsRequired();
            Property(x => x.AverageSpeed).IsRequired();
            Property(x => x.MaxSpeed).IsRequired();
            Property(x => x.AverageCadence).IsRequired();
            Property(x => x.AveragePower).IsRequired();
            Property(x => x.AverageHeartrate).IsRequired();
            Property(x => x.MaxHeartrate).IsRequired();
            Property(x => x.LapIndex).IsRequired();
            
            HasRequired(x => x.Activity).WithMany(x => x.ActivityLaps).HasForeignKey(x => x.ActivityId);
        }
    }
}
