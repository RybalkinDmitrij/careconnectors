﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Strava;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Strava
{
    internal class BikeConfig : AbstractEntityWithoutPkIdConfig<Bike>
    {
        public BikeConfig()
            : base("str", "Bike")
        {
            HasKey(x => x.GearId);

            Property(x => x.Brand).IsRequired();
            Property(x => x.Model).IsRequired();
            Property(x => x.BikeType).IsRequired();
            Property(x => x.Description).IsRequired();

            HasRequired(x => x.Athlete).WithMany(x => x.Bikes).HasForeignKey(x => x.AthleteId);
        }
    }
}
