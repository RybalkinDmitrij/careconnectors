﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Strava
{
    internal class SegmentConfig : AbstractEntityConfig<Segment>
    {
        public SegmentConfig()
            : base("str", "Segment")
        {
            Property(x => x.Id).IsRequired();
            Property(x => x.ResourceState).IsRequired();
            Property(x => x.Name).IsRequired();
            Property(x => x.ActivityType).IsRequired();
            Property(x => x.Distance).IsRequired();
            Property(x => x.AverageGrade).IsRequired();
            Property(x => x.MaxGrade).IsRequired();
            Property(x => x.MaxElevation).IsRequired();
            Property(x => x.MinElevation).IsRequired();
            Property(x => x.ClimbCategory).IsRequired();
            Property(x => x.City).IsRequired();
            Property(x => x.State).IsRequired();
            Property(x => x.Country).IsRequired();
            Property(x => x.IsPrivate).IsRequired();
            Property(x => x.IsStarred).IsRequired();
            Property(x => x.CreatedAt).IsRequired();
            Property(x => x.UpdatedAt).IsRequired();
            Property(x => x.TotalElevationGain).IsRequired();
            Property(x => x.EffortCount).IsRequired();
            Property(x => x.AthleteCount).IsRequired();
            Property(x => x.IsHazardous).IsRequired();
            Property(x => x.PersonalRecordTime).IsRequired();
            Property(x => x.PersonalRecordDistance).IsRequired();
            Property(x => x.StarCount).IsRequired();

            HasRequired(x => x.Map).WithMany(x => x.Segments).HasForeignKey(x => x.MapId).WillCascadeOnDelete(false);
        }
    }
}
