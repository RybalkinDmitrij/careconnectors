﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Strava
{
    internal class PhotoConfig : AbstractEntityConfig<Photo>
    {
        public PhotoConfig()
            : base("str", "Photo")
        {
            Property(x => x.Id).IsRequired();
            Property(x => x.ResourceState).IsRequired();
            Property(x => x.ImageUrl).IsRequired();
            Property(x => x.ExternalUid).IsRequired();
            Property(x => x.Caption).IsRequired();
            Property(x => x.Type).IsRequired();
            Property(x => x.UploadedAt).IsRequired();
            Property(x => x.CreatedAt).IsRequired();
            Property(x => x.Longitude).IsRequired();
            Property(x => x.Latitude).IsRequired();

            HasRequired(x => x.Activity).WithMany(x => x.Photos).HasForeignKey(x => x.ActivityId);
        }
    }
}
