﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Strava
{
    internal class GearConfig : AbstractEntityConfig<Gear>
    {
        public GearConfig()
            : base("str", "Gear")
        {
            Property(x => x.Id).IsRequired();
            Property(x => x.IsPrimary).IsRequired();
            Property(x => x.Name).IsRequired();
            Property(x => x.Distance).IsRequired();
            Property(x => x.ResourceState).IsRequired();

            HasOptional(x => x.Shoes).WithRequired(x => x.Gear);
            HasOptional(x => x.Bike).WithRequired(x => x.Gear); 
        }
    }
}
