﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Withings;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Withings
{
    internal class SleepMeasureConfig : AbstractEntityConfig<SleepMeasure>
    {
        public SleepMeasureConfig()
            : base("wth", "SleepMeasure")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.StartDate).IsRequired();
            Property(x => x.EndDate).IsRequired();
            Property(x => x.State).IsRequired();
        }
    }
}
