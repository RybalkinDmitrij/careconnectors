﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Withings;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Withings
{
    internal class BodyMeasureItemConfig : AbstractEntityConfig<BodyMeasureItem>
    {
        public BodyMeasureItemConfig()
            : base("wth", "BodyMeasureItem")
        {
            Property(x => x.Value).IsRequired();
            Property(x => x.Type).IsRequired();
            Property(x => x.Unit).IsRequired();

            HasRequired(x => x.BodyMeasure).WithMany(x => x.Measures).HasForeignKey(x => x.BodyMeasureId);
        }
    }
}
