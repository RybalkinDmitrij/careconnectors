﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Withings;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Withings
{
    internal class BodyMeasureConfig : AbstractEntityConfig<BodyMeasure>
    {
        public BodyMeasureConfig()
            : base("wth", "BodyMeasure")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.GrpId).IsRequired();
            Property(x => x.Attrib).IsRequired();
            Property(x => x.Date).IsRequired();
            Property(x => x.Category).IsRequired();

            Property(x => x.Type).IsRequired();
        }
    }
}
