﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Withings;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Withings
{
    internal class SleepSummaryWConfig : AbstractEntityConfig<SleepSummaryW>
    {
        public SleepSummaryWConfig()
            : base("wth", "SleepSummary")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Id).IsRequired();
            Property(x => x.Timezone).IsRequired();
            Property(x => x.Model).IsRequired();
            Property(x => x.StartDate).IsRequired();
            Property(x => x.EndDate).IsRequired();
            Property(x => x.Date).IsRequired();
            Property(x => x.WakeUpDuration).IsRequired();
            Property(x => x.LightSleepDuration).IsRequired();
            Property(x => x.DeepSleepDuration).IsRequired();
            Property(x => x.RemSleepDuration).IsRequired();
            Property(x => x.DurationToSleep).IsRequired();
            Property(x => x.DurationToWakeup).IsRequired();
            Property(x => x.WakeupCount).IsRequired();
            Property(x => x.Modified).IsRequired();
        }
    }
}
