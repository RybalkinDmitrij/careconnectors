﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Withings;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Withings
{
    internal class ActivityMeasureConfig : AbstractEntityConfig<ActivityMeasure>
    {
        public ActivityMeasureConfig()
            : base("wth", "ActivityMeasure")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Date).IsRequired();
            Property(x => x.Steps).IsRequired();
            Property(x => x.Distance).IsRequired();
            Property(x => x.Calories).IsRequired();
            Property(x => x.TotalCalories).IsRequired();
            Property(x => x.Elevation).IsRequired();
            Property(x => x.Soft).IsRequired();
            Property(x => x.Moderate).IsRequired();
            Property(x => x.Intense).IsRequired();
            Property(x => x.Timezone).IsRequired();
        }
    }
}
