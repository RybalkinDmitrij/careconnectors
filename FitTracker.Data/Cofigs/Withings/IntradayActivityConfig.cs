﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Withings;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Withings
{
    internal class IntradayActivityConfig : AbstractEntityConfig<IntradayActivity>
    {
        public IntradayActivityConfig()
            : base("wth", "IntradayActivity")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Name).IsRequired();
            Property(x => x.Calories).IsRequired();
            Property(x => x.Duration).IsRequired();
            Property(x => x.Steps).IsRequired();
            Property(x => x.Elevation).IsRequired();
            Property(x => x.Distance).IsRequired();
        }
    }
}
