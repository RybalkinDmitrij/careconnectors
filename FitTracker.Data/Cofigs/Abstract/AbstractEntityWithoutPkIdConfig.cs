﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Abstract
{
    internal abstract class AbstractEntityWithoutPkIdConfig<T> : EntityTypeConfiguration<T>
        where T : AbstractEntityWithoutPkId
    {
        protected AbstractEntityWithoutPkIdConfig(String tablePrefix, String tableName)
        {
            ToTable(tablePrefix + "_" + tableName);

            Property(x => x.IsDeleted).IsRequired();

            Property(x => x.DateCreate).IsRequired();
            Property(x => x.DateUpdate).IsRequired();
            Property(x => x.DateDelete).IsOptional();

            Property(x => x.UserCreateId).IsRequired();
            Property(x => x.UserUpdateId).IsRequired();
            Property(x => x.UserDeleteId).IsRequired();
        }
    }
}
