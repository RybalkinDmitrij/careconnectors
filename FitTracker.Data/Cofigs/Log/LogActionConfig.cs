﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Log;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Log
{
    internal class LogActionConfig : EntityTypeConfiguration<LogAction>
    {
        public LogActionConfig()
        {
            ToTable("sys_LogAction");

            HasKey(x => x.Id);

            Property(x => x.Action).IsRequired();
            Property(x => x.Url).IsRequired();
            Property(x => x.UserId).IsRequired();

            Property(x => x.Date).IsRequired();
        }
    }
}
