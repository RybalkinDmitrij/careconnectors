﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Log;

namespace FitTracker.Data.Cofigs.Log
{
    internal class LogErrorConfig : EntityTypeConfiguration<LogError>
    {
        public LogErrorConfig()
        {
            ToTable("sys_LogError");

            HasKey(x => x.Id);

            Property(x => x.Text).IsRequired();
            Property(x => x.UserId).IsRequired();

            Property(x => x.Date).IsRequired();
        }
    }
}
