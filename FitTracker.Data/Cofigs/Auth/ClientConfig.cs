﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Auth
{
    internal class ClientConfig : AbstractEntityConfig<Client>
    {
        public ClientConfig()
            : base("ath", "Client")
        {
            Property(x => x.ExternalId).IsRequired();

            Property(x => x.AccessToken).IsRequired().IsMaxLength();

            Property(x => x.AspNetId).IsRequired();
            Property(x => x.AspNetUserName).IsRequired();
            Property(x => x.AspNetPassword).IsRequired();
            
            Property(x => x.FitbitId).IsOptional();
            Property(x => x.FitbitAccessToken).IsRequired();
            Property(x => x.FitbitSecret).IsRequired();

            Property(x => x.WithingsId).IsOptional();
            Property(x => x.WithingsAccessToken).IsRequired();
            Property(x => x.WithingsSecret).IsRequired();

            Property(x => x.JawboneId).IsOptional();
            Property(x => x.JawboneAccessToken).IsRequired();
            Property(x => x.JawboneRefreshToken).IsRequired();

            Property(x => x.RunkeeperId).IsOptional();
            Property(x => x.RunkeeperAccessToken).IsRequired();
            Property(x => x.RunkeeperRefreshToken).IsRequired();

            Property(x => x.StravaId).IsOptional();
            Property(x => x.StravaAccessToken).IsRequired();
            Property(x => x.StravaRefreshToken).IsRequired();

            Property(x => x.MisfitId).IsOptional();
            Property(x => x.MisfitAccessToken).IsRequired();
            Property(x => x.MisfitRefreshToken).IsRequired();

            //Property(x => x.ApplicantId).IsRequired();
            HasRequired(x => x.Applicant).WithMany(x => x.Clients).HasForeignKey(x => x.ApplicantId);
        }
    }
}
