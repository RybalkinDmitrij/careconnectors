﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Auth
{
    internal class DbUserConfig : AbstractEntityConfig<DbUser>
    {
        public DbUserConfig()
            : base("ath", "DbUser")
        {
            Property(x => x.Login).IsRequired();
            Property(x => x.PasswordHash).IsRequired();

            Property(x => x.FirstName).IsRequired();
            Property(x => x.LastName).IsRequired();
        }
    }
}
