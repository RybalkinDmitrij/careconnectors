﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Auth
{
    internal class ApplicantConfig : AbstractEntityConfig<Applicant>
    {
        public ApplicantConfig()
            : base("ath", "Applicant")
        {
            Property(x => x.UniqueId).IsRequired();
            Property(x => x.ConsumerKey).IsRequired();
            Property(x => x.ConsumerSecret).IsRequired();

            Property(x => x.NotificationUrl).IsRequired();

            HasRequired(x => x.User).WithMany(x => x.Applicants).HasForeignKey(x => x.UserId);
        }
    }
}
