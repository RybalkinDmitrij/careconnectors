﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class UserProfileConfig : AbstractEntityConfig<UserProfile>
    {
        public UserProfileConfig()
            : base("fbt", "UserProfile")
        {
            Property(x => x.EncodedId).IsRequired().HasMaxLength(255);
            Property(x => x.DisplayName).IsRequired().HasMaxLength(255);
            Property(x => x.Gender).IsRequired();
            Property(x => x.DateOfBirth).IsRequired();
            Property(x => x.Height).IsRequired();
            Property(x => x.Weight).IsRequired();
            Property(x => x.StrideLengthWalking).IsRequired();
            Property(x => x.StrideLengthRunning).IsRequired();
            Property(x => x.FullName).IsRequired().HasMaxLength(255);
            Property(x => x.Nickname).IsRequired().HasMaxLength(255);
            Property(x => x.Country).IsRequired().HasMaxLength(255);
            Property(x => x.State).IsRequired().HasMaxLength(255);
            Property(x => x.City).IsRequired().HasMaxLength(255);
            Property(x => x.AboutMe).IsRequired().IsMaxLength();
            Property(x => x.MemberSince).IsRequired();
            Property(x => x.Timezone).IsRequired().HasMaxLength(255);
            Property(x => x.OffsetFromUTCMillis).IsRequired();
            Property(x => x.Locale).IsRequired();
            Property(x => x.Avatar).IsRequired();
            Property(x => x.WeightUnit).IsRequired();
            Property(x => x.DistanceUnit).IsRequired();
            Property(x => x.HeightUnit).IsRequired();
            Property(x => x.WaterUnit).IsRequired();
            Property(x => x.GlucoseUnit).IsRequired();
            Property(x => x.VolumeUnit).IsRequired();
        }
    }
}
