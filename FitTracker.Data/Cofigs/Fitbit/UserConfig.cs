﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class UserConfig : AbstractEntityConfig<User>
    {
        public UserConfig()
            : base("fbt", "User")
        {
            Property(x => x.DateOfBirth).IsRequired();
            Property(x => x.DisplayName).IsRequired().HasMaxLength(255);
            Property(x => x.EncodedId).IsRequired().HasMaxLength(255);
            Property(x => x.Gender).IsRequired().HasMaxLength(255);
            Property(x => x.Height).IsRequired();
            Property(x => x.OffsetFromUTCMillis).IsRequired();
            Property(x => x.Weight).IsRequired();
        }
    }
}
