﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class FoodSummaryConfig : AbstractEntityConfig<FoodSummary>
    {
        public FoodSummaryConfig()
            : base("fbt", "FoodSummary")
        {
            Property(x => x.UserId).IsRequired();
            Property(x => x.Date).IsRequired();
            Property(x => x.Calories).IsRequired();
            Property(x => x.Carbs).IsRequired();
            Property(x => x.Fat).IsRequired();
            Property(x => x.Fiber).IsRequired();
            Property(x => x.Protein).IsRequired();
            Property(x => x.Sodium).IsRequired();
            Property(x => x.Water).IsRequired();
        }
    }
}
