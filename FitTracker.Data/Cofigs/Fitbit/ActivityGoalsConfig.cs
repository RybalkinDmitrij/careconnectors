﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class ActivityGoalsConfig : AbstractEntityConfig<ActivityGoals>
    {
        public ActivityGoalsConfig()
            : base("fbt", "ActivityGoals")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Date).IsRequired();

            Property(x => x.CaloriesOut).IsRequired();
            Property(x => x.Distance).IsRequired();
            Property(x => x.Floors).IsOptional();
            Property(x => x.Steps).IsRequired();
        }
    }
}
