﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class ActivityDistanceConfig : AbstractEntityConfig<ActivityDistance>
    {
        public ActivityDistanceConfig()
            : base("fbt", "ActivityDistance")
        {
            Property(x => x.Activity).IsRequired();
            Property(x => x.Distance).IsRequired();

            HasRequired(x => x.ActivitySummary).WithMany(x => x.Distances).HasForeignKey(x => x.ActivitySummaryId);
        }
    }
}
