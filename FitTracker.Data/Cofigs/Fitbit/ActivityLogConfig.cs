﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class ActivityLogConfig : AbstractEntityConfig<ActivityLog>
    {
        public ActivityLogConfig()
            : base("fbt", "ActivityLog")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Date).IsRequired();

            Property(x => x.ActivityId).IsRequired();
            Property(x => x.ActivityParentId).IsRequired();
            Property(x => x.Calories).IsRequired();
            Property(x => x.Description).IsRequired();
            Property(x => x.Distance).IsRequired();
            Property(x => x.Duration).IsRequired();
            Property(x => x.HasStartTime).IsRequired();
            Property(x => x.IsFavorite).IsRequired();
            Property(x => x.LogId).IsRequired();
            Property(x => x.Name).IsRequired();
            Property(x => x.StartTime).IsRequired();
            Property(x => x.Steps).IsRequired();
        }
    }
}
