﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class FoodGoalsConfig : AbstractEntityConfig<FoodGoals>
    {
        public FoodGoalsConfig()
            : base("fbt", "FoodGoals")
        {
            Property(x => x.UserId).IsRequired();
            Property(x => x.Date).IsRequired();
            Property(x => x.Calories).IsRequired();
        }
    }
}
