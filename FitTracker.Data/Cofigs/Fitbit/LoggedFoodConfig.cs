﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class LoggedFoodConfig : AbstractEntityConfig<LoggedFood>
    {
        public LoggedFoodConfig()
            : base("fbt", "LoggedFood")
        {
            Property(x => x.AccessLevel).IsRequired();
            Property(x => x.Amount).IsRequired();
            Property(x => x.Brand).IsRequired();
            Property(x => x.Calories).IsRequired();
            Property(x => x.FoodId).IsRequired();
            Property(x => x.MealTypeId).IsRequired();
            Property(x => x.Locale).IsRequired();
            Property(x => x.Name).IsRequired();
            
            HasOptional(x => x.Unit).WithMany(x => x.LoggedFoods).HasForeignKey(x => x.UnitId);
        }
    }
}
