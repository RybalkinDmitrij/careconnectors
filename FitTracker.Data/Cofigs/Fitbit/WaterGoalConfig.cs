﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class WaterGoalConfig : AbstractEntityConfig<WaterGoal>
    {
        public WaterGoalConfig()
            : base("fbt", "WaterGoal")
        {
            Property(x => x.UserId).IsRequired();
            Property(x => x.Goal).IsRequired();
        }
    }
}
