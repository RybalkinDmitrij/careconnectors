﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class SleepSummaryFConfig : AbstractEntityConfig<SleepSummaryF>
    {
        public SleepSummaryFConfig()
            : base("fbt", "SleepSummary")
        {
            Property(x => x.UserId).IsRequired();
            Property(x => x.Date).IsRequired();
            Property(x => x.TotalSleepRecords).IsRequired();
            Property(x => x.TotalMinutesAsleep).IsRequired();
            Property(x => x.TotalTimeInBed).IsRequired();
        }
    }
}
