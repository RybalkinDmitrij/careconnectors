﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class FoodLogUnitConfig : AbstractEntityConfig<FoodLogUnit>
    {
        public FoodLogUnitConfig()
            : base("fbt", "FoodLogUnit")
        {
            Property(x => x.Id).IsRequired();
            Property(x => x.Name).IsRequired();
            Property(x => x.Plural).IsRequired();
        }
    }
}
