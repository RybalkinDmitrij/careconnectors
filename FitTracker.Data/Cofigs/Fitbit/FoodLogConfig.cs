﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class FoodLogConfig : AbstractEntityConfig<FoodLog>
    {
        public FoodLogConfig()
            : base("fbt", "FoodLog")
        {
            Property(x => x.UserId).IsRequired();
            Property(x => x.IsFavorite).IsRequired();
            Property(x => x.LogDate).IsRequired();
            Property(x => x.LogId).IsRequired();

            HasOptional(x => x.LoggedFood).WithMany(x => x.FoodLogs).HasForeignKey(x => x.LoggedFoodId);
            HasOptional(x => x.NutritionalValues).WithMany(x => x.FoodLogs).HasForeignKey(x => x.NutritionalValuesId);
        }
    }
}
