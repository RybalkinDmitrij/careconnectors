﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class SleepLogConfig : AbstractEntityConfig<SleepLog>
    {
        public SleepLogConfig()
            : base("fbt", "SleepLog")
        {
            Property(x => x.UserId).IsRequired();
            Property(x => x.Date).IsRequired();

            Property(x => x.IsMainSleep).IsRequired();
            Property(x => x.LogId).IsRequired();
            Property(x => x.Efficiency).IsRequired();
            Property(x => x.StartTime).IsRequired();
            Property(x => x.Duration).IsRequired();
            Property(x => x.MinutesToFallAsleep).IsRequired();
            Property(x => x.MinutesAsleep).IsRequired();
            Property(x => x.MinutesAwake).IsRequired();
            Property(x => x.MinutesAfterWakeup).IsRequired();
            Property(x => x.AwakeningsCount).IsRequired();
            Property(x => x.AwakeCount).IsRequired();
            Property(x => x.AwakeDuration).IsRequired();
            Property(x => x.RestlessCount).IsRequired();
            Property(x => x.RestlessDuration).IsRequired();
            Property(x => x.TimeInBed).IsRequired();
        }
    }
}
