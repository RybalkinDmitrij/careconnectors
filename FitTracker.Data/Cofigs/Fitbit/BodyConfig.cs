﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class BodyConfig : AbstractEntityConfig<Body>
    {
        public BodyConfig()
            : base("fbt", "Body")
        {
            Property(x => x.UserId).IsRequired();
            Property(x => x.Date).IsRequired();

            Property(x => x.Bicep).IsRequired();
            Property(x => x.BMI).IsRequired();
            Property(x => x.Calf).IsRequired();
            Property(x => x.Chest).IsRequired();
            Property(x => x.Fat).IsRequired();
            Property(x => x.Forearm).IsRequired();
            Property(x => x.Hips).IsRequired();
            Property(x => x.Neck).IsRequired();
            Property(x => x.Thigh).IsRequired();
            Property(x => x.Waist).IsRequired();
            Property(x => x.Weight).IsRequired();
        }
    }
}
