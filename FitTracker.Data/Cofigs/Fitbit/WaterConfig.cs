﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class WaterConfig : AbstractEntityConfig<Water>
    {
        public WaterConfig()
            : base("fbt", "Water")
        {
            Property(x => x.UserId).IsRequired();
            Property(x => x.LogId).IsRequired();
            Property(x => x.Amount).IsRequired();
        }
    }
}
