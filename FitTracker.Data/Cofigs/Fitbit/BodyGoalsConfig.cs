﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class BodyGoalsConfig : AbstractEntityConfig<BodyGoals>
    {
        public BodyGoalsConfig()
            : base("fbt", "BodyGoals")
        {
            Property(x => x.UserId).IsRequired();
            Property(x => x.Date).IsRequired();

            Property(x => x.Weight).IsRequired();
        }
    }
}
