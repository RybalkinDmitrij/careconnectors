﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class ActivitySummaryConfig : AbstractEntityConfig<ActivitySummary>
    {
        public ActivitySummaryConfig()
            : base("fbt", "ActivitySummary")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Date).IsRequired();

            Property(x => x.ActivityCalories).IsRequired();
            Property(x => x.CaloriesBMR).IsRequired();
            Property(x => x.Elevation).IsRequired();
            Property(x => x.Floors).IsRequired();
            Property(x => x.MarginalCalories).IsRequired();
            Property(x => x.VeryActiveMinutes).IsRequired();
            Property(x => x.CaloriesOut).IsRequired();
            Property(x => x.FairlyActiveMinutes).IsRequired();
            Property(x => x.LightlyActiveMinutes).IsRequired();
            Property(x => x.SedentaryMinutes).IsRequired();
            Property(x => x.Steps).IsRequired();
        }
    }
}
