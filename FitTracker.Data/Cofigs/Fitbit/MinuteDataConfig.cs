﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class MinuteDataConfig : AbstractEntityConfig<MinuteData>
    {
        public MinuteDataConfig()
            : base("fbt", "MinuteData")
        {
            Property(x => x.DateTime).IsRequired();
            Property(x => x.Value).IsRequired();

            HasRequired(x => x.SleepLog).WithMany(x => x.MinuteData).HasForeignKey(x => x.SleepLogId);
        }
    }
}
