﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class FatLogConfig : AbstractEntityConfig<FatLog>
    {
        public FatLogConfig()
            : base("fbt", "FatLog")
        {
            Property(x => x.UserId).IsRequired();
            Property(x => x.Date).IsRequired();
            Property(x => x.Fat).IsRequired();
            Property(x => x.LogId).IsRequired();
            Property(x => x.Time).IsRequired();
        }
    }
}
