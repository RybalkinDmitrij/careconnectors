﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class NutritionalValuesConfig : AbstractEntityConfig<NutritionalValues>
    {
        public NutritionalValuesConfig()
            : base("fbt", "NutritionalValues")
        {
            Property(x => x.Calories).IsRequired();
            Property(x => x.Carbs).IsRequired();
            Property(x => x.Fat).IsRequired();
            Property(x => x.Fiber).IsRequired();
            Property(x => x.Protein).IsRequired();
            Property(x => x.Sodium).IsRequired();
        }
    }
}
