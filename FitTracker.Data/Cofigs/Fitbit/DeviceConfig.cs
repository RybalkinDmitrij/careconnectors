﻿using System.Data.Entity.ModelConfiguration;

using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Cofigs.Abstract;

namespace FitTracker.Data.Cofigs.Fitbit
{
    internal class DeviceConfig : AbstractEntityConfig<Device>
    {
        public DeviceConfig()
            : base("fbt", "Device")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Id).IsRequired();
            Property(x => x.Battery).IsRequired();
            Property(x => x.LastSyncTime).IsRequired();
            Property(x => x.Type).IsRequired();
            Property(x => x.DeviceVersion).IsRequired();
        }
    }
}
