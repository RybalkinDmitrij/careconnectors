﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Jawbone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Jawbone
{
    internal class MovesTickConfig : AbstractEntityConfig<MovesTick>
    {
        public MovesTickConfig()
            : base("jwb", "MovesTick")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Distance).IsRequired();
            Property(x => x.TimeCompleted).IsRequired();
            Property(x => x.ActiveTime).IsRequired();
            Property(x => x.Calories).IsRequired();
            Property(x => x.Steps).IsRequired();
            Property(x => x.Time).IsRequired();
            Property(x => x.Speed).IsRequired();
        }
    }
}
