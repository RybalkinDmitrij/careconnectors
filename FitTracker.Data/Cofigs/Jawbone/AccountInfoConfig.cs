﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Jawbone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Jawbone
{
    internal class AccountInfoConfig : AbstractEntityConfig<AccountInfo>
    {
        public AccountInfoConfig()
            : base("jwb", "AccountInfo")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Xid).IsRequired();
            Property(x => x.First).IsRequired();
            Property(x => x.Last).IsRequired();
            Property(x => x.Image).IsRequired();
            Property(x => x.Weight).IsRequired();
            Property(x => x.Height).IsRequired();
        }
    }
}
