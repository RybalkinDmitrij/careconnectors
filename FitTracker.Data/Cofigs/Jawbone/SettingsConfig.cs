﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Jawbone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Jawbone
{
    internal class SettingsConfig : AbstractEntityConfig<Settings>
    {
        public SettingsConfig()
            : base("jwb", "Settings")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Xid).IsRequired();
            Property(x => x.Metric).IsRequired();
            Property(x => x.ShareSleep).IsRequired();
            Property(x => x.ShareMood).IsRequired();
            Property(x => x.ShareBody).IsRequired();
            Property(x => x.ShareEat).IsRequired();
            Property(x => x.ShareMove).IsRequired();
            Property(x => x.Steps).IsRequired();
            Property(x => x.Total).IsRequired();
        }
    }
}
