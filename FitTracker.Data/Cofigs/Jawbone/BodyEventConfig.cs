﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Jawbone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Jawbone
{
    internal class BodyEventConfig : AbstractEntityConfig<BodyEvent>
    {
        public BodyEventConfig()
            : base("jwb", "BodyEvent")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Xid).IsRequired();
            Property(x => x.Title).IsRequired();
            Property(x => x.Type).IsRequired();
            Property(x => x.TimeCreated).IsRequired();
            Property(x => x.TimeUpdated).IsRequired();
            Property(x => x.Date).IsRequired();
            Property(x => x.PlaceLat).IsRequired();
            Property(x => x.PlaceLon).IsRequired();
            Property(x => x.PlaceAcc).IsRequired();
            Property(x => x.PlaceName).IsRequired();
            Property(x => x.Note).IsRequired();
            Property(x => x.LeanMass).IsRequired();
            Property(x => x.Weight).IsRequired();
            Property(x => x.BodyFat).IsRequired();
            Property(x => x.BMI).IsRequired();
            Property(x => x.Timezone).IsRequired();
        }
    }
}
