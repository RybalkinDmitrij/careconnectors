﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Jawbone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Jawbone
{
    internal class HourlyTotalConfig : AbstractEntityConfig<HourlyTotal>
    {
        public HourlyTotalConfig()
            : base("jwb", "HourlyTotal")
        {
            Property(x => x.Distance).IsRequired();
            Property(x => x.Calories).IsRequired();
            Property(x => x.Steps).IsRequired();
            Property(x => x.ActiveTime).IsRequired();
            Property(x => x.InactiveTime).IsRequired();
            Property(x => x.LongestActiveTime).IsRequired();
            Property(x => x.LongestIdleTime).IsRequired();

            HasRequired(x => x.Moves).WithMany(x => x.HourlyTotals).HasForeignKey(x => x.MovesId);
        }
    }
}
