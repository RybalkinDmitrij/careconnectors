﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Jawbone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Jawbone
{
    internal class MovesConfig : AbstractEntityConfig<Moves>
    {
        public MovesConfig()
            : base("jwb", "Moves")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Xid).IsRequired();
            Property(x => x.Title).IsRequired();
            Property(x => x.Type).IsRequired();
            Property(x => x.TimeCreated).IsRequired();
            Property(x => x.TimeUpdated).IsRequired();
            Property(x => x.TimeCompleted).IsRequired();
            Property(x => x.Date).IsRequired();
            Property(x => x.SnapshotImage).IsRequired();
            Property(x => x.Distance).IsRequired();
            Property(x => x.Km).IsRequired();
            Property(x => x.Steps).IsRequired();
            Property(x => x.ActiveTime).IsRequired();
            Property(x => x.LongestActive).IsRequired();
            Property(x => x.InactiveTime).IsRequired();
            Property(x => x.LongestIdle).IsRequired();
            Property(x => x.Calories).IsRequired();
            Property(x => x.BmrDay).IsRequired();
            Property(x => x.Bmr).IsRequired();
            Property(x => x.BgCalories).IsRequired();
            Property(x => x.WoCalories).IsRequired();
            Property(x => x.WoTime).IsRequired();
            Property(x => x.WoActiveTime).IsRequired();
            Property(x => x.WoCount).IsRequired();
            Property(x => x.WoLongest).IsRequired();
            Property(x => x.Sunrise).IsRequired();
            Property(x => x.Sunset).IsRequired();
            Property(x => x.Timezone).IsRequired();
        }
    }
}
