﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Jawbone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Jawbone
{
    internal class WorkoutsConfig : AbstractEntityConfig<Workouts>
    {
        public WorkoutsConfig()
            : base("jwb", "Workouts")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Xid).IsRequired();
            Property(x => x.Title).IsRequired();
            Property(x => x.Type).IsRequired();
            Property(x => x.SubType).IsRequired();
            Property(x => x.TimeCreated).IsRequired();
            Property(x => x.TimeUpdated).IsRequired();
            Property(x => x.TimeCompleted).IsRequired();
            Property(x => x.Date).IsRequired();
            Property(x => x.PlaceLat).IsRequired();
            Property(x => x.PlaceLon).IsRequired();
            Property(x => x.PlaceAcc).IsRequired();
            Property(x => x.PlaceName).IsRequired();
            Property(x => x.Route).IsRequired();
            Property(x => x.Image).IsRequired();
            Property(x => x.SnapshotImage).IsRequired();

            Property(x => x.Steps).IsRequired();
            Property(x => x.Time).IsRequired();
            Property(x => x.BgActiveTime).IsRequired();
            Property(x => x.Meters).IsRequired();
            Property(x => x.Km).IsRequired();
            Property(x => x.Intensity).IsRequired();
            Property(x => x.Calories).IsRequired();
            Property(x => x.Bmr).IsRequired();
            Property(x => x.BgCalories).IsRequired();
            Property(x => x.BmrCalories).IsRequired();
            Property(x => x.Timezone).IsRequired();

            Ignore(x => x.SubTypeValue);
            Ignore(x => x.IntensityValue);
        }
    }
}
