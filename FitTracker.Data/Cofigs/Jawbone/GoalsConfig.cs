﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Jawbone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Jawbone
{
    internal class GoalsConfig : AbstractEntityConfig<Goals>
    {
        public GoalsConfig()
            : base("jwb", "Goals")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.MoveSteps).IsRequired();
            Property(x => x.SleepTotal).IsRequired();
            Property(x => x.BodyWeight).IsRequired();
            Property(x => x.BodyWeightIntent).IsRequired();
            Property(x => x.IntakeCaloriesRemaining).IsRequired();
            Property(x => x.MoveStepsRemaining).IsRequired();
            Property(x => x.SleepSecondsRemaining).IsRequired();
        }
    }
}
