﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Jawbone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Jawbone
{
    internal class TimezoneConfig : AbstractEntityConfig<Timezone>
    {
        public TimezoneConfig()
            : base("jwb", "Timezone")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Date).IsRequired();
            Property(x => x.Tz).IsRequired();
            Property(x => x.Time).IsRequired();
        }
    }
}
