﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Jawbone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Jawbone
{
    internal class SleepsConfig : AbstractEntityConfig<Sleeps>
    {
        public SleepsConfig()
            : base("jwb", "Sleeps")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Xid).IsRequired();
            Property(x => x.Title).IsRequired();
            Property(x => x.SubType).IsRequired();
            Property(x => x.Type).IsRequired();
            Property(x => x.TimeCreated).IsRequired();
            Property(x => x.TimeCompleted).IsRequired();
            Property(x => x.Date).IsRequired();
            Property(x => x.PlaceLat).IsRequired();
            Property(x => x.PlaceLon).IsRequired();
            Property(x => x.PlaceAcc).IsRequired();
            Property(x => x.PlaceName).IsRequired();
            Property(x => x.SnapshotImage).IsRequired();
        }
    }
}
