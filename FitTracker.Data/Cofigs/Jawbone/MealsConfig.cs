﻿using FitTracker.Data.Cofigs.Abstract;
using FitTracker.Data.Entities.Jawbone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Cofigs.Jawbone
{
    internal class MealsConfig : AbstractEntityConfig<Meals>
    {
        public MealsConfig()
            : base("jwb", "Meals")
        {
            Property(x => x.UserId).IsRequired();

            Property(x => x.Xid).IsRequired();
            Property(x => x.Title).IsRequired();
            Property(x => x.Note).IsRequired();
            Property(x => x.Type).IsRequired();
            Property(x => x.SubType).IsRequired();
            Property(x => x.TimeCreated).IsRequired();
            Property(x => x.TimeUpdated).IsRequired();
            Property(x => x.TimeCompleted).IsRequired();
            Property(x => x.Date).IsRequired();
            Property(x => x.PlaceLat).IsRequired();
            Property(x => x.PlaceLon).IsRequired();
            Property(x => x.PlaceAcc).IsRequired();
            Property(x => x.PlaceName).IsRequired();
            Property(x => x.NumDrinks).IsRequired();
            Property(x => x.NumWater).IsRequired();
            Property(x => x.NumFoods).IsRequired();
            Property(x => x.OnlyWaters).IsRequired();
            Property(x => x.NumMealitemsGreen).IsRequired();
            Property(x => x.NumMealitemsYellow).IsRequired();
            Property(x => x.NumMealitemsRed).IsRequired();
            Property(x => x.NumMealitemsWithScore).IsRequired();
            Property(x => x.Fiber).IsRequired();
            Property(x => x.PolyunsaturatedFat).IsRequired();
            Property(x => x.Potassium).IsRequired();
            Property(x => x.Fat).IsRequired();
            Property(x => x.Carbohydrate).IsRequired();
            Property(x => x.SaturatedFat).IsRequired();
            Property(x => x.Protein).IsRequired();
            Property(x => x.MonounsaturatedFat).IsRequired();
            Property(x => x.Sodium).IsRequired();
            Property(x => x.VitaminC).IsRequired();
            Property(x => x.VitaminA).IsRequired();
            Property(x => x.Calories).IsRequired();
            Property(x => x.UnsaturatedFat).IsRequired();
            Property(x => x.Sugar).IsRequired();
            Property(x => x.Calcium).IsRequired();
            Property(x => x.Iron).IsRequired();
            Property(x => x.Cholesterol).IsRequired();
            Property(x => x.Caffeine).IsRequired();
            Property(x => x.Accuracy).IsRequired();
            Property(x => x.Timezone).IsRequired();
        }
    }
}
