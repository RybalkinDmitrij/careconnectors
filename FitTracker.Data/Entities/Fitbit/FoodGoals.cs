﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class FoodGoals : AbstractEntity
    {
        public String UserId { get; set; }

        public DateTime Date { get; set; }

        public Int32 Calories { get; set; }
    }
}
