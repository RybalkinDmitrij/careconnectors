﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Auth;
using FitTracker.Data.Enums.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class Device : AbstractEntity
    {
        public String UserId { get; set; }

        public String Id { get; set; }

        public String Battery { get; set; }

        public DateTime LastSyncTime { get; set; }

        public DeviceType Type { get; set; }

        public String DeviceVersion { get; set; }
    }
}
