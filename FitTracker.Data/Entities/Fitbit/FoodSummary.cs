﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class FoodSummary : AbstractEntity
    {
        public String UserId { get; set; }

        public DateTime Date { get; set; }

        public Decimal Calories { get; set; }

        public Decimal Carbs { get; set; }

        public Decimal Fat { get; set; }

        public Decimal Fiber { get; set; }

        public Decimal Protein { get; set; }

        public Decimal Sodium { get; set; }

        public Decimal Water { get; set; }
    }
}
