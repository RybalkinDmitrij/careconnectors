﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class FoodLog : AbstractEntity
    {
        public String UserId { get; set; }

        public Boolean IsFavorite { get; set; }

        public DateTime LogDate { get; set; }

        public Int64 LogId { get; set; }

        public Int64? LoggedFoodId { get; set; }
        
        public virtual LoggedFood LoggedFood { get; set; }

        public Int64? NutritionalValuesId { get; set; }

        public virtual NutritionalValues NutritionalValues { get; set; }
    }
}
