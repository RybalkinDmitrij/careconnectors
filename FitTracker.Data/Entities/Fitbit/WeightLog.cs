﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class WeightLog : AbstractEntity
    {
        public String UserId { get; set; }

        public Int64 LogId { get; set; }

        public Decimal Bmi { get; set; }

        public DateTime Date { get; set; }

        public TimeSpan Time { get; set; }

        public Decimal Weight { get; set; }
    }
}
