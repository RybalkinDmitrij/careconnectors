﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;

namespace FitTracker.Data.Entities.Fitbit
{
    public class Body : AbstractEntity
    {
        public String UserId { get; set; }

        public DateTime Date { get; set; }

        public Decimal Bicep { get; set; }

        public Decimal BMI { get; set; }

        public Decimal Calf { get; set; }

        public Decimal Chest { get; set; }

        public Decimal Fat { get; set; }

        public Decimal Forearm { get; set; }

        public Decimal Hips { get; set; }

        public Decimal Neck { get; set; }

        public Decimal Thigh { get; set; }

        public Decimal Waist { get; set; }

        public Decimal Weight { get; set; }
    }
}
