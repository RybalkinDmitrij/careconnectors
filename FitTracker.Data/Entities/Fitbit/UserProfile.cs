﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class UserProfile : AbstractEntity
    {
        public String EncodedId { get; set; }

        public String DisplayName { get; set; }

        public Gender Gender { get; set; }

        public DateTime DateOfBirth { get; set; }

        public Double Height { get; set; }

        public Double Weight { get; set; }

        public Double StrideLengthWalking { get; set; }

        public Double StrideLengthRunning { get; set; }

        public String FullName { get; set; }

        public String Nickname { get; set; }

        public String Country { get; set; }

        public String State { get; set; }

        public String City { get; set; }

        public String AboutMe { get; set; }

        public DateTime MemberSince { get; set; }

        public String Timezone { get; set; }
        
        public Int32 OffsetFromUTCMillis { get; set; }

        public String Locale { get; set; }

        public String Avatar { get; set; }

        public String WeightUnit { get; set; }

        public String DistanceUnit { get; set; }

        public String HeightUnit { get; set; }

        public String WaterUnit { get; set; }

        public String GlucoseUnit { get; set; }

        public String VolumeUnit { get; set; }
    }
}
