﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class ActivityLog : AbstractEntity
    {
        public String UserId { get; set; }

        public DateTime Date { get; set; }

        public Int64 ActivityId {get; set;}

        public Int64 ActivityParentId { get; set; }

        public Int32 Calories { get; set; }

        public String Description { get; set; }

        public Decimal Distance { get; set; }

        public Int64 Duration { get; set; }

        public Boolean HasStartTime { get; set; }

        public Boolean IsFavorite { get; set; }

        public Int64 LogId { get; set; }

        public String Name { get; set; }

        public String StartTime { get; set; }

        public Int32 Steps { get; set; }
    }
}
