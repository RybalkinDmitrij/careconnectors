﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    /// <summary>
    /// This is a needed entity for RestSharp matching of the GetFriends call. 
    /// A friend is a user profile, but in XML it is a contained node
    /// </summary>
    public class Friend : AbstractEntity
    {
        public Int64 UserId { get; set; }

        public virtual UserProfile User { get; set; }
    }
}
