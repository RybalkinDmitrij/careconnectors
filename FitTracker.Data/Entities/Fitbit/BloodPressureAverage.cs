﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Fitbit
{
    public class BloodPressureAverage : AbstractEntity
    {
        public String Condition { get; set; }

        public Int32 Diastolic { get; set; }

        public Int32 Systolic { get; set; }
    }
}
