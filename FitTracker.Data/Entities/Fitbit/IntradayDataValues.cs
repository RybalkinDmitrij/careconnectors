﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class IntradayDataValues : AbstractEntity
    {
        public DateTime Time { get; set; }

        public String Value { get; set; }

        public String Level { get; set; }

        public String METs { get; set; }
    }
}
