﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FitTracker.Data.Entities.Fitbit
{
    public class User : AbstractEntity
	{
		public DateTime DateOfBirth { get; set; }

		public String DisplayName { get; set; }

		public String EncodedId { get; set; }

		public String Gender { get; set; }

		public Decimal Height { get; set; }

		public Int64 OffsetFromUTCMillis { get; set; }

		public Decimal Weight { get; set; }
	}
}