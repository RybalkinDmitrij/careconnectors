﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Fitbit
{
    public class BloodPressure : AbstractEntity
    {
        public Int32 Diastolic { get; set; }

        public Int32 LogId { get; set; }

        public Int32 Systolic { get; set; }

        public DateTime Time { get; set; }
    }
}
