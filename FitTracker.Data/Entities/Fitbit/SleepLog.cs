﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class SleepLog : AbstractEntity
    {
        public String UserId { get; set; }

        public DateTime Date { get; set; }

        public Int32 AwakeningsCount { get; set; }

        public Int32 Duration { get; set; }

        public Int32 Efficiency { get; set; }

        public Boolean IsMainSleep { get; set; }

        public Int64 LogId { get; set; }

        public Int32 MinutesAfterWakeup { get; set; }
        
        public Int32 MinutesAsleep { get; set; }
        
        public Int32 MinutesAwake { get; set; }
        
        public Int32 MinutesToFallAsleep { get; set; }
        
        public DateTime StartTime { get; set; }
        
        public Int32 TimeInBed { get; set; }
        
        public Int32 AwakeCount { get; set; }
        
        public Int32 AwakeDuration { get; set; }
        
        public Int32 RestlessCount { get; set; }
        
        public Int32 RestlessDuration { get; set; }

        public virtual ICollection<MinuteData> MinuteData { get; set; }
    }
}
