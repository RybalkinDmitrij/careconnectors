﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class UpdatedResource : AbstractEntity
    {
        public APICollectionType CollectionType { get; set; }

        public DateTime Date { get; set; }

        public String OwnerId { get; set; }

        public ResourceOwnerType OwnerType { get; set; }

        public String SubscriptionId { get; set; }
    }
}
