﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class FatLog : AbstractEntity
    {
        public String UserId { get; set; }

        public Int64 LogId { get; set; }

        public DateTime Date { get; set; }

        public DateTime Time { get; set; }

        public Decimal Fat { get; set; }
    }
}
