﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class Food : AbstractEntity
    {
        public Int64 SummaryId { get; set; }

        public virtual FoodSummary Summary { get; set; }

        public Int64 GoalsId { get; set; }

        public virtual FoodGoals Goals { get; set; }

        public virtual ICollection<FoodLog> Foods { get; set; }
    }
}
