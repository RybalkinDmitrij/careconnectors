﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class TimeSeriesData : AbstractEntity
    {
        public DateTime DateTime { get; set; }

        public String Value { get; set; }
    }
}
