﻿using FitTracker.Data.Entities.Abstract;
using System;

namespace FitTracker.Data.Entities.Fitbit
{
    public class MinuteData : AbstractEntity
    {
        public DateTime DateTime { get; set; }

        public Int32 Value { get; set; }

        public Int64 SleepLogId { get; set; }

        public virtual SleepLog SleepLog { get; set; }
    }
}