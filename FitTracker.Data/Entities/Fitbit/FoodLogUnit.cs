﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Fitbit
{
    public class FoodLogUnit : AbstractEntity
    {
        public Int32 Id { get; set; }

        public String Name { get; set; }

        public String Plural { get; set; }

        public virtual ICollection<LoggedFood> LoggedFoods { get; set; }
    }
}
