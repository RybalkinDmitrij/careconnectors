﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class ActivityGoals : AbstractEntity
    {
        public String UserId { get; set; }

        public DateTime Date { get; set; }

        public Int32 CaloriesOut { get; set; }

        public Int32 Steps { get; set; }

        public Double Distance { get; set; }

        public Int32? Floors { get; set; }
    }
}
