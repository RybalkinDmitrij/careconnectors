﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;

namespace FitTracker.Data.Entities.Fitbit
{
    public class BodyGoals : AbstractEntity
    {
        public String UserId { get; set; }

        public DateTime Date { get; set; }

        public Decimal Weight { get; set; }
    }
}
