﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Fitbit
{
    public class ApiError : AbstractEntity
    {
        public ErrorType ErrorType { get; set; }

        public String FieldName { get; set; }

        public String Message { get; set; }
    }
}
