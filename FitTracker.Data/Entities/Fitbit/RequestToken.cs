﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Fitbit
{
    public class RequestToken : AbstractEntity
    {
        public String Token { get; set; }

        public String Secret { get; set; }

        public String Verifier { get; set; }
    }
}
