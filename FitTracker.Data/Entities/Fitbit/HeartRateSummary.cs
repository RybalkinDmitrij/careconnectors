﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Fitbit
{
    public class HeartRateSummary : AbstractEntity
    {
        public Int32 HeartRate { get; set; }

        public String Tracker { get; set; }
    }
}
