﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class LoggedFood : AbstractEntity
    {
        public String AccessLevel { get; set; }

        public Decimal Amount { get; set; }

        public String Brand { get; set; }

        public Decimal Calories { get; set; }

        public Int64 FoodId { get; set; }

        public Int64 MealTypeId { get; set; }

        public String Locale { get; set; }

        public String Name { get; set; }

        public Int64? UnitId { get; set; }

        public virtual FoodLogUnit Unit { get; set; }

        public virtual ICollection<FoodLog> FoodLogs { get; set; }
    }
}
