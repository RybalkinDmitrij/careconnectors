﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Fitbit
{
    public class TrackerAlarm : AbstractEntity
    {
        public Int32 AlarmId { get; set; }

        public Boolean Deleted { get; set; }

        public Boolean Enabled { get; set; }

        public String Label { get; set; }

        public Boolean Recurring { get; set; }

        public Int32 SnoozeCount { get; set; }

        public Int32 SnoozeLength { get; set; }

        public Boolean SyncedToDevice { get; set; }

        public String Time { get; set; }

        public String Vibe { get; set; }

        public virtual ICollection<WeekDay> WeekDays { get; set; }
    }

}
