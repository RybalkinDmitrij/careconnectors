﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Fitbit
{
    public class HeartRates : AbstractEntity
    {
        public virtual ICollection<HeartRateSummary> Average { get; set; }

        public virtual ICollection<HeartRateLog> Heart { get; set; }
    }
}
