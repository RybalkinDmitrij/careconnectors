﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FitTracker.Data.Entities.Fitbit
{
    public class ActivitySummary : AbstractEntity
    {
        public String UserId { get; set; }

        public DateTime Date { get; set; }

        public Int32 ActivityCalories { get; set; }

        public Int32 CaloriesBMR { get; set; }

        public Decimal Elevation { get; set; }

        public Int32 Floors { get; set; }

        public Int32 MarginalCalories { get; set; }

        public Int32 VeryActiveMinutes { get; set; }

        public Int32 CaloriesOut { get; set; }
        
        public Decimal FairlyActiveMinutes { get; set; }

        public Decimal LightlyActiveMinutes { get; set; }

        public Decimal SedentaryMinutes { get; set; }
        
        public Int32 Steps { get; set; }
        
        public virtual ICollection<ActivityDistance> Distances { get; set; }
    }
}