﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class Water : AbstractEntity
    {
        public String UserId { get; set; }

        public Int32 LogId { get; set; }

        public Int32 Amount { get; set; }
    }
}
