﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class SleepSummaryF : AbstractEntity
    {
        public String UserId { get; set; }

        public DateTime Date { get; set; }

        public Int32 TotalSleepRecords { get; set; }
        
        public Int32 TotalMinutesAsleep { get; set; }

        public Int32 TotalTimeInBed { get; set; }
    }
}
