﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class TimeSeriesDataInt : AbstractEntity
    {
        public DateTime DateTime { get; set; }

        public Int32 Value { get; set; }
    }
}
