﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class NutritionalValues : AbstractEntity
    {
        public Decimal Calories { get; set; }

        public Decimal Carbs { get; set; }

        public Decimal Fat { get; set; }
        
        public Decimal Fiber { get; set; }

        public Decimal Protein { get; set; }

        public Decimal Sodium { get; set; }

        public virtual ICollection<FoodLog> FoodLogs { get; set; }
    }
}
