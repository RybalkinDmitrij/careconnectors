﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Fitbit
{
    public class BloodPressureData : AbstractEntity
    {
        public Int64 AverageId { get; set; }

        public virtual BloodPressureAverage Average { get; set; }

        public virtual ICollection<BloodPressure> BP { get; set; }
    }
}
