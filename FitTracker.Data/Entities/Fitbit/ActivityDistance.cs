﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class ActivityDistance : AbstractEntity
    {
        public Int64 ActivitySummaryId { get; set; }

        public virtual ActivitySummary ActivitySummary { get; set; }

        public String Activity { get; set; }

        public Decimal Distance { get; set; }
    }
}
