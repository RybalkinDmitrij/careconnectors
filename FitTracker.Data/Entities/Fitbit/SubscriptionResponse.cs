﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class SubscriptionResponse : AbstractEntity
    {
        public String CollectionType { get; set; }

        public String OwnerId { get; set; }

        public String OwnerType { get; set; }

        public String SubscriberId { get; set; }

        public String SubscriptionId { get; set; }
    }
}
