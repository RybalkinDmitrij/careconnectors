﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Fitbit
{
    public class IntradayData : AbstractEntity
    {
        public virtual ICollection<IntradayDataValues> DataSet { get; set; }
    }
}
