﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Runkeeper
{
    public class Profile : AbstractEntity
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// The user's full name (omitted if not yet specified). Read only.
        /// </summary>     
        public String Name { get; internal set; }

        /// <summary>
        /// The user's geographical location (omitted if not yet specified). Read only.
        /// </summary>
        public String Location { get; internal set; }

        /// <summary>
        /// One of the following values: Athlete, Runner, Marathoner, Ultra Marathoner, Cyclist, Tri-Athlete, Walker, Hiker, Skier, Snowboarder, Skater, Swimmer, Rower (omitted if not yet specified).
        /// </summary>
        public String AthleteType { get; set; }

        /// <summary>
        /// One of the following values: M, F (omitted if not yet specified). Read only.
        /// </summary>       
        public String Gender { get; internal set; }

        /// <summary>
        /// The user's birthday (e.g., Sat, 1 Jan 2011 00:00:00) (omitted if not yet specified). Read only.
        /// </summary>
        public DateTime? Birthday { get; internal set; }

        /// <summary>
        /// true if the user subscribes to RunKeeper Elite, false otherwise. Read only.
        /// </summary>
        public Boolean Elite { get; internal set; }
    }
}
