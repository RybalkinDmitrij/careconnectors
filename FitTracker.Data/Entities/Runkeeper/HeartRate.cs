﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Runkeeper
{
    public class HeartRates : AbstractEntity
    {
        /// <summary>
        /// Identify of Fitness Activities
        /// </summary>
        public Int64 FitnessActivityId { get; set; }

        public virtual FitnessActivities FitnessActivity { get; set; }

        /// <summary>
        /// The number of seconds since the start of the activity.
        /// </summary>
        public Decimal Timestamp { get; set; }

        /// <summary>
        /// The total calories burned since the start of the activity.
        /// </summary>
        public Decimal HeartRate { get; set; }
    }
}
