﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Runkeeper
{
    public class Sets : AbstractEntity
    {
        /// <summary>
        /// Identify of Exercises
        /// </summary>
        public Int64 ExerciseId { get; set; }

        public virtual Exercises Exercise { get; set; }

        /// <summary>
        /// The weight for the set, in kg.
        /// </summary>
        public Decimal Weight { get; set; }

        /// <summary>
        /// The number of repetitions in this set.
        /// </summary>
        public Int32 Repetitions { get; set; }

        /// <summary>
        /// Any notes for this set (max. 1024 characters; optional).
        /// </summary>
        public String Notes { get; set; }
    }
}
