﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Runkeeper
{
    public class StrengthTraining : AbstractEntity
    {
        /// <summary>
        /// The unique ID of the user for the activity. Read only
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// The starting time for the activity (e.g., Sat, 1 Jan 2011 00:00:00).
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// The total calories burned (omitted if not available).
        /// </summary>
        public Decimal TotalCalories { get; set; }

        /// <summary>
        /// Any notes that the user has associated with the activity (max. 1024 characters; optional).
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// The sequence of time-stamped distance measurements (empty if not available). Read only.
        /// </summary>
        public virtual ICollection<Exercises> Exercises { get; set; }

        /// <summary>
        /// The name of the application that last modified this activity. Read only.
        /// </summary>
        public String Source { get; internal set; }
    }
}
