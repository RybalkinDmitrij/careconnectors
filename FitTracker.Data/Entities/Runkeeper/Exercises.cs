﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Runkeeper
{
    public class Exercises : AbstractEntity
    {
        /// <summary>
        /// Identify of Strength Training
        /// </summary>
        public Int64 StrengthTrainingId { get; set; }

        public virtual StrengthTraining StrengthTraining { get; set; }

        /// <summary>
        /// The primary exercise type, as one of the following values: Barbell Curl, Dumbbell Curl, Barbell Tricep Press, Dumbbell Tricep Press, Overhead Press, Wrist Curl, Tricep Kickback, Bench Press, Cable Crossover, Dumbbell Fly, Incline Bench, Dips, Pushup, Pullup, Back Raise, Bent-Over Row, Seated Row, Chinup, Lat Pulldown, Seated Reverse Fly, Military Press, Upright Row, Front Raise, Side Lateral Raise, Snatch, Push Press, Shrug, Crunch Machine, Crunch, Ab Twist, Bicycle Kick, Hanging Leg Raise, Hanging Knee Raise, Reverse Crunch, V Up, Situp, Squat, Lunge, Dead Lift, Hamstring Curl, Good Morning, Clean, Leg Press, Leg Extension, Other.
        /// </summary>
        public String PrimaryType { get; set; }

        /// <summary>
        /// The secondary exercise type (optional; provides greater detail as free-form text if supplied).
        /// </summary>
        public String SecondaryType { get; set; }

        /// <summary>
        /// The primary muscle group, as one of the following values: Arms, Chest, Back, Shoulders, Abs, Legs.
        /// </summary>
        public String PrimaryMuscleGroup { get; set; }

        /// <summary>
        /// The secondary muscle group, as one of the following values: Arms, Chest, Back, Shoulders, Abs, Legs (optional).
        /// </summary>
        public String SecondaryMuscleGroup { get; set; }

        /// <summary>
        /// Free-form tag for the routine of which this exercise is a part (max. 32 characters; optional).
        /// </summary>
        public String Routine { get; set; }

        /// <summary>
        /// Any notes for this exercise (max. 1024 characters; optional).
        /// </summary>
        public String Notes { get; set; }

        /// <summary>
        /// The sequence of time-stamped distance measurements (empty if not available). Read only.
        /// </summary>
        public virtual ICollection<Sets> Sets { get; set; }
    }
}
