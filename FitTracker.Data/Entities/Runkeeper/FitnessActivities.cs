﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Runkeeper
{
    public class FitnessActivities : AbstractEntity
    {
        /// <summary>
        /// The unique ID of the user for the activity. Read only
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// The type of activity, as one of the following values: Running, Cycling, Mountain Biking, Walking, Hiking, Downhill Skiing, Cross-Country Skiing, Snowboarding, Skating, Swimming, Wheelchair, Rowing, Elliptical, Other.
        /// </summary>
        public String Type { get; set; }

        /// <summary>
        /// The secondary type of the activity, as a free-form string (max. 64 characters). This field is used only if the type field is Other."
        /// </summary>
        public String SecondaryType { get; set; }

        /// <summary>
        /// The equipment used to complete this activity, as one of the following values: None, Treadmill, Stationary Bike, Elliptical, Row Machine. (Optional; if not specified, None is assumed.)
        /// </summary>
        public String Equipment { get; set; }

        /// <summary>
        /// The starting time for the activity (e.g., Sat, 1 Jan 2011 00:00:00).
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// The total distance traveled, in meters.
        /// </summary>
        public Decimal TotalDistance { get; set; }

        /// <summary>
        /// The sequence of time-stamped distance measurements (empty if not available). Read only.
        /// </summary>
        public virtual ICollection<Distances> Distances { get; set; }

        /// <summary>
        /// The duration of the activity, in seconds.
        /// </summary>
        public Decimal Duration { get; set; }

        /// <summary>
        /// The user’s average heart rate, in beats per minute (omitted if not available).
        /// </summary>
        public Int32? AverageHeartRate { get; set; }

        /// <summary>
        /// The sequence of time-stamped heart rate measurements (empty if not available).
        /// </summary>
        public virtual ICollection<HeartRates> HeartRate { get; set; }

        /// <summary>
        /// The total calories burned.
        /// </summary>
        public Decimal TotalCalories { get; set; }

        /// <summary>
        /// The sequence of time-stamped caloric burn measurements (empty if not available). Read only.
        /// </summary>
        public virtual ICollection<CaloriesMod> Calories { get; set; }

        /// <summary>
        /// The total elevation climbed over the course of the activity, in meters. Read only.
        /// </summary>
        public Decimal Climb { get; set; }

        /// <summary>
        /// Any notes that the user has associated with the activity
        /// </summary>
        public String Notes { get; set; }

        /// <summary>
        /// Whether this activity is currently being tracked via RunKeeper Live. Read only.
        /// </summary>
        public Boolean IsLive { get; set; }

        /// <summary>
        /// The sequence of geographical points along the route (empty for stationary activities).
        /// </summary>
        public virtual ICollection<Path> Path { get; set; }

        /// <summary>
        /// The sequence of images along the route (empty if not available). Read only.
        /// </summary>
        public virtual ICollection<Image> Images { get; set; }

        /// <summary>
        /// The name of the application that last modified this activity. Read only.
        /// </summary>
        public String Source { get; internal set; }
    }
}
