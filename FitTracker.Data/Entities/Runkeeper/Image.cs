﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Runkeeper
{
    public class Image : AbstractEntity
    {
        /// <summary>
        /// Identify of Fitness Activities
        /// </summary>
        public Int64 FitnessActivityId { get; set; }

        public virtual FitnessActivities FitnessActivity { get; set; }

        /// <summary>
        /// The number of seconds since the start of the activity.
        /// </summary>
        public Decimal Timestamp { get; set; }

        /// <summary>
        /// The latitude, in degrees (values increase northward and decrease southward).
        /// </summary>
        public Decimal Latitude { get; set; }

        /// <summary>
        /// The longitude, in degrees (values increase eastward and decrease westward).
        /// </summary>
        public Decimal Longitude { get; set; }

        /// <summary>
        /// The URI of the image.
        /// </summary>
        public String URI { get; set; }
    }
}
