﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Strava
{
    public class ActivityLap: AbstractEntity
    {
        /// <summary>
        /// The Strava id of the lap.
        /// </summary>
        public Int64 Id { get; set; }

        /// <summary>
        /// Indicates the level of detail.
        /// </summary>
        public Int32 ResourceState { get; set; }

        /// <summary>
        /// The name of the lap.
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Contains basic information about the activity.
        /// </summary>
        public Int64 ActivityId { get; set; }

        /// <summary>
        /// Contains basic information about the activity.
        /// </summary>
        public virtual Activity Activity { get; set; }

        /// <summary>
        /// The elapsed time of the lap in seconds.
        /// </summary>
        public Int32 ElapsedTime { get; set; }

        /// <summary>
        /// The moving the of the lap in seconds.
        /// </summary>
        public Int32 MovingTime { get; set; }

        /// <summary>
        /// The date and time when the lap was started.
        /// </summary>
        public DateTime Start { get; set; }

        /// <summary>
        /// The local date and time when the lap was started.
        /// </summary>
        public DateTime StartLocal { get; set; }

        /// <summary>
        /// The distance of the lap measured in meters.
        /// </summary>
        public Decimal Distance { get; set; }

        /// <summary>
        /// The start index point of the file.
        /// </summary>
        public Int32 StartIndex { get; set; }

        /// <summary>
        /// The end index point of the file.
        /// </summary>
        public Int32 EndIndex { get; set; }

        /// <summary>
        /// Gained meters in the lap.
        /// </summary>
        public Decimal TotalElevationGain { get; set; }

        /// <summary>
        /// The average speed measured in meters per second.
        /// </summary>
        public Decimal AverageSpeed { get; set; }

        /// <summary>
        /// The max speed measured in meters per second.
        /// </summary>
        public Decimal MaxSpeed { get; set; }

        /// <summary>
        /// The average cadence.
        /// </summary>
        public Decimal AverageCadence { get; set; }

        /// <summary>
        /// The average power measured in watts.
        /// </summary>
        public Decimal AveragePower { get; set; }

        /// <summary>
        /// The average heartrate in beats per minute.
        /// </summary>
        public Decimal AverageHeartrate { get; set; }

        /// <summary>
        /// The max heartrate in beats per minute.
        /// </summary>
        public Decimal MaxHeartrate { get; set; }

        /// <summary>
        /// The index of the lap.
        /// </summary>
        public Int32 LapIndex { get; set; }
    }
}
