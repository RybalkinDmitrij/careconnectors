﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Strava
{
    public class Activity: AbstractEntity
    {
        /// <summary>
        /// The unique ID of the user for the activity. Read only
        /// </summary>
        public Int64 AthleteId { get; set; }

        /// <summary>
        /// The unique ID of the user for the activity. Read only
        /// </summary>
        public virtual Athlete Athlete { get; set; }

        /// <summary>
        /// The id of the activity. This id is provided by Strava at upload.
        /// </summary>
        public Int64 Id { get; set; }

        /// <summary>
        /// The activity's name.
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// The external id of the activity. Provided upon upload. Can not be changed.
        /// </summary>
        public String ExternalId { get; set; }

        /// <summary>
        /// The type of the activity.
        /// </summary>
        public ActivityType Type { get; set; }

        /// <summary>
        /// The distance travelled.
        /// </summary>
        public Decimal Distance { get; set; }

        /// <summary>
        /// Time in movement in seconds.
        /// </summary>
        public Int32 MovingTime { get; set; }

        /// <summary>
        /// The elapsed time in seconds.
        /// </summary>
        public Int32 ElapsedTime { get; set; }

        /// <summary>
        /// The total elevation gain in meters.
        /// </summary>
        public Decimal ElevationGain { get; set; }

        /// <summary>
        /// True if the currently authenticated athlete has kudoed this activity.
        /// </summary>
        public Boolean HasKudoed { get; set; }

        /// <summary>
        /// The athlete's average heartrate during this activity.
        /// </summary>
        public Decimal AverageHeartrate { get; set; }

        /// <summary>
        /// The athlete's max heartrate.
        /// </summary>
        public Decimal MaxHeartrate { get; set; }

        /// <summary>
        /// Only present if activity is owned by authenticated athlete, returns 0 if not truncated by privacy zones.
        /// </summary>
        public Int32? Truncated { get; set; }

        /// <summary>
        /// The city where this activity was started.
        /// </summary>
        public String City { get; set; }

        /// <summary>
        /// The state where this activity was started.
        /// </summary>
        public String State { get; set; }

        /// <summary>
        /// The country where this activity was started.
        /// </summary>
        public String Country { get; set; }

        /// <summary>
        /// The average speed in meters per seconds.
        /// </summary>
        public Decimal AverageSpeed { get; set; }

        /// <summary>
        /// The max speed in meters per second.
        /// </summary>
        public Decimal MaxSpeed { get; set; }

        /// <summary>
        /// The average cadence. Only returned if activity is a bike ride.
        /// </summary>
        public Decimal AverageCadence { get; set; }

        /// <summary>
        /// The average temperature during this activity. Only returned if data is provided upon upload.
        /// </summary>
        public Decimal AverageTemperature { get; set; }

        /// <summary>
        /// The average power during this activity. Only returned if data is provided upon upload.
        /// </summary>
        public Decimal AveragePower { get; set; }

        /// <summary>
        /// Kilojoules. Rides only.
        /// </summary>
        public Decimal Kilojoules { get; set; }

        /// <summary>
        /// True if the activity was recorded on a stationary trainer.
        /// </summary>
        public Boolean IsTrainer { get; set; }

        /// <summary>
        /// True if activity is a a commute.
        /// </summary>
        public Boolean IsCommute { get; set; }

        /// <summary>
        /// True if the ride was crated manually.
        /// </summary>
        public Boolean IsManual { get; set; }

        /// <summary>
        /// True if the activity is private.
        /// </summary>
        public Boolean IsPrivate { get; set; }

        /// <summary>
        /// True if the activity was flagged.
        /// </summary>
        public Boolean IsFlagged { get; set; }

        /// <summary>
        /// Achievement count.
        /// </summary>
        public Int32 AchievementCount { get; set; }

        /// <summary>
        /// Activity's kudos count.
        /// </summary>
        public Int32 KudosCount { get; set; }

        /// <summary>
        /// Activity's comment count.
        /// </summary>
        public Int32 CommentCount { get; set; }

        /// <summary>
        /// Number of athletes on this activity.
        /// </summary>
        public Int32 AthleteCount { get; set; }

        /// <summary>
        /// Number of photos.
        /// </summary>
        public Int32 PhotoCount { get; set; }

        /// <summary>
        /// Returns the StartDate-Property as a DateTime object.
        /// </summary>
        public DateTime DateTimeStart { get; set; }

        /// <summary>
        /// Returns the StartDateLocal-Property as a DateTime object.
        /// </summary>
        public DateTime DateTimeStartLocal { get; set; }

        /// <summary>
        /// Timezone of the activity.
        /// </summary>
        public String TimeZone { get; set; }

        /// <summary>
        /// Coordinate where the activity was started.
        /// </summary>
        public Decimal? StartLatitude { get; set; }

        /// <summary>
        /// Rides with power meter data only similar to xPower or Normalized Power.
        /// </summary>
        public Int32 WeightedAverageWatts { get; set; }

        /// <summary>
        /// Coordinate where the activity was started.
        /// </summary>
        public Decimal? StartLongitude { get; set; }

        /// <summary>
        /// Coordinate where the activity was ended.
        /// </summary>
        public Decimal? EndLatitude { get; set; }

        /// <summary>
        /// Coordinate where the activity was ended.
        /// </summary>
        public Decimal? EndLongitude { get; set; }

        /// <summary>
        /// True if the power data comes from a power meter, false if the data is estimated.
        /// </summary>
        public Boolean HasPowerMeter { get; set; }

        /// <summary>
        /// Map representing the route of the activity.
        /// </summary>
        public Int64 MapId { get; set; }

        /// <summary>
        /// Map representing the route of the activity.
        /// </summary>
        public virtual Map Map { get; set; }

        /// <summary>
        /// A list of segment effort objects.
        /// </summary>
        public virtual ICollection<SegmentEffort> SegmentEfforts { get; set; }

        /// <summary>
        /// the burned kilocalories.
        /// </summary>
        public Decimal Calories { get; set; }

        /// <summary>
        /// The activity's description.
        /// </summary>
        public String Description { get; set; }

        /// <summary>
        /// The id of the gear used.
        /// </summary>
        public Int64 GearId { get; set; }

        /// <summary>
        /// The id of the gear used.
        /// </summary>
        public virtual Gear Gear { get; set; }

        /// <summary>
        /// A list of comments.
        /// </summary>
        public virtual ICollection<Comment> Comments { get; set; }

        /// <summary>
        /// A list of ActivityLap.
        /// </summary>
        public virtual ICollection<ActivityLap> ActivityLaps { get; set; }

        /// <summary>
        /// A list of photo.
        /// </summary>
        public virtual ICollection<Photo> Photos { get; set; }
    }
}
