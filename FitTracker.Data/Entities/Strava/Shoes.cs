﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Strava
{
    public class Shoes : AbstractEntityWithoutPkId
    {
        /// <summary>
        /// Identify of Athlete
        /// </summary>
        public Int64 AthleteId { get; set; }

        public virtual Athlete Athlete { get; set; }

        // <summary>
        /// Identify of Gear
        /// </summary>
        public Int64 GearId { get; set; }

        public virtual Gear Gear { get; set; }

        /// <summary>
        /// The gear's brand name.
        /// </summary>
        public String Brand { get; set; }

        /// <summary>
        /// The gear's model.
        /// </summary>
        public String Model { get; set; }

        /// <summary>
        /// The gear's description.
        /// </summary>
        public String Description { get; set; }
    }
}
