﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Strava
{
    public class Gear : AbstractEntity
    {
        /// <summary>
        /// The gear's id.
        /// </summary>
        public String Id { get; set; }

        /// <summary>
        /// True if this is the primary gear.
        /// </summary>
        public Boolean IsPrimary { get; set; }

        /// <summary>
        /// Gear's name. Athlete entered for bikes, generated from brand and model for shoes
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Distance travelled with gear in meters.
        /// </summary>
        public Decimal Distance { get; set; }

        /// <summary>
        /// Resource state / level of details.
        /// </summary>
        public Int32 ResourceState { get; set; }

        /// <summary>
        /// A list of activity.
        /// </summary>
        public virtual ICollection<Activity> Activities { get; set; }

        /// <summary>
        /// Shoes discription.
        /// </summary>
        public virtual Shoes Shoes { get; set; }

        /// <summary>
        /// Bike discription.
        /// </summary>
        public virtual Bike Bike { get; set; }
    }
}
