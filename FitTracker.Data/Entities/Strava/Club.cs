﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Strava
{
    public class Club : AbstractEntity
    {
        /// <summary>
        /// Identify of Athlete
        /// </summary>
        public Int64 AthleteId { get; set; }

        public virtual Athlete Athlete { get; set; }

        /// <summary>
        /// The id of the club. The id is provided by Strava and can't be changed.
        /// </summary>
        public Int64 Id { get; set; }

        /// <summary>
        /// The level of detail of the Club.
        /// </summary>
        public Int32 ResourceState { get; set; }

        /// <summary>
        /// The name of the club.
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Url to a 62x62 pixel picture of the club. Use the ImageLoader class to load the picture.
        /// </summary>
        public String ProfileMedium { get; set; }

        /// <summary>
        /// The club's description.
        /// </summary>
        public String Description { get; set; }

        /// <summary>
        /// The club's type.
        /// </summary>
        public ClubType ClubType { get; set; }

        /// <summary>
        /// The sports type of the club.
        /// </summary>
        public SportType SportType { get; set; }
       
        /// <summary>
        /// the club's city.
        /// </summary>
        public String City { get; set; }

        /// <summary>
        /// The club's state.
        /// </summary>
        public String State { get; set; }

        /// <summary>
        /// The club's country.
        /// </summary>
        public String Country { get; set; }

        /// <summary>
        /// True if the club is a private club.
        /// </summary>
        public Boolean IsPrivate { get; set; }

        /// <summary>
        /// The club's member count.
        /// </summary>
        public Int32 MemberCount { get; set; }
    }
}
