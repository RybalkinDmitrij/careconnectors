﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Strava
{
    public class Comment : AbstractEntity
    {
        /// <summary>
        /// The comment id.
        /// </summary>
        public Int64 Id { get; set; }

        /// <summary>
        /// The resource state.
        /// </summary>
        public Int32 ResourceState { get; set; }

        /// <summary>
        /// The activity to which the comment was posted.
        /// </summary>
        public Int64 ActivityId { get; set; }

        /// <summary>
        /// The activity to which the comment was posted.
        /// </summary>
        public virtual Activity Activity { get; set; }

        /// <summary>
        /// The comment's text.
        /// </summary>
        public String Text { get; set; }

        /// <summary>
        /// The athlete who wrote the comment.
        /// </summary>
        public String Author { get; set; }

        /// <summary>
        /// The time when the comment was crated.
        /// </summary>
        public String TimeCreated { get; set; }
    }
}
