﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Strava
{
    public class Segment : AbstractEntity
    {
        /// <summary>
        /// The Segment id provided by Strava.
        /// </summary>
        public Int32 Id { get; set; }

        /// <summary>
        /// The resource state.
        /// </summary>
        public Int32 ResourceState { get; set; }

        /// <summary>
        /// The name of the segment.
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// The type of activity.
        /// </summary>
        public String ActivityType { get; set; }

        /// <summary>
        /// The segment's distance.
        /// </summary>
        public Decimal Distance { get; set; }

        /// <summary>
        /// The average grade of the segment.
        /// </summary>
        public Decimal AverageGrade { get; set; }

        /// <summary>
        /// The max grade of the segment.
        /// </summary>
        public Decimal MaxGrade { get; set; }

        /// <summary>
        /// The segment's highest elevation.
        /// </summary>
        public Decimal MaxElevation { get; set; }

        /// <summary>
        /// The segment's lowest elevation.
        /// </summary>
        public Decimal MinElevation { get; set; }

        /// <summary>
        /// the climb category of the segment.
        /// </summary>
        public ClimbCategory ClimbCategory { get; set; }

        /// <summary>
        /// The city where this segment is located in.
        /// </summary>
        public String City { get; set; }

        /// <summary>
        /// The state where this segment is located in.
        /// </summary>
        public String State { get; set; }

        /// <summary>
        /// The country where this segment is located in.
        /// </summary>
        public String Country { get; set; }

        /// <summary>
        /// True if this segment is private.
        /// </summary>
        public Boolean IsPrivate { get; set; }

        /// <summary>
        /// True if the segment is starred by the currently authenticated athlete.
        /// </summary>
        public Boolean IsStarred { get; set; }

        /// <summary>
        /// The date when the segment was created.
        /// </summary>
        public String CreatedAt { get; set; }

        /// <summary>
        /// The date when the segment was updated.
        /// </summary>
        public String UpdatedAt { get; set; }

        /// <summary>
        /// The total elevation gain of the segment.
        /// </summary>
        public Decimal TotalElevationGain { get; set; }

        /// <summary>
        /// The map id.
        /// </summary>
        public Int64 MapId { get; set; }

        /// <summary>
        /// A map with the segment's route.
        /// </summary>
        public virtual Map Map { get; set; }

        /// <summary>
        /// The effort count.
        /// </summary>
        public Int32 EffortCount { get; set; }

        /// <summary>
        /// The number of athletes that run or rode this segment.
        /// </summary>
        public Int32 AthleteCount { get; set; }

        /// <summary>
        /// True if the segment was marked as hazardous.
        /// </summary>
        public Boolean IsHazardous { get; set; }

        /// <summary>
        /// The personal record time.
        /// </summary>
        public Int32 PersonalRecordTime { get; set; }

        /// <summary>
        /// The personal record distance.
        /// </summary>
        public Decimal PersonalRecordDistance { get; set; }

        /// <summary>
        /// Number of stars on this segment. 
        /// </summary>
        public Int32 StarCount { get; set; }

        /// <summary>
        /// A list of Segment Efforts.
        /// </summary>
        public virtual ICollection<SegmentEffort> SegmentEfforts { get; set; }


    }
}
