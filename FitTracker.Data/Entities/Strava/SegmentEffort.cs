﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Strava
{
    public class SegmentEffort : AbstractEntity
    {
        /// <summary>
        /// Identify of Activity 
        /// </summary>
        public Int64 ActivityId { get; set; }

        public virtual Activity Activity { get; set; }

        /// <summary>
        /// The segment effort's id.
        /// </summary>
        public Int64 Id { get; set; }

        /// <summary>
        /// The average cadence.
        /// </summary>
        public Decimal AverageCadence { get; set; }

        /// <summary>
        /// The average power in watts.
        /// </summary>
        public Decimal AveragePower { get; set; }

        /// <summary>
        /// The average heartrate.
        /// </summary>
        public Decimal AverageHeartrate { get; set; }

        /// <summary>
        /// The max heartrate.
        /// </summary>
        public Decimal MaxHeartrate { get; set; }

        /// <summary>
        /// Indicates level of detail
        /// </summary>
        public Int32 ResourceState { get; set; }

        /// <summary>
        /// The name of the segment.
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// The Segment id provided by Strava.
        /// </summary>
        public Int64 SegmentId { get; set; }

        /// <summary>
        /// The Segment id provided by Strava.
        /// </summary>
        public virtual Segment Segment { get; set; }

        /// <summary>
        /// The unique ID of the user for the activity. Read only
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// 1-10 rank on segment at time of upload
        /// </summary>
        public Int32? KingOfMountainRank { get; set; }

        /// <summary>
        /// 1-3 personal record on segment at time of upload
        /// </summary>
        public Int32? PersonalRecordRank { get; set; }

        /// <summary>
        /// Moving time in seconds.
        /// </summary>
        public Int32 MovingTime { get; set; }

        /// <summary>
        /// Elapsed time in seconds.
        /// </summary>
        public Int32 ElapsedTime { get; set; }

        /// <summary>
        /// Returns the StartDate-Property as a DateTime object.
        /// </summary>
        public DateTime DateTimeStart { get; set; }

        /// <summary>
        /// Distance in meters.
        /// </summary>
        public Decimal SegmentDistance { get; set; }

        /// <summary>
        /// The activity stream index of the start of this effort
        /// </summary>
        public Int32 StartIndex { get; set; }

        /// <summary>
        /// The activity stream index of the end of this effort
        /// </summary>
        public Int32 EndIndex { get; set; }
    }
}
