﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Strava
{
    public class Map : AbstractEntity
    {
        /// <summary>
        /// The list of activities
        /// </summary>
        public virtual ICollection<Activity> Activities { get; set; }

        /// <summary>
        /// A list of Segment.
        /// </summary>
        public virtual ICollection<Segment> Segments { get; set; }

        /// <summary>
        /// The map id.
        /// </summary>
        public String Id { get; set; }

        /// <summary>
        /// The map's polyline. This polyline can be converted to coordinates.
        /// </summary>
        public String Polyline { get; set; }

        /// <summary>
        /// A summary of the polyline.
        /// </summary>
        public String SummaryPolyline { get; set; }

        /// <summary>
        /// The resource state gives information about the level of details of the map.
        /// </summary>
        public Int32 ResourceState { get; set; }
    }
}
