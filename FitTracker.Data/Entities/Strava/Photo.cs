﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Strava
{
    public class Photo : AbstractEntity
    {
        /// <summary>
        /// The comment id.
        /// </summary>
        public Int64 Id { get; set; }

        /// <summary>
        /// The resource state.
        /// </summary>
        public Int32 ResourceState { get; set; }

        /// <summary>
        /// The activity to which the comment was posted.
        /// </summary>
        public Int64 ActivityId { get; set; }

        /// <summary>
        /// The activity to which the comment was posted.
        /// </summary>
        public virtual Activity Activity { get; set; }

        /// <summary>
        /// Url to the picture. Use the ImageLoader class to download the picture.
        /// </summary>
        public String ImageUrl { get; set; }

        /// <summary>
        /// The photo's external id.
        /// </summary>
        public String ExternalUid { get; set; }

        /// <summary>
        /// The caption.
        /// </summary>
        public String Caption { get; set; }

        /// <summary>
        /// Image source. Currently only "InstagramPhoto"
        /// </summary>
        public String Type { get; set; }

        /// <summary>
        /// The date when the image was uploaded.
        /// </summary>
        public String UploadedAt { get; set; }

        /// <summary>
        /// The date when the image was crated.
        /// </summary>
        public String CreatedAt { get; set; }

        /// <summary>
        /// Coordinate where the photo was added.
        /// </summary>
        public Decimal? Latitude { get; set; }

        /// <summary>
        /// Coordinate where the photo was added.
        /// </summary>
        public Decimal? Longitude { get; set; }
    }
}
