﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Strava;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Strava
{
    public class Athlete : AbstractEntity
    {
        /// <summary>
        /// The Strava athlete id.
        /// </summary>
        public Int64 Id { get; set; }

        /// <summary>
        /// The representation of the Athlete object:
        /// 1 - meta
        /// 2 - summary
        /// 3 - detailed
        /// Listed in increasing levels of detail.
        /// </summary>
        public Int32 ResourceState { get; set; }

        /// <summary>
        /// The athletes first name.
        /// </summary>
        public String FirstName { get; set; }

        /// <summary>
        /// The athletes last name.
        /// </summary>
        public String LastName { get; set; }

        /// <summary>
        /// Url to a 62x62 pixel profile picture. You can use the ImageLoader class to load this picture.
        /// </summary>
        public String ProfileMedium { get; set; }

        /// <summary>
        /// Url to a 124x124 pixel profile picture. You can use the ImageLoader class to load this picture.
        /// </summary>
        public String Profile { get; set; }

        /// <summary>
        /// The city where the athlete lives.
        /// </summary>
        public String City { get; set; }

        /// <summary>
        /// The state where the athlete lives.
        /// </summary>
        public String State { get; set; }

        /// <summary>
        /// The state where the athlete lives.
        /// </summary>
        public String Country { get; set; }

        /// <summary>
        /// The athlete's sex.
        /// </summary>
        public String Sex { get; set; }

        /// <summary>
        /// The authenticated athlete’s friend status of this athlete.
        /// Values are 'pending', 'accepted', 'blocked' or 'null'.
        /// </summary>
        public String Friend { get; set; }

        /// <summary>
        /// The authenticated athlete’s following status of this athlete.
        /// Values are 'pending', 'accepted', 'blocked' or 'null'.
        /// </summary>
        public String Follower { get; set; }

        /// <summary>
        /// True, if the athlete is a Strava premium member. In some cases this attribute is important, for example when leaderboards are filtered
        /// by either weight class or age group.
        /// </summary>
        public Boolean IsPremium { get; set; }

        /// <summary>
        /// The date when this athlete was created. ISO 8601 time string.
        /// </summary>
        public String CreatedAt { get; set; }

        /// <summary>
        /// The date when this athlete was updated. ISO 8601 time string.
        /// </summary>
        public String UpdatedAt { get; set; }

        /// <summary>
        /// True, if enhanced privacy is enabled.
        /// </summary>
        public Boolean ApproveFollowers { get; set; }

        /// <summary>
        /// The count of the athlete's followers.
        /// </summary>
        public Int32 FollowerCount { get; set; }

        /// <summary>
        /// The count of the athlete's friends.
        /// </summary>
        public Int32 FriendCount { get; set; }

        /// <summary>
        /// The count of the athlete's friends that both this athlete and the currently authenticated athlete are following.
        /// </summary>
        public Int32 MutualFriendCount { get; set; }

        /// <summary>
        /// The date preference. ISO 8601 time string.
        /// </summary>
        public String DatePreference { get; set; }

        /// <summary>
        /// Either 'feet' or 'meters'
        /// </summary>
        public String MeasurementPreference { get; set; }

        /// <summary>
        /// The email address.
        /// </summary>
        public String Email { get; set; }

        /// <summary>
        /// The functional threshold power.
        /// </summary>
        public Int32? Ftp { get; set; }

        /// <summary>
        /// A list of the athlete's bikes.
        /// </summary>
        public virtual ICollection<Bike> Bikes { get; set; }

        /// <summary>
        /// A list of the athlete's shoes.
        /// </summary>
        public virtual ICollection<Shoes> Shoes { get; set; }

        /// <summary>
        /// A list of the athlete's clubs.
        /// </summary>
        public virtual ICollection<Club> Clubs { get; set; }

        /// <summary>
        /// A list of the athlete's activities.
        /// </summary>
        public virtual ICollection<Activity> Activities { get; set; }
    }
}
