﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Jawbone
{
    public class AccountInfo : AbstractEntity
    {
        public String UserId { get; set; }

        public String Xid { get; set; }

        public String First { get; set; }

        public String Last { get; set; }

        public String Image { get; set; }

        public Decimal Weight { get; set; }

        public Decimal Height { get; set; }
    }
}
