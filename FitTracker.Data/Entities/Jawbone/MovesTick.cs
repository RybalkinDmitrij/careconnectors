﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Jawbone
{
    public class MovesTick : AbstractEntity
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Distance travelled in meters.
        /// </summary>
        public Decimal Distance { get; set; }

        /// <summary>
        /// Time stamp for end of this tick bucket.
        /// </summary>
        public DateTime TimeCompleted { get; set; }

        /// <summary>
        /// Active time during this tick bucket, in seconds.
        /// </summary>
        public Int32 ActiveTime { get; set; }

        /// <summary>
        /// Calories burned during this tick bucket.
        /// </summary>
        public Decimal Calories { get; set; }

        /// <summary>
        /// Steps taken during this tick bucket.
        /// </summary>
        public Int32 Steps { get; set; }

        /// <summary>
        /// Time stamp for start of this tick bucket.
        /// </summary>
        public Int64 Time { get; set; }

        /// <summary>
        /// Speed is calculated as distance/active_time (m/s)
        /// </summary>
        public Decimal Speed { get; set; }
    }
}