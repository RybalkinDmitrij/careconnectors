﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Jawbone
{
    public class HourlyTotal : AbstractEntity
    {
        public Int64 MovesId { get; set; }

        public virtual Moves Moves { get; set; }

        public Int32 Distance { get; set; }

        public Decimal Calories { get; set; }

        public Int32 Steps { get; set; }

        public Int32 ActiveTime { get; set; }

        public Int32 InactiveTime { get; set; }

        public Int32 LongestActiveTime { get; set; }

        public Int32 LongestIdleTime { get; set; }
    }
}