﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Jawbone
{
    public class Workouts : AbstractEntity
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Unique ID for this event. Can be used to retrieve data for this specific event, see below.
        /// </summary>
        public String Xid { get; set; }

        /// <summary>
        /// Title of this workout
        /// </summary>
        public String Title { get; set; }

        /// <summary>
        /// Type of event, in this case a workout.
        /// </summary>
        public String Type { get; set; }

        /// <summary>
        /// Workout type, see Type table below.
        /// 1 - walk
        /// 2 - run
        /// 3 - lift weights
        /// 4 - cross train
        /// 5 - nike training
        /// 6 - yoga
        /// 7 - pilates
        /// 8 - body weight exercise
        /// 9 - crossfit
        /// 10 - p90x
        /// 11 - zumba
        /// 12 - trx
        /// 13 - swim
        /// 14 - bike
        /// 15 - elliptical
        /// 16 - bar method
        /// 17 - kinect exercises
        /// 18 - tennis
        /// 19 - basketball
        /// 20 - golf
        /// 21 - soccer
        /// 22 - ski snowboard
        /// 23 - dance
        /// 24 - hike
        /// 25 - cross country skiing
        /// 26 - stationary bike
        /// 27 - cardio
        /// 28 - game
        /// 29 - other
        /// </summary>
        public Int32 SubType { get; set; }

        public String SubTypeValue
        {
            get
            {
                switch (this.SubType)
                {
                    case 1: return "walk";
                    case 2: return "run";
                    case 3: return "lift weights";
                    case 4: return "cross train";
                    case 5: return "nike training";
                    case 6: return "yoga";
                    case 7: return "pilates";
                    case 8: return "body weight exercise";
                    case 9: return "crossfit";
                    case 10: return "p90x";
                    case 11: return "zumba";
                    case 12: return "trx";
                    case 13: return "swim";
                    case 14: return "bike";
                    case 15: return "elliptical";
                    case 16: return "bar method";
                    case 17: return "kinect exercises";
                    case 18: return "tennis";
                    case 19: return "basketball";
                    case 20: return "golf";
                    case 21: return "soccer";
                    case 22: return "ski snowboard";
                    case 23: return "dance";
                    case 24: return "hike";
                    case 25: return "cross country skiing";
                    case 26: return "stationary bike";
                    case 27: return "cardio";
                    case 28: return "game";
                    case 29: return "other";
                    default: return String.Empty;
                }
            }
        }

        /// <summary>
        /// Epoch timestamp when this workout was created.
        /// </summary>
        public DateTime TimeCreated { get; set; }

        /// <summary>
        /// Epoch timestamp when this workout was updated.
        /// </summary>
        public DateTime TimeUpdated { get; set; }

        /// <summary>
        /// Epoch timestamp when this workout was completed.
        /// </summary>
        public DateTime TimeCompleted { get; set; }

        /// <summary>
        /// Date when this workout was created, formatted as YYYYMMDD.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Latitude of location where workout was logged.
        /// </summary>
        public Decimal PlaceLat { get; set; }

        /// <summary>
        /// Longitude of location where workout was logged.
        /// </summary>
        public Decimal PlaceLon { get; set; }

        /// <summary>
        /// Accuracy of location where workout was logged, in meters.
        /// </summary>
        public String PlaceAcc { get; set; }

        /// <summary>
        /// Name of location where workout was logged.
        /// </summary>
        public String PlaceName { get; set; }

        /// <summary>
        /// Link to the image of route taken for workout (relative, add prefix https://jawbone.com)
        /// </summary>
        public String Route { get; set; }

        /// <summary>
        /// Link to the image of this workout (relative, add prefix https://jawbone.com)
        /// </summary>
        public String Image { get; set; }

        /// <summary>
        /// Link to the image of this workout's intensity (relative, add prefix https://jawbone.com)
        /// </summary>
        public String SnapshotImage { get; set; }

        /// <summary>
        /// Number of steps during this workout. Note steps will only be provided if the user was wearing the physical UP band during workout.
        /// </summary>
        public Int32 Steps { get; set; }

        /// <summary>
        /// Total time for this workout, in seconds.
        /// </summary>
        public Int32 Time { get; set; }

        /// <summary>
        /// Total time in movement as logged by the UP band. Note bg_active_time will only be provided if the user was wearing the physical UP band during workout.
        /// </summary>
        public Int32 BgActiveTime { get; set; }

        /// <summary>
        /// Total distance travelled, in meters.
        /// </summary>
        public Int32 Meters { get; set; }

        /// <summary>
        /// Total distance travelled, in kilometers.
        /// </summary>
        public Decimal Km { get; set; }

        /// <summary>
        /// Intensity of the workout as selected by the user or 3rd party app. 1 = easy, 2 = moderate, 3 = intermediate, 4 = difficult, 5 = hard
        /// </summary>
        public Int32 Intensity { get; set; }

        public String IntensityValue
        {
            get
            {
                switch (this.Intensity)
                {
                    case 1: return "easy";
                    case 2: return "moderate";
                    case 3: return "intermediate";
                    case 4: return "difficult";
                    case 5: return "hard";
                    default: return String.Empty;
                }
            }
        }

        /// <summary>
        /// Total calories burned during workout.
        /// </summary>
        public Int32 Calories { get; set; }

        /// <summary>
        /// Basal metabolic rate during workout.
        /// </summary>
        public Decimal Bmr { get; set; }

        /// <summary>
        /// Calories burned as logged by the UP band. Note bg_calories will only be provided if the user was wearing the physical UP band during workout.
        /// </summary>
        public Decimal BgCalories { get; set; }

        public Decimal BmrCalories { get; set; }

        /// <summary>
        /// Time zone when this event was generated. Whenever possible, Olson format (e.g., "America/Los Angeles") will be returned, otherwise the GMT offset (e.g., "GMT+0800") will be returned.
        /// </summary>
        public String Timezone { get; set; }
    }
}