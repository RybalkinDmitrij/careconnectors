﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Jawbone
{
    public class Moves : AbstractEntity
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Unique ID for this event. Can be used to retrieve data for this specific event, see below.
        /// </summary>
        public String Xid { get; set; }

        /// <summary>
        /// Title of this move.
        /// </summary>
        public String Title { get; set; }

        /// <summary>
        /// Type of event, in this case a move.
        /// </summary>
        public String Type { get; set; }

        /// <summary>
        /// Epoch timestamp when this move was created.
        /// </summary>
        public DateTime TimeCreated { get; set; }

        /// <summary>
        /// Epoch timestamp when this move was last updated. NOTE: unlike other endpoints, this value is not when the user last submitted data, but is always updated in real time due to the fact that the BMR value (see below) is always changing throughout the day. This is true even for move events from previous days.
        /// </summary>
        public DateTime TimeUpdated { get; set; }

        /// <summary>
        /// Epoch timestamp when this move was completed. This is the timestamp to use for the last time the move data from the user's band was updated.
        /// </summary>
        public DateTime TimeCompleted { get; set; }

        /// <summary>
        /// Date when this move was created, formatted as YYYYMMDD.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Link to the image of this move event (relative, add prefix https://jawbone.com)
        /// </summary>
        public String SnapshotImage { get; set; }

        /// <summary>
        /// Distance travelled, in meters.
        /// </summary>
        public Int32 Distance { get; set; }

        /// <summary>
        /// Distance travelled, in kilometers.
        /// </summary>
        public Decimal Km { get; set; }

        /// <summary>
        /// Number of steps taken.
        /// </summary>
        public Int32 Steps { get; set; }

        /// <summary>
        /// Total active time for move, in seconds.
        /// </summary>
        public Int32 ActiveTime { get; set; }

        /// <summary>
        /// Longest consecutive active period, in seconds.
        /// </summary>
        public Int32 LongestActive { get; set; }

        /// <summary>
        /// Total inactive time for move, in seconds.
        /// </summary>
        public Int32 InactiveTime { get; set; }

        /// <summary>
        /// Longest consecutive inactive period, in seconds.
        /// </summary>
        public Int32 LongestIdle { get; set; }

        /// <summary>
        /// Total calories burned. This is computed by this formula: wo_calories+bg_calories+bmr_day / 86400 * active_time
        /// </summary>
        public Decimal Calories { get; set; }

        /// <summary>
        /// Estimated basal metabolic rate for entire day, in calories.
        /// </summary>
        public Decimal BmrDay { get; set; }

        /// <summary>
        /// Estimated basal metabolic rate at current time. For previous days should approximately equal bmr_day.
        /// </summary>
        public Decimal Bmr { get; set; }

        /// <summary>
        /// Calories directly from UP band activity outside the context of a workout.
        /// </summary>
        public Decimal BgCalories { get; set; }

        /// <summary>
        /// Calories burned from workouts.
        /// </summary>
        public Decimal WoCalories { get; set; }

        /// <summary>
        /// Total time spent in workouts, in seconds.
        /// </summary>
        public Int32 WoTime { get; set; }

        /// <summary>
        /// Actual active time during workout (where user was stepping) in seconds.
        /// </summary>
        public Int32 WoActiveTime { get; set; }

        /// <summary>
        /// Number of workouts logged during this move.
        /// </summary>
        public Int32 WoCount { get; set; }

        /// <summary>
        /// Longest workout period, in seconds.
        /// </summary>
        public Int32 WoLongest { get; set; }

        /// <summary>
        /// Epoch timestamp of sunrise.
        /// </summary>
        public Int64 Sunrise { get; set; }

        /// <summary>
        /// Epoch timestamp of sunset.
        /// </summary>
        public Int64 Sunset { get; set; }

        /// <summary>
        /// Move can have more than one timezone associated with it if the user has crossed timezone in the given day. Epoch timestamp for the period starting in that time zone, and the time zone string.
        /// </summary>
        public String Timezone { get; set; }

        /// <summary>
        /// Data broken out by hour (values as above).
        /// </summary>
        public virtual ICollection<HourlyTotal> HourlyTotals { get; set; }
    }
}