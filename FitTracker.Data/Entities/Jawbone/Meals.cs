﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Jawbone
{
    public class Meals : AbstractEntity
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Unique ID for this event. Can be used to retrieve data for this specific event, see below.
        /// </summary>
        public String Xid { get; set; }

        /// <summary>
        /// Title of this meal
        /// </summary>
        public String Title { get; set; }

        /// <summary>
        /// Note on this meal
        /// </summary>
        public String Note { get; set; }

        /// <summary>
        /// Type of event, in this case a meal.
        /// </summary>
        public String Type { get; set; }

        /// <summary>
        /// Meal type. 1=Breakfast, 2=Lunch, 3=Dinner
        /// </summary>
        public Int32 SubType { get; set; }

        /// <summary>
        /// Epoch timestamp when this meal was created.
        /// </summary>
        public DateTime TimeCreated { get; set; }

        /// <summary>
        /// Epoch timestamp when this meal was last updated.
        /// </summary>
        public DateTime TimeUpdated { get; set; }

        /// <summary>
        /// Epoch timestamp when this meal was completed.
        /// </summary>
        public DateTime TimeCompleted { get; set; }

        /// <summary>
        /// Date when this meal was created, formatted as YYYYMMDD.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Latitude of location where meal was logged.
        /// </summary>
        public Decimal PlaceLat { get; set; }

        /// <summary>
        /// Longitude of location where meal was logged.
        /// </summary>
        public Decimal PlaceLon { get; set; }

        /// <summary>
        /// Accuracy of location where meal was logged, in meters.
        /// </summary>
        public String PlaceAcc { get; set; }

        /// <summary>
        /// Name of location where meal was logged.
        /// </summary>
        public String PlaceName { get; set; }

        /// <summary>
        /// Number of drinks in this meal.
        /// </summary>
        public Int32 NumDrinks { get; set; }

        /// <summary>
        /// Number of Waters in this meal.
        /// </summary>
        public Int32 NumWater { get; set; }

        /// <summary>
        /// Number of foods in this meal.
        /// </summary>
        public Int32 NumFoods { get; set; }

        /// <summary>
        /// If the meal contains only Waters as Meal items.
        /// </summary>
        public Boolean OnlyWaters { get; set; }

        /// <summary>
        /// Number of Meal items in the given meal having Food Score in Green zone.
        /// </summary>
        public Int32 NumMealitemsGreen { get; set; }

        /// <summary>
        /// Number of Meal items in the given meal having Food Score in Yellow zone.
        /// </summary>
        public Int32 NumMealitemsYellow { get; set; }

        /// <summary>
        /// Number of Meal items in the given meal having Food Score in Red zone.
        /// </summary>
        public Int32 NumMealitemsRed { get; set; }

        /// <summary>
        /// Number of Meal items in the given meal containing Food Score.
        /// </summary>
        public Int32 NumMealitemsWithScore { get; set; }

        /// <summary>
        /// Fiber content (in grams)
        /// </summary>
        public Int32 Fiber { get; set; }

        /// <summary>
        /// Polyunsaturated fat content (in grams)
        /// </summary>
        public Decimal PolyunsaturatedFat { get; set; }

        /// <summary>
        /// Potassium content (in milligrams)
        /// </summary>
        public Decimal Potassium { get; set; }

        /// <summary>
        /// Fat content (in grams)
        /// </summary>
        public Decimal Fat { get; set; }

        /// <summary>
        /// Carbohydrate content (in grams)
        /// </summary>
        public Decimal Carbohydrate { get; set; }

        /// <summary>
        /// Saturated fat content (in grams)
        /// </summary>
        public Decimal SaturatedFat { get; set; }

        /// <summary>
        /// Protein content (in grams)
        /// </summary>
        public Decimal Protein { get; set; }

        /// <summary>
        /// Monosaturated fat content (in grams)
        /// </summary>
        public Decimal MonounsaturatedFat { get; set; }

        /// <summary>
        /// Sodium content (in milligrams)
        /// </summary>
        public Decimal Sodium { get; set; }

        /// <summary>
        /// Percentage of daily recommended vitamin C (based on a 2000 calorie diet)
        /// </summary>
        public Decimal VitaminC { get; set; }

        /// <summary>
        /// Percentage of daily recommended vitamin A (based on a 2000 calorie diet)
        /// </summary>
        public Decimal VitaminA { get; set; }

        /// <summary>
        /// Calories for this meal.
        /// </summary>
        public Decimal Calories { get; set; }

        /// <summary>
        /// Unsaturated fat content (in grams)
        /// </summary>
        public Decimal UnsaturatedFat { get; set; }

        /// <summary>
        /// Sugar content (in grams)
        /// </summary>
        public Decimal Sugar { get; set; }

        /// <summary>
        /// Calcium content (in milligrams)
        /// </summary>
        public Decimal Calcium { get; set; }

        /// <summary>
        /// Percentage of daily recommended iron (based on a 2000 calorie diet)
        /// </summary>
        public Decimal Iron { get; set; }

        /// <summary>
        /// Cholesterol content (in milligrams)
        /// </summary>
        public Decimal Cholesterol { get; set; }

        /// <summary>
        /// Caffeine content (in milligrams)
        /// </summary>
        public Decimal Caffeine { get; set; }

        public Decimal Accuracy { get; set; }

        /// <summary>
        /// Time zone when this event was generated. Whenever possible, Olson format (e.g., "America/Los Angeles") will be returned, otherwise the GMT offset (e.g., "GMT+0800") will be returned.
        /// </summary>
        public String Timezone { get; set; }
    }
}