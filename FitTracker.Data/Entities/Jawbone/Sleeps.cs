﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Jawbone
{
    public class Sleeps : AbstractEntity
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Unique ID for this event. Can be used to retrieve data for this specific event, see below.
        /// </summary>
        public String Xid { get; set; }

        /// <summary>
        /// Title of this sleep
        /// </summary>
        public String Title { get; set; }

        /// <summary>
        /// Type of sleep. 0=normal, 1=power_nap, 2=nap
        /// </summary>
        public Int32 SubType { get; set; }

        /// <summary>
        /// Type of event, in this case a sleep.
        /// </summary>
        public String Type { get; set; }

        /// <summary>
        /// Epoch timestamp when this sleep was created.
        /// </summary>
        public DateTime TimeCreated { get; set; }

        /// <summary>
        /// Epoch timestamp when this sleep was completed.
        /// </summary>
        public DateTime TimeCompleted { get; set; }

        /// <summary>
        /// Date when this sleep was created factoring in user timezone, formatted as YYYYMMDD.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Latitude of location where sleep was logged.
        /// </summary>
        public String PlaceLat { get; set; }

        /// <summary>
        /// Longitude of location where sleep was logged.
        /// </summary>
        public String PlaceLon { get; set; }

        /// <summary>
        /// Accuracy of location where sleep was logged, in meters.
        /// </summary>
        public String PlaceAcc { get; set; }

        /// <summary>
        /// Name of location where sleep was logged.
        /// </summary>
        public String PlaceName { get; set; }

        /// <summary>
        /// Link to the image of this sleep event (relative, add prefix https://jawbone.com)
        /// </summary>
        public String SnapshotImage { get; set; }

        /// <summary>
        /// Epoch timestamp when smart alarm was fired.
        /// </summary>
        public DateTime SmartAlarmFire { get; set; }

        /// <summary>
        /// Epoch timestamp when the user awoke.
        /// </summary>
        public DateTime AwakeTime { get; set; }

        /// <summary>
        /// Epoch timestamp when the user fell asleep.
        /// </summary>
        public DateTime AsleepTime { get; set; }

        /// <summary>
        /// Number of times the user awoke during sleep period.
        /// </summary>
        public Int32 Awakenings { get; set; }

        /// <summary>
        /// REM sleep duration in seconds.
        /// </summary>
        public Int32 Rem { get; set; }

        /// <summary>
        /// Total light sleep time, in seconds.
        /// </summary>
        public Int32 Light { get; set; }

        /// <summary>
        /// Total sound sleep time, in seconds.
        /// </summary>
        public Int32 Sound { get; set; }

        /// <summary>
        /// Total time spent awake, in seconds.
        /// </summary>
        public Int32 Awake { get; set; }

        /// <summary>
        /// Total time for this sleep event, in seconds.
        /// </summary>
        public Int32 Duration { get; set; }

        /// <summary>
        /// Sleep quality for the night. Based on a proprietary formula of light and deep sleep vs wake time. Note this is a different value than the percentage shown in the UP app (which is the percentage of sleep goal completed).
        /// </summary>
        public Int32 Quality { get; set; }

        /// <summary>
        /// Time zone when this event was generated. Whenever possible, Olson format (e.g., "America/Los Angeles") will be returned, otherwise the GMT offset (e.g., "GMT+0800") will be returned.
        /// </summary>
        public String Timezone { get; set; }
    }
}