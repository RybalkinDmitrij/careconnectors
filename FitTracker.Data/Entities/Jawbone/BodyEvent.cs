﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Jawbone
{
    public class BodyEvent : AbstractEntity
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Unique ID for this event. Can be used to retrieve data for this specific event, see below.
        /// </summary>
        public String Xid { get; set; }

        /// <summary>
        /// Title of this body event
        /// </summary>
        public String Title { get; set; }

        /// <summary>
        /// Type of event, in this case a body event.
        /// </summary>
        public String Type { get; set; }

        /// <summary>
        /// Epoch timestamp when this body event was created.
        /// </summary>
        public DateTime TimeCreated { get; set; }

        /// <summary>
        /// Epoch timestamp when this body event was updated.
        /// </summary>
        public DateTime TimeUpdated { get; set; }

        /// <summary>
        /// Date when this body event was created, formatted as YYYYMMDD.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Latitude of location where body event was logged.
        /// </summary>
        public Decimal PlaceLat { get; set; }

        /// <summary>
        /// Longitude of location where body event was logged.
        /// </summary>
        public Decimal PlaceLon { get; set; }

        /// <summary>
        /// Accuracy of location where body event was logged, in meters.
        /// </summary>
        public String PlaceAcc { get; set; }

        /// <summary>
        /// Name of location where body event was logged.
        /// </summary>
        public String PlaceName { get; set; }

        /// <summary>
        /// Note on this body event
        /// </summary>
        public String Note { get; set; }

        /// <summary>
        /// Lean mass percentage
        /// </summary>
        public Int32 LeanMass { get; set; }

        /// <summary>
        /// Body weight in kilograms.
        /// </summary>
        public Int32 Weight { get; set; }

        /// <summary>
        /// Body fat percentage.
        /// </summary>
        public Int32 BodyFat { get; set; }

        /// <summary>
        /// Body mass index.
        /// </summary>
        public Int32 BMI { get; set; }

        /// <summary>
        /// Time zone when this event was generated. Whenever possible, Olson format (e.g., "America/Los Angeles") will be returned, otherwise the GMT offset (e.g., "GMT+0800") will be returned.
        /// </summary>
        public String Timezone { get; set; }
    }
}
