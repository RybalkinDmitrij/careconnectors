﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Jawbone
{
    public class Mood : AbstractEntity
    {
        /// <summary>
        /// Identity of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Unique ID for this event. Can be used to retrieve data for this specific event, see below.
        /// </summary>
        public String Xid { get; set; }

        /// <summary>
        /// Title of this mood.
        /// </summary>
        public String Title { get; set; }

        /// <summary>
        /// Epoch timestamp when this mood was created.
        /// </summary>
        public DateTime TimeCreated { get; set; }

        /// <summary>
        /// Epoch timestamp when this mood was last updated.
        /// </summary>
        public DateTime TimeUpdated { get; set; }

        /// <summary>
        /// Date when this mood was created, formatted as YYYYMMDD.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Type of event, in this case a mood.
        /// </summary>
        public String Type { get; set; }

        /// <summary>
        /// Mood type. 1 = Amazing, 2 = Pumped UP, 3 = Energized, 8= Good, 4 = Meh, 5 = Dragging, 6 = Exhausted, 7 = Totally Done. NOTE: mood number 8 is out of sequence, see below.
        /// </summary>
        public Int32 SubType { get; set; }

        /// <summary>
        /// Latitude of location where mood was logged.
        /// </summary>
        public Decimal PlaceLat { get; set; }

        /// <summary>
        /// Longitude of location where mood was logged.
        /// </summary>
        public Decimal PlaceLon { get; set; }

        /// <summary>
        /// Accuracy of location where mood was logged, in meters.
        /// </summary>
        public String PlaceAcc { get; set; }

        /// <summary>
        /// Name of location where mood was logged.
        /// </summary>
        public String PlaceName { get; set; }

        /// <summary>
        /// Time zone when this event was generated. Whenever possible, Olson format (e.g., "America/Los Angeles") will be returned, otherwise the GMT offset (e.g., "GMT+0800") will be returned.
        /// </summary>
        public String Timezone { get; set; }
    }
}