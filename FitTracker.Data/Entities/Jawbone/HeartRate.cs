﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Jawbone
{
    public class HeartRate : AbstractEntity
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Unique ID for this event. Can be used to retrieve data for this specific event, see below.
        /// </summary>
        public String Xid { get; set; }

        /// <summary>
        /// Title of this cardiac event
        /// </summary>
        public String Title { get; set; }

        /// <summary>
        /// Epoch timestamp when this cardiac event was created.
        /// </summary>
        public DateTime TimeCreated { get; set; }

        /// <summary>
        /// Epoch timestamp when this cardiac event was updated.
        /// </summary>
        public DateTime TimeUpdated { get; set; }

        /// <summary>
        /// Date when this cardiac event was created, formatted as YYYYMMDD.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Latitude of location where cardiac event was logged.
        /// </summary>
        public String PlaceLat { get; set; }

        /// <summary>
        /// Longitude of location where cardiac event was logged.
        /// </summary>
        public String PlaceLon { get; set; }

        /// <summary>
        /// Accuracy of location where cardiac event was logged, in meters.
        /// </summary>
        public String PlaceAcc { get; set; }

        /// <summary>
        /// Name of location where cardiac event was logged.
        /// </summary>
        public String PlaceName { get; set; }

        /// <summary>
        /// Resting heart rate of user for the day.
        /// </summary>
        public Int32 RestingHeartrate { get; set; }

        /// <summary>
        /// Time zone when this event was generated. Whenever possible, Olson format (e.g., "America/Los Angeles") will be returned, otherwise the GMT offset (e.g., "GMT+0800") will be returned.
        /// </summary>
        public String Timezone { get; set; }
    }
}