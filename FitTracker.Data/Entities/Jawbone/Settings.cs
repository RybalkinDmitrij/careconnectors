﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Jawbone
{
    public class Settings : AbstractEntity
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Unique ID for user.
        /// </summary>
        public String Xid { get; set; }

        /// <summary>
        /// User setting for units. 0 = Imperial (American) system, 1 = Metric system
        /// </summary>
        public Int32 Metric { get; set; }

        /// <summary>
        /// Whether the user shares sleep events with their teammates. Requires sleep_read scope.
        /// </summary>
        public Boolean ShareSleep { get; set; }

        /// <summary>
        /// Whether the user shares mood events with their teammates. Requires mood_read scope.
        /// </summary>
        public Boolean ShareMood { get; set; }

        /// <summary>
        /// Whether the user shares body events with their teammates. Requires weight_read scope.
        /// </summary>
        public Boolean ShareBody { get; set; }

        /// <summary>
        /// Whether the user shares meal events with their teammates. Requires meal_read scope.
        /// </summary>
        public Boolean ShareEat { get; set; }

        /// <summary>
        /// Whether the user shares move events with their teammates. Requires move_read scope.
        /// </summary>
        public Boolean ShareMove { get; set; }

        public Int32 Steps { get; set; }

        public Int32 Total { get; set; }
    }
}