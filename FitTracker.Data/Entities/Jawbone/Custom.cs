﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Jawbone
{
    public class Custom : AbstractEntity
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Unique ID for this event. Can be used to retrieve data for this specific event, see below.
        /// </summary>
        public String Xid { get; set; }

        /// <summary>
        /// Title of this generic event
        /// </summary>
        public String Title { get; set; }

        /// <summary>
        /// Type of event, in this case a generic event.
        /// </summary>
        public String Type { get; set; }

        /// <summary>
        /// Epoch timestamp when this generic event was created.
        /// </summary>
        public DateTime TimeCreated { get; set; }

        /// <summary>
        /// Epoch timestamp when this generic event was updated.
        /// </summary>
        public DateTime TimeUpdated { get; set; }

        /// <summary>
        /// Date when this generic event was created, formatted as YYYYMMDD.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Latitude of location where generic event was logged.
        /// </summary>
        public String PlaceLat { get; set; }

        /// <summary>
        /// Longitude of location where generic event was logged.
        /// </summary>
        public String PlaceLon { get; set; }

        /// <summary>
        /// Accuracy of location where generic event was logged, in meters.
        /// </summary>
        public String PlaceAcc { get; set; }

        /// <summary>
        /// Name of location where generic event was logged.
        /// </summary>
        public String PlaceName { get; set; }

        /// <summary>
        /// Link to the image of this generic event (relative, add prefix https://jawbone.com)
        /// </summary>
        public String Image { get; set; }

        public String Description { get; set; }

        /// <summary>
        /// Time zone when this event was generated. Whenever possible, Olson format (e.g., "America/Los Angeles") will be returned, otherwise the GMT offset (e.g., "GMT+0800") will be returned.
        /// </summary>
        public String Timezone { get; set; }
    }
}
