﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Jawbone
{
    public class Timezone : AbstractEntity
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Date in YYYYMMDD format, calculated based on 'time' and 'tz'.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Time zone when this event was generated. Whenever possible, Olson format (e.g., "America/Los Angeles") will be returned, otherwise the GMT offset (e.g., "GMT+0800") will be returned.
        /// </summary>
        public String Tz { get; set; }

        /// <summary>
        /// Epoch timestamp when timezone was set.
        /// </summary>
        public DateTime Time { get; set; }
    }
}