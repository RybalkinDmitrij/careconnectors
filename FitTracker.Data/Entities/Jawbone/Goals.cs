﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Jawbone
{
    public class Goals : AbstractEntity
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Number of steps per day goal.
        /// </summary>
        public Int32 MoveSteps { get; set; }

        /// <summary>
        /// Sleep goal, in seconds.
        /// </summary>
        public Int32 SleepTotal { get; set; }

        /// <summary>
        /// Body weight goal.
        /// </summary>
        public Decimal BodyWeight { get; set; }

        /// <summary>
        /// User's intent for weight management. 0=lose, 1=maintain, 2=gain
        /// </summary>
        public Int32 BodyWeightIntent { get; set; }

        /// <summary>
        /// Number of calories available for the user to consume for today, in order to achieve their weight goal. If negative, the user has already went over the limit by that amount.
        /// </summary>
        public Decimal IntakeCaloriesRemaining { get; set; }

        /// <summary>
        /// Number of steps remaining for today for user to achieve their step goal. If negative, the user has already surpassed their goal for today by that number of steps.
        /// </summary>
        public Int32 MoveStepsRemaining { get; set; }

        /// <summary>
        /// Number of seconds the user must sleep for today in order to achieve their sleep gaol. If negative, the user has already surpassed their goal for today by that number of seconds.
        /// </summary>
        public Int32 SleepSecondsRemaining { get; set; }
    }
}