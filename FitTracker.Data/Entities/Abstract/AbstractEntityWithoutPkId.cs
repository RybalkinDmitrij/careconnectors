﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Abstract
{
    public abstract class AbstractEntityWithoutPkId
    {
        public Boolean IsDeleted { get; set; }

        public DateTime DateCreate { get; set; }

        public DateTime DateUpdate { get; set; }

        public DateTime? DateDelete { get; set; }

        public Int64 UserCreateId { get; set; }

        public Int64 UserUpdateId { get; set; }

        public Int64 UserDeleteId { get; set; }
    }
}
