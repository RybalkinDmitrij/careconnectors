﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Auth
{
    public class Client : AbstractEntity
    {
        public String ExternalId { get; set; }

        public String AccessToken { get; set; }

        public String AspNetId { get; set; }

        public String AspNetUserName { get; set; }

        public String AspNetPassword { get; set; }

        public Int64 ApplicantId { get; set; }

        public virtual Applicant Applicant { get; set; }
        
        public String FitbitId { get; set; }

        public String FitbitAccessToken { get; set; }

        public String FitbitSecret { get; set; }

        public String WithingsId { get; set; }

        public String WithingsAccessToken { get; set; }

        public String WithingsSecret { get; set; }

        public String JawboneId { get; set; }

        public String JawboneAccessToken { get; set; }

        public String JawboneRefreshToken { get; set; }

        public String RunkeeperId { get; set; }

        public String RunkeeperAccessToken { get; set; }

        public String RunkeeperRefreshToken { get; set; }

        public String StravaId { get; set; }

        public String StravaAccessToken { get; set; }

        public String StravaRefreshToken { get; set; }

        public String MisfitId { get; set; }

        public String MisfitAccessToken { get; set; }

        public String MisfitRefreshToken { get; set; }
        
        public void Init()
        {
            this.AccessToken = String.Empty;

            this.FitbitId = String.Empty;
            this.FitbitAccessToken = String.Empty;
            this.FitbitSecret = String.Empty;
            this.WithingsId = String.Empty;
            this.WithingsAccessToken = String.Empty;
            this.WithingsSecret = String.Empty;
            this.JawboneId = String.Empty;
            this.JawboneAccessToken = String.Empty;
            this.JawboneRefreshToken = String.Empty;
            this.RunkeeperId = String.Empty;
            this.RunkeeperAccessToken = String.Empty;
            this.RunkeeperRefreshToken = String.Empty;
            this.StravaId = String.Empty;
            this.StravaAccessToken = String.Empty;
            this.StravaRefreshToken = String.Empty;
            this.MisfitId = String.Empty;
            this.MisfitAccessToken = String.Empty;
            this.MisfitRefreshToken = String.Empty;
        }
    }
}