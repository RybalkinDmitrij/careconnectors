﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Auth
{
    public class Applicant : AbstractEntity
    {
        public Int64 UserId { get; set; }

        public virtual DbUser User { get; set; }
        
        public String UniqueId { get; set; }

        public String ConsumerKey { get; set; }

        public String ConsumerSecret { get; set; }

        public String NotificationUrl { get; set; }

        public virtual ICollection<Client> Clients { get; set; }
    }
}
