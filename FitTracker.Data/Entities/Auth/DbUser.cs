﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Auth
{
    public class DbUser : AbstractEntity
    {
        public String Login { get; set; }

        public String PasswordHash { get; set; }

        public String LastName { get; set; }

        public String FirstName { get; set; }

        public virtual ICollection<Applicant> Applicants { get; set; }
    }
}
