﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Misfit
{
    public class Goal : AbstractEntity
    {
        public Int64 ProfileInfoId { get; set; }

        public virtual ProfileInfo ProfileInfo { get; set; }

        public String Id { get; set; }

        public DateTime Date { get; set; }

        public Decimal Points { get; set; }

        public Int32 TargetPoints { get; set; }
    }
}
