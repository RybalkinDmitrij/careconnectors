﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Misfit
{
    public class Session : AbstractEntity
    {
        public Int64 ProfileInfoId { get; set; }

        public virtual ProfileInfo ProfileInfo { get; set; }

        public String Id { get; set; }

        public String ActivityType { get; set; }

        public DateTime StartTime { get; set; }

        public Int32 Duration { get; set; }

        public Decimal Points { get; set; }

        public Int32 Steps { get; set; }

        public Decimal Calories { get; set; }

        public Decimal Distance { get; set; }
    }
}
