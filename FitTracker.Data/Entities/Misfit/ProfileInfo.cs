﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Misfit
{
    public class ProfileInfo : AbstractEntity
    {
        public String UserId { get; set; }

        public String Name { get; set; }

        public String Birthday { get; set; }

        public String Gender { get; set; }

        public String Email { get; set; }

        public virtual ICollection<DeviceMisfit> Devices { get; set; }

        public virtual ICollection<Goal> Goals { get; set; }

        public virtual ICollection<Session> Sessions { get; set; }

        public virtual ICollection<SleepMisfit> Sleeps { get; set; }

        public virtual ICollection<Summary> Summaries { get; set; }
    }
}
