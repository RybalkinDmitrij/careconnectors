﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Misfit
{
    public class DeviceMisfit : AbstractEntity
    {
        public Int64 ProfileInfoId { get; set; }

        public virtual ProfileInfo ProfileInfo { get; set; }

        public String Id { get; set; }

        public String DeviceType { get; set; }

        public String SerialNumber { get; set; }

        public String FirmwareVersion { get; set; }

        public Int32 BatteryLevel { get; set; }

        public Int32 LastSyncTime { get; set; }
    }
}
