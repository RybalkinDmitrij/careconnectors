﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Misfit
{
    public class SleepDetail : AbstractEntity
    {
        public Int64 SleepId { get; set; }

        public virtual SleepMisfit Sleep { get; set; }

        public DateTime Datetime { get; set; }

        public Int32 Value { get; set; }
    }
}
