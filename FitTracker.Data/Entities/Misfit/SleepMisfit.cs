﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitTracker.Data.Entities.Misfit
{
    public class SleepMisfit : AbstractEntity
    {
        public Int64 ProfileInfoId { get; set; }

        public virtual ProfileInfo ProfileInfo { get; set; }

        public String Id { get; set; }

        public Boolean AutoDetected { get; set; }

        public DateTime StartTime { get; set; }

        public Int32 Duration { get; set; }

        public virtual ICollection<SleepDetail> SleepDetail { get; set; }
    }
}
