﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Log
{
    public class LogError
    {
        public Guid Id { get; set; }

        public String Text { get; set; }

        public Int64 UserId { get; set; }

        public DateTime Date { get; set; }
    }
}
