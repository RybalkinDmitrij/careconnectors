﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Log
{
    public class LogAction
    {
        public Guid Id { get; set; }

        public String Action { get; set; }

        public String Url { get; set; }

        public Int64 UserId { get; set; }

        public DateTime Date { get; set; }
    }
}
