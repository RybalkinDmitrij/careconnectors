﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Runkeeper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FitTracker.Data.Enums.Xamarin;

namespace FitTracker.Data.Entities.Xamarin
{
    public class FitnessActivity : AbstractEntity
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public Int64 UserId { get; set; }

        /// <summary>
        /// The value of the measured quantity.
        /// </summary>
        public Decimal? Measurement { get; set; }

        /// <summary>
        /// The measurement type.
        /// </summary>
        public FitnessActivityType Type { get; set; }

        /// <summary>
        /// The time at which the measurement was taken (e.g., "Sat, 1 Jan 2011 00:00:00"). Read only.
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// The name of the application that last modified this activity. Read only.
        /// </summary>
        public String Source { get; set; }
    }
}
