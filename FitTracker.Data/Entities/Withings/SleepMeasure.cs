﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Withings
{
    public class SleepMeasure : AbstractEntity
    {
        /// <summary>
        /// Identity of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// The starting datetime for the sleep state data
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// The end datetime for the sleep data. A single call can span up to 7 days maximum. To cover an wider time range, you'll need to perform mutliple calls
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// The state of sleeping. Values can be :
        /// 0 : awake
        /// 1 : light sleep
        /// 2 : deep sleep
        /// 3 : REM sleep (only if model is 32)
        /// </summary>
        public Int32 State { get; set; }
    }
}
