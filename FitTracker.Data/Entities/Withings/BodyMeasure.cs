﻿using FitTracker.Data.Entities.Abstract;
using FitTracker.Data.Enums.Withings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Withings
{
    public class BodyMeasure : AbstractEntity
    {
        /// <summary>
        /// Identity of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Id of the measure group.
        /// </summary>
        public Int32 GrpId { get; set; }

        /// <summary>
        /// The way the measure was attributed to the user. Values can be : 
        /// 0 : The measuregroup has been captured by a device and is known to belong to this user (and is not ambiguous)
        /// 1 : The measuregroup has been captured by a device but may belong to other users as well as this one (it is ambiguous)
        /// 2 : The measuregroup has been entered manually for this particular user
        /// 4 : The measuregroup has been entered manually during user creation (and may not be accurate)r
        /// </summary>
        public Int32 Attrib { get; set; }

        /// <summary>
        /// Date the measures in the group were taken.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Category for the measures in the group (see category input parameter).
        /// </summary>
        public Int32 Category { get; set; }

        /// <summary>
        /// Body scale or heart rates ?
        /// </summary>
        public MeasureType Type { get; set; }

        /// <summary>
        /// List of measures in the group
        /// </summary>
        public virtual ICollection<BodyMeasureItem> Measures { get; set; }
    }
}
