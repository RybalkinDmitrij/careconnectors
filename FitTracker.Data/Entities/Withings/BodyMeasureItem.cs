﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Withings
{
    public class BodyMeasureItem : AbstractEntity
    {
        public Int64 BodyMeasureId { get; set; }

        public BodyMeasure BodyMeasure { get; set; }

        /// <summary>
        /// Value for the measure in S.I units (kilogram, meters, etc.). Value should be multiplied by 10 to the power of "unit" (see below) to get the real value.
        /// </summary>
        public Int32 Value { get; set; }

        /// <summary>
        /// Type of the measure.
        /// 1 : Weight (kg)
        /// 4 : Height (meter)
        /// 5 : Fat Free Mass (kg)
        /// 6 : Fat Ratio (%)
        /// 8 : Fat Mass Weight (kg)
        /// 9 : Diastolic Blood Pressure (mmHg)
        /// 10 : Systolic Blood Pressure (mmHg)
        /// 11 : Heart Pulse (bpm)
        /// 54 : SP02(%)
        /// </summary>
        public Int32 Type { get; set; }

        /// <summary>
        /// Power of ten the "value" parameter should be multiplied to to get the real value. Eg : value = 20 and unit=-1 means the value really is 2.0
        /// </summary>
        public Int32 Unit { get; set; }
    }
}
