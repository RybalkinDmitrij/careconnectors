﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Withings
{
    public class ActivityMeasure : AbstractEntity
    {
        /// <summary>
        /// Identity of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Date at which the measure was taken or entered.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Number of steps for the day.
        /// </summary>
        public Int32 Steps { get; set; }

        /// <summary>
        /// Distance travelled for the day (in meters).
        /// </summary>
        public Int32 Distance { get; set; }

        /// <summary>
        /// Active Calories burned in the day (in kcal).
        /// </summary>
        public Decimal Calories { get; set; }

        /// <summary>
        /// Total Calories burned in the day (in kcal).
        /// </summary>
        public Decimal TotalCalories { get; set; }

        /// <summary>
        /// Elevation climbed during the day (in meters).
        /// </summary>
        public Decimal Elevation { get; set; }

        /// <summary>
        /// Duration of soft activities (in seconds).
        /// </summary>
        public Int32 Soft { get; set; }

        /// <summary>
        /// Duration of moderate activities (in seconds).
        /// </summary>
        public Int32 Moderate { get; set; }

        /// <summary>
        /// Duration of intense activities (in seconds).
        /// </summary>
        public Int32 Intense { get; set; }

        /// <summary>
        /// Timezone for the date.
        /// </summary>
        public String Timezone { get; set; }
    }
}
