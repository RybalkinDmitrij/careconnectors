﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Withings
{
    public class IntradayActivity : AbstractEntity
    {
        /// <summary>
        /// Identity of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public DateTime Name { get; set; }

        /// <summary>
        /// Calories burned (in kcal).
        /// </summary>
        public Decimal Calories { get; set; }

        /// <summary>
        /// Duration of the activity (in seconds).
        /// </summary>
        public Int32 Duration { get; set; }

        /// <summary>
        /// Number of steps performed.
        /// </summary>
        public Int32 Steps { get; set; }

        /// <summary>
        /// Elevation climbed (in meters).
        /// </summary>
        public Decimal Elevation { get; set; }

        /// <summary>
        /// Distance travelled (in meters).
        /// </summary>
        public Decimal Distance { get; set; }
    }
}
