﻿using FitTracker.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTracker.Data.Entities.Withings
{
    public class SleepSummaryW : AbstractEntity
    {
        /// <summary>
        /// Identity of user
        /// </summary>
        public String UserId { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public Int32 Id { get; set; }

        /// <summary>
        /// Timezone for the date.
        /// </summary>
        public String Timezone { get; set; }

        /// <summary>
        /// Source for the sleep data. Values can be :
        /// 16 : Pulse
        /// 32 : Aura
        /// </summary>
        public Int32 Model { get; set; }

        /// <summary>
        /// The starting datetime for the sleep state data
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// The end datetime for the sleep data. A single call can span up to 200 days maximum. To cover an wider time range, you'll need to perform mutliple calls
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// The date of the sleep data ( please note that the date corresponds to the end of the sleep
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Duration wake up
        /// </summary>
        public Int32 WakeUpDuration { get; set; }

        /// <summary>
        /// Duration in state light sleep
        /// </summary>
        public Int32 LightSleepDuration { get; set; }

        /// <summary>
        /// Duration in state deep sleep
        /// </summary>
        public Int32 DeepSleepDuration { get; set; }

        /// <summary>
        /// Duration in state REM sleep
        /// </summary>
        public Int32 RemSleepDuration { get; set; }

        /// <summary>
        /// Time to sleep
        /// </summary>
        public Int32 DurationToSleep { get; set; }

        /// <summary>
        /// Time to wake up
        /// </summary>
        public Int32 DurationToWakeup { get; set; }

        /// <summary>
        /// Number of times the user woke up
        /// </summary>
        public Int32 WakeupCount { get; set; }

        public DateTime Modified { get; set; }
    }
}