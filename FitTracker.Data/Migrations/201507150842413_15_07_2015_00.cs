namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _15_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ath_Client", "StravaId", c => c.String());
            AddColumn("dbo.ath_Client", "StravaAccessToken", c => c.String(nullable: false));
            AddColumn("dbo.ath_Client", "StravaRefreshToken", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ath_Client", "StravaRefreshToken");
            DropColumn("dbo.ath_Client", "StravaAccessToken");
            DropColumn("dbo.ath_Client", "StravaId");
        }
    }
}
