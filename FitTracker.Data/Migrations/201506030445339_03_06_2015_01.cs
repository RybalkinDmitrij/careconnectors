namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _03_06_2015_01 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.jwb_BodyEvent", "PlaceAcc", c => c.String(nullable: false));
            AlterColumn("dbo.jwb_Custom", "PlaceAcc", c => c.String(nullable: false));
            AlterColumn("dbo.jwb_HeartRate", "PlaceAcc", c => c.String(nullable: false));
            AlterColumn("dbo.jwb_Meals", "PlaceAcc", c => c.String(nullable: false));
            AlterColumn("dbo.jwb_Mood", "PlaceAcc", c => c.String(nullable: false));
            AlterColumn("dbo.jwb_Workouts", "PlaceAcc", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.jwb_Workouts", "PlaceAcc", c => c.Int(nullable: false));
            AlterColumn("dbo.jwb_Mood", "PlaceAcc", c => c.Int(nullable: false));
            AlterColumn("dbo.jwb_Meals", "PlaceAcc", c => c.Int(nullable: false));
            AlterColumn("dbo.jwb_HeartRate", "PlaceAcc", c => c.Int(nullable: false));
            AlterColumn("dbo.jwb_Custom", "PlaceAcc", c => c.Int(nullable: false));
            AlterColumn("dbo.jwb_BodyEvent", "PlaceAcc", c => c.Int(nullable: false));
        }
    }
}
