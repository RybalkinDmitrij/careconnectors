namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _15_05_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ath_Client", "AccessToken", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ath_Client", "AccessToken");
        }
    }
}
