namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _24_04_2015_00 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.fbt_FoodLog", "LoggedFoodId", "dbo.fbt_LoggedFood");
            DropForeignKey("dbo.fbt_FoodLog", "NutritionalValuesId", "dbo.fbt_NutritionalValues");
            DropForeignKey("dbo.fbt_LoggedFood", "UnitId", "dbo.fbt_FoodLogUnit");
            DropIndex("dbo.fbt_FoodLog", new[] { "LoggedFoodId" });
            DropIndex("dbo.fbt_FoodLog", new[] { "NutritionalValuesId" });
            DropIndex("dbo.fbt_LoggedFood", new[] { "UnitId" });
            AlterColumn("dbo.fbt_FoodLog", "LoggedFoodId", c => c.Long());
            AlterColumn("dbo.fbt_FoodLog", "NutritionalValuesId", c => c.Long());
            AlterColumn("dbo.fbt_LoggedFood", "UnitId", c => c.Long());
            CreateIndex("dbo.fbt_FoodLog", "LoggedFoodId");
            CreateIndex("dbo.fbt_FoodLog", "NutritionalValuesId");
            CreateIndex("dbo.fbt_LoggedFood", "UnitId");
            AddForeignKey("dbo.fbt_FoodLog", "LoggedFoodId", "dbo.fbt_LoggedFood", "PkID");
            AddForeignKey("dbo.fbt_FoodLog", "NutritionalValuesId", "dbo.fbt_NutritionalValues", "PkID");
            AddForeignKey("dbo.fbt_LoggedFood", "UnitId", "dbo.fbt_FoodLogUnit", "PkID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.fbt_LoggedFood", "UnitId", "dbo.fbt_FoodLogUnit");
            DropForeignKey("dbo.fbt_FoodLog", "NutritionalValuesId", "dbo.fbt_NutritionalValues");
            DropForeignKey("dbo.fbt_FoodLog", "LoggedFoodId", "dbo.fbt_LoggedFood");
            DropIndex("dbo.fbt_LoggedFood", new[] { "UnitId" });
            DropIndex("dbo.fbt_FoodLog", new[] { "NutritionalValuesId" });
            DropIndex("dbo.fbt_FoodLog", new[] { "LoggedFoodId" });
            AlterColumn("dbo.fbt_LoggedFood", "UnitId", c => c.Long(nullable: false));
            AlterColumn("dbo.fbt_FoodLog", "NutritionalValuesId", c => c.Long(nullable: false));
            AlterColumn("dbo.fbt_FoodLog", "LoggedFoodId", c => c.Long(nullable: false));
            CreateIndex("dbo.fbt_LoggedFood", "UnitId");
            CreateIndex("dbo.fbt_FoodLog", "NutritionalValuesId");
            CreateIndex("dbo.fbt_FoodLog", "LoggedFoodId");
            AddForeignKey("dbo.fbt_LoggedFood", "UnitId", "dbo.fbt_FoodLogUnit", "PkID", cascadeDelete: true);
            AddForeignKey("dbo.fbt_FoodLog", "NutritionalValuesId", "dbo.fbt_NutritionalValues", "PkID", cascadeDelete: true);
            AddForeignKey("dbo.fbt_FoodLog", "LoggedFoodId", "dbo.fbt_LoggedFood", "PkID", cascadeDelete: true);
        }
    }
}
