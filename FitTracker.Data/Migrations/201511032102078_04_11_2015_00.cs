namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _04_11_2015_00 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.hlk_BaseData",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        Measurement = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Type = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Source = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.hlk_BodyMeasurement",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        Measurement = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Type = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Source = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.hlk_FitnessActivity",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        Measurement = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Type = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Source = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.hlk_Sleep",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        Measurement = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Type = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Source = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.hlk_Sleep");
            DropTable("dbo.hlk_FitnessActivity");
            DropTable("dbo.hlk_BodyMeasurement");
            DropTable("dbo.hlk_BaseData");
        }
    }
}
