namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _27_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ath_Client", "MisfitId", c => c.String());
            AddColumn("dbo.ath_Client", "MisfitAccessToken", c => c.String(nullable: false));
            AddColumn("dbo.ath_Client", "MisfitRefreshToken", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ath_Client", "MisfitRefreshToken");
            DropColumn("dbo.ath_Client", "MisfitAccessToken");
            DropColumn("dbo.ath_Client", "MisfitId");
        }
    }
}
