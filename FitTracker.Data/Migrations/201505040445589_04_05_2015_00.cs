namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _04_05_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ath_Client", "JawboneId", c => c.String());
            AddColumn("dbo.ath_Client", "JawboneAccessToken", c => c.String(nullable: false, defaultValue: ""));
            AddColumn("dbo.ath_Client", "JawboneRefreshToken", c => c.String(nullable: false, defaultValue: ""));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ath_Client", "JawboneRefreshToken");
            DropColumn("dbo.ath_Client", "JawboneAccessToken");
            DropColumn("dbo.ath_Client", "JawboneId");
        }
    }
}
