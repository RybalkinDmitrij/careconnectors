namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _13_05_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ath_Client", "AspNetId", c => c.String(nullable: false));
            AddColumn("dbo.ath_Client", "AspNetUserName", c => c.String(nullable: false));
            AddColumn("dbo.ath_Client", "AspNetPassword", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ath_Client", "AspNetPassword");
            DropColumn("dbo.ath_Client", "AspNetUserName");
            DropColumn("dbo.ath_Client", "AspNetId");
        }
    }
}
