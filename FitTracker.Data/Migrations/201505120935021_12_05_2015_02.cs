namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _12_05_2015_02 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ath_MMApplicantClient",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        ClientId = c.Long(nullable: false),
                        ApplicantId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.ath_Applicant", t => t.ApplicantId, cascadeDelete: true)
                .ForeignKey("dbo.ath_Client", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId)
                .Index(t => t.ApplicantId);
            
            CreateTable(
                "dbo.ath_Applicant",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        ClientId = c.Long(nullable: false),
                        UniqueId = c.String(nullable: false),
                        ConsumerKey = c.String(nullable: false),
                        ConsumerSecret = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            AddColumn("dbo.ath_Client", "Login", c => c.String(nullable: false));
            AddColumn("dbo.ath_Client", "PasswordHash", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ath_MMApplicantClient", "ClientId", "dbo.ath_Client");
            DropForeignKey("dbo.ath_MMApplicantClient", "ApplicantId", "dbo.ath_Applicant");
            DropIndex("dbo.ath_MMApplicantClient", new[] { "ApplicantId" });
            DropIndex("dbo.ath_MMApplicantClient", new[] { "ClientId" });
            DropColumn("dbo.ath_Client", "PasswordHash");
            DropColumn("dbo.ath_Client", "Login");
            DropTable("dbo.ath_Applicant");
            DropTable("dbo.ath_MMApplicantClient");
        }
    }
}
