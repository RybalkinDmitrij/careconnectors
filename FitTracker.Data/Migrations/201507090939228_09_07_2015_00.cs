namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _09_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.rk_BackgroundActivities", "Source", c => c.String(nullable: false));
            AddColumn("dbo.rk_FitnessActivities", "Source", c => c.String(nullable: false));
            AddColumn("dbo.rk_DiabetesMeasurements", "Source", c => c.String(nullable: false));
            AddColumn("dbo.rk_StrengthTraining", "Source", c => c.String(nullable: false));
            AddColumn("dbo.rk_GeneralMeasurements", "Source", c => c.String(nullable: false));
            AddColumn("dbo.rk_Nutrition", "Source", c => c.String(nullable: false));
            AddColumn("dbo.rk_Sleep", "Source", c => c.String(nullable: false));
            AddColumn("dbo.rk_Weight", "Source", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.rk_Weight", "Source");
            DropColumn("dbo.rk_Sleep", "Source");
            DropColumn("dbo.rk_Nutrition", "Source");
            DropColumn("dbo.rk_GeneralMeasurements", "Source");
            DropColumn("dbo.rk_StrengthTraining", "Source");
            DropColumn("dbo.rk_DiabetesMeasurements", "Source");
            DropColumn("dbo.rk_FitnessActivities", "Source");
            DropColumn("dbo.rk_BackgroundActivities", "Source");
        }
    }
}
