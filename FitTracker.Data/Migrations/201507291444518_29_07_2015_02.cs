namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _29_07_2015_02 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.mft_SleepDetail",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        SleepId = c.Long(nullable: false),
                        Datetime = c.DateTime(nullable: false),
                        Value = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.mft_Sleep", t => t.SleepId, cascadeDelete: true)
                .Index(t => t.SleepId);
            
            DropColumn("dbo.mft_Sleep", "Datetime");
            DropColumn("dbo.mft_Sleep", "Value");
        }
        
        public override void Down()
        {
            AddColumn("dbo.mft_Sleep", "Value", c => c.String(nullable: false));
            AddColumn("dbo.mft_Sleep", "Datetime", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.mft_SleepDetail", "SleepId", "dbo.mft_Sleep");
            DropIndex("dbo.mft_SleepDetail", new[] { "SleepId" });
            DropTable("dbo.mft_SleepDetail");
        }
    }
}
