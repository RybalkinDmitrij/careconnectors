namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _25_05_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.jwb_Sleeps", "SmartAlarmFire", c => c.DateTime(nullable: false));
            AddColumn("dbo.jwb_Sleeps", "AwakeTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.jwb_Sleeps", "AsleepTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.jwb_Sleeps", "Awakenings", c => c.Int(nullable: false));
            AddColumn("dbo.jwb_Sleeps", "Rem", c => c.Int(nullable: false));
            AddColumn("dbo.jwb_Sleeps", "Light", c => c.Int(nullable: false));
            AddColumn("dbo.jwb_Sleeps", "Sound", c => c.Int(nullable: false));
            AddColumn("dbo.jwb_Sleeps", "Awake", c => c.Int(nullable: false));
            AddColumn("dbo.jwb_Sleeps", "Duration", c => c.Int(nullable: false));
            AddColumn("dbo.jwb_Sleeps", "Quality", c => c.Int(nullable: false));
            AddColumn("dbo.jwb_Sleeps", "Timezone", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.jwb_Sleeps", "Timezone");
            DropColumn("dbo.jwb_Sleeps", "Quality");
            DropColumn("dbo.jwb_Sleeps", "Duration");
            DropColumn("dbo.jwb_Sleeps", "Awake");
            DropColumn("dbo.jwb_Sleeps", "Sound");
            DropColumn("dbo.jwb_Sleeps", "Light");
            DropColumn("dbo.jwb_Sleeps", "Rem");
            DropColumn("dbo.jwb_Sleeps", "Awakenings");
            DropColumn("dbo.jwb_Sleeps", "AsleepTime");
            DropColumn("dbo.jwb_Sleeps", "AwakeTime");
            DropColumn("dbo.jwb_Sleeps", "SmartAlarmFire");
        }
    }
}
