namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _25_05_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ath_Client", "NotificationUrl", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ath_Client", "NotificationUrl");
        }
    }
}
