namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _29_07_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.mft_Summary", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.mft_Summary", "Date");
        }
    }
}
