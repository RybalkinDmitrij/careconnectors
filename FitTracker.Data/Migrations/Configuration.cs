using System;
using System.Linq;
using System.Data.Entity.Migrations;

using FitTracker.Data.Entities;
using FitTracker.Core.Unity;
using FitTracker.Data.Repo;

namespace FitTracker.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<FitTrackerContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
    }
}
