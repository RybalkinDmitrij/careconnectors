namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _21_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.str_Bike");
            DropPrimaryKey("dbo.str_Shoes");
            AddColumn("dbo.str_Athlete", "ResourceState", c => c.Int(nullable: false));
            AddColumn("dbo.str_Bike", "GearId", c => c.Long(nullable: false));
            AddColumn("dbo.str_Shoes", "GearId", c => c.Long(nullable: false));
            AddColumn("dbo.str_Shoes", "Brand", c => c.String(nullable: false));
            AddColumn("dbo.str_Shoes", "Model", c => c.String(nullable: false));
            AddColumn("dbo.str_Shoes", "Description", c => c.String(nullable: false));
            AddPrimaryKey("dbo.str_Bike", "GearId");
            AddPrimaryKey("dbo.str_Shoes", "GearId");
            CreateIndex("dbo.str_Bike", "GearId");
            CreateIndex("dbo.str_Shoes", "GearId");
            AddForeignKey("dbo.str_Bike", "GearId", "dbo.str_Gear", "PkID");
            AddForeignKey("dbo.str_Shoes", "GearId", "dbo.str_Gear", "PkID");
            DropColumn("dbo.str_Bike", "PkID");
            DropColumn("dbo.str_Shoes", "PkID");
            DropColumn("dbo.str_Shoes", "Id");
            DropColumn("dbo.str_Shoes", "IsPrimary");
            DropColumn("dbo.str_Shoes", "Name");
            DropColumn("dbo.str_Shoes", "Distance");
            DropColumn("dbo.str_Shoes", "ResourceState");
        }
        
        public override void Down()
        {
            AddColumn("dbo.str_Shoes", "ResourceState", c => c.Int(nullable: false));
            AddColumn("dbo.str_Shoes", "Distance", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.str_Shoes", "Name", c => c.String(nullable: false));
            AddColumn("dbo.str_Shoes", "IsPrimary", c => c.Boolean(nullable: false));
            AddColumn("dbo.str_Shoes", "Id", c => c.String(nullable: false));
            AddColumn("dbo.str_Shoes", "PkID", c => c.Long(nullable: false, identity: true));
            AddColumn("dbo.str_Bike", "PkID", c => c.Long(nullable: false, identity: true));
            DropForeignKey("dbo.str_Shoes", "GearId", "dbo.str_Gear");
            DropForeignKey("dbo.str_Bike", "GearId", "dbo.str_Gear");
            DropIndex("dbo.str_Shoes", new[] { "GearId" });
            DropIndex("dbo.str_Bike", new[] { "GearId" });
            DropPrimaryKey("dbo.str_Shoes");
            DropPrimaryKey("dbo.str_Bike");
            DropColumn("dbo.str_Shoes", "Description");
            DropColumn("dbo.str_Shoes", "Model");
            DropColumn("dbo.str_Shoes", "Brand");
            DropColumn("dbo.str_Shoes", "GearId");
            DropColumn("dbo.str_Bike", "GearId");
            DropColumn("dbo.str_Athlete", "ResourceState");
            AddPrimaryKey("dbo.str_Shoes", "PkID");
            AddPrimaryKey("dbo.str_Bike", "PkID");
        }
    }
}
