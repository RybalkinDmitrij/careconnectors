namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _04_05_2015_01 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.jwb_BodyEvent",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Xid = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        Type = c.String(nullable: false),
                        TimeCreated = c.DateTime(nullable: false),
                        TimeUpdated = c.DateTime(nullable: false),
                        Date = c.DateTime(nullable: false),
                        PlaceLat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PlaceLon = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PlaceAcc = c.Int(nullable: false),
                        PlaceName = c.String(nullable: false),
                        Note = c.String(nullable: false),
                        LeanMass = c.Int(nullable: false),
                        Weight = c.Int(nullable: false),
                        BodyFat = c.Int(nullable: false),
                        BMI = c.Int(nullable: false),
                        Timezone = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.jwb_BodyEvent");
        }
    }
}
