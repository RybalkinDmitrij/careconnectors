namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _25_05_2015_02 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.wth_ActivityMeasure", "TotalCalories", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.jwb_Meals", "Caffeine", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.jwb_Settings", "ShareMove", c => c.Boolean(nullable: false));
            AddColumn("dbo.jwb_Sleeps", "Type", c => c.String(nullable: false));
            AddColumn("dbo.jwb_Workouts", "Steps", c => c.Int(nullable: false));
            AddColumn("dbo.jwb_Workouts", "Time", c => c.Int(nullable: false));
            AddColumn("dbo.jwb_Workouts", "BgActiveTime", c => c.Int(nullable: false));
            AddColumn("dbo.jwb_Workouts", "Meters", c => c.Int(nullable: false));
            AddColumn("dbo.jwb_Workouts", "Km", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.jwb_Workouts", "Intensity", c => c.Int(nullable: false));
            AddColumn("dbo.jwb_Workouts", "Calories", c => c.Int(nullable: false));
            AddColumn("dbo.jwb_Workouts", "Bmr", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.jwb_Workouts", "BgCalories", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.jwb_Workouts", "BmrCalories", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.jwb_Workouts", "Timezone", c => c.String(nullable: false));
            AlterColumn("dbo.jwb_Goals", "BodyWeightIntent", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.jwb_Goals", "BodyWeightIntent", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.jwb_Workouts", "Timezone");
            DropColumn("dbo.jwb_Workouts", "BmrCalories");
            DropColumn("dbo.jwb_Workouts", "BgCalories");
            DropColumn("dbo.jwb_Workouts", "Bmr");
            DropColumn("dbo.jwb_Workouts", "Calories");
            DropColumn("dbo.jwb_Workouts", "Intensity");
            DropColumn("dbo.jwb_Workouts", "Km");
            DropColumn("dbo.jwb_Workouts", "Meters");
            DropColumn("dbo.jwb_Workouts", "BgActiveTime");
            DropColumn("dbo.jwb_Workouts", "Time");
            DropColumn("dbo.jwb_Workouts", "Steps");
            DropColumn("dbo.jwb_Sleeps", "Type");
            DropColumn("dbo.jwb_Settings", "ShareMove");
            DropColumn("dbo.jwb_Meals", "Caffeine");
            DropColumn("dbo.wth_ActivityMeasure", "TotalCalories");
        }
    }
}
