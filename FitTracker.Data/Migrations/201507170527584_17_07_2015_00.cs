namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _17_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.str_Segment", new[] { "Map_PkID" });
            DropColumn("dbo.str_Segment", "MapId");
            RenameColumn(table: "dbo.str_Segment", name: "Map_PkID", newName: "MapId");
            AlterColumn("dbo.str_Segment", "MapId", c => c.Long(nullable: false));
            CreateIndex("dbo.str_Segment", "MapId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.str_Segment", new[] { "MapId" });
            AlterColumn("dbo.str_Segment", "MapId", c => c.Long());
            RenameColumn(table: "dbo.str_Segment", name: "MapId", newName: "Map_PkID");
            AddColumn("dbo.str_Segment", "MapId", c => c.Long(nullable: false));
            CreateIndex("dbo.str_Segment", "Map_PkID");
        }
    }
}
