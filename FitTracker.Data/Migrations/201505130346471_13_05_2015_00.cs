namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _13_05_2015_00 : DbMigration
    {
        public override void Up()
        {
            Sql("DELETE FROM dbo.ath_Client");

            DropForeignKey("dbo.ath_MMApplicantClient", "ApplicantId", "dbo.ath_Applicant");
            DropForeignKey("dbo.ath_MMApplicantClient", "ClientId", "dbo.ath_Client");
            DropIndex("dbo.ath_MMApplicantClient", new[] { "ClientId" });
            DropIndex("dbo.ath_MMApplicantClient", new[] { "ApplicantId" });

            DropColumn("dbo.ath_Client", "Login");
            DropColumn("dbo.ath_Client", "PasswordHash");
            DropColumn("dbo.ath_Client", "LastName");
            DropColumn("dbo.ath_Client", "FirstName");
            DropColumn("dbo.ath_Applicant", "ClientId");
            DropTable("dbo.ath_MMApplicantClient");

            CreateTable(
                "dbo.ath_DbUser",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        Login = c.String(nullable: false),
                        PasswordHash = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        FirstName = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            AddColumn("dbo.ath_Client", "ExternalId", c => c.String(nullable: false));
            AddColumn("dbo.ath_Client", "ApplicantId", c => c.Long(nullable: false));
            AddColumn("dbo.ath_Applicant", "UserId", c => c.Long(nullable: false));
            CreateIndex("dbo.ath_Client", "ApplicantId");
            CreateIndex("dbo.ath_Applicant", "UserId");
            AddForeignKey("dbo.ath_Applicant", "UserId", "dbo.ath_DbUser", "PkID", cascadeDelete: true);
            AddForeignKey("dbo.ath_Client", "ApplicantId", "dbo.ath_Applicant", "PkID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ath_Client", "ApplicantId", "dbo.ath_Applicant");
            DropForeignKey("dbo.ath_Applicant", "UserId", "dbo.ath_DbUser");
            DropIndex("dbo.ath_Applicant", new[] { "UserId" });
            DropIndex("dbo.ath_Client", new[] { "ApplicantId" });
            DropColumn("dbo.ath_Applicant", "UserId");
            DropColumn("dbo.ath_Client", "ApplicantId");
            DropColumn("dbo.ath_Client", "ExternalId");
            DropTable("dbo.ath_DbUser");

            CreateTable(
                "dbo.ath_MMApplicantClient",
                c => new
                {
                    PkID = c.Long(nullable: false, identity: true),
                    ClientId = c.Long(nullable: false),
                    ApplicantId = c.Long(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    DateCreate = c.DateTime(nullable: false),
                    DateUpdate = c.DateTime(nullable: false),
                    DateDelete = c.DateTime(),
                    UserCreateId = c.Long(nullable: false),
                    UserUpdateId = c.Long(nullable: false),
                    UserDeleteId = c.Long(nullable: false),
                })
                .PrimaryKey(t => t.PkID);

            AddColumn("dbo.ath_Applicant", "ClientId", c => c.Long(nullable: false));
            AddColumn("dbo.ath_Client", "FirstName", c => c.String(nullable: false));
            AddColumn("dbo.ath_Client", "LastName", c => c.String(nullable: false));
            AddColumn("dbo.ath_Client", "PasswordHash", c => c.String(nullable: false));
            AddColumn("dbo.ath_Client", "Login", c => c.String(nullable: false));

            CreateIndex("dbo.ath_MMApplicantClient", "ApplicantId");
            CreateIndex("dbo.ath_MMApplicantClient", "ClientId");
            AddForeignKey("dbo.ath_MMApplicantClient", "ClientId", "dbo.ath_Client", "PkID", cascadeDelete: true);
            AddForeignKey("dbo.ath_MMApplicantClient", "ApplicantId", "dbo.ath_Applicant", "PkID", cascadeDelete: true);
        }
    }
}
