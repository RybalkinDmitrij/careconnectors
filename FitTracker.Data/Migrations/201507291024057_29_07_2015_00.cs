namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _29_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.mft_Device", "ProfileInfoId", c => c.Long(nullable: false));
            AddColumn("dbo.mft_Goal", "ProfileInfoId", c => c.Long(nullable: false));
            AddColumn("dbo.mft_Session", "ProfileInfoId", c => c.Long(nullable: false));
            AddColumn("dbo.mft_Sleep", "ProfileInfoId", c => c.Long(nullable: false));
            AddColumn("dbo.mft_Summary", "ProfileInfoId", c => c.Long(nullable: false));
            CreateIndex("dbo.mft_Device", "ProfileInfoId");
            CreateIndex("dbo.mft_Goal", "ProfileInfoId");
            CreateIndex("dbo.mft_Session", "ProfileInfoId");
            CreateIndex("dbo.mft_Sleep", "ProfileInfoId");
            CreateIndex("dbo.mft_Summary", "ProfileInfoId");
            AddForeignKey("dbo.mft_Goal", "ProfileInfoId", "dbo.mft_Profile", "PkID", cascadeDelete: true);
            AddForeignKey("dbo.mft_Session", "ProfileInfoId", "dbo.mft_Profile", "PkID", cascadeDelete: true);
            AddForeignKey("dbo.mft_Sleep", "ProfileInfoId", "dbo.mft_Profile", "PkID", cascadeDelete: true);
            AddForeignKey("dbo.mft_Summary", "ProfileInfoId", "dbo.mft_Profile", "PkID", cascadeDelete: true);
            AddForeignKey("dbo.mft_Device", "ProfileInfoId", "dbo.mft_Profile", "PkID", cascadeDelete: true);
            DropColumn("dbo.mft_Device", "UserId");
            DropColumn("dbo.mft_Goal", "UserId");
            DropColumn("dbo.mft_Session", "UserId");
            DropColumn("dbo.mft_Sleep", "UserId");
            DropColumn("dbo.mft_Summary", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.mft_Summary", "UserId", c => c.String(nullable: false));
            AddColumn("dbo.mft_Sleep", "UserId", c => c.String(nullable: false));
            AddColumn("dbo.mft_Session", "UserId", c => c.String(nullable: false));
            AddColumn("dbo.mft_Goal", "UserId", c => c.String(nullable: false));
            AddColumn("dbo.mft_Device", "UserId", c => c.String(nullable: false));
            DropForeignKey("dbo.mft_Device", "ProfileInfoId", "dbo.mft_Profile");
            DropForeignKey("dbo.mft_Summary", "ProfileInfoId", "dbo.mft_Profile");
            DropForeignKey("dbo.mft_Sleep", "ProfileInfoId", "dbo.mft_Profile");
            DropForeignKey("dbo.mft_Session", "ProfileInfoId", "dbo.mft_Profile");
            DropForeignKey("dbo.mft_Goal", "ProfileInfoId", "dbo.mft_Profile");
            DropIndex("dbo.mft_Summary", new[] { "ProfileInfoId" });
            DropIndex("dbo.mft_Sleep", new[] { "ProfileInfoId" });
            DropIndex("dbo.mft_Session", new[] { "ProfileInfoId" });
            DropIndex("dbo.mft_Goal", new[] { "ProfileInfoId" });
            DropIndex("dbo.mft_Device", new[] { "ProfileInfoId" });
            DropColumn("dbo.mft_Summary", "ProfileInfoId");
            DropColumn("dbo.mft_Sleep", "ProfileInfoId");
            DropColumn("dbo.mft_Session", "ProfileInfoId");
            DropColumn("dbo.mft_Goal", "ProfileInfoId");
            DropColumn("dbo.mft_Device", "ProfileInfoId");
        }
    }
}
