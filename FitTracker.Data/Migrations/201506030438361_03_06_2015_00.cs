namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _03_06_2015_00 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.jwb_Sleeps", "PlaceAcc", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.jwb_Sleeps", "PlaceAcc", c => c.Int(nullable: false));
        }
    }
}
