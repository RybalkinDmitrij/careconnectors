namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _16_04_2015_00 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.fbt_Device",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Id = c.String(nullable: false),
                        Battery = c.String(nullable: false),
                        LastSyncTime = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                        DeviceVersion = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.fbt_Device");
        }
    }
}
