namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _28_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.mft_Device",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Id = c.String(nullable: false),
                        DeviceType = c.String(nullable: false),
                        SerialNumber = c.String(nullable: false),
                        FirmwareVersion = c.String(nullable: false),
                        BatteryLevel = c.Int(nullable: false),
                        LastSyncTime = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.mft_Goal",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Id = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Points = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TargetPoints = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.mft_Profile",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Birthday = c.String(nullable: false),
                        Gender = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.mft_Session",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Id = c.String(nullable: false),
                        ActivityType = c.String(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        Duration = c.Int(nullable: false),
                        Points = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Steps = c.Int(nullable: false),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Distance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.mft_Sleep",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Id = c.String(nullable: false),
                        AutoDetected = c.Boolean(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        Duration = c.Int(nullable: false),
                        Datetime = c.DateTime(nullable: false),
                        Value = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.mft_Summary",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Points = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Steps = c.Int(nullable: false),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ActivityCalories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Distance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.mft_Summary");
            DropTable("dbo.mft_Sleep");
            DropTable("dbo.mft_Session");
            DropTable("dbo.mft_Profile");
            DropTable("dbo.mft_Goal");
            DropTable("dbo.mft_Device");
        }
    }
}
