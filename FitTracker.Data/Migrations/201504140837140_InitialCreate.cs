namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ath_Client",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        LastName = c.String(nullable: false),
                        FirstName = c.String(nullable: false),
                        FitbitId = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.sys_LogError",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Text = c.String(nullable: false),
                        UserId = c.Long(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.sys_LogAction",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Action = c.String(nullable: false),
                        Url = c.String(nullable: false),
                        UserId = c.Long(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.fbt_User",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        DateOfBirth = c.DateTime(nullable: false),
                        DisplayName = c.String(nullable: false, maxLength: 255),
                        EncodedId = c.String(nullable: false, maxLength: 255),
                        Gender = c.String(nullable: false, maxLength: 255),
                        Height = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OffsetFromUTCMillis = c.Long(nullable: false),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_UserProfile",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        EncodedId = c.String(nullable: false, maxLength: 255),
                        DisplayName = c.String(nullable: false, maxLength: 255),
                        Gender = c.Int(nullable: false),
                        DateOfBirth = c.DateTime(nullable: false),
                        Height = c.Double(nullable: false),
                        Weight = c.Double(nullable: false),
                        StrideLengthWalking = c.Double(nullable: false),
                        StrideLengthRunning = c.Double(nullable: false),
                        FullName = c.String(nullable: false, maxLength: 255),
                        Nickname = c.String(nullable: false, maxLength: 255),
                        Country = c.String(nullable: false, maxLength: 255),
                        State = c.String(nullable: false, maxLength: 255),
                        City = c.String(nullable: false, maxLength: 255),
                        AboutMe = c.String(nullable: false),
                        MemberSince = c.DateTime(nullable: false),
                        Timezone = c.String(nullable: false, maxLength: 255),
                        OffsetFromUTCMillis = c.Int(nullable: false),
                        Locale = c.String(nullable: false),
                        Avatar = c.String(nullable: false),
                        WeightUnit = c.String(nullable: false),
                        DistanceUnit = c.String(nullable: false),
                        HeightUnit = c.String(nullable: false),
                        WaterUnit = c.String(nullable: false),
                        GlucoseUnit = c.String(nullable: false),
                        VolumeUnit = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_ActivityDistance",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        ActivitySummaryId = c.Long(nullable: false),
                        Activity = c.String(nullable: false),
                        Distance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.fbt_ActivitySummary", t => t.ActivitySummaryId, cascadeDelete: true)
                .Index(t => t.ActivitySummaryId);
            
            CreateTable(
                "dbo.fbt_ActivitySummary",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        ActivityCalories = c.Int(nullable: false),
                        CaloriesBMR = c.Int(nullable: false),
                        Elevation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Floors = c.Int(nullable: false),
                        MarginalCalories = c.Int(nullable: false),
                        VeryActiveMinutes = c.Int(nullable: false),
                        CaloriesOut = c.Int(nullable: false),
                        FairlyActiveMinutes = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LightlyActiveMinutes = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SedentaryMinutes = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Steps = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_ActivityGoals",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        CaloriesOut = c.Int(nullable: false),
                        Steps = c.Int(nullable: false),
                        Distance = c.Double(nullable: false),
                        Floors = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_ActivityLog",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        ActivityId = c.Long(nullable: false),
                        ActivityParentId = c.Long(nullable: false),
                        Calories = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                        Distance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Duration = c.Long(nullable: false),
                        HasStartTime = c.Boolean(nullable: false),
                        IsFavorite = c.Boolean(nullable: false),
                        LogId = c.Long(nullable: false),
                        Name = c.String(nullable: false),
                        StartTime = c.String(nullable: false),
                        Steps = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_Body",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Bicep = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BMI = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Calf = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Chest = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Fat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Forearm = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Hips = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Neck = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Thigh = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Waist = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_BodyGoals",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_FatLog",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        LogId = c.Long(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Time = c.DateTime(nullable: false),
                        Fat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_FoodGoals",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Calories = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_FoodLog",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        IsFavorite = c.Boolean(nullable: false),
                        LogDate = c.DateTime(nullable: false),
                        LogId = c.Long(nullable: false),
                        LoggedFoodId = c.Long(nullable: false),
                        NutritionalValuesId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.fbt_LoggedFood", t => t.LoggedFoodId, cascadeDelete: true)
                .ForeignKey("dbo.fbt_NutritionalValues", t => t.NutritionalValuesId, cascadeDelete: true)
                .Index(t => t.LoggedFoodId)
                .Index(t => t.NutritionalValuesId);
            
            CreateTable(
                "dbo.fbt_LoggedFood",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        AccessLevel = c.String(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Brand = c.String(nullable: false),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FoodId = c.Long(nullable: false),
                        MealTypeId = c.Long(nullable: false),
                        Locale = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        UnitId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.fbt_FoodLogUnit", t => t.UnitId, cascadeDelete: true)
                .Index(t => t.UnitId);
            
            CreateTable(
                "dbo.fbt_FoodLogUnit",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        Id = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Plural = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_NutritionalValues",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Carbs = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Fat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Fiber = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Protein = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Sodium = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_FoodSummary",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Carbs = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Fat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Fiber = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Protein = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Sodium = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Water = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_MinuteData",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        DateTime = c.DateTime(nullable: false),
                        Value = c.Int(nullable: false),
                        SleepLogId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.fbt_SleepLog", t => t.SleepLogId, cascadeDelete: true)
                .Index(t => t.SleepLogId);
            
            CreateTable(
                "dbo.fbt_SleepLog",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(),
                        AwakeningsCount = c.Int(nullable: false),
                        Duration = c.Int(nullable: false),
                        Efficiency = c.Int(nullable: false),
                        IsMainSleep = c.Boolean(nullable: false),
                        LogId = c.Long(nullable: false),
                        MinutesAfterWakeup = c.Int(nullable: false),
                        MinutesAsleep = c.Int(nullable: false),
                        MinutesAwake = c.Int(nullable: false),
                        MinutesToFallAsleep = c.Int(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        TimeInBed = c.Int(nullable: false),
                        AwakeCount = c.Int(nullable: false),
                        AwakeDuration = c.Int(nullable: false),
                        RestlessCount = c.Int(nullable: false),
                        RestlessDuration = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_SleepSummary",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        TotalSleepRecords = c.Int(nullable: false),
                        TotalMinutesAsleep = c.Int(nullable: false),
                        TotalTimeInBed = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_Water",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        LogId = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_WaterGoal",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Goal = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.fbt_WeightLog",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        LogId = c.Long(nullable: false),
                        Bmi = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Date = c.DateTime(nullable: false),
                        Time = c.Time(nullable: false, precision: 7),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.fbt_MinuteData", "SleepLogId", "dbo.fbt_SleepLog");
            DropForeignKey("dbo.fbt_FoodLog", "NutritionalValuesId", "dbo.fbt_NutritionalValues");
            DropForeignKey("dbo.fbt_FoodLog", "LoggedFoodId", "dbo.fbt_LoggedFood");
            DropForeignKey("dbo.fbt_LoggedFood", "UnitId", "dbo.fbt_FoodLogUnit");
            DropForeignKey("dbo.fbt_ActivityDistance", "ActivitySummaryId", "dbo.fbt_ActivitySummary");
            DropIndex("dbo.fbt_MinuteData", new[] { "SleepLogId" });
            DropIndex("dbo.fbt_LoggedFood", new[] { "UnitId" });
            DropIndex("dbo.fbt_FoodLog", new[] { "NutritionalValuesId" });
            DropIndex("dbo.fbt_FoodLog", new[] { "LoggedFoodId" });
            DropIndex("dbo.fbt_ActivityDistance", new[] { "ActivitySummaryId" });
            DropTable("dbo.fbt_WeightLog");
            DropTable("dbo.fbt_WaterGoal");
            DropTable("dbo.fbt_Water");
            DropTable("dbo.fbt_SleepSummary");
            DropTable("dbo.fbt_SleepLog");
            DropTable("dbo.fbt_MinuteData");
            DropTable("dbo.fbt_FoodSummary");
            DropTable("dbo.fbt_NutritionalValues");
            DropTable("dbo.fbt_FoodLogUnit");
            DropTable("dbo.fbt_LoggedFood");
            DropTable("dbo.fbt_FoodLog");
            DropTable("dbo.fbt_FoodGoals");
            DropTable("dbo.fbt_FatLog");
            DropTable("dbo.fbt_BodyGoals");
            DropTable("dbo.fbt_Body");
            DropTable("dbo.fbt_ActivityLog");
            DropTable("dbo.fbt_ActivityGoals");
            DropTable("dbo.fbt_ActivitySummary");
            DropTable("dbo.fbt_ActivityDistance");
            DropTable("dbo.fbt_UserProfile");
            DropTable("dbo.fbt_User");
            DropTable("dbo.sys_LogAction");
            DropTable("dbo.sys_LogError");
            DropTable("dbo.ath_Client");
        }
    }
}
