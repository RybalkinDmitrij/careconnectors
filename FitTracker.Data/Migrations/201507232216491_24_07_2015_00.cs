namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _24_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.rk_Calories", name: "FitnessActivitiesId", newName: "FitnessActivityId");
            RenameColumn(table: "dbo.rk_Distance", name: "FitnessActivitiesId", newName: "FitnessActivityId");
            RenameColumn(table: "dbo.rk_HeartRate", name: "FitnessActivitiesId", newName: "FitnessActivityId");
            RenameColumn(table: "dbo.rk_Image", name: "FitnessActivitiesId", newName: "FitnessActivityId");
            RenameColumn(table: "dbo.rk_Path", name: "FitnessActivitiesId", newName: "FitnessActivityId");
            RenameIndex(table: "dbo.rk_Calories", name: "IX_FitnessActivitiesId", newName: "IX_FitnessActivityId");
            RenameIndex(table: "dbo.rk_Distance", name: "IX_FitnessActivitiesId", newName: "IX_FitnessActivityId");
            RenameIndex(table: "dbo.rk_HeartRate", name: "IX_FitnessActivitiesId", newName: "IX_FitnessActivityId");
            RenameIndex(table: "dbo.rk_Image", name: "IX_FitnessActivitiesId", newName: "IX_FitnessActivityId");
            RenameIndex(table: "dbo.rk_Path", name: "IX_FitnessActivitiesId", newName: "IX_FitnessActivityId");
            CreateIndex("dbo.str_Activity", "AthleteId");
            AddForeignKey("dbo.str_Activity", "AthleteId", "dbo.str_Athlete", "PkID", cascadeDelete: true);
            DropColumn("dbo.str_ActivityLap", "AthleteId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.str_ActivityLap", "AthleteId", c => c.Long(nullable: false));
            DropForeignKey("dbo.str_Activity", "AthleteId", "dbo.str_Athlete");
            DropIndex("dbo.str_Activity", new[] { "AthleteId" });
            RenameIndex(table: "dbo.rk_Path", name: "IX_FitnessActivityId", newName: "IX_FitnessActivitiesId");
            RenameIndex(table: "dbo.rk_Image", name: "IX_FitnessActivityId", newName: "IX_FitnessActivitiesId");
            RenameIndex(table: "dbo.rk_HeartRate", name: "IX_FitnessActivityId", newName: "IX_FitnessActivitiesId");
            RenameIndex(table: "dbo.rk_Distance", name: "IX_FitnessActivityId", newName: "IX_FitnessActivitiesId");
            RenameIndex(table: "dbo.rk_Calories", name: "IX_FitnessActivityId", newName: "IX_FitnessActivitiesId");
            RenameColumn(table: "dbo.rk_Path", name: "FitnessActivityId", newName: "FitnessActivitiesId");
            RenameColumn(table: "dbo.rk_Image", name: "FitnessActivityId", newName: "FitnessActivitiesId");
            RenameColumn(table: "dbo.rk_HeartRate", name: "FitnessActivityId", newName: "FitnessActivitiesId");
            RenameColumn(table: "dbo.rk_Distance", name: "FitnessActivityId", newName: "FitnessActivitiesId");
            RenameColumn(table: "dbo.rk_Calories", name: "FitnessActivityId", newName: "FitnessActivitiesId");
        }
    }
}
