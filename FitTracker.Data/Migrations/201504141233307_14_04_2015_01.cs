namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _14_04_2015_01 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ath_Client", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_User", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_UserProfile", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_ActivityDistance", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_ActivitySummary", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_ActivityGoals", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_ActivityLog", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_Body", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_BodyGoals", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_FatLog", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_FoodGoals", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_FoodLog", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_LoggedFood", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_FoodLogUnit", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_NutritionalValues", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_FoodSummary", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_MinuteData", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_SleepLog", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_SleepSummary", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_Water", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_WaterGoal", "DateDelete", c => c.DateTime());
            AlterColumn("dbo.fbt_WeightLog", "DateDelete", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.fbt_WeightLog", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_WaterGoal", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_Water", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_SleepSummary", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_SleepLog", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_MinuteData", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_FoodSummary", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_NutritionalValues", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_FoodLogUnit", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_LoggedFood", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_FoodLog", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_FoodGoals", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_FatLog", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_BodyGoals", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_Body", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_ActivityLog", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_ActivityGoals", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_ActivitySummary", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_ActivityDistance", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_UserProfile", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_User", "DateDelete", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ath_Client", "DateDelete", c => c.DateTime(nullable: false));
        }
    }
}
