namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _02_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ath_Client", "RunkeeperId", c => c.String());
            AddColumn("dbo.ath_Client", "RunkeeperAccessToken", c => c.String(nullable: false));
            AddColumn("dbo.ath_Client", "RunkeeperRefreshToken", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ath_Client", "RunkeeperRefreshToken");
            DropColumn("dbo.ath_Client", "RunkeeperAccessToken");
            DropColumn("dbo.ath_Client", "RunkeeperId");
        }
    }
}
