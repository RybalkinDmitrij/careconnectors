namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _15_05_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.fbt_ActivitySummary", "Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.fbt_ActivityGoals", "Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.fbt_ActivityLog", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.fbt_ActivityLog", "Date");
            DropColumn("dbo.fbt_ActivityGoals", "Date");
            DropColumn("dbo.fbt_ActivitySummary", "Date");
        }
    }
}
