namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _08_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.rk_Path", "Type", c => c.String(nullable: false));
            DropColumn("dbo.rk_Calories", "Timestamp");
            DropColumn("dbo.rk_Distance", "Timestamp");
            DropColumn("dbo.rk_HeartRate", "Timestamp");
            DropColumn("dbo.rk_Image", "Timestamp");
            DropColumn("dbo.rk_Path", "Timestamp");
            AddColumn("dbo.rk_Calories", "Timestamp", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.rk_Distance", "Timestamp", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.rk_HeartRate", "Timestamp", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.rk_Image", "Timestamp", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.rk_Path", "Timestamp", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.rk_Path", "Timestamp");
            DropColumn("dbo.rk_Image", "Timestamp");
            DropColumn("dbo.rk_HeartRate", "Timestamp");
            DropColumn("dbo.rk_Distance", "Timestamp");
            DropColumn("dbo.rk_Calories", "Timestamp");
            AddColumn("dbo.rk_Path", "Timestamp", c => c.DateTime(nullable: false));
            AddColumn("dbo.rk_Image", "Timestamp", c => c.DateTime(nullable: false));
            AddColumn("dbo.rk_HeartRate", "Timestamp", c => c.DateTime(nullable: false));
            AddColumn("dbo.rk_Distance", "Timestamp", c => c.DateTime(nullable: false));
            AddColumn("dbo.rk_Calories", "Timestamp", c => c.DateTime(nullable: false));
            AlterColumn("dbo.rk_Path", "Type", c => c.String());
        }
    }
}
