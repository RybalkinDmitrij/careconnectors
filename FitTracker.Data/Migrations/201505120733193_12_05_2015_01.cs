namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _12_05_2015_01 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.jwb_Meals", "PlaceLat", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.jwb_Meals", "PlaceLon", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.jwb_Mood", "PlaceLat", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.jwb_Mood", "PlaceLon", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.jwb_Workouts", "PlaceLat", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.jwb_Workouts", "PlaceLon", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.jwb_Workouts", "PlaceLon", c => c.String(nullable: false));
            AlterColumn("dbo.jwb_Workouts", "PlaceLat", c => c.String(nullable: false));
            AlterColumn("dbo.jwb_Mood", "PlaceLon", c => c.String(nullable: false));
            AlterColumn("dbo.jwb_Mood", "PlaceLat", c => c.String(nullable: false));
            AlterColumn("dbo.jwb_Meals", "PlaceLon", c => c.String(nullable: false));
            AlterColumn("dbo.jwb_Meals", "PlaceLat", c => c.String(nullable: false));
        }
    }
}
