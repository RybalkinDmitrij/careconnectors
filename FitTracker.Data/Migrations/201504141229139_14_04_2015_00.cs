namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _14_04_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ath_Client", "FitbitAccessToken", c => c.String(nullable: false));
            AddColumn("dbo.ath_Client", "FitbitSecret", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ath_Client", "FitbitSecret");
            DropColumn("dbo.ath_Client", "FitbitAccessToken");
        }
    }
}
