namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _27_05_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ath_Applicant", "NotificationUrl", c => c.String(nullable: false));
            DropColumn("dbo.ath_Client", "NotificationUrl");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ath_Client", "NotificationUrl", c => c.String(nullable: false));
            DropColumn("dbo.ath_Applicant", "NotificationUrl");
        }
    }
}
