namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _16_04_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ath_Client", "WithingsId", c => c.String());
            AddColumn("dbo.ath_Client", "WithingsAccessToken", c => c.String(nullable: false));
            AddColumn("dbo.ath_Client", "WithingsSecret", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ath_Client", "WithingsSecret");
            DropColumn("dbo.ath_Client", "WithingsAccessToken");
            DropColumn("dbo.ath_Client", "WithingsId");
        }
    }
}
