namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _21_04_2015_00 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.wth_ActivityMeasure",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Steps = c.Int(nullable: false),
                        Distance = c.Int(nullable: false),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Elevation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Soft = c.Int(nullable: false),
                        Moderate = c.Int(nullable: false),
                        Intense = c.Int(nullable: false),
                        Timezone = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.wth_BodyMeasure",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        GrpId = c.Int(nullable: false),
                        Attrib = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Category = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.wth_BodyMeasureItem",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        BodyMeasureId = c.Long(nullable: false),
                        Value = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Unit = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.wth_BodyMeasure", t => t.BodyMeasureId, cascadeDelete: true)
                .Index(t => t.BodyMeasureId);
            
            CreateTable(
                "dbo.wth_IntradayActivity",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Name = c.DateTime(nullable: false),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Duration = c.Int(nullable: false),
                        Steps = c.Int(nullable: false),
                        Elevation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Distance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.wth_SleepMeasure",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        State = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.wth_SleepSummary",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Id = c.Int(nullable: false),
                        Timezone = c.String(nullable: false),
                        Model = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Date = c.DateTime(nullable: false),
                        WakeUpDuration = c.Int(nullable: false),
                        LightSleepDuration = c.Int(nullable: false),
                        DeepSleepDuration = c.Int(nullable: false),
                        RemSleepDuration = c.Int(nullable: false),
                        DurationToSleep = c.Int(nullable: false),
                        DurationToWakeup = c.Int(nullable: false),
                        WakeupCount = c.Int(nullable: false),
                        Modified = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.wth_BodyMeasureItem", "BodyMeasureId", "dbo.wth_BodyMeasure");
            DropIndex("dbo.wth_BodyMeasureItem", new[] { "BodyMeasureId" });
            DropTable("dbo.wth_SleepSummary");
            DropTable("dbo.wth_SleepMeasure");
            DropTable("dbo.wth_IntradayActivity");
            DropTable("dbo.wth_BodyMeasureItem");
            DropTable("dbo.wth_BodyMeasure");
            DropTable("dbo.wth_ActivityMeasure");
        }
    }
}
