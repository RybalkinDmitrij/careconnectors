namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _15_04_2015_00 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.fbt_ActivityGoals", "Floors", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.fbt_ActivityGoals", "Floors", c => c.Int(nullable: false));
        }
    }
}
