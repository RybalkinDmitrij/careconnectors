namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _03_06_2015_02 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.wth_BodyMeasure", "Type", c => c.Int(nullable: false, defaultValue: 1));
        }
        
        public override void Down()
        {
            DropColumn("dbo.wth_BodyMeasure", "Type");
        }
    }
}
