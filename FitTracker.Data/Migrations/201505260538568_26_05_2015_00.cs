namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _26_05_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.fbt_Body", "Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.fbt_BodyGoals", "Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.fbt_FoodGoals", "Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.fbt_FoodSummary", "Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.fbt_SleepLog", "Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.fbt_SleepSummary", "Date", c => c.DateTime(nullable: false));
            AlterColumn("dbo.fbt_SleepLog", "UserId", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.fbt_SleepLog", "UserId", c => c.String());
            DropColumn("dbo.fbt_SleepSummary", "Date");
            DropColumn("dbo.fbt_SleepLog", "Date");
            DropColumn("dbo.fbt_FoodSummary", "Date");
            DropColumn("dbo.fbt_FoodGoals", "Date");
            DropColumn("dbo.fbt_BodyGoals", "Date");
            DropColumn("dbo.fbt_Body", "Date");
        }
    }
}
