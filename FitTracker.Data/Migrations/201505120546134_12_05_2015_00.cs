namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _12_05_2015_00 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.jwb_AccountInfo",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Xid = c.String(nullable: false),
                        First = c.String(nullable: false),
                        Last = c.String(nullable: false),
                        Image = c.String(nullable: false),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Height = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.jwb_Custom",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Xid = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        Type = c.String(nullable: false),
                        TimeCreated = c.DateTime(nullable: false),
                        TimeUpdated = c.DateTime(nullable: false),
                        Date = c.DateTime(nullable: false),
                        PlaceLat = c.String(nullable: false),
                        PlaceLon = c.String(nullable: false),
                        PlaceAcc = c.Int(nullable: false),
                        PlaceName = c.String(nullable: false),
                        Image = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        Timezone = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.jwb_Goals",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        MoveSteps = c.Int(nullable: false),
                        SleepTotal = c.Int(nullable: false),
                        BodyWeight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BodyWeightIntent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IntakeCaloriesRemaining = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MoveStepsRemaining = c.Int(nullable: false),
                        SleepSecondsRemaining = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.jwb_HeartRate",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Xid = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        TimeCreated = c.DateTime(nullable: false),
                        TimeUpdated = c.DateTime(nullable: false),
                        Date = c.DateTime(nullable: false),
                        PlaceLat = c.String(nullable: false),
                        PlaceLon = c.String(nullable: false),
                        PlaceAcc = c.Int(nullable: false),
                        PlaceName = c.String(nullable: false),
                        RestingHeartrate = c.Int(nullable: false),
                        Timezone = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.jwb_HourlyTotal",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        MovesId = c.Long(nullable: false),
                        Distance = c.Int(nullable: false),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Steps = c.Int(nullable: false),
                        ActiveTime = c.Int(nullable: false),
                        InactiveTime = c.Int(nullable: false),
                        LongestActiveTime = c.Int(nullable: false),
                        LongestIdleTime = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.jwb_Moves", t => t.MovesId, cascadeDelete: true)
                .Index(t => t.MovesId);
            
            CreateTable(
                "dbo.jwb_Moves",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Xid = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        Type = c.String(nullable: false),
                        TimeCreated = c.DateTime(nullable: false),
                        TimeUpdated = c.DateTime(nullable: false),
                        TimeCompleted = c.DateTime(nullable: false),
                        Date = c.DateTime(nullable: false),
                        SnapshotImage = c.String(nullable: false),
                        Distance = c.Int(nullable: false),
                        Km = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Steps = c.Int(nullable: false),
                        ActiveTime = c.Int(nullable: false),
                        LongestActive = c.Int(nullable: false),
                        InactiveTime = c.Int(nullable: false),
                        LongestIdle = c.Int(nullable: false),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BmrDay = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Bmr = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BgCalories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WoCalories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WoTime = c.Int(nullable: false),
                        WoActiveTime = c.Int(nullable: false),
                        WoCount = c.Int(nullable: false),
                        WoLongest = c.Int(nullable: false),
                        Sunrise = c.Long(nullable: false),
                        Sunset = c.Long(nullable: false),
                        Timezone = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.jwb_Meals",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Xid = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        Note = c.String(nullable: false),
                        Type = c.String(nullable: false),
                        SubType = c.Int(nullable: false),
                        TimeCreated = c.DateTime(nullable: false),
                        TimeUpdated = c.DateTime(nullable: false),
                        TimeCompleted = c.DateTime(nullable: false),
                        Date = c.DateTime(nullable: false),
                        PlaceLat = c.String(nullable: false),
                        PlaceLon = c.String(nullable: false),
                        PlaceAcc = c.Int(nullable: false),
                        PlaceName = c.String(nullable: false),
                        NumDrinks = c.Int(nullable: false),
                        NumWater = c.Int(nullable: false),
                        NumFoods = c.Int(nullable: false),
                        OnlyWaters = c.Boolean(nullable: false),
                        NumMealitemsGreen = c.Int(nullable: false),
                        NumMealitemsYellow = c.Int(nullable: false),
                        NumMealitemsRed = c.Int(nullable: false),
                        NumMealitemsWithScore = c.Int(nullable: false),
                        Fiber = c.Int(nullable: false),
                        PolyunsaturatedFat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Potassium = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Fat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Carbohydrate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SaturatedFat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Protein = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MonounsaturatedFat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Sodium = c.Decimal(nullable: false, precision: 18, scale: 2),
                        VitaminC = c.Decimal(nullable: false, precision: 18, scale: 2),
                        VitaminA = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnsaturatedFat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Sugar = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Calcium = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Iron = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Cholesterol = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Accuracy = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Timezone = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.jwb_Mood",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Xid = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        TimeCreated = c.DateTime(nullable: false),
                        TimeUpdated = c.DateTime(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Type = c.String(nullable: false),
                        SubType = c.Int(nullable: false),
                        PlaceLat = c.String(nullable: false),
                        PlaceLon = c.String(nullable: false),
                        PlaceAcc = c.Int(nullable: false),
                        PlaceName = c.String(nullable: false),
                        Timezone = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.jwb_MovesTick",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Distance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TimeCompleted = c.DateTime(nullable: false),
                        ActiveTime = c.Int(nullable: false),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Steps = c.Int(nullable: false),
                        Time = c.Long(nullable: false),
                        Speed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.jwb_Settings",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Xid = c.String(nullable: false),
                        Metric = c.Int(nullable: false),
                        ShareSleep = c.Boolean(nullable: false),
                        ShareMood = c.Boolean(nullable: false),
                        ShareBody = c.Boolean(nullable: false),
                        ShareEat = c.Boolean(nullable: false),
                        Steps = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.jwb_Sleeps",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Xid = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        SubType = c.Int(nullable: false),
                        TimeCreated = c.DateTime(nullable: false),
                        TimeCompleted = c.DateTime(nullable: false),
                        Date = c.DateTime(nullable: false),
                        PlaceLat = c.String(nullable: false),
                        PlaceLon = c.String(nullable: false),
                        PlaceAcc = c.Int(nullable: false),
                        PlaceName = c.String(nullable: false),
                        SnapshotImage = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.jwb_Timezone",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Tz = c.String(nullable: false),
                        Time = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.jwb_Workouts",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Xid = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        Type = c.String(nullable: false),
                        SubType = c.Int(nullable: false),
                        TimeCreated = c.DateTime(nullable: false),
                        TimeUpdated = c.DateTime(nullable: false),
                        TimeCompleted = c.DateTime(nullable: false),
                        Date = c.DateTime(nullable: false),
                        PlaceLat = c.String(nullable: false),
                        PlaceLon = c.String(nullable: false),
                        PlaceAcc = c.Int(nullable: false),
                        PlaceName = c.String(nullable: false),
                        Route = c.String(nullable: false),
                        Image = c.String(nullable: false),
                        SnapshotImage = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.jwb_HourlyTotal", "MovesId", "dbo.jwb_Moves");
            DropIndex("dbo.jwb_HourlyTotal", new[] { "MovesId" });
            DropTable("dbo.jwb_Workouts");
            DropTable("dbo.jwb_Timezone");
            DropTable("dbo.jwb_Sleeps");
            DropTable("dbo.jwb_Settings");
            DropTable("dbo.jwb_MovesTick");
            DropTable("dbo.jwb_Mood");
            DropTable("dbo.jwb_Meals");
            DropTable("dbo.jwb_Moves");
            DropTable("dbo.jwb_HourlyTotal");
            DropTable("dbo.jwb_HeartRate");
            DropTable("dbo.jwb_Goals");
            DropTable("dbo.jwb_Custom");
            DropTable("dbo.jwb_AccountInfo");
        }
    }
}
