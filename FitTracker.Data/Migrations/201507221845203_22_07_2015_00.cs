namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _22_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.str_Activity", "CommentCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.str_Activity", "CommentCount", c => c.Short(nullable: false));
        }
    }
}
