namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _06_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.rk_BackgroundActivities",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Measurement = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Type = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.rk_Calories",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        FitnessActivitiesId = c.Long(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.rk_FitnessActivities", t => t.FitnessActivitiesId, cascadeDelete: true)
                .Index(t => t.FitnessActivitiesId);
            
            CreateTable(
                "dbo.rk_FitnessActivities",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Type = c.String(nullable: false),
                        SecondaryType = c.String(nullable: false),
                        Equipment = c.String(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        TotalDistance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Duration = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AverageHeartRate = c.Int(nullable: false),
                        TotalCalories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Climb = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Notes = c.String(nullable: false),
                        IsLive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.rk_Distance",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        FitnessActivitiesId = c.Long(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Distance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.rk_FitnessActivities", t => t.FitnessActivitiesId, cascadeDelete: true)
                .Index(t => t.FitnessActivitiesId);
            
            CreateTable(
                "dbo.rk_HeartRate",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        FitnessActivitiesId = c.Long(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        HeartRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.rk_FitnessActivities", t => t.FitnessActivitiesId, cascadeDelete: true)
                .Index(t => t.FitnessActivitiesId);
            
            CreateTable(
                "dbo.rk_Image",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        FitnessActivitiesId = c.Long(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        URI = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.rk_FitnessActivities", t => t.FitnessActivitiesId, cascadeDelete: true)
                .Index(t => t.FitnessActivitiesId);
            
            CreateTable(
                "dbo.rk_Path",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        FitnessActivitiesId = c.Long(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Altitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        type = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.rk_FitnessActivities", t => t.FitnessActivitiesId, cascadeDelete: true)
                .Index(t => t.FitnessActivitiesId);
            
            CreateTable(
                "dbo.rk_DiabetesMeasurements",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Measurement = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Type = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.rk_Exercises",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        StrengthTrainingId = c.Long(nullable: false),
                        PrimaryType = c.String(nullable: false),
                        SecondaryType = c.String(nullable: false),
                        PrimaryMuscleGroup = c.String(nullable: false),
                        SecondaryMuscleGroup = c.String(nullable: false),
                        Routine = c.String(nullable: false),
                        Notes = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.rk_StrengthTraining", t => t.StrengthTrainingId, cascadeDelete: true)
                .Index(t => t.StrengthTrainingId);
            
            CreateTable(
                "dbo.rk_Sets",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        ExerciseId = c.Long(nullable: false),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Repetitions = c.Int(nullable: false),
                        Notes = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.rk_Exercises", t => t.ExerciseId, cascadeDelete: true)
                .Index(t => t.ExerciseId);
            
            CreateTable(
                "dbo.rk_StrengthTraining",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        TotalCalories = c.Double(nullable: false),
                        Notes = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.rk_GeneralMeasurements",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Measurement = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Type = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.rk_Nutrition",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Measurement = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Type = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.rk_Profile",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Location = c.String(nullable: false),
                        AthleteType = c.String(nullable: false),
                        Gender = c.String(nullable: false),
                        Birthday = c.DateTime(nullable: false),
                        Elite = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.rk_Sleep",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Measurement = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Type = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.rk_Weight",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Measurement = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Type = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.rk_Exercises", "StrengthTrainingId", "dbo.rk_StrengthTraining");
            DropForeignKey("dbo.rk_Sets", "ExerciseId", "dbo.rk_Exercises");
            DropForeignKey("dbo.rk_Calories", "FitnessActivitiesId", "dbo.rk_FitnessActivities");
            DropForeignKey("dbo.rk_Path", "FitnessActivitiesId", "dbo.rk_FitnessActivities");
            DropForeignKey("dbo.rk_Image", "FitnessActivitiesId", "dbo.rk_FitnessActivities");
            DropForeignKey("dbo.rk_HeartRate", "FitnessActivitiesId", "dbo.rk_FitnessActivities");
            DropForeignKey("dbo.rk_Distance", "FitnessActivitiesId", "dbo.rk_FitnessActivities");
            DropIndex("dbo.rk_Sets", new[] { "ExerciseId" });
            DropIndex("dbo.rk_Exercises", new[] { "StrengthTrainingId" });
            DropIndex("dbo.rk_Path", new[] { "FitnessActivitiesId" });
            DropIndex("dbo.rk_Image", new[] { "FitnessActivitiesId" });
            DropIndex("dbo.rk_HeartRate", new[] { "FitnessActivitiesId" });
            DropIndex("dbo.rk_Distance", new[] { "FitnessActivitiesId" });
            DropIndex("dbo.rk_Calories", new[] { "FitnessActivitiesId" });
            DropTable("dbo.rk_Weight");
            DropTable("dbo.rk_Sleep");
            DropTable("dbo.rk_Profile");
            DropTable("dbo.rk_Nutrition");
            DropTable("dbo.rk_GeneralMeasurements");
            DropTable("dbo.rk_StrengthTraining");
            DropTable("dbo.rk_Sets");
            DropTable("dbo.rk_Exercises");
            DropTable("dbo.rk_DiabetesMeasurements");
            DropTable("dbo.rk_Path");
            DropTable("dbo.rk_Image");
            DropTable("dbo.rk_HeartRate");
            DropTable("dbo.rk_Distance");
            DropTable("dbo.rk_FitnessActivities");
            DropTable("dbo.rk_Calories");
            DropTable("dbo.rk_BackgroundActivities");
        }
    }
}
