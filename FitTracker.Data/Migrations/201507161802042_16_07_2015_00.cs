namespace FitTracker.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _16_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.str_Activity",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        AthleteId = c.Long(nullable: false),
                        Id = c.Long(nullable: false),
                        Name = c.String(nullable: false),
                        ExternalId = c.String(nullable: false),
                        Type = c.Int(nullable: false),
                        Distance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MovingTime = c.Int(nullable: false),
                        ElapsedTime = c.Int(nullable: false),
                        ElevationGain = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HasKudoed = c.Boolean(nullable: false),
                        AverageHeartrate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaxHeartrate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Truncated = c.Int(nullable: false),
                        City = c.String(nullable: false),
                        State = c.String(nullable: false),
                        Country = c.String(nullable: false),
                        AverageSpeed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaxSpeed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AverageCadence = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AverageTemperature = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AveragePower = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Kilojoules = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsTrainer = c.Boolean(nullable: false),
                        IsCommute = c.Boolean(nullable: false),
                        IsManual = c.Boolean(nullable: false),
                        IsPrivate = c.Boolean(nullable: false),
                        IsFlagged = c.Boolean(nullable: false),
                        AchievementCount = c.Int(nullable: false),
                        KudosCount = c.Int(nullable: false),
                        CommentCount = c.Short(nullable: false),
                        AthleteCount = c.Int(nullable: false),
                        PhotoCount = c.Int(nullable: false),
                        DateTimeStart = c.DateTime(nullable: false),
                        DateTimeStartLocal = c.DateTime(nullable: false),
                        TimeZone = c.String(nullable: false),
                        StartLatitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WeightedAverageWatts = c.Int(nullable: false),
                        StartLongitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EndLatitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EndLongitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HasPowerMeter = c.Boolean(nullable: false),
                        MapId = c.Long(nullable: false),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(nullable: false),
                        GearId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.str_Gear", t => t.GearId, cascadeDelete: true)
                .ForeignKey("dbo.str_Map", t => t.MapId, cascadeDelete: true)
                .Index(t => t.MapId)
                .Index(t => t.GearId);
            
            CreateTable(
                "dbo.str_ActivityLap",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        Id = c.Long(nullable: false),
                        ResourceState = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        ActivityId = c.Long(nullable: false),
                        AthleteId = c.Long(nullable: false),
                        ElapsedTime = c.Int(nullable: false),
                        MovingTime = c.Int(nullable: false),
                        Start = c.DateTime(nullable: false),
                        StartLocal = c.DateTime(nullable: false),
                        Distance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StartIndex = c.Int(nullable: false),
                        EndIndex = c.Int(nullable: false),
                        TotalElevationGain = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AverageSpeed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaxSpeed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AverageCadence = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AveragePower = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AverageHeartrate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaxHeartrate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LapIndex = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.str_Activity", t => t.ActivityId, cascadeDelete: true)
                .Index(t => t.ActivityId);
            
            CreateTable(
                "dbo.str_Comment",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        Id = c.Long(nullable: false),
                        ResourceState = c.Int(nullable: false),
                        ActivityId = c.Long(nullable: false),
                        Text = c.String(nullable: false),
                        Author = c.String(nullable: false),
                        TimeCreated = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.str_Activity", t => t.ActivityId, cascadeDelete: true)
                .Index(t => t.ActivityId);
            
            CreateTable(
                "dbo.str_Gear",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        Id = c.String(nullable: false),
                        IsPrimary = c.Boolean(nullable: false),
                        Name = c.String(nullable: false),
                        Distance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ResourceState = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.str_Map",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        Id = c.String(nullable: false),
                        Polyline = c.String(nullable: false),
                        SummaryPolyline = c.String(nullable: false),
                        ResourceState = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.str_Segment",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        Id = c.Int(nullable: false),
                        ResourceState = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        ActivityType = c.String(nullable: false),
                        Distance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AverageGrade = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaxGrade = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaxElevation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MinElevation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClimbCategory = c.Int(nullable: false),
                        City = c.String(nullable: false),
                        State = c.String(nullable: false),
                        Country = c.String(nullable: false),
                        IsPrivate = c.Boolean(nullable: false),
                        IsStarred = c.Boolean(nullable: false),
                        CreatedAt = c.String(nullable: false),
                        UpdatedAt = c.String(nullable: false),
                        TotalElevationGain = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MapId = c.Long(nullable: false),
                        EffortCount = c.Int(nullable: false),
                        AthleteCount = c.Int(nullable: false),
                        IsHazardous = c.Boolean(nullable: false),
                        PersonalRecordTime = c.Int(nullable: false),
                        PersonalRecordDistance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StarCount = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                        Map_PkID = c.Long(),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.str_Map", t => t.Map_PkID)
                .Index(t => t.Map_PkID);
            
            CreateTable(
                "dbo.str_SegmentEffort",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        ActivityId = c.Long(nullable: false),
                        Id = c.Long(nullable: false),
                        AverageCadence = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AveragePower = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AverageHeartrate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaxHeartrate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ResourceState = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        SegmentId = c.Long(nullable: false),
                        UserId = c.String(nullable: false),
                        KingOfMountainRank = c.Int(nullable: false),
                        PersonalRecordRank = c.Int(nullable: false),
                        MovingTime = c.Int(nullable: false),
                        ElapsedTime = c.Int(nullable: false),
                        DateTimeStart = c.DateTime(nullable: false),
                        SegmentDistance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StartIndex = c.Int(nullable: false),
                        EndIndex = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.str_Activity", t => t.ActivityId, cascadeDelete: true)
                .ForeignKey("dbo.str_Segment", t => t.SegmentId, cascadeDelete: true)
                .Index(t => t.ActivityId)
                .Index(t => t.SegmentId);
            
            CreateTable(
                "dbo.str_Photo",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        Id = c.Long(nullable: false),
                        ResourceState = c.Int(nullable: false),
                        ActivityId = c.Long(nullable: false),
                        ImageUrl = c.String(nullable: false),
                        ExternalUid = c.String(nullable: false),
                        Caption = c.String(nullable: false),
                        Type = c.String(nullable: false),
                        UploadedAt = c.String(nullable: false),
                        CreatedAt = c.String(nullable: false),
                        Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.str_Activity", t => t.ActivityId, cascadeDelete: true)
                .Index(t => t.ActivityId);
            
            CreateTable(
                "dbo.str_Athlete",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        Id = c.Long(nullable: false),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        ProfileMedium = c.String(nullable: false),
                        Profile = c.String(),
                        City = c.String(nullable: false),
                        State = c.String(nullable: false),
                        Country = c.String(nullable: false),
                        Sex = c.String(nullable: false),
                        Friend = c.String(nullable: false),
                        Follower = c.String(nullable: false),
                        IsPremium = c.Boolean(nullable: false),
                        CreatedAt = c.String(nullable: false),
                        UpdatedAt = c.String(nullable: false),
                        ApproveFollowers = c.Boolean(nullable: false),
                        FollowerCount = c.Int(nullable: false),
                        FriendCount = c.Int(nullable: false),
                        MutualFriendCount = c.Int(nullable: false),
                        DatePreference = c.String(nullable: false),
                        MeasurementPreference = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Ftp = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID);
            
            CreateTable(
                "dbo.str_Bike",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        AthleteId = c.Long(nullable: false),
                        Brand = c.String(nullable: false),
                        Model = c.String(nullable: false),
                        BikeType = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.str_Athlete", t => t.AthleteId, cascadeDelete: true)
                .Index(t => t.AthleteId);
            
            CreateTable(
                "dbo.str_Club",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        AthleteId = c.Long(nullable: false),
                        Id = c.Long(nullable: false),
                        ResourceState = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        ProfileMedium = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        ClubType = c.Int(nullable: false),
                        SportType = c.Int(nullable: false),
                        City = c.String(nullable: false),
                        State = c.String(nullable: false),
                        Country = c.String(nullable: false),
                        IsPrivate = c.Boolean(nullable: false),
                        MemberCount = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.str_Athlete", t => t.AthleteId, cascadeDelete: true)
                .Index(t => t.AthleteId);
            
            CreateTable(
                "dbo.str_Shoes",
                c => new
                    {
                        PkID = c.Long(nullable: false, identity: true),
                        AthleteId = c.Long(nullable: false),
                        Id = c.String(nullable: false),
                        IsPrimary = c.Boolean(nullable: false),
                        Name = c.String(nullable: false),
                        Distance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ResourceState = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(),
                        UserCreateId = c.Long(nullable: false),
                        UserUpdateId = c.Long(nullable: false),
                        UserDeleteId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PkID)
                .ForeignKey("dbo.str_Athlete", t => t.AthleteId, cascadeDelete: true)
                .Index(t => t.AthleteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.str_Shoes", "AthleteId", "dbo.str_Athlete");
            DropForeignKey("dbo.str_Club", "AthleteId", "dbo.str_Athlete");
            DropForeignKey("dbo.str_Bike", "AthleteId", "dbo.str_Athlete");
            DropForeignKey("dbo.str_Photo", "ActivityId", "dbo.str_Activity");
            DropForeignKey("dbo.str_Activity", "MapId", "dbo.str_Map");
            DropForeignKey("dbo.str_SegmentEffort", "SegmentId", "dbo.str_Segment");
            DropForeignKey("dbo.str_SegmentEffort", "ActivityId", "dbo.str_Activity");
            DropForeignKey("dbo.str_Segment", "Map_PkID", "dbo.str_Map");
            DropForeignKey("dbo.str_Activity", "GearId", "dbo.str_Gear");
            DropForeignKey("dbo.str_Comment", "ActivityId", "dbo.str_Activity");
            DropForeignKey("dbo.str_ActivityLap", "ActivityId", "dbo.str_Activity");
            DropIndex("dbo.str_Shoes", new[] { "AthleteId" });
            DropIndex("dbo.str_Club", new[] { "AthleteId" });
            DropIndex("dbo.str_Bike", new[] { "AthleteId" });
            DropIndex("dbo.str_Photo", new[] { "ActivityId" });
            DropIndex("dbo.str_SegmentEffort", new[] { "SegmentId" });
            DropIndex("dbo.str_SegmentEffort", new[] { "ActivityId" });
            DropIndex("dbo.str_Segment", new[] { "Map_PkID" });
            DropIndex("dbo.str_Comment", new[] { "ActivityId" });
            DropIndex("dbo.str_ActivityLap", new[] { "ActivityId" });
            DropIndex("dbo.str_Activity", new[] { "GearId" });
            DropIndex("dbo.str_Activity", new[] { "MapId" });
            DropTable("dbo.str_Shoes");
            DropTable("dbo.str_Club");
            DropTable("dbo.str_Bike");
            DropTable("dbo.str_Athlete");
            DropTable("dbo.str_Photo");
            DropTable("dbo.str_SegmentEffort");
            DropTable("dbo.str_Segment");
            DropTable("dbo.str_Map");
            DropTable("dbo.str_Gear");
            DropTable("dbo.str_Comment");
            DropTable("dbo.str_ActivityLap");
            DropTable("dbo.str_Activity");
        }
    }
}
