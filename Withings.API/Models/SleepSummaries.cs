﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Withings.API.Models
{
    public class SleepSummary
    {
        public Int64 Id { get; set; }

        public String Timezone { get; set; }

        public Int32 Model { get; set; }

        public Int64 StartDate { get; set; }

        public Int64 EndDate { get; set; }

        public DateTime Date { get; set; }

        public SleepSummaryData Data { get; set; }

        public Int64 Modified { get; set; }
    }

    public class SleepSummaryData
    {
        public Int32 WakeUpDuration { get; set; }

        public Int32 LightSleepDuration { get; set; }

        public Int32 DeepSleepDuration { get; set; }

        public Int32 RemSleepDuration { get; set; }

        public Int32 DurationToSleep { get; set; }

        public Int32 DurationToWakeup { get; set; }

        public Int32 WakeupCount { get; set; }
    }

    [DataContract]
    internal class InternalSleepSummary
    {
        [DataMember]
        public int status { get; set; }

        [DataMember]
        public InternalBodySleepSummary body { get; set; }
    }

    [DataContract]
    internal class InternalBodySleepSummary
    {
        [DataMember]
        public List<InternalSleepSummarySeria> series { get; set; }

        [DataMember]
        public Boolean more { get; set; }
    }

    [DataContract]
    public class InternalSleepSummarySeria
    {
        [DataMember]
        public Int64 id { get; set; }

        [DataMember]
        public String timezone { get; set; }

        [DataMember]
        public Int32 model { get; set; }

        [DataMember]
        public Int64 startdate { get; set; }

        [DataMember]
        public Int64 enddate { get; set; }

        [DataMember]
        public String date { get; set; }

        [DataMember]
        public InternalSleepSummaryData data { get; set; }

        [DataMember]
        public Int64 modified { get; set; }
    }

    [DataContract]
    public class InternalSleepSummaryData
    {
        [DataMember]
        public Int32 wakeupduration { get; set; }

        [DataMember]
        public Int32 lightsleepduration { get; set; }

        [DataMember]
        public Int32 deepsleepduration { get; set; }

        [DataMember]
        public Int32 remsleepduration { get; set; }

        [DataMember]
        public Int32 durationtosleep { get; set; }

        [DataMember]
        public Int32 durationtowakeup { get; set; }

        [DataMember]
        public Int32 wakeupcount { get; set; }
    }
}
