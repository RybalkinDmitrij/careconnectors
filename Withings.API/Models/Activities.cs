﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Withings.API.Models
{
    public class Activity
    {
        public DateTime Date { get; set; }

        public Int32 Steps { get; set; }

        public Decimal Distance { get; set; }

        public Decimal Calories { get; set; }

        public Decimal TotalCalories { get; set; }

        public Decimal Elevation { get; set; }

        public Int32 Soft { get; set; }

        public Int32 Moderate { get; set; }

        public Int32 Intense { get; set; }

        public String Timezone { get; set; }
    }

    [DataContract]
    internal class InternalActivitySingle
    {
        [DataMember]
        public int status { get; set; }
        [DataMember]
        public InternalBodyActivity body { get; set; }
    }

    [DataContract]
    internal class InternalActivityRange
    {
        [DataMember]
        public int status { get; set; }
        [DataMember]
        public InternalBodyActivities body { get; set; }
    }

    [DataContract]
    internal class InternalBodyActivities
    {
        [DataMember]
        public List<InternalBodyActivity> activities { get; set; }
    }

    [DataContract]
    internal class InternalBodyActivity
    {
        [DataMember]
        public String date { get; set; }

        [DataMember]
        public Int32 steps { get; set; }

        [DataMember]
        public Decimal distance { get; set; }

        [DataMember]
        public Decimal calories { get; set; }

        [DataMember]
        public Decimal totalcalories { get; set; }

        [DataMember]
        public Decimal elevation { get; set; }

        [DataMember]
        public Int32 soft { get; set; }

        [DataMember]
        public Int32 moderate { get; set; }

        [DataMember]
        public Int32 intense { get; set; }

        [DataMember]
        public String timezone { get; set; }
    }
}
