﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Withings.API.Models
{
    public class UpdatedResource
    {
        public Appli Appli { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public String UserId { get; set; }
    }
}
