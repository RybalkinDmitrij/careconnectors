﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Withings.API.Models
{
    public class NotificationProfile
    {
        public Int32 Appli { get; set; }

        public String Callbackurl { get; set; }

        public DateTime Expires { get; set; }

        public String Comment { get; set; }
    }
    
    public enum NotificationType
    {
        Weight = 1,
        Height = 4,
        FateFreeMass = 5,
        FatRatio = 6,
        FatMassWeight = 8,
        DiastolicBloodPressure = 9,
        SystolicBloodPressure = 10,
        HeartPulse = 11,
    }

    public class Notification
    {
        public NotificationType MeasureType { get; set; }
        public double Value { get; set; } //real_value = value * 10^unit 
    }

    [DataContract]
    internal class InternalNotification
    {
        [DataMember]
        public int status { get; set; }
        [DataMember]
        public InternalBodyNotification body { get; set; }
    }

    [DataContract]
    internal class InternalBodyNotification
    {
        [DataMember]
        public List<InternalProfileNotification> profiles { get; set; }
    }

    [DataContract]
    internal class InternalProfileNotification
    {
        [DataMember]
        public Int32 appli { get; set; }

        [DataMember]
        public String callbackurl { get; set; }

        [DataMember]
        public Int64 expires { get; set; }

        [DataMember]
        public String comment { get; set; }
    }
}
