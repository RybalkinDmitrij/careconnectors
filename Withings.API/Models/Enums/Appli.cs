﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Withings.API.Models
{
    public enum Appli
    {
        BodyScale = 1,
        BloodPressure = 4,
        Pulse = 16,
        Sleep = 44
    }
}