﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Withings.API.Models
{
    public class Notifications
    {
        public String status { get; set; }

        public Object body { get; set; }
    }
}
