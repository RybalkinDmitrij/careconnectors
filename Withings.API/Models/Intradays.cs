﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Withings.API.Models
{
    public class Intraday
    {
        public Decimal Calories { get; set; }

        public Int32 Duration { get; set; }

        public Int32 Steps { get; set; }

        public Decimal Elevation { get; set; }

        public Decimal Distance { get; set; }
    }

    [DataContract]
    internal class InternalIntraday
    {
        [DataMember]
        public int status { get; set; }
        [DataMember]
        public InternalBodySeries body { get; set; }
    }

    [DataContract]
    internal class InternalBodySeries
    {
        [DataMember]
        public List<InternalIntradaySeria> series { get; set; }
    }

    [DataContract]
    internal class InternalIntradaySeria
    {
        [DataMember]
        public Decimal calories { get; set; }

        [DataMember]
        public Int32 duration { get; set; }

        [DataMember]
        public Int32 steps { get; set; }

        [DataMember]
        public Decimal elevation { get; set; }

        [DataMember]
        public Decimal distance { get; set; }
    }
}
