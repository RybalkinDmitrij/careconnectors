﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Withings.API.Models
{
    public class Sleep
    {
        public Int64 StartDate { get; set; }

        public Int32 State { get; set; }

        public Int64 EndDate { get; set; }
    }

    [DataContract]
    internal class InternalSleep
    {
        [DataMember]
        public int status { get; set; }

        [DataMember]
        public InternalBodySleep body { get; set; }
    }

    [DataContract]
    internal class InternalBodySleep
    {
        [DataMember]
        public List<InternalSleepSeria> series { get; set; }

        [DataMember]
        public Int32 model { get; set; }
    }

    [DataContract]
    internal class InternalSleepSeria
    {
        [DataMember]
        public Int64 startdate { get; set; }

        [DataMember]
        public Int32 state { get; set; }

        [DataMember]
        public Int64 enddate { get; set; }
    }
}
