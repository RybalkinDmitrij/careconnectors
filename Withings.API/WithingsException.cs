﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Withings.API
{
    /// <summary>
    /// Custom exception class to be used to gather response data from the API when there's an error
    /// </summary>
    public class WithingsException : Exception
    {
        public HttpStatusCode HttpStatusCode { get; set; }

        public IList<Withings.API.Models.ApiError> ApiErrors { get; set; }

        /// <summary>
        /// Number of seconds until the request can be retried - not null if provided by fitbit
        /// </summary>
        public int? retryAfter { get; set; }

        public WithingsException(string message, HttpStatusCode statusCode)
            : this(message, statusCode, new List<Withings.API.Models.ApiError>())
        {
        }

        public WithingsException(string message, HttpStatusCode statusCode, IList<Withings.API.Models.ApiError> apiErrors)
            : base(message)
        {
            this.HttpStatusCode = statusCode;
            this.ApiErrors = apiErrors;
        }

        public bool containsRateError
        {
            get
            {
                return 429 == (int)HttpStatusCode;
            }
        }
    }
}
