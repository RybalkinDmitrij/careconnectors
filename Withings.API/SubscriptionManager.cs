﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Withings.API.Models;
using RestSharp;

namespace Withings.API
{
    public class SubscriptionManager
    {
        #region singleton

        public static SubscriptionManager Instance
        {
            get
            {
                if (_instance == null) _instance = new SubscriptionManager();
                return _instance;
            }
        }

        private static SubscriptionManager _instance = null;

        #endregion singleton

        public UpdatedResource ProcessUpdateReponseBody(String bodyContent)
        {
            var deserializer = new RestSharp.Deserializers.JsonDeserializer();

            UpdatedResource result = deserializer.Deserialize<UpdatedResource>(new RestResponse() { Content = bodyContent });

            return result;
        }

        public String AddNotification(WithingsClient client, String userId, String callbackUrl, String comment)
        {
            return client.CreateNotification(userId, callbackUrl, comment);
        }

        public String RevokeNotification(WithingsClient client, String userId, String callbackUrl, String comment)
        {
            return client.RevokeNotification(userId, callbackUrl, comment);
        }
    }
}
