using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;
using System.Net;
using Withings.API.Models;
using System.IO;
using System.Runtime.Serialization.Json;
using DevDefined.OAuth.Storage.Basic;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;
using System.Security.Cryptography.X509Certificates;
using DevDefined.OAuth.Tests;

namespace Withings.API
{
    public class WithingsClient : IWithingsClient
    {
        private readonly string _consumerKey;
        private readonly string _consumerSecret;
        private readonly string _accessToken;
        private readonly string _accessSecret;

        private OAuthSession _session;
        private OAuthConsumerContext _context;
        
        private const string RequestUrl = "http://oauth.withings.com/account/request_token";
        private const string UserAuthorizeUrl = "http://oauth.withings.com/account/authorize";
        private const string AccessUrl = "http://oauth.withings.com/account/access_token";
        
        public WithingsClient(string consumerKey, string consumerSecret)
        {
            this._consumerKey = consumerKey;
            this._consumerSecret = consumerSecret;
        }

        /// <summary>
        /// Initialize the WithingsClient using the provided access and the default API endpoint and RestSharp RestClient
        /// </summary>
        /// <param name="consumerKey"></param>
        /// <param name="consumerSecret"></param>
        /// <param name="accessToken"></param>
        /// <param name="accessSecret"></param>
        public WithingsClient(string consumerKey, string consumerSecret, string accessToken, string accessSecret)
        {
            this._consumerKey = consumerKey;
            this._consumerSecret = consumerSecret;
            this._accessToken = accessToken;
            this._accessSecret = accessSecret;

            this.CreateSession();
        }

        public List<NotificationProfile> GetNotifications(String userId)
        {
            List<NotificationProfile> result = new List<NotificationProfile>();
            result.AddRange(GetAppliNotification(userId, Appli.BodyScale));
            result.AddRange(GetAppliNotification(userId, Appli.BloodPressure));
            result.AddRange(GetAppliNotification(userId, Appli.Pulse));
            result.AddRange(GetAppliNotification(userId, Appli.Sleep));

            return result;
        }

        public String CreateNotification(String userId, String callbackUrl, String comment)
        {
            String codeBodyScale = CreateAppliNotification(userId, callbackUrl, comment, Appli.BodyScale);
            String codeBloodPressure = CreateAppliNotification(userId, callbackUrl, comment, Appli.BloodPressure);
            String codePulse = CreateAppliNotification(userId, callbackUrl, comment, Appli.Pulse);
            String codeSleep = CreateAppliNotification(userId, callbackUrl, comment, Appli.Sleep);
            
            return String.Join(" ", codeBodyScale, codeBloodPressure, codePulse, codeSleep);
        }

        public String RevokeNotification(String userId, String callbackUrl, String comment)
        {
            String codeBodyScale = RevokeAppliNotification(userId, callbackUrl, comment, Appli.BodyScale);
            String codeBloodPressure = RevokeAppliNotification(userId, callbackUrl, comment, Appli.BloodPressure);
            String codePulse = RevokeAppliNotification(userId, callbackUrl, comment, Appli.Pulse);
            String codeSleep = RevokeAppliNotification(userId, callbackUrl, comment, Appli.Sleep);

            return String.Join(" ", codeBodyScale, codeBloodPressure, codePulse, codeSleep);
        }

        public List<MeasureGroup> GetMeasures(String userId, DateTime lastUpdate, int offset = -1, int limit = -1)
        {
            var apiCall = String.Format("http://wbsapi.withings.net/measure?action=getmeas&userid={0}&lastupdate={1}", userId, ToUnixTime(lastUpdate));
            if (offset >= 0)
                apiCall += String.Format("&offset={0}", offset);
            if (limit >= 0)
                apiCall += String.Format("&limit={0}", offset);

            var accessToken = CreateAccessToken();

            String responseText = _session.Request(accessToken).Get().ForUrl(apiCall).ToString();
            return TransformMeasures(responseText);
        }

        public List<MeasureGroup> GetMeasures(String userId, DateTime startDate, DateTime endDate, int offset = -1, int limit = -1)
        {
            var startDateBeginDay = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
            var endDateEndDay = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 0);

            var apiCall = String.Format("http://wbsapi.withings.net/measure?action=getmeas&userid={0}&startdate={1}&enddate={2}", userId, ToUnixTime(startDateBeginDay), ToUnixTime(endDateEndDay));
            if (offset >= 0)
                apiCall += String.Format("&offset={0}", offset);
            if (limit >= 0)
                apiCall += String.Format("&limit={0}", offset);

            var accessToken = CreateAccessToken();

            String responseText = _session.Request(accessToken).Get().ForUrl(apiCall).ToString();
            return TransformMeasures(responseText);
        }

        public Activity GetActivity(String userId, DateTime dateSingle)
        {
            var apiCall = String.Format("http://wbsapi.withings.net/v2/measure?action=getactivity&userid={0}&date={1}", userId, dateSingle.ToString("yyyy-MM-dd"));

            var accessToken = CreateAccessToken();

            String responseText = _session.Request(accessToken).Get().ForUrl(apiCall).ToString();
            return TransformActivitySingle(responseText);
        }

        public List<Activity> GetActivity(String userId, DateTime startDate, DateTime endDate)
        {
            var apiCall = String.Format("http://wbsapi.withings.net/v2/measure?action=getactivity&userid={0}&startdateymd={1}&enddateymd={2}", userId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));

            var accessToken = CreateAccessToken();

            String responseText = _session.Request(accessToken).Get().ForUrl(apiCall).ToString();
            responseText = responseText.Replace("'", "\"");

            return TransformActivityRange(responseText);
        }

        public List<Intraday> GetIntraday(String userId, DateTime startDate, DateTime endDate)
        {
            var apiCall = String.Format("http://wbsapi.withings.net/v2/measure?action=getintradayactivity&userid={0}&startdate={1}&enddate={2}", userId, ToUnixTime(startDate), ToUnixTime(endDate));

            var accessToken = CreateAccessToken();

            String responseText = _session.Request(accessToken).Get().ForUrl(apiCall).ToString();
            return TransformIntraday(responseText);
        }

        public List<Sleep> GetSleep(String userId, DateTime startDate, DateTime endDate)
        {
            var apiCall = String.Format("http://wbsapi.withings.net/v2/sleep?action=get&userid={0}&startdate={1}&enddate={2}", userId, ToUnixTime(startDate), ToUnixTime(endDate));

            var accessToken = CreateAccessToken();

            String responseText = _session.Request(accessToken).Get().ForUrl(apiCall).ToString();
            return TransformSleep(responseText);
        }

        public List<SleepSummary> GetSleepSummary(String userId, DateTime startDate, DateTime endDate)
        {
            var apiCall = String.Format("http://wbsapi.withings.net/v2/sleep?action=getsummary&startdateymd={0}&enddate={1}", startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));

            var accessToken = CreateAccessToken();

            String responseText = _session.Request(accessToken).Get().ForUrl(apiCall).ToString();
            return TransformSleepSummary(responseText);
        }

        private AccessToken CreateAccessToken()
        {
            var accessToken = new AccessToken()
            {
                ConsumerKey = this._consumerKey,
                Token = this._accessToken,
                TokenSecret = this._accessSecret,
            };
            return accessToken;
        }

        private void CreateSession()
        {
            X509Certificate2 certificate = TestCertificates.OAuthTestCertificate();
            var consumerContext = new OAuthConsumerContext
            {
                SignatureMethod = SignatureMethod.HmacSha1,
                ConsumerKey = this._consumerKey,
                ConsumerSecret = this._consumerSecret,
                UseHeaderForOAuthParameters = false,
            };
            this._context = consumerContext;
            this._session = new OAuthSession(this._context, RequestUrl, UserAuthorizeUrl, AccessUrl);
        }

        private static List<MeasureGroup> TransformMeasures(string responseText)
        {
            DataContractJsonSerializer sr = new DataContractJsonSerializer(typeof(InternalMeasure));
            Byte[] bytes = Encoding.Unicode.GetBytes(responseText);
            InternalMeasure mesu = null;
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                mesu = sr.ReadObject(stream) as InternalMeasure;
            }

            if (mesu != null)
            {
                List<MeasureGroup> ret = new List<MeasureGroup>();
                foreach (var m in mesu.body.measuregrps)
                {
                    var me = new MeasureGroup();
                    me.Attribution = (AttributionType)m.attrib;
                    me.Category = (CategoryType)m.category;
                    me.Date = FromUnixTime(m.date);
                    me.Id = m.grpid;

                    me.Measures = new List<Measure>();
                    foreach (var internalMe in m.measures)
                    {
                        var newMe = new Measure();
                        newMe.MeasureType = (MeasureType)internalMe.type;
                        newMe.Value = internalMe.value;
                        newMe.Unit = internalMe.unit;
                        me.Measures.Add(newMe);
                    }

                    ret.Add(me);
                }

                return ret;
            }
            else
                throw new Exception();
        }

        private static List<NotificationProfile> TransformNotifications(String responseText)
        {
            DataContractJsonSerializer sr = new DataContractJsonSerializer(typeof(InternalNotification));
            Byte[] bytes = Encoding.Unicode.GetBytes(responseText);
            InternalNotification mesu = null;
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                mesu = sr.ReadObject(stream) as InternalNotification;
            }

            if (mesu != null)
            {
                List<NotificationProfile> ret = new List<NotificationProfile>();

                if (mesu.body == null) return ret;

                foreach (var m in mesu.body.profiles)
                {
                    var entity = new NotificationProfile();

                    entity.Appli = m.appli;
                    entity.Callbackurl = m.callbackurl;
                    entity.Comment = m.comment;
                    entity.Expires = FromUnixTime(m.expires);

                    ret.Add(entity);
                }

                return ret;
            }
            else
                throw new Exception();
        }

        private static Activity TransformActivitySingle(String responseText)
        {
            DataContractJsonSerializer sr = new DataContractJsonSerializer(typeof(InternalActivitySingle));
            Byte[] bytes = Encoding.Unicode.GetBytes(responseText);
            InternalActivitySingle mesu = null;
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                mesu = sr.ReadObject(stream) as InternalActivitySingle;
            }

            if (mesu != null)
            {
                Activity entity = new Activity();

                entity.Date = Convert.ToDateTime(mesu.body.date);
                entity.Steps = mesu.body.steps;
                entity.Distance = mesu.body.distance;
                entity.Calories = mesu.body.calories;
                entity.TotalCalories = mesu.body.totalcalories;
                entity.Elevation = mesu.body.elevation;
                entity.Soft = mesu.body.soft;
                entity.Moderate = mesu.body.moderate;
                entity.Intense = mesu.body.intense;
                entity.Timezone = mesu.body.timezone;

                return entity;
            }
            else
            {
                throw new Exception();
            }
        }

        private static List<Activity> TransformActivityRange(String responseText)
        {
            DataContractJsonSerializer sr = new DataContractJsonSerializer(typeof(InternalActivityRange));
            Byte[] bytes = Encoding.Unicode.GetBytes(responseText);
            InternalActivityRange mesu = null;
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                mesu = sr.ReadObject(stream) as InternalActivityRange;
            }

            if (mesu != null)
            {
                List<Activity> data = new List<Activity>();

                foreach (var item in mesu.body.activities)
                {
                    Activity entity = new Activity();

                    entity.Date = Convert.ToDateTime(item.date);
                    entity.Steps = item.steps;
                    entity.Distance = item.distance;
                    entity.Calories = item.calories;
                    entity.TotalCalories = item.totalcalories;
                    entity.Elevation = item.elevation;
                    entity.Soft = item.soft;
                    entity.Moderate = item.moderate;
                    entity.Intense = item.intense;
                    entity.Timezone = item.timezone;

                    data.Add(entity);
                }

                return data;
            }
            else
            {
                throw new Exception();
            }
        }

        private static List<Intraday> TransformIntraday(String responseText)
        {
            DataContractJsonSerializer sr = new DataContractJsonSerializer(typeof(InternalIntraday));
            Byte[] bytes = Encoding.Unicode.GetBytes(responseText);
            InternalIntraday mesu = null;
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                mesu = sr.ReadObject(stream) as InternalIntraday;
            }

            if (mesu != null)
            {
                List<Intraday> data = new List<Intraday>();

                foreach (var item in mesu.body.series)
                {
                    Intraday entity = new Intraday();

                    entity.Calories = item.calories;
                    entity.Distance = item.distance;
                    entity.Duration = item.duration;
                    entity.Elevation = item.elevation;
                    entity.Steps = item.steps;

                    data.Add(entity);
                }

                return data;
            }
            else
            {
                throw new Exception();
            }
        }

        private static List<Sleep> TransformSleep(String responseText)
        {
            DataContractJsonSerializer sr = new DataContractJsonSerializer(typeof(InternalSleep));
            Byte[] bytes = Encoding.Unicode.GetBytes(responseText);
            InternalSleep mesu = null;
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                mesu = sr.ReadObject(stream) as InternalSleep;
            }

            if (mesu != null)
            {
                List<Sleep> data = new List<Sleep>();

                foreach (var item in mesu.body.series)
                {
                    Sleep entity = new Sleep();

                    entity.StartDate = item.startdate;
                    entity.State = item.state;
                    entity.EndDate = item.enddate;

                    data.Add(entity);
                }

                return data;
            }
            else
            {
                throw new Exception();
            }
        }

        private static List<SleepSummary> TransformSleepSummary(String responseText)
        {
            DataContractJsonSerializer sr = new DataContractJsonSerializer(typeof(InternalSleepSummary));
            Byte[] bytes = Encoding.Unicode.GetBytes(responseText);
            InternalSleepSummary mesu = null;
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                mesu = sr.ReadObject(stream) as InternalSleepSummary;
            }

            if (mesu != null)
            {
                List<SleepSummary> data = new List<SleepSummary>();

                foreach (var item in mesu.body.series)
                {
                    SleepSummary entity = new SleepSummary();

                    entity.Id = item.id;
                    entity.Timezone = item.timezone;
                    entity.Model = item.model;
                    entity.StartDate = item.startdate;
                    entity.EndDate = item.enddate;
                    entity.Date = Convert.ToDateTime(item.date);

                    entity.Data = new SleepSummaryData();
                    entity.Data.WakeUpDuration = item.data.wakeupduration;
                    entity.Data.LightSleepDuration = item.data.lightsleepduration;
                    entity.Data.DeepSleepDuration = item.data.deepsleepduration;
                    entity.Data.RemSleepDuration = item.data.remsleepduration;
                    entity.Data.DurationToSleep = item.data.durationtosleep;
                    entity.Data.DurationToWakeup = item.data.durationtowakeup;
                    entity.Data.WakeupCount = item.data.wakeupcount;

                    entity.Modified = item.modified;

                    data.Add(entity);
                }

                return data;
            }
            else
            {
                throw new Exception();
            }
        }

        #region Helpers

        private void HandleResponse(IRestResponse response)
        {
            System.Net.HttpStatusCode httpStatusCode = response.StatusCode;
            if (httpStatusCode == System.Net.HttpStatusCode.OK ||        //200
                httpStatusCode == System.Net.HttpStatusCode.Created ||   //201
                httpStatusCode == System.Net.HttpStatusCode.NoContent)   //204
            {
                return;
            }
            else
            {
                Console.WriteLine("HttpError:" + httpStatusCode.ToString());
                IList<ApiError> errors;
                try
                {
                    var xmlDeserializer = new RestSharp.Deserializers.XmlDeserializer() { RootElement = "errors" };
                    errors = xmlDeserializer.Deserialize<List<ApiError>>(new RestResponse { Content = response.Content });
                }
                catch (Exception) // If there's an issue deserializing the error we still want to raise a fitbit exception
                {
                    errors = new List<ApiError>();
                }

                WithingsException exception = new WithingsException("Http Error:" + httpStatusCode.ToString(), httpStatusCode, errors);

                var retryAfterHeader = response.Headers.FirstOrDefault(h => h.Name == "Retry-After");
                if (retryAfterHeader != null)
                {
                    int retryAfter;
                    if (int.TryParse(retryAfterHeader.Value.ToString(), out retryAfter))
                    {
                        exception.retryAfter = retryAfter;
                    }
                }
                throw exception;
            }
        }

        private string GetActivityApiExtentionURL(DateTime activityDate)
        {
            const string ApiExtention = "/1/user/-/activities/date/{0}-{1}-{2}.xml";
            return string.Format(ApiExtention, activityDate.Year.ToString(), activityDate.Month.ToString(), activityDate.Day.ToString());
        }

        private void AddPostParameter(IRestRequest request, string name, object value)
        {
            Parameter p = new Parameter();
            p.Type = ParameterType.GetOrPost;
            p.Name = name;
            p.Value = value;
            request.AddParameter(p);
        }

        private static DateTime FromUnixTime(long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        private static long ToUnixTime(DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date - epoch).TotalSeconds);
        }

        private String CreateAppliNotification(String userId, String callbackUrl, String comment, Appli appli)
        {
            var apiCall = String.Format(@"https://wbsapi.withings.net/notify?action=subscribe&userid={0}&callbackurl={1}&comment={2}&appli={3}", userId, WebUtility.UrlEncode(callbackUrl), WebUtility.UrlEncode(comment), (Int32)appli);

            var accessToken = CreateAccessToken();

            IConsumerRequest request = _session.Request(accessToken);

            return request.Get().ForUrl(apiCall).ToString();
        }

        private List<NotificationProfile> GetAppliNotification(String userId, Appli appli)
        {
            var apiCall = String.Format(@"http://wbsapi.withings.net/notify?action=list&userid={0}&appli={1}", userId, (Int32)appli);

            var accessToken = CreateAccessToken();
            String responseText = _session.Request(accessToken).Get().ForUrl(apiCall).ToString();
            return TransformNotifications(responseText);
        }

        private String RevokeAppliNotification(String userId, String callbackUrl, String comment, Appli appli)
        {
            var apiCall = String.Format(@"https://wbsapi.withings.net/notify?action=revoke&userid={0}&callbackurl={1}&appli={2}", userId, callbackUrl, (Int32)appli);

            var accessToken = CreateAccessToken();

            IConsumerRequest request = _session.Request(accessToken);

            String code = request.Get().ForUrl(apiCall).ToString();

            return code;
        }

        #endregion
    }
}
