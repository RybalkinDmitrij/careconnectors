﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Contrib;
using Withings.API.Models;

namespace Withings.API
{
	public class Authenticator
	{
        const string WithingsBaseUrl = "https://oauth.withings.com";

		private string ConsumerKey;
		private string ConsumerSecret;
		private string RequestTokenUrl;
		private string AccessTokenUrl;
		private string AuthorizeUrl;
		
        private readonly IRestClient client;

		public Authenticator(string ConsumerKey, string ConsumerSecret, string RequestTokenUrl, string AccessTokenUrl,
		                     string AuthorizeUrl, IRestClient restClient = null)
		{
			this.ConsumerKey = ConsumerKey;
			this.ConsumerSecret = ConsumerSecret;
			this.RequestTokenUrl = RequestTokenUrl;
			this.AccessTokenUrl = AccessTokenUrl;
			this.AuthorizeUrl = AuthorizeUrl;
            client = restClient ?? new RestClient(WithingsBaseUrl);
		}

        public string GenerateAuthUrlFromRequestToken(RequestToken token, bool forceLogoutBeforeAuth)
		{
            RestRequest request = null;

            //if(forceLogoutBeforeAuth)
            //    request = new RestRequest("account/logout_and_authorize"); //this url will force the user to type in username and password
            //else
            //    request = new RestRequest("account/authorize");           //this url will show allow/deny if a user is currently logged in

            request = new RestRequest("account/authorize");
			request.AddParameter("oauth_token", token.Token);
			var url = client.BuildUri(request).ToString();

			return url;
		} 

        /// <summary>
        /// First step in the OAuth process is to ask Fitbit for a temporary request token. 
        /// From this you should store the RequestToken returned for later processing the auth token.
        /// </summary>
        /// <returns></returns>
        public RequestToken GetRequestToken()
        {
            return GetRequestToken(string.Empty);
        }
        public RequestToken GetRequestToken(string callback)
        {
            client.Authenticator = OAuth1Authenticator.ForRequestToken(this.ConsumerKey, this.ConsumerSecret, callback);

            var request = new RestRequest("account/request_token", Method.GET);
            var response = client.Execute(request);

            var qs = HttpUtility.ParseQueryString(response.Content);

            RequestToken token = new RequestToken();

            token.Token = qs["oauth_token"];
            token.Secret = qs["oauth_token_secret"];

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
                throw new Exception("Request Token Step Failed");

            return token;
        }

		/// <summary>
		/// For Desktop authentication. Your code should direct the user to the FitBit website to get
		/// Their pin, they can then enter it here.
		/// </summary>
		/// <param name="pin"></param>
		/// <returns></returns>
		public AuthCredential GetAuthCredentialFromPin(string pin, RequestToken token)
		{
            var request = new RestRequest("account/access_token", Method.GET);
			client.Authenticator = OAuth1Authenticator.ForAccessToken(ConsumerKey, ConsumerSecret, token.Token, token.Secret, pin);
			
			var response = client.Execute(request);
			var qs = RestSharp.Contrib.HttpUtility.ParseQueryString(response.Content);

			return new AuthCredential()
			{
				AuthToken = qs["oauth_token"],
				AuthTokenSecret = qs["oauth_token_secret"],
				UserId = qs["encoded_user_id"]
			};
		}

		public AuthCredential ProcessApprovedAuthCallback(RequestToken token)
		{
            if (string.IsNullOrWhiteSpace(token.Token))
                throw new Exception("RequestToken. Token must not be null");
            //else if 

			client.Authenticator = OAuth1Authenticator.ForRequestToken(this.ConsumerKey, this.ConsumerSecret);

            var request = new RestRequest("account/access_token", Method.GET);
			

			client.Authenticator = OAuth1Authenticator.ForAccessToken(
				this.ConsumerKey, this.ConsumerSecret, token.Token, token.Secret, token.Verifier
			);
			
			var response = client.Execute(request);

			if (response.StatusCode != HttpStatusCode.OK)
				throw new WithingsException(response.Content, response.StatusCode);

			var qs = HttpUtility.ParseQueryString(response.Content);
			var oauth_token = qs["oauth_token"];
			var oauth_token_secret = qs["oauth_token_secret"];
			var encoded_user_id = qs["encoded_user_id"];
            
			return new AuthCredential()
			{
				AuthToken = oauth_token,
				AuthTokenSecret = oauth_token_secret,
				UserId = encoded_user_id
			};
		}
	}
}