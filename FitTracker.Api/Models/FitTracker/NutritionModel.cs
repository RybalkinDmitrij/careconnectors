﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Api.Models.FitTracker
{
    public class NutritionModel
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String _userId { get; set; }

        /// <summary>
        /// Timestamp for the measurement set
        /// </summary>
        public String timestamp { get; set; }

        /// <summary>
        /// Timezone information for the measurement set
        /// </summary>
        public String utc_offset { get; set; }

        /// <summary>
        /// The value of the measured quantity
        /// </summary>
        public Decimal calories { get; set; }

        /// <summary>
        /// The value of the measured quantity in g
        /// </summary>
        public Decimal carbohydrates { get; set; }

        /// <summary>
        /// The value of the measured quantity in g
        /// </summary>
        public Decimal fat { get; set; }

        /// <summary>
        /// The value of the measured quantity in g
        /// </summary>
        public Decimal fiber { get; set; }

        /// <summary>
        /// The value of the measured quantity in g
        /// </summary>
        public Decimal protein { get; set; }

        /// <summary>
        /// The value of the measured quantity in mg
        /// </summary>
        public Decimal sodium { get; set; }

        /// <summary>
        /// The value of the measured quantity in fl oz
        /// </summary>
        public Decimal water { get; set; }

        /// <summary>
        /// The meal, for example: pizza, Coke, chicken breast, other, unspecified
        /// </summary>
        public String meal { get; set; }

        /// <summary>
        /// The short name of the application that recorded the activity
        /// </summary>
        public String source { get; set; }
    }
}