﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Api.Models.FitTracker
{
    public class TobaccoModel
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String _userId { get; set; }

        /// <summary>
        /// Timestamp for the measurement set
        /// </summary>
        public String timestamp { get; set; }

        /// <summary>
        /// Timezone information for the measurement set
        /// </summary>
        public String utc_offset { get; set; }

        /// <summary>
        /// Number of cigarettes budgeted for a given day
        /// </summary>
        public Int32 cigarettes_allowed { get; set; }

        /// <summary>
        /// Number of cigarettes smoked for a given day
        /// </summary>
        public Int32 cigarettes_smoked { get; set; }

        /// <summary>
        /// Number of cravings a user has for a given day
        /// </summary>
        public Int32 cravings { get; set; }

        /// <summary>
        /// Timestamp for the last cigarette smoked
        /// </summary>
        public String last_smoked { get; set; }

        /// <summary>
        /// The short name of the application that recorded the activity
        /// </summary>
        public String source { get; set; }
    }
}