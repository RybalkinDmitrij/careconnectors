﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Api.Models.FitTracker
{
    public class WeightModel
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String _userId { get; set; }

        /// <summary>
        /// Timestamp for the measurement set
        /// </summary>
        public String timestamp { get; set; }

        /// <summary>
        /// Timezone information for the measurement set
        /// </summary>
        public String utc_offset { get; set; }

        /// <summary>
        /// The value of the measured quantity in kg
        /// </summary>
        public Decimal weight { get; set; }

        /// <summary>
        /// The value of the measured quantity in cm
        /// </summary>
        public Decimal height { get; set; }

        /// <summary>
        /// The value of the measured quantity in kg
        /// </summary>
        public Decimal free_mass { get; set; }

        /// <summary>
        /// The value of the measured quantity
        /// </summary>
        public Decimal fat_percent { get; set; }

        /// <summary>
        /// The value of the measured quantity in kg
        /// </summary>
        public Decimal mass_weight { get; set; }

        /// <summary>
        /// The value of the measured quantity
        /// </summary>
        public Decimal bmi { get; set; }

        /// <summary>
        /// The short name of the application that recorded the activity
        /// </summary>
        public String source { get; set; }
    }
}