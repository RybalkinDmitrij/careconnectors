﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Api.Models.FitTracker
{
    public class DiabetesModel
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String _userId { get; set; }

        /// <summary>
        /// Timestamp for the measurement set
        /// </summary>
        public String timestamp { get; set; }

        /// <summary>
        /// Timezone information for the measurement set
        /// </summary>
        public String utc_offset { get; set; }

        /// <summary>
        /// The value of the measured quantity in ng/mL
        /// </summary>
        public Decimal c_peptide { get; set; }

        /// <summary>
        /// The value of the measured quantity in mg/dL
        /// </summary>
        public Decimal fasting_plasma_glucose_test { get; set; }

        /// <summary>
        /// The value of the measured quantity in %
        /// </summary>
        public Decimal hba1c { get; set; }

        /// <summary>
        /// The value of the measured quantity in U
        /// </summary>
        public Decimal insulin { get; set; }

        /// <summary>
        /// The value of the measured quantity in mg/dL
        /// </summary>
        public Decimal oral_glucose_tolerance_test { get; set; }

        /// <summary>
        /// The value of the measured quantity in mg/dL
        /// </summary>
        public Decimal random_plasma_glucose_test { get; set; }

        /// <summary>
        /// The value of the measured quantity in mg/dL
        /// </summary>
        public Decimal triglyceride { get; set; }

        /// <summary>
        /// The value of the measured quantity in mg/dL
        /// </summary>
        public Decimal blood_glucose { get; set; }

        /// <summary>
        /// The short name of the application that recorded the activity
        /// </summary>
        public String source { get; set; }
    }
}