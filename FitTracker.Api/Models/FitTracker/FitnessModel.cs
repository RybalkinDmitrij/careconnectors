﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Api.Models.FitTracker
{
    public class FitnessModel
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String _userId { get; set; }

        /// <summary>
        /// Timestamp for the measurement set
        /// </summary>
        public String timestamp { get; set; }

        /// <summary>
        /// Timezone information for the measurement set
        /// </summary>
        public String utc_offset { get; set; }

        /// <summary>
        /// Sample types of activities: Running, Cycling, Mountain Biking, Walking, Hiking, Downhill Skiing, Cross-country Skiing, Snowboarding, Skating, Swimming, Rowing, Elliptical, Other. Activities vary from source to source and are returned as provided by source.
        /// </summary>
        public String type { get; set; }

        /// <summary>
        /// String Subjective intensity with which an activity was performed. Examples are: low, medium, high. Returned as provided by source.
        /// </summary>
        public String intensity { get; set; }

        /// <summary>
        /// String Starting time for the activity. Preferred format is an ISO 8601 timestamp (YYYY-MM-DDThh:mm:ssZ, e.g., 2013-03-10T07:12:16-05:00).
        /// </summary>
        public String start_time { get; set; }

        /// <summary>
        /// Total distance for the activity in meters
        /// </summary>
        public Decimal distance { get; set; }

        /// <summary>
        /// Duration of the activity in seconds
        /// </summary>
        public Decimal duration { get; set; }

        /// <summary>
        /// Total calories burned for the activity
        /// </summary>
        public Decimal calories { get; set; }

        /// <summary>
        /// The short name of the application that recorded the activity
        /// </summary>
        public String source { get; set; }
    }
}