﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Api.Models.FitTracker
{
    public class PulseModel
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String _userId { get; set; }

        /// <summary>
        /// Timestamp for the measurement set
        /// </summary>
        public String timestamp { get; set; }

        /// <summary>
        /// Timezone information for the measurement set
        /// </summary>
        public String utc_offset { get; set; }

        /// <summary>
        /// Diastolic blood pressure
        /// </summary>
        public Decimal diastolic_blood_pressure { get; set; }

        /// <summary>
        /// Systolic blood pressure
        /// </summary>
        public Decimal systolic_blood_pressure { get; set; }

        /// <summary>
        /// Heart pulse
        /// </summary>
        public Decimal heart_pulse { get; set; }

        /// <summary>
        /// Source
        /// </summary>
        public String source { get; set; }
    }
}