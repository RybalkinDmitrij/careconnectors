﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Api.Models.FitTracker
{
    public class BiometricsModel
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String _userId { get; set; }

        /// <summary>
        /// Timestamp for the measurement set
        /// </summary>
        public String timestamp { get; set; }

        /// <summary>
        /// Timezone information for the measurement set
        /// </summary>
        public String utc_offset { get; set; }

        /// <summary>
        /// The value of the measured quantity in mg/dL
        /// </summary>
        public Decimal blood_calcium { get; set; }

        /// <summary>
        /// The value of the measured quantity in µg/L
        /// </summary>
        public Decimal blood_chromium { get; set; }

        /// <summary>
        /// The value of the measured quantity in ng/mL
        /// </summary>
        public Decimal blood_folic_acid { get; set; }

        /// <summary>
        /// The value of the measured quantity in mg/dL
        /// </summary>
        public Decimal blood_magnesium { get; set; }

        /// <summary>
        /// The value of the measured quantity in mEq/L
        /// </summary>
        public Decimal blood_potassium { get; set; }

        /// <summary>
        /// The value of the measured quantity in mEq/L
        /// </summary>
        public Decimal blood_sodium { get; set; }

        /// <summary>
        /// The value of the measured quantity in pg/mL
        /// </summary>
        public Decimal blood_vitamin_b12 { get; set; }

        /// <summary>
        /// The value of the measured quantity in µg/dL
        /// </summary>
        public Decimal blood_zinc { get; set; }

        /// <summary>
        /// The value of the measured quantity in U/L
        /// </summary>
        public Decimal creatine_kinase { get; set; }

        /// <summary>
        /// The value of the measured quantity in mg/L
        /// </summary>
        public Decimal crp { get; set; }

        /// <summary>
        /// The value of the measured quantity in mmHg
        /// </summary>
        public Decimal diastolic { get; set; }

        /// <summary>
        /// The value of the measured quantity in ng/mL
        /// </summary>
        public Decimal ferritin { get; set; }

        /// <summary>
        /// The value of the measured quantity in mg/dL
        /// </summary>
        public Decimal hdl { get; set; }

        /// <summary>
        /// The value of the measured quantity in mg/L
        /// </summary>
        public Decimal hscrp { get; set; }

        /// <summary>
        /// The value of the measured quantity in pg/mL
        /// </summary>
        public Decimal il6 { get; set; }

        /// <summary>
        /// The value of the measured quantity in mg/dL
        /// </summary>
        public Decimal ldl { get; set; }

        /// <summary>
        /// The value of the measured quantity in bpm
        /// </summary>
        public Decimal resting_heartrate { get; set; }

        /// <summary>
        /// The value of the measured quantity in mmHg
        /// </summary>
        public Decimal systolic { get; set; }

        /// <summary>
        /// The value of the measured quantity in ng/dL
        /// </summary>
        public Decimal testosterone { get; set; }

        /// <summary>
        /// The value of the measured quantity in mg/dL
        /// </summary>
        public Decimal total_cholesterol { get; set; }

        /// <summary>
        /// The value of the measured quantity in mIU/L
        /// </summary>
        public Decimal tsh { get; set; }

        /// <summary>
        /// The value of the measured quantity in mg/dL
        /// </summary>
        public Decimal uric_acid { get; set; }

        /// <summary>
        /// The value of the measured quantity in ng/mL
        /// </summary>
        public Decimal vitamin_d { get; set; }

        /// <summary>
        /// The value of the measured quantity in cells/µL
        /// </summary>
        public Decimal white_cell_count { get; set; }

        /// <summary>
        /// The value of the measured quantity in %
        /// </summary>
        public Decimal spo2 { get; set; }

        /// <summary>
        /// The value of the measured quantity in Celsius
        /// </summary>
        public Decimal temperature { get; set; }

        /// <summary>
        /// The short name of the application that recorded the activity
        /// </summary>
        public String source { get; set; }
    }
}