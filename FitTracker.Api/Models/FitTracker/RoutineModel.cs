﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Api.Models.FitTracker
{
    public class RoutineModel
    {
        /// <summary>
        /// Identify of user
        /// </summary>
        public String _userId { get; set; }

        /// <summary>
        /// Timestamp for the measurement set
        /// </summary>
        public String timestamp { get; set; }

        /// <summary>
        /// Timezone information for the measurement set
        /// </summary>
        public String utc_offset { get; set; }

        /// <summary>
        /// The value of the measured quantity
        /// </summary>
        public Int32 steps { get; set; }

        /// <summary>
        /// The value of the measured quantity in meters
        /// </summary>
        public Decimal distance { get; set; }

        /// <summary>
        /// The value of the measured quantity
        /// </summary>
        public Int32 floors { get; set; }

        /// <summary>
        /// The value of the measured quantity in meters
        /// </summary>
        public Decimal elevation { get; set; }

        /// <summary>
        /// The value of the measured quantity
        /// </summary>
        public Decimal calories_burned { get; set; }

        /// <summary>
        /// The short name of the application that recorded the activity
        /// </summary>
        public String source { get; set; }
    }
}