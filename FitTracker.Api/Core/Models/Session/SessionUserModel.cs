﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Api.Core.Models.Session
{
    public class SessionUserModel
    {
        public Int64 ID { get; set; }

        public String ExternalId { get; set; }

        public String FitbitId { get; set; }

        public String FitbitAccessToken { get; set; }

        public String WithingsId { get; set; }

        public String WithingsAccessToken { get; set; }

        public String JawboneId { get; set; }

        public String JawboneAccessToken { get; set; }

        public String RunkeeperId { get; set; }

        public String RunkeeperAccessToken { get; set; }
    }
}