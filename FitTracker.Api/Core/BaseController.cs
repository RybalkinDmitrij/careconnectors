﻿using FitTracker.Api.Core.Models.Session;
using FitTracker.Core.Unity;
using FitTracker.Data;
using FitTracker.Data.Repo.Auth;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace FitTracker.Api.Core
{
    public class BaseController : ApiController
    {
        public SessionUserModel CurrentUser
        {
            get
            {
                if (String.IsNullOrEmpty(User.Identity.Name))
                {
                    return null;
                }

                var key = User.Identity.Name + "_session_user";
                var cache = HttpContext.Current.Cache;
                //var user = cache[key] != null ? (SessionUserModel)cache[key] : null;
                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IClientRepo>();
                    var client = repo.GetByAspNetUserName(User.Identity.Name);

                    var user = new SessionUserModel
                    {
                        ID = client.PkID,
                        ExternalId = client.ExternalId,
                        FitbitId = client.FitbitId,
                        FitbitAccessToken = client.FitbitAccessToken,
                        WithingsId = client.WithingsId,
                        WithingsAccessToken = client.WithingsAccessToken,
                        JawboneId = client.JawboneId,
                        JawboneAccessToken = client.JawboneAccessToken,
                        RunkeeperId = client.RunkeeperId,
                        RunkeeperAccessToken = client.RunkeeperAccessToken
                    };

                    //cache[key] = user;
                    return user;
                }
            }
        }
    }

}
