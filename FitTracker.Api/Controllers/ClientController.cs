﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using FitTracker.Api.Models;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using FitTracker.Data.Helpers;
using FitTracker.Core.Unity;
using FitTracker.Data;
using FitTracker.Data.Repo.Auth;
using FitTracker.Data.Entities.Auth;
using System.Web;
using System.Text;
using System.IO;
using Newtonsoft.Json;


namespace FitTracker.Api.Controllers
{
    public class ClientController : ApiController
    {
        /// <summary>
        /// Registration new user and getting token
        /// </summary>
        /// <param name="userExternalId"></param>
        /// <param name="consumerKey"></param>
        /// <param name="consumerSecret"></param>
        /// <returns></returns>
        [AcceptVerbs("POST")]
        [AllowAnonymous]
        [Route("client/register")]
        public async Task<IHttpActionResult> Register(String userExternalId, String consumerKey, String consumerSecret)
        {
            try
            {
                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repoApp = uow.GetRepo<IApplicantRepo>();
                    Applicant app = repoApp.GetByKeyAndSecret(consumerKey, consumerSecret);

                    if (app == null)
                        throw new Exception(String.Format("Such app is not exist. Consumer Key: {0}. Consumer Secret: {1}", consumerKey, consumerSecret));

                    if (String.IsNullOrEmpty(userExternalId))
                        throw new Exception("User should have external id");

                    var repoClient = uow.GetRepo<IClientRepo>();
                    Client client = repoClient.GetByExternalId(userExternalId, app.PkID);

                    if (client != null)
                    {
                        TokenResponseModel tmodel = await GetAndSaveToken(userExternalId, consumerKey, consumerSecret);
                        return Ok(tmodel);
                    }

                    var userName = Crypto.DoAspNetUserName();
                    var email = Crypto.DoAspNetEmail();
                    var password = Crypto.DoAspNetUserPassword();

                    var aspnetUser = new ApplicationUser() { UserName = userName, Email = email };

                    IdentityResult result = await UserManager.CreateAsync(aspnetUser, password);

                    if (!result.Succeeded)
                    {
                        return BadRequest();
                    }

                    client = new Client();
                    client.Init();

                    client.ApplicantId = app.PkID;
                    client.ExternalId = userExternalId;
                    client.AspNetId = aspnetUser.Id;
                    client.AspNetUserName = userName;
                    client.AspNetPassword = password;

                    repoClient.Save(client);

                    uow.Commit();

                    TokenResponseModel model = await GetAndSaveToken(userExternalId, consumerKey, consumerSecret);
                    return Ok(model);
                }
            }
            catch (Exception ex)
            {
                LogErrorHelper.SaveError(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Login user and getting token
        /// </summary>
        /// <param name="userExternalId"></param>
        /// <param name="consumerKey"></param>
        /// <param name="consumerSecret"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("client/login")]
        public async Task<IHttpActionResult> Login(String userExternalId, String consumerKey, String consumerSecret)
        {
            try
            {
                TokenResponseModel model = await GetAndSaveToken(userExternalId, consumerKey, consumerSecret);
                return Ok(model);
            }
            catch (Exception ex)
            {
                LogErrorHelper.SaveError(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private async Task<TokenResponseModel> GetAndSaveToken(String userExternalId, String consumerKey, String consumerSecret)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repoApp = uow.GetRepo<IApplicantRepo>();
                Applicant app = repoApp.GetByKeyAndSecret(consumerKey, consumerSecret);

                if (app == null)
                    throw new Exception(String.Format("Such app is not exist. Consumer Key: {0}. Consumer Secret: {1}", consumerKey, consumerSecret));

                if (String.IsNullOrEmpty(userExternalId))
                    throw new Exception("User should have external id");

                var repoClient = uow.GetRepo<IClientRepo>();
                Client client = repoClient.GetByExternalId(userExternalId, app.PkID);

                if (client == null)
                    throw new Exception("Such user is not exist");

                #if DEBUG
                    var baseAddress = @"http://localhost:46017/";
                #else
                    var baseAddress = @"http://localhost:81/";
                #endif

                WebRequest request = WebRequest.Create(new Uri(String.Format("{0}Token", baseAddress)));
                request.Method = "POST";

                String postString = String.Format(@"username={0}&password={1}&grant_type=password", HttpUtility.HtmlEncode(client.AspNetUserName), HttpUtility.HtmlEncode(client.AspNetPassword));

                Byte[] bytes = Encoding.UTF8.GetBytes(postString);

                using (Stream requestStream = await request.GetRequestStreamAsync())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                HttpWebResponse httpResponse = (HttpWebResponse)(await request.GetResponseAsync());
                String json;

                using (Stream responseStream = httpResponse.GetResponseStream())
                {
                    json = new StreamReader(responseStream).ReadToEnd();
                }

                TokenResponseModel tokenResponse = JsonConvert.DeserializeObject<TokenResponseModel>(json);

                client.AccessToken = tokenResponse.AccessToken;

                repoClient.Save(client);
                uow.Commit();

                return tokenResponse;
            }
        }

        private ApplicationUserManager _userManager;
    }
}
