﻿using FitTracker.Api.Core;
using FitTracker.Api.Models.FitTracker;
using FitTracker.Core.Unity;
using FitTracker.Data;
using FitTracker.Data.Entities.Fitbit;
using FitTracker.Data.Entities.Jawbone;
using FitTracker.Data.Entities.Withings;
using FitTracker.Data.Entities.Runkeeper;
using FitTracker.Data.Enums.Auth;

using RepoFitbit = FitTracker.Data.Repo.Fitbit;
using RepoJawbone = FitTracker.Data.Repo.Jawbone;
using RepoWithings = FitTracker.Data.Repo.Withings;
using RepoRunkeeper = FitTracker.Data.Repo.Runkeeper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using FitTracker.Data.Entities.Xamarin;
using FitTracker.Data.Enums.Runkeeper;
using FitTracker.Data.Enums.Xamarin;
using FitTracker.Data.Repo.Auth;
using FitTracker.Data.Repo.Xamarin;

namespace FitTracker.Api.Controllers
{
    public class FitTrackerController : BaseController
    {
        /// <summary>
        /// Get data about nutrition, foods and meals
        /// </summary>
        /// <param name="startDateParam"></param>
        /// <param name="endDateParam"></param>
        /// <param name="tracker"></param>
        /// <param name="isAllOrganization"></param>
        /// <returns></returns>
        [Authorize]
        [Route("fitTracker/getNutrition")]
        public IHttpActionResult GetNutrition(DateTime? startDateParam = null, DateTime? endDateParam = null, TypeTracker tracker = TypeTracker.All, Boolean isAllOrganization = false)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                DateTime startDate = startDateParam.HasValue ? startDateParam.Value : DateTime.Now.AddDays(-10);
                DateTime endDate = endDateParam.HasValue ? endDateParam.Value : DateTime.Now;

                // FitBit
                List<NutritionModel> fromFitbit = new List<NutritionModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Fitbit)
                {
                    var repoFoodSummary = uow.GetRepo<RepoFitbit.IFoodSummaryRepo>();
                    List<FoodSummary> foodSummaries = repoFoodSummary.GetByUserId(CurrentUser.FitbitId, startDate, endDate);

                    // "summary":{"calories":752,"carbs":66.5,"fat":49,"fiber":0.5,"protein":12.5,"sodium":186,"water":0},
                    fromFitbit = foodSummaries.Select(x => new NutritionModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.Date.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        calories = x.Calories,
                        carbohydrates = x.Carbs,
                        fat = x.Fat,
                        fiber = x.Fiber,
                        protein = x.Protein,
                        sodium = x.Sodium,
                        water = x.Water,
                        meal = "Anytime",
                        source = "Fitbit"
                    })
                    .ToList();
                }

                // Withings
                List<NutritionModel> fromWithings = new List<NutritionModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Withings)
                {
                    // INFO: withings hasn't this info
                }

                // Jawbone
                List<NutritionModel> fromJawbone = new List<NutritionModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Jawbone)
                {
                    var repoMeals = uow.GetRepo<RepoJawbone.IMealsRepo>();
                    List<Meals> meals = repoMeals.GetByUserId(CurrentUser.JawboneId, startDate, endDate);

                    // "calories": 530.0, "carbohydrate": 64.75, "fat": 23.15, "fiber": 0,"protein": 16.77, "sodium": 504.0, "num_water": 1,
                    fromJawbone = meals.Select(x => new NutritionModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.Date.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        calories = x.Calories,
                        carbohydrates = x.Carbohydrate,
                        fat = x.Fat,
                        fiber = x.Fiber,
                        protein = x.Protein,
                        sodium = x.Sodium,
                        water = x.NumWater,
                        meal = x.Title,
                        source = "Jawbone"
                    })
                    .ToList();
                }

                List<NutritionModel> fromRunkeeper = new List<NutritionModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Runkeeper)
                {
                    var repoNutrition = uow.GetRepo<RepoRunkeeper.INutritionRepo>();
                    var nutrition = repoNutrition.GetByUserId(CurrentUser.RunkeeperId, startDate, endDate)
                        .Select(x => new
                        {
                            x.Timestamp
                        }).GroupBy(x => new {x.Timestamp});

                    List<Nutrition> nutritionAll = repoNutrition.GetByUserId(CurrentUser.RunkeeperId, startDate, endDate);

                    // "calories": 530.0, "carbohydrate": 64.75, "fat": 23.15, "fiber": 0,"protein": 16.77, "sodium": 504.0, "num_water": 1,
                    fromRunkeeper = nutrition.Select(x =>
                    {
                        var calories = nutritionAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == NutritionType.Calories).Sum(y => y.Measurement);
                        var carbohydrates = nutritionAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == NutritionType.Carbohydrates).Sum(y => y.Measurement);
                        var fat = nutritionAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == NutritionType.Fat).Sum(y => y.Measurement);
                        var fiber = nutritionAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == NutritionType.Fiber).Sum(y => y.Measurement);
                        var protein = nutritionAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == NutritionType.Protein).Sum(y => y.Measurement);
                        var sodium = nutritionAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == NutritionType.Sodium).Sum(y => y.Measurement);
                        var water = nutritionAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == NutritionType.Water).Sum(y => y.Measurement);

                        return new NutritionModel
                        {
                            _userId = CurrentUser.ExternalId,
                            timestamp = x.Key.Timestamp.ToString("yyyy-MM-dd"),
                            utc_offset = "",
                            calories = (decimal)calories,
                            carbohydrates = (decimal)carbohydrates,
                            fat = (decimal)fat,
                            fiber = (decimal)fiber,
                            protein = (decimal)protein,
                            sodium = (decimal)sodium,
                            water = (decimal)water,
                            meal = "Anytime",
                            source = "Runkeeper"
                        };
                    }).ToList();
                }   

                List<NutritionModel> result = new List<NutritionModel>();
                result.AddRange(fromFitbit);
                result.AddRange(fromWithings);
                result.AddRange(fromJawbone);
                result.AddRange(fromRunkeeper);

                return Ok(result);
            }
        }

        /// <summary>
        /// Get info about weight of user
        /// </summary>
        /// <param name="startDateParam"></param>
        /// <param name="endDateParam"></param>
        /// <param name="tracker"></param>
        /// <param name="isAllOrganization"></param>
        /// <returns></returns>
        [Authorize]
        [Route("fitTracker/getWeight")]
        public IHttpActionResult GetWeight(DateTime? startDateParam = null, DateTime? endDateParam = null, TypeTracker tracker = TypeTracker.All, Boolean isAllOrganization = false)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                DateTime startDate = startDateParam.HasValue ? startDateParam.Value : DateTime.Now.AddDays(-10);
                DateTime endDate = endDateParam.HasValue ? endDateParam.Value : DateTime.Now;

                // FitBit
                List<WeightModel> fromFitbit = new List<WeightModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Fitbit)
                {
                    var repoWeightLog = uow.GetRepo<RepoFitbit.IWeightLogRepo>();
                    List<WeightLog> weightLogs = repoWeightLog.GetByUserId(CurrentUser.FitbitId, startDate, endDate);

                    // "weight":73

                    fromFitbit = weightLogs.Select(x => new WeightModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.Date.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        weight = x.Weight,
                        height = 0,
                        free_mass = 0,
                        fat_percent = 0,
                        mass_weight = 0,
                        bmi = x.Bmi,
                        source = "Fitbit"
                    })
                    .ToList();
                }

                // Withings
                List<WeightModel> fromWithings = new List<WeightModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Withings)
                {
                    var repoBodyMeasure = uow.GetRepo<RepoWithings.IBodyMeasureRepo>();
                    var repoBodyMeasureItem = uow.GetRepo<RepoWithings.IBodyMeasureItemRepo>();

                    List<BodyMeasure> bodyMeasures = repoBodyMeasure.GetByUserId(CurrentUser.WithingsId, startDate, endDate);
                    List<BodyMeasureItem> bodyMeasureItems = repoBodyMeasureItem.GetByMeasureIds(bodyMeasures.Select(x => x.PkID).ToList());

                    // "value": 79300, "type": 1, "unit": -3

                    fromWithings = bodyMeasures.Select(x => new WeightModel()
                        {
                            _userId = CurrentUser.ExternalId,
                            timestamp = x.Date.ToString("yyyy-MM-dd"),
                            utc_offset = "",
                            weight = bodyMeasureItems
                                        .Where(t => t.BodyMeasureId == x.PkID && t.Type == 1)
                                        .Select(t => (Decimal)(t.Value * Math.Pow(10, t.Unit)))
                                        .FirstOrDefault(),
                            height = bodyMeasureItems
                                        .Where(t => t.BodyMeasureId == x.PkID && t.Type == 4)
                                        .Select(t => (Decimal)(t.Value * Math.Pow(10, t.Unit)))
                                        .FirstOrDefault(),
                            free_mass = bodyMeasureItems
                                        .Where(t => t.BodyMeasureId == x.PkID && t.Type == 5)
                                        .Select(t => (Decimal)(t.Value * Math.Pow(10, t.Unit)))
                                        .FirstOrDefault(),
                            fat_percent = bodyMeasureItems
                                        .Where(t => t.BodyMeasureId == x.PkID && t.Type == 6)
                                        .Select(t => (Decimal)(t.Value * Math.Pow(10, t.Unit)))
                                        .FirstOrDefault(),
                            mass_weight = bodyMeasureItems
                                        .Where(t => t.BodyMeasureId == x.PkID && t.Type == 8)
                                        .Select(t => (Decimal)(t.Value * Math.Pow(10, t.Unit)))
                                        .FirstOrDefault(),
                            bmi = 0,
                            source = "Withings"
                        })
                        .ToList();
                }

                // Jawbone
                List<WeightModel> fromJawbone = new List<WeightModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Jawbone)
                {
                    var repoBodyEvent = uow.GetRepo<RepoJawbone.IBodyEventRepo>();
                    List<BodyEvent> bodyEvents = repoBodyEvent.GetByUserId(CurrentUser.JawboneId, startDate, endDate);

                    //"weight": 75,

                    fromJawbone = bodyEvents.Select(x => new WeightModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.Date.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        weight = x.Weight,
                        height = 0,
                        free_mass = 0,
                        fat_percent = 0,
                        mass_weight = 0,
                        bmi = x.BMI,
                        source = "Jawbone"
                    })
                    .ToList();
                }

                List<WeightModel> fromRunkeeper = new List<WeightModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Runkeeper)
                {
                    var repoWeight = uow.GetRepo<RepoRunkeeper.IWeightRepo>();
                    var weight = repoWeight.GetByUserId(CurrentUser.RunkeeperId, startDate, endDate)
                        .Select(x => new
                        {
                            x.Timestamp
                        })
                        .GroupBy(x => new { x.Timestamp });

                    List<Weight> weightAll = repoWeight.GetByUserId(CurrentUser.RunkeeperId, startDate, endDate);

                    // "calories": 530.0, "carbohydrate": 64.75, "fat": 23.15, "fiber": 0,"protein": 16.77, "sodium": 504.0, "num_water": 1,
                    fromRunkeeper = weight.Select(x =>
                    {
                        var bmi = weightAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == WeightType.BMI).Select(y => y.Measurement).FirstOrDefault();
                        var fatPercent = weightAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == WeightType.FatPercent).Select(y => y.Measurement).FirstOrDefault();
                        var freeMass = weightAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == WeightType.FreeMass).Select(y => y.Measurement).FirstOrDefault();
                        var massWeight = weightAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == WeightType.MassWeight).Select(y => y.Measurement).FirstOrDefault();
                        var weightRow = weightAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == WeightType.Weight).Select(y => y.Measurement).FirstOrDefault();

                        return new WeightModel
                        {
                            _userId = CurrentUser.ExternalId,
                            timestamp = x.Key.Timestamp.ToString("yyyy-MM-dd"),
                            utc_offset = "",
                            weight = (weightRow != null) ? (decimal)weightRow : 0,
                            height = 0,
                            free_mass = (freeMass != null) ? (decimal)freeMass : 0,
                            fat_percent = (fatPercent != null) ? (decimal)fatPercent : 0,
                            mass_weight = (massWeight != null) ? (decimal)massWeight : 0,
                            bmi = (bmi != null) ? (decimal)bmi : 0,
                            source = "Runkeeper"
                        };
                    }).ToList();
                }   

                List<WeightModel> result = new List<WeightModel>();
                result.AddRange(fromFitbit);
                result.AddRange(fromWithings);
                result.AddRange(fromJawbone);
                result.AddRange(fromRunkeeper);

                return Ok(result);
            }
        }

        /// <summary>
        /// Get info about routine of user
        /// </summary>
        /// <param name="startDateParam"></param>
        /// <param name="endDateParam"></param>
        /// <param name="tracker"></param>
        /// <param name="isAllOrganization"></param>
        /// <returns></returns>
        [Authorize]
        [Route("fitTracker/getRoutine")]
        public IHttpActionResult GetRoutine(DateTime? startDateParam = null, DateTime? endDateParam = null, TypeTracker tracker = TypeTracker.All, Boolean isAllOrganization = false)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                DateTime startDate = startDateParam.HasValue ? startDateParam.Value : DateTime.Now.AddDays(-10);
                DateTime endDate = endDateParam.HasValue ? endDateParam.Value : DateTime.Now;

                // FitBit
                List<RoutineModel> fromFitbit = new List<RoutineModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Fitbit)
                {
                    var repoActivityDistance = uow.GetRepo<RepoFitbit.IActivityDistanceRepo>();
                    var repoActivitySummary = uow.GetRepo<RepoFitbit.IActivitySummaryRepo>();
                    List<ActivityDistance> activityDistances = repoActivityDistance.GetByUserId(CurrentUser.FitbitId, startDate, endDate);
                    List<ActivitySummary> activitySummaries = repoActivitySummary.GetByUserId(CurrentUser.FitbitId, startDate, endDate);

                    // "steps":0, "distance":8.05, "floors":16, "elevation":48.77, "activityCalories":230,

                    fromFitbit = activitySummaries.Select(x => new RoutineModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.Date.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        steps = x.Steps,
                        distance = activityDistances.Where(t => t.ActivitySummaryId == x.PkID).Sum(t => t.Distance) * 1000,
                        floors = x.Floors,
                        elevation = x.Elevation,
                        calories_burned = x.ActivityCalories,
                        source = "Fitbit"
                    })
                    .ToList();
                }

                // Withings
                List<RoutineModel> fromWithings = new List<RoutineModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Withings)
                {
                    var repoActivityMeasure = uow.GetRepo<RepoWithings.IActivityMeasureRepo>();
                    List<ActivityMeasure> activityMeasure = repoActivityMeasure.GetByUserId(CurrentUser.WithingsId, startDate, endDate);

                    // "steps": 10233, "distance": 7439.44, "elevation": 808.24, "calories": 530.79,

                    fromWithings = activityMeasure.Select(x => new RoutineModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.Date.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        steps = x.Steps,
                        distance = x.Distance,
                        floors = 0,
                        elevation = x.Elevation,
                        calories_burned = x.Calories,
                        source = "Withings"
                    })
                    .ToList();
                }

                // Jawbone
                List<RoutineModel> fromJawbone = new List<RoutineModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Jawbone)
                {
                    var repoMoves = uow.GetRepo<RepoJawbone.IMovesRepo>();
                    List<Moves> moves = repoMoves.GetByUserId(CurrentUser.JawboneId, startDate, endDate);

                    // "steps": 16804, "distance": 14745, "calories": 1760.30480012,

                    fromJawbone = moves.Select(x => new RoutineModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.Date.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        steps = x.Steps,
                        distance = x.Distance,
                        floors = 0,
                        elevation = 0,
                        calories_burned = x.Calories,
                        source = "Jawbone"
                    })
                    .ToList();
                }

                //Runkeeper
                List<RoutineModel> fromRunkeeper = new List<RoutineModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Runkeeper)
                {
                    var repoFitnessActivities = uow.GetRepo<RepoRunkeeper.IFitnessActivitiesRepo>();
                    List<FitnessActivities> moves = repoFitnessActivities.GetByUserId(CurrentUser.RunkeeperId, startDate, endDate);

                    // "steps": 16804, "distance": 14745, "calories": 1760.30480012,

                    fromRunkeeper = moves.Select(x => new RoutineModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.StartTime.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        steps = 0,
                        distance = x.TotalDistance,
                        floors = 0,
                        elevation = x.Climb,
                        calories_burned = x.TotalCalories,
                        source = "Runkeeper"
                    })
                    .ToList();
                }

                List<RoutineModel> result = new List<RoutineModel>();
                result.AddRange(fromFitbit);
                result.AddRange(fromWithings);
                result.AddRange(fromJawbone);
                result.AddRange(fromRunkeeper);

                return Ok(result);
            }
        }

        /// <summary>
        ///  Get info about fitness of user
        /// </summary>
        /// <param name="startDateParam"></param>
        /// <param name="endDateParam"></param>
        /// <param name="tracker"></param>
        /// <param name="isAllOrganization"></param>
        /// <returns></returns>
        [Authorize]
        [Route("fitTracker/getFitness")]
        public IHttpActionResult GetFitness(DateTime? startDateParam = null, DateTime? endDateParam = null, TypeTracker tracker = TypeTracker.All, Boolean isAllOrganization = false)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                DateTime startDate = startDateParam.HasValue ? startDateParam.Value : DateTime.Now.AddDays(-10);
                DateTime endDate = endDateParam.HasValue ? endDateParam.Value : DateTime.Now;

                // FitBit
                List<FitnessModel> fromFitbit = new List<FitnessModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Fitbit)
                {
                    var repoActivityLog = uow.GetRepo<RepoFitbit.IActivityLogRepo>();
                    List<ActivityLog> activityLogs = repoActivityLog.GetByUserId(CurrentUser.FitbitId, startDate, endDate);

                    fromFitbit = activityLogs.Select(x => new FitnessModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.Date.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        type = x.Name,
                        intensity = "",
                        start_time = x.StartTime,
                        distance = x.Distance * 1000,
                        duration = x.Duration,
                        calories = x.Calories,
                        source = "Fitbit"
                    })
                    .ToList();
                }

                // Withings
                List<FitnessModel> fromWithings = new List<FitnessModel>();
                
                // Jawbone
                List<FitnessModel> fromJawbone = new List<FitnessModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Jawbone)
                {
                    var repoWorkouts = uow.GetRepo<RepoJawbone.IWorkoutsRepo>();
                    List<Workouts> workouts = repoWorkouts.GetByUserId(CurrentUser.JawboneId, startDate, endDate);

                    fromJawbone = workouts.Select(x => new FitnessModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.Date.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        type = x.SubTypeValue,
                        intensity = x.IntensityValue,
                        start_time = "",
                        distance = x.Meters,
                        duration = x.Time,
                        calories = x.Calories,
                        source = "Jawbone" 
                    })
                    .ToList();
                }

                // Runkeeper
                List<FitnessModel> fromRunkeeper = new List<FitnessModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Runkeeper)
                {
                    var repoFitnessActivities = uow.GetRepo<RepoRunkeeper.IFitnessActivitiesRepo>();
                    List<FitnessActivities> workouts = repoFitnessActivities.GetByUserId(CurrentUser.RunkeeperId, startDate, endDate);

                    fromRunkeeper = workouts.Select(x => new FitnessModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.StartTime.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        type = x.Type,
                        intensity = "",
                        start_time = x.StartTime.ToString("yyyy-MM-dd"),
                        distance = x.TotalDistance,
                        duration = x.Duration,
                        calories = x.TotalCalories,
                        source = "Runkeeper"
                    })
                    .ToList();
                }

                List<FitnessModel> result = new List<FitnessModel>();
                result.AddRange(fromFitbit);
                result.AddRange(fromWithings);
                result.AddRange(fromJawbone);
                result.AddRange(fromRunkeeper);

                return Ok(result);
            }
        }

        /// <summary>
        /// Get info about sleep of user
        /// </summary>
        /// <param name="startDateParam"></param>
        /// <param name="endDateParam"></param>
        /// <param name="tracker"></param>
        /// <param name="isAllOrganization"></param>
        /// <returns></returns>
        [Authorize]
        [Route("fitTracker/getSleep")]
        public IHttpActionResult GetSleep(DateTime? startDateParam = null, DateTime? endDateParam = null, TypeTracker tracker = TypeTracker.All, Boolean isAllOrganization = false)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                DateTime startDate = startDateParam.HasValue ? startDateParam.Value : DateTime.Now.AddDays(-10);
                DateTime endDate = endDateParam.HasValue ? endDateParam.Value : DateTime.Now;

                // FitBit
                List<SleepModel> fromFitbit = new List<SleepModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Fitbit)
                {
                    var repoSleepLog = uow.GetRepo<RepoFitbit.ISleepLogRepo>();
                    var repoSleepSummary = uow.GetRepo<RepoFitbit.ISleepSummaryRepo>();
                    List<SleepLog> sleepLogs = repoSleepLog.GetByUserId(CurrentUser.FitbitId, startDate, endDate);
                    List<SleepSummaryF> sleepSummaries = repoSleepSummary.GetByUserId(CurrentUser.FitbitId, startDate, endDate);

                    fromFitbit = sleepLogs.Select(x => new SleepModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.StartTime.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        awake = x.MinutesAwake,
                        deep = 0,
                        light = 0,
                        rem = 0,
                        times_woken = x.AwakeningsCount,
                        total_sleep = x.MinutesAwake + 0 + 0 + 0,
                        source = "Fitbit"
                    })
                    .ToList();
                }

                // Withings
                List<SleepModel> fromWithings = new List<SleepModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Withings)
                {
                    var repoSleepMeasure = uow.GetRepo<RepoWithings.ISleepMeasureRepo>();
                    var repoSleepSummary = uow.GetRepo<RepoWithings.ISleepSummaryRepo>();

                    List<SleepMeasure> sleepMeasure = repoSleepMeasure.GetByUserId(CurrentUser.WithingsId, startDate, endDate);
                    List<SleepSummaryW> sleepSummary = repoSleepSummary.GetByUserId(CurrentUser.WithingsId, startDate, endDate);

                    fromWithings = sleepSummary.Select(x => new SleepModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.Date.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        awake = x.WakeUpDuration,
                        deep = x.DeepSleepDuration,
                        light = x.LightSleepDuration,
                        rem = x.RemSleepDuration,
                        times_woken = x.WakeupCount,
                        total_sleep = x.WakeUpDuration + x.DeepSleepDuration + x.LightSleepDuration + x.RemSleepDuration,
                        source = "Withings"
                    })
                    .ToList();
                }

                // Jawbone
                List<SleepModel> fromJawbone = new List<SleepModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Jawbone)
                {
                    var repoSleeps = uow.GetRepo<RepoJawbone.ISleepsRepo>();
                    List<Sleeps> sleeps = repoSleeps.GetByUserId(CurrentUser.JawboneId, startDate, endDate);

                    fromJawbone = sleeps.Select(x => new SleepModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.Date.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        awake = x.Awake,
                        deep = x.Sound,
                        light = x.Light,
                        rem = x.Rem,
                        times_woken = x.Awakenings,
                        total_sleep = x.Awake + x.Sound + x.Light + x.Rem,
                        source = "Jawbone"
                    })
                    .ToList();
                }

                // Runkeeper
                List<SleepModel> fromRunkeeper = new List<SleepModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Runkeeper)
                {
                    var repoSleep = uow.GetRepo<RepoRunkeeper.ISleepRepo>();
                    var sleep = repoSleep.GetByUserId(CurrentUser.RunkeeperId, startDate, endDate)
                        .Select(x => new
                        {
                            x.Timestamp
                        })
                        .GroupBy(x => new { x.Timestamp });

                    List<Sleep> sleepAll = repoSleep.GetByUserId(CurrentUser.RunkeeperId, startDate, endDate);

                    // "calories": 530.0, "carbohydrate": 64.75, "fat": 23.15, "fiber": 0,"protein": 16.77, "sodium": 504.0, "num_water": 1,
                    fromRunkeeper = sleep.Select(x =>
                    {
                        var awake = sleepAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == SleepType.Awake).Sum(y => (Int32)y.Measurement);
                        var deep = sleepAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == SleepType.Deep).Sum(y => (Int32)y.Measurement);
                        var light = sleepAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == SleepType.Light).Sum(y => (Int32)y.Measurement);
                        var rem = sleepAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == SleepType.REM).Sum(y => (Int32)y.Measurement);
                        var times_woken = sleepAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == SleepType.TimesWoken).Sum(y => (Int32)y.Measurement);
                        var total_sleep = sleepAll.Where(y => y.Timestamp == x.Key.Timestamp && y.Type == SleepType.TotalSleep).Sum(y => (Int32)y.Measurement);

                        return new SleepModel
                        {
                            _userId = CurrentUser.ExternalId,
                            timestamp = x.Key.Timestamp.ToString("yyyy-MM-dd"),
                            utc_offset = "",
                            awake = awake,
                            deep = deep,
                            light = light,
                            rem = rem,
                            times_woken = (Int32)times_woken,
                            total_sleep = (total_sleep != 0) ? total_sleep : awake + deep + light + rem,
                            source = "Runkeeper"
                        };
                    }).ToList();
                }

                List<SleepModel> result = new List<SleepModel>();
                result.AddRange(fromFitbit);
                result.AddRange(fromWithings);
                result.AddRange(fromJawbone);
                result.AddRange(fromRunkeeper);

                return Ok(result);
            }
        }

        /// <summary>
        /// Get info about pulse and pressure of user
        /// </summary>
        /// <param name="startDateParam"></param>
        /// <param name="endDateParam"></param>
        /// <param name="tracker"></param>
        /// <param name="isAllOrganization"></param>
        /// <returns></returns>
        [Route("fitTracker/getPulse")]
        public IHttpActionResult GetPulse(DateTime? startDateParam = null, DateTime? endDateParam = null, TypeTracker tracker = TypeTracker.All, Boolean isAllOrganization = false)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                DateTime startDate = startDateParam.HasValue ? startDateParam.Value : DateTime.Now.AddDays(-10);
                DateTime endDate = endDateParam.HasValue ? endDateParam.Value : DateTime.Now;

                // FitBit
                List<PulseModel> fromFitbit = new List<PulseModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Fitbit)
                {
                }

                // Runkeeper
                List<PulseModel> fromRunkeeper = new List<PulseModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Runkeeper)
                {
                }

                // Withings
                List<PulseModel> fromWithings = new List<PulseModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Withings)
                {
                    var repoBodyMeasure = uow.GetRepo<RepoWithings.IBodyMeasureRepo>();
                    var repoBodyMeasureItem = uow.GetRepo<RepoWithings.IBodyMeasureItemRepo>();

                    List<BodyMeasure> bodyMeasures = repoBodyMeasure.GetByUserId(CurrentUser.WithingsId, startDate, endDate);
                    List<BodyMeasureItem> bodyMeasureItems = repoBodyMeasureItem.GetByMeasureIds(bodyMeasures.Select(x => x.PkID).ToList());

                    // "value": 79300, "type": 1, "unit": -3

                    fromWithings = bodyMeasures.Select(x => new PulseModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.Date.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        diastolic_blood_pressure = bodyMeasureItems
                                    .Where(t => t.BodyMeasureId == x.PkID && t.Type == 9)
                                    .Select(t => (Decimal)(t.Value * Math.Pow(10, t.Unit)))
                                    .FirstOrDefault(),
                        systolic_blood_pressure = bodyMeasureItems
                                    .Where(t => t.BodyMeasureId == x.PkID && t.Type == 10)
                                    .Select(t => (Decimal)(t.Value * Math.Pow(10, t.Unit)))
                                    .FirstOrDefault(),
                        heart_pulse = bodyMeasureItems
                                    .Where(t => t.BodyMeasureId == x.PkID && t.Type == 11)
                                    .Select(t => (Decimal)(t.Value * Math.Pow(10, t.Unit)))
                                    .FirstOrDefault(),
                        source = "Withings"
                    })
                    .ToList();
                }

                // Jawbone
                List<PulseModel> fromJawbone = new List<PulseModel>();
                if (tracker == TypeTracker.All || tracker == TypeTracker.Jawbone)
                {
                    var repoHeartRate = uow.GetRepo<RepoJawbone.IHeartRateRepo>();

                    List<HeartRate> heartRates = repoHeartRate.GetByUserId(CurrentUser.WithingsId, startDate, endDate);

                    // "value": 79300, "type": 1, "unit": -3

                    fromJawbone = heartRates.Select(x => new PulseModel()
                    {
                        _userId = CurrentUser.ExternalId,
                        timestamp = x.Date.ToString("yyyy-MM-dd"),
                        utc_offset = "",
                        diastolic_blood_pressure = 0,
                        systolic_blood_pressure = 0,
                        heart_pulse = x.RestingHeartrate,
                        source = "Jawbone"
                    })
                    .ToList();
                }

                List<PulseModel> result = new List<PulseModel>();
                result.AddRange(fromFitbit);
                result.AddRange(fromWithings);
                result.AddRange(fromJawbone);
                result.AddRange(fromRunkeeper);

                return Ok(result);
            }
        }

        /// <summary>
        /// Autg From Healhkit
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("fitTracker/auth")]
        public IHttpActionResult Auth(String login, String password)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var authRepo = uow.GetRepo<IDbUserRepo>();
                var auth = authRepo.GetByLoginAndPassword(login, password);
                return Ok(auth);
            }
        }

        [HttpGet]
        [Route("fitTracker/HealhKitBaseData")]
        public void SaveBaseData(Int64 userId, Decimal? measurement, Int32 type, DateTime timestamp, String source = "")
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var baseDataRepo = uow.GetRepo<IBaseDataRepo>();
                BaseData baseData = new BaseData();
                baseData.Timestamp = timestamp;
                baseData.Measurement = measurement;
                baseData.Source = source;
                baseData.Type = (BaseDataType)type;
                baseData.UserId = userId;
                baseDataRepo.SaveBaseData(baseData);
            }
        }

        [HttpGet]
        [Route("fitTracker/HealhKitBodyMeasurement")]
        public void SaveBodyMeasurement(Int64 userId, Decimal? measurement, Int32 type, DateTime timestamp, String source = "")
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var bodyMeasurementRepo = uow.GetRepo<IBodyMeasurementRepo>();
                BodyMeasurement bodyMeasurement = new BodyMeasurement();
                bodyMeasurement.Timestamp = timestamp;
                bodyMeasurement.Measurement = measurement;
                bodyMeasurement.Source = source;
                bodyMeasurement.Type = (BodyMeasurementType)type;
                bodyMeasurement.UserId = userId;
                bodyMeasurementRepo.SaveBodyMeasurement(bodyMeasurement);
            }
        }

        [HttpGet]
        [Route("fitTracker/HealhKitFitnessActivity")]
        public void SaveFitnessActivity(Int64 userId, Decimal? measurement, Int32 type, DateTime timestamp, String source = "")
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var fitnessActivityRepo = uow.GetRepo<IFitnessActivityRepo>();
                FitnessActivity fitnessActivity = new FitnessActivity();
                fitnessActivity.Timestamp = timestamp;
                fitnessActivity.Measurement = measurement;
                fitnessActivity.Source = source;
                fitnessActivity.Type = (FitnessActivityType)type;
                fitnessActivity.UserId = userId;
                fitnessActivityRepo.SaveFitnessActivity(fitnessActivity);
            }
        }

        [HttpGet]
        [Route("fitTracker/HealhKitSleep")]
        public void SaveSleep(Int64 userId, Decimal? measurement, Int32 type, DateTime timestamp, String source = "")
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var sleepRepo = uow.GetRepo<ISleepRepo>();
                HealhKitSleep sleep = new HealhKitSleep();
                sleep.Timestamp = timestamp;
                sleep.Measurement = measurement;
                sleep.Source = source;
                sleep.Type = (HealhKitSleepType)type;
                sleep.UserId = userId;
                sleepRepo.SaveSleep(sleep);
            }
        }
    }
}