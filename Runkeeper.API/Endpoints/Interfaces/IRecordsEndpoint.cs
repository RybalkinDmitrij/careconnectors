﻿using System;
using System.Collections.Generic;
using Runkeeper.API.Models;

namespace Runkeeper.API.Endpoints.Interfaces
{
	public interface IRecordsEndpoint
	{
		List<RecordsFeedItemModel> GetRecordsFeed();
		void GetRecordsFeedAsync(Action<List<RecordsFeedItemModel>> success, Action<HealthGraphException> failure);
	}
}