﻿using System;
using Runkeeper.API.Models;

namespace Runkeeper.API.Endpoints.Interfaces
{
	public interface IUsersEndpoint
	{
		UsersModel GetUser();
		void GetUserAsync(Action<UsersModel> success, Action<HealthGraphException> failure);
	}
}