﻿using System;
using Runkeeper.API.Models;

namespace Runkeeper.API.Endpoints.Interfaces
{
	public interface ISettingsEndpoint
	{
		SettingsModel GetSettings();
		void GetSettingsAsync(Action<SettingsModel> success, Action<HealthGraphException> failure);
		SettingsModel UpdateSettings(SettingsModel settingsToUpdate);

		void UpdateSettingsAsync(Action<SettingsModel> success, Action<HealthGraphException> failure,
			SettingsModel settingsToUpdate);
	}
}