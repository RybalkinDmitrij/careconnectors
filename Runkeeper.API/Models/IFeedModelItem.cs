﻿using System;

namespace Runkeeper.API.Models
{
	public interface IFeedModelItem
	{
		string Uri { get; }
	}
}