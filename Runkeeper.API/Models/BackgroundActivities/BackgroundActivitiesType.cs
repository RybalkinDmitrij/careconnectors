﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runkeeper.API.Models
{
	public enum BackgroundActivitiesType
	{
		CaloriesBurned,
		Steps
	}
}