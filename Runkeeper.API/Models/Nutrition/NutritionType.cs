﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runkeeper.API.Models
{
	public enum NutritionType
	{
		Calories,
		Carbohydrates,
		Fat,
		Fiber,
		Protein,
		Sodium,
		Water
	}
}