﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FitTracker.Web2.Core.Attributes
{
    public class FitTrackerAuthorizeAttribute: AuthorizeAttribute
    {
        public FitTrackerAuthorizeAttribute()
        {
        }
        
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!base.AuthorizeCore(httpContext)) return false;
            
            var user = httpContext.Session.GetUser();
            if (user == null) return false;

            return true;
        }
    }
}