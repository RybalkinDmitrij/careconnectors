﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace FitTracker.Web2.Core.FitTrackers
{
    public class UserModel
    {
        public string SessionToken { get; set; }
        public string ObjectId { get; set; }
        public string participantId { get; set; }
        public string externalId { get; set; }
        public Dictionary<string, string> LastSync { get; set; }
        public string Username { get; set; }
        [Required(ErrorMessage = "First Name is required.")]
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TimeZone { get; set; }
        public string Gender { get; set; }
        public string WeightLbs { get; set; }
        public string HeightInches { get; set; }
        public Dictionary<string, string> Dob { get; set; }
        public Dictionary<string, string> ProfilePhoto { get; set; }
        public Dictionary<string, string> CompanyId { get; set; }
        public string PrimaryFitTracker { get; set; }

        /// <summary>
        /// Creates pointer to user object
        /// </summary>
        public Dictionary<string, string> UserIdPointer
        {
            get
            {
                Dictionary<string, string> userIdParams = new Dictionary<string, string>();

                userIdParams.Add("__type", "Pointer");
                userIdParams.Add("className", "_User");
                userIdParams.Add("objectId", ObjectId);

                return userIdParams;
            }
        }
        
        /// <summary>
        /// Converts DOB ISO date representation used by Parse to regular DateTime
        /// </summary>
        public string DobDate
        {
            get
            {
                string returnDate = String.Empty;

                if (Dob != null)
                {
                    string isoDate = Dob["iso"];
                    returnDate = DateTime.Parse(isoDate, null, DateTimeStyles.RoundtripKind).ToString("d");
                }

                return returnDate;
            }
            set
            {
                if (value != null)
                {
                    DateTime lastSync = DateTime.Parse(value);

                    var dateParams = new Dictionary<string, string>();
                    var isoDate = lastSync.ToString("yyyy-MM-ddTHH:mm:ssZ");

                    dateParams.Add("__type", "Date");
                    dateParams.Add("iso", isoDate);

                    Dob = dateParams;
                }
            }
        }

        /// <summary>
        /// Converts weight string into integer
        /// </summary>
        public int? WeightLbsInt
        {
            get
            {
                int? returnWeight = 0;

                if (WeightLbs != null)
                {
                    returnWeight = int.Parse(WeightLbs);
                }

                return returnWeight;
            }
        }

        /// <summary>
        /// Converts height string into feet value
        /// </summary>
        public int? HeightFeetInt
        {
            get
            {
                int? returnFeet = 0;

                if (HeightInches != null)
                {
                    returnFeet = int.Parse(HeightInches) / 12;
                }

                return returnFeet;
            }
        }

        /// <summary>
        /// Converts height string into remaining inches value
        /// </summary>
        public int? HeightInchesInt
        {
            get
            {
                int? returnInches = 0;

                if (HeightInches != null)
                {
                    returnInches = int.Parse(HeightInches) % 12;
                }

                return returnInches;
            }
        }


        /// <summary>
        /// Check if user is in a specified role
        /// </summary>
        /// <param name="roleName">Role to check</param>
        /// <returns></returns>
        public bool IsInRole(string roleName)
        {
            bool isInRole = false;

            return isInRole;
        }
    }
}