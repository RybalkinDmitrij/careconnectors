﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Web2.Core.Models
{
    public class SessionUserModel
    {
        public Int64 UserId { get; set; }
        
        public String Login { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String FullName { get; set; }
    }
}