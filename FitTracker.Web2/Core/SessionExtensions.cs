﻿using System;
using System.Web;
using System.Web.Security;

using FitTracker.Web2.Core.Models;
using FitTracker.Core.Unity;
using FitTracker.Data;
using FitTracker.Data.Repo.Auth;

namespace FitTracker.Web2.Core
{
    public static class SessionExtensions
    {
        public static SessionUserModel GetUser(this HttpSessionStateWrapper session)
        {
            return GetUser(session);
        }

        public static SessionUserModel GetUser(this HttpSessionStateBase session)
        {
            var user = (SessionUserModel)session["user"];
            if (user == null || user.Login != HttpContext.Current.User.Identity.Name)
            {
                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IDbUserRepo>();
                    var dbUser = repo.GetByLogin(HttpContext.Current.User.Identity.Name);
                    if (dbUser == null)
                    {
                        FormsAuthentication.SignOut();
                        return null;
                    }
                    user = new SessionUserModel
                    {
                        UserId = dbUser.PkID,
                        Login = dbUser.Login,
                        FirstName = dbUser.FirstName,
                        LastName = dbUser.LastName,
                        FullName = dbUser.LastName + ", " + dbUser.FirstName
                    };

                    session["user"] = user;
                }
            }

            return user;
        }
    }
}