﻿using FitTracker.Core.Unity;
using FitTracker.Web2.Core.Models;
using System;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FitTracker.Web2.Core
{
    public class BaseController : Controller
    {
        public UnityManager UnityManager
        {
            get
            {
                return UnityManager.Instance;
            }
        }

        public Int64 CallbackUserId
        {
            get
            {
                if (Session["CallbackUserId"] != null)
                {
                    return Convert.ToInt64(Session["CallbackUserId"]);
                }

                throw new Exception("Callback user Id is not exist");
            }

            set
            {
                Session["CallbackUserId"] = value;
            }
        }

        public String CallbackUrlRemote
        {
            get
            {
                if (Session["CallbackUrl"] != null)
                {
                    return Session["CallbackUrl"].ToString();
                }

                return String.Empty;
            }

            set
            {
                Session["CallbackUrl"] = value;
            }
        }

        public new SessionUserModel AuthUser
        {
            get
            {
                return this.HttpContext.Session.GetUser();
            }
        }
        
        protected JsonResult JsonRes()
        {
            return JsonRes(Status.OK, "OK", null, JsonRequestBehavior.DenyGet);
        }

        protected JsonResult JsonRes(Object obj)
        {
            return JsonRes(Status.OK, "OK", obj, JsonRequestBehavior.DenyGet);
        }

        protected JsonResult JsonRes(Status status, String message)
        {
            return JsonRes(status, message, null, JsonRequestBehavior.DenyGet);
        }

        protected JsonResult JsonRes(Status status, String message, Object obj)
        {
            return JsonRes(status, message, obj, JsonRequestBehavior.DenyGet);
        }

        protected JsonResult JsonRes(Status status, String message, Object obj, JsonRequestBehavior behavior)
        {
            return JsonRes(status, message, new JavaScriptSerializer().Serialize(obj), JsonRequestBehavior.DenyGet);
        }

        protected JsonResult JsonRes(Status status, String message, String obj, JsonRequestBehavior behavior)
        {
            return base.Json(new
            {
                ErrorCode = status,
                Message = message,
                Obj = obj
            }, behavior);
        }

        protected enum Status
        {
            OK = 200,
            Error = 500,
            NoEnoughBudget = 800
        }
    }
}