﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Web2.Enums
{
    public enum TypeNotify
    {
        Nutrition = 1,
        Weight = 2,
        Routine = 3,
        Fitness = 4,
        Sleep = 5
    }
}