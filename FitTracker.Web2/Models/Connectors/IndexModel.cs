﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Web2.Models.Connectors
{
    public class IndexModel
    {
        public Int64 UserId { get; set; }

        public String CallbackUrlRemote { get; set; }

        public String AccessToken { get; set; }

        public String FitbitId { get; set; }

        public Boolean HasFitbit
        {
            get
            {
                return !String.IsNullOrEmpty(this.FitbitId);
            }
        }

        public String WithingsId { get; set; }

        public Boolean HasWithings
        {
            get
            {
                return !String.IsNullOrEmpty(this.WithingsId);
            }
        }

        public String JawboneId { get; set; }

        public Boolean HasJawbone
        {
            get
            {
                return !String.IsNullOrEmpty(this.JawboneId);
            }
        }

        public String RunkeeperId { get; set; }

        public Boolean HasRunkeeper
        {
            get
            {
                return !String.IsNullOrEmpty(this.RunkeeperId);
            }
        }

        public String StravaId { get; set; }

        public Boolean HasStrava
        {
            get
            {
                return !String.IsNullOrEmpty(this.StravaId);
            }
        }

        public String MisfitId { get; set; }

        public Boolean HasMisfit
        {
            get
            {
                return !String.IsNullOrEmpty(this.MisfitId);
            }
        }
    }
}