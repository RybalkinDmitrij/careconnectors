﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Web2.Models.FitTrackersSubscription
{
    public class FitnessTrackerModel
    {
        public string ObjectId { get; set; }
        public string UserId { get; set; }
        public string DeviceType { get; set; }
        public FitTrackerAuthSettingsModel AuthSettings { get; set; }
        public List<SubscriptionResponse> Subscriptions { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
    }

    public class SubscriptionResponse
    {
        public string CollectionType { get; set; }
        public string OwnerId { get; set; }
        public string OwnerType { get; set; }
        public string SubscriberId { get; set; }
        public string SubscriptionId { get; set; }
    }

    public class FitTrackerAuthSettingsModel
    {
        public string FitBitAuthToken { get; set; }
        public string FitBitAuthTokenSecret { get; set; }
        public string FitBitUserId { get; set; }
    }
}