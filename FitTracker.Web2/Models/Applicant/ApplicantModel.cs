﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Web2.Models.Applicant
{
    public class ApplicantModel
    {
        public Int64 AppId { get; set; }

        public String Name { get; set; }

        public String ConsumerKey { get; set; }

        public String ConsumerSecret { get; set; }

        public String NotificationUrl { get; set; }
    }
}