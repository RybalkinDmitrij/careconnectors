﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Web2.Models.Applicant
{
    public class IndexModel
    {
        public Int64 UserId { get; set; }

        public List<ApplicantModel> Applicants { get; set; }
    }
}