﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FitTracker.Web2.Models.Applicant
{
    public class RegisterModel
    {
        public Int64 AppId { get; set; }

        [Required]
        public Int64 UserId { get; set; }

        [Required]
        [Display(Name = "App name")]
        public String AppName { get; set; }

        [Required]
        [Display(Name = "Notification Url")]
        public String NotificationUrl { get; set; }
    }
}