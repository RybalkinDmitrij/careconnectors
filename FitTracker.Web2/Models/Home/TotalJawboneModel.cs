﻿using FitTracker.Data.Entities.Jawbone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DJ = FitTracker.Data.Entities.Jawbone;

namespace FitTracker.Web2.Models.Home
{
    public class TotalJawboneModel
    {
        public String Message { get; set; }

        public List<DJ.BodyEvent> BodyEvents { get; set; }

        public List<DJ.Custom> Customs { get; set; }

        public List<DJ.Goals> Goals { get; set; }

        public List<DJ.HeartRate> HeartRates { get; set; }

        public List<DJ.Meals> Meals { get; set; }

        public List<DJ.Mood> Moods { get; set; }

        public List<DJ.Moves> Moves { get; set; }

        public List<DJ.MovesTick> MovesTicks { get; set; }

        public List<DJ.Settings> Settings { get; set; }

        public List<DJ.Sleeps> Sleeps { get; set; }

        public List<DJ.Timezone> Timezones { get; set; }

        public List<DJ.Workouts> Workouts { get; set; }
    }
}