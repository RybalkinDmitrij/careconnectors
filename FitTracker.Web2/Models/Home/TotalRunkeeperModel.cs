﻿using FitTracker.Data.Entities.Jawbone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RK = FitTracker.Data.Entities.Runkeeper;

namespace FitTracker.Web2.Models.Home
{
    public class TotalRunkeeperModel
    {
        public String Message { get; set; }

        public List<RK.BackgroundActivities> BackgroundActivities { get; set; }

        public List<RK.CaloriesMod> CaloriesMod { get; set; }

        public List<RK.DiabetesMeasurements> DiabetesMeasurements { get; set; }

        public List<RK.Distances> Distances { get; set; }

        public List<RK.Exercises> Exercises { get; set; }

        public List<RK.FitnessActivities> FitnessActivities { get; set; }

        public List<RK.GeneralMeasurements> GeneralMeasurements { get; set; }

        public List<RK.HeartRates> HeartRates { get; set; }

        public List<RK.Nutrition> Nutrition { get; set; }

        public List<RK.Path> Path { get; set; }

        public RK.Profile Profile { get; set; }

        public List<RK.Sets> Sets { get; set; }

        public List<RK.Sleep> Sleep { get; set; }

        public List<RK.StrengthTraining> StrengthTraining { get; set; }

        public List<RK.Weight> Weight { get; set; }
    }
}