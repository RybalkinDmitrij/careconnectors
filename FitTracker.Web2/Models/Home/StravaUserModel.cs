﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Web2.Models.Home
{
    public class StravaUserModel
    {
        public Int64 id { get; set; }

        public String resource_state { get; set; }

        public String firstname { get; set; }

        public String lastname { get; set; }

        public String city { get; set; }

        public String state { get; set; }

        public String country { get; set; }

        public String sex { get; set; }
    }
}