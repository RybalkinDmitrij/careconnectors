﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Web2.Models.Home
{
    public class ClientModel
    {
        public Int64 ApplicantId { get; set; }

        public String FitbitId { get; set; }

        public String FitbitAccessToken { get; set; }

        public String FitbitSecret { get; set; }

        public String WithingsId { get; set; }

        public String WithingsAccessToken { get; set; }

        public String WithingsSecret { get; set; }

        public String JawboneId { get; set; }

        public String JawboneAccessToken { get; set; }

        public String JawboneRefreshToken { get; set; }

        public String RunkeeperId { get; set; }

        public String RunkeeperAccessToken { get; set; }

        public String RunkeeperRefreshToken { get; set; }

        public String StravaId { get; set; }

        public String StravaAccessToken { get; set; }

        public String StravaRefreshToken { get; set; }

        public String MisfitId { get; set; }

        public String MisfitAccessToken { get; set; }

        public String MisfitRefreshToken { get; set; }

        public List<DeviceModel> Devices { get; set; }
    }
}