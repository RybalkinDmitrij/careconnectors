﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DF = FitTracker.Data.Entities.Fitbit;

namespace FitTracker.Web2.Models.Home
{
    public class TotalFitbitModel
    {
        public String Message { get; set; }

        public List<DF.ActivityLog> ActivityLogs { get; set; }

        public List<DF.ActivitySummary> ActivitySummary { get; set; }

        public List<DF.ActivityDistance> ActivityDistances { get; set; }

        public List<DF.ActivityGoals> ActivityGoals { get; set; }

        public List<DF.Body> Body { get; set; }

        public List<DF.BodyGoals> BodyGoals { get; set; }

        public List<DF.FoodLog> FoodLogs { get; set; }

        public DF.FoodSummary FoodSummary { get; set; }

        public List<DF.FoodGoals> FoodGoals { get; set; }
    }
}