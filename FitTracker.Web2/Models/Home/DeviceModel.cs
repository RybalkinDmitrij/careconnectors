﻿using FitTracker.Data.Enums.Fitbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Web2.Models.Home
{
    public class DeviceModel
    {
        public String UserId { get; set; }

        public String Battery { get; set; }

        public String Id { get; set; }

        public DateTime LastSyncTime { get; set; }

        public DeviceType Type { get; set; }

        public String DeviceVersion { get; set; }
    }
}