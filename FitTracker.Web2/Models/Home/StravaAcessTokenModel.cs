﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Web2.Models.Home
{
    public class StravaAccessTokenModel
    {
        public String access_token { get; set; }

        public String token_type { get; set; }

        public StravaUserModel athlete { get; set; }
    }
}