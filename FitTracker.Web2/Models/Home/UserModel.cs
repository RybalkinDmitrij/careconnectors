﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Web2.Models.Home
{
    public class UserModel
    {
        public Int64 Id { get; set; }

        public String LastName { get; set; }

        public String FirstName { get; set; }
    }
}