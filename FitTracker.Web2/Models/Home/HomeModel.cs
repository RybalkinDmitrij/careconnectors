﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitTracker.Web2.Models.Home
{
    public class HomeModel
    {
        public List<ClientModel> Clients { get; set; }

        public List<UserModel> Users { get; set; }
    }
}