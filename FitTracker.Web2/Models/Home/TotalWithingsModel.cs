﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DW = FitTracker.Data.Entities.Withings;

namespace FitTracker.Web2.Models.Home
{
    public class TotalWithingsModel
    {
        public String Message { get; set; }

        public List<ActivityMeasure> ActivityMeasures { get; set; }

        public List<BodyMeasure> BodyMeasures { get; set; }

        public List<IntradayActivity> IntradayActivities { get; set; }

        public List<SleepMeasure> SleepMeasures { get; set; }

        public List<SleepSummaryW> SleepSummaries { get; set; }
    }

    public class ActivityMeasure
    {
        public DateTime Date { get; set; }

        public Int32 Steps { get; set; }

        public Int32 Distance { get; set; }

        public Decimal Calories { get; set; }

        public Decimal Elevation { get; set; }

        public Int32 Soft { get; set; }

        public Int32 Moderate { get; set; }

        public Int32 Intense { get; set; }

        public String Timezone { get; set; }
    }

    public class BodyMeasure
    {
        public Int32 GrpId { get; set; }

        public Int32 Attrib { get; set; }

        public DateTime Date { get; set; }

        public Int32 Category { get; set; }

        public List<BodyMeasureItem> Measures { get; set; }
    }

    public class BodyMeasureItem
    {
        public Decimal Value { get; set; }

        public String Type { get; set; }
    }

    public class IntradayActivity
    {
        public DateTime Name { get; set; }

        public Decimal Calories { get; set; }

        public Int32 Duration { get; set; }

        public Int32 Steps { get; set; }

        public Decimal Elevation { get; set; }

        public Decimal Distance { get; set; }
    }

    public class SleepMeasure
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public Int32 State { get; set; }
    }

    public class SleepSummaryW
    {
        public Int32 Id { get; set; }

        public String Timezone { get; set; }

        public Int32 Model { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime Date { get; set; }

        public Int32 WakeUpDuration { get; set; }

        public Int32 LightSleepDuration { get; set; }

        public Int32 DeepSleepDuration { get; set; }

        public Int32 RemSleepDuration { get; set; }

        public Int32 DurationToSleep { get; set; }

        public Int32 DurationToWakeup { get; set; }

        public Int32 WakeupCount { get; set; }

        public DateTime Modified { get; set; }
    }
}