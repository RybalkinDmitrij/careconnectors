﻿using FitTracker.Data.Helpers;
using Microsoft.Owin;
using Owin;
using System;
using System.Configuration;

[assembly: OwinStartupAttribute(typeof(FitTracker.Web2.Startup))]
namespace FitTracker.Web2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            #if DEBUG

            String fitbitConsumerKey = ConfigurationManager.AppSettings["FitbitConsumerKeyDebug"];
            String fitbitConsumerSecret = ConfigurationManager.AppSettings["FitbitConsumerSecretDebug"];
            String withingsConsumerKey = ConfigurationManager.AppSettings["WithingsConsumerKeyDebug"];
            String withingsConsumerSecret = ConfigurationManager.AppSettings["WithingsConsumerSecretDebug"];
            String jawboneConsumerKey = ConfigurationManager.AppSettings["JawboneConsumerKeyDebug"];
            String jawboneConsumerSecret = ConfigurationManager.AppSettings["JawboneConsumerSecretDebug"];
            String runkeeperConsumerKey = ConfigurationManager.AppSettings["RunkeeperConsumerKeyDebug"];
            String runkeeperConsumerSecret = ConfigurationManager.AppSettings["RunkeeperConsumerSecretDebug"];
            String stravaConsumerKey = ConfigurationManager.AppSettings["StravaConsumerKeyDebug"];
            String stravaConsumerSecret = ConfigurationManager.AppSettings["StravaConsumerSecretDebug"];
            String misfitConsumerKey = ConfigurationManager.AppSettings["MisfitConsumerKeyDebug"];
            String misfitConsumerSecret = ConfigurationManager.AppSettings["MisfitConsumerSecretDebug"];

            #else

            String fitbitConsumerKey = ConfigurationManager.AppSettings["FitbitConsumerKeyRelease"];
            String fitbitConsumerSecret = ConfigurationManager.AppSettings["FitbitConsumerSecretRelease"];
            String withingsConsumerKey = ConfigurationManager.AppSettings["WithingsConsumerKeyRelease"];
            String withingsConsumerSecret = ConfigurationManager.AppSettings["WithingsConsumerSecretRelease"];
            String jawboneConsumerKey = ConfigurationManager.AppSettings["JawboneConsumerKeyRelease"];
            String jawboneConsumerSecret = ConfigurationManager.AppSettings["JawboneConsumerSecretRelease"];
            String runkeeperConsumerKey = ConfigurationManager.AppSettings["RunkeeperConsumerKeyRelease"];
            String runkeeperConsumerSecret = ConfigurationManager.AppSettings["RunkeeperConsumerSecretRelease"];
            String stravaConsumerKey = ConfigurationManager.AppSettings["StravaConsumerKeyRelease"];
            String stravaConsumerSecret = ConfigurationManager.AppSettings["StravaConsumerSecretRelease"];
            String misfitConsumerKey = ConfigurationManager.AppSettings["MisfitConsumerKeyRelease"];
            String misfitConsumerSecret = ConfigurationManager.AppSettings["MisfitConsumerSecretRelease"];

#endif

            SettingsHelper.Initialize(
                fitBitConsumerKey: fitbitConsumerKey,
                fitBitConsumerSecret: fitbitConsumerSecret,
                withingsConsumerKey: withingsConsumerKey,
                withingsConsumerSecret: withingsConsumerSecret,
                jawboneConsumerKey: jawboneConsumerKey,
                jawboneConsumerSecret: jawboneConsumerSecret,
                runkeeperConsumerKey: runkeeperConsumerKey,
                runkeeperConsumerSecret: runkeeperConsumerSecret,
                stravaConsumerKey: stravaConsumerKey,
                stravaConsumerSecret: stravaConsumerSecret,
                misfitConsumerKey: misfitConsumerKey,
                misfitConsumerSecret: misfitConsumerSecret
            );
        }
    }
}
