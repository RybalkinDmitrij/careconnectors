﻿using FitTracker.Data;
using FitTracker.Data.Entities.Auth;
using FitTracker.Data.Helpers;
using FitTracker.Data.Repo.Auth;

using System;
using System.Web.Mvc;
using FitTracker.Core.Unity;
using FitTracker.Web2.Models.Connectors;

namespace FitTracker.Web2.Controllers
{
    public class ConnectorsController : Controller
    {
        // GET: Connectors
        public ActionResult Index(String accessToken, String callbackUrl = "")
        {
            try
            {
                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repoClient = uow.GetRepo<IClientRepo>();
                    Client client = repoClient.GetByAccessToken(accessToken);

                    if (client == null)
                        throw new Exception("Such client does not exist");

                    IndexModel model = new IndexModel();

                    model.AccessToken = accessToken;

                    model.UserId = client.PkID;
                    model.CallbackUrlRemote = callbackUrl;

                    model.FitbitId = client.FitbitId;
                    model.WithingsId = client.WithingsId;
                    model.JawboneId = client.JawboneId;
                    model.RunkeeperId = client.RunkeeperId;
                    model.StravaId = client.StravaId;
                    model.MisfitId = client.MisfitId;

                    return View(model);
                }
            }
            catch (Exception ex)
            {
                LogErrorHelper.SaveError(ex.Message);

                return RedirectToAction("Error", new { message = ex.Message });
            }
        }

        public ActionResult Error(String message)
        {
            ViewBag.Message = message;
            return View();
        }
    }
}