﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using FitTracker.Web2.Core;
using FitTracker.Data;
using FitTracker.Data.Repo.Auth;
using FitTracker.Data.Entities.Auth;
using Withings.API;
using Withings.API.Models;
using FitTracker.Data.Repo.Log;
using FitTracker.Data.Helpers;

namespace FitTracker.Web2.Controllers
{
    public class WithingsController : BaseController
    {
        public ActionResult Authorize(Int64 userId, String callbackUrlRemote)
        {
            Withings.API.Authenticator authenticator = new Withings.API.Authenticator(SettingsHelper.WithingsConsumerKey,
                                                                                      SettingsHelper.WithingsConsumerSecret,
                                                                                    "https://oauth.withings.com/account/request_token",
                                                                                    "https://oauth.withings.com/account/access_token",
                                                                                    "https://oauth.withings.com/account/authorize");

            #if DEBUG
            String callbackUrl = Url.Action("Callback", "Withings").ToAbsoluteUrl();
            #else
            String callbackUrl = @"http://216.195.85.166:61080/Withings/Callback";
            #endif

            RequestToken token = authenticator.GetRequestToken(callbackUrl);
            Session.Add("WithingsRequestTokenSecret", token.Secret);

            String authUrl = authenticator.GenerateAuthUrlFromRequestToken(token, true);

            CallbackUserId = userId;
            CallbackUrlRemote = callbackUrlRemote;

            return Redirect(authUrl);
        }

        //Final step. Take this authorization information and use it in the app
        public ActionResult Callback()
        {
            RequestToken token = new RequestToken();
            token.Token = Request.Params["oauth_token"];
            token.Secret = Session["WithingsRequestTokenSecret"].ToString();
            token.Verifier = Request.Params["oauth_verifier"];

            Session["WithingsUserId"] = Request.Params["userid"];

            Withings.API.Authenticator authenticator = new Withings.API.Authenticator(SettingsHelper.WithingsConsumerKey,
                                                                                    SettingsHelper.WithingsConsumerSecret,
                                                                                    "https://oauth.withings.com/account/request_token",
                                                                                    "https://oauth.withings.com/account/access_token",
                                                                                    "https://oauth.withings.com/account/authorize");

            AuthCredential credential = authenticator.ProcessApprovedAuthCallback(token);

            Session["WithingsAuthToken"] = credential.AuthToken;
            Session["WithingsAuthTokenSecret"] = credential.AuthTokenSecret;
            
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();

                Client clientDb = repo.Get(CallbackUserId);

                clientDb.WithingsId = WithingsUserId;
                clientDb.WithingsAccessToken = credential.AuthToken;
                clientDb.WithingsSecret = credential.AuthTokenSecret;

                repo.Save(clientDb);

                uow.Commit();

                // here, we add subscription
                WithingsClient client = FitTrackerClients.GetWithingsClient(WithingsUserId);

                List<NotificationProfile> notifications = client.GetNotifications(WithingsUserId);
                foreach (var item in notifications)
                {
                    SubscriptionManager.Instance.RevokeNotification(client, WithingsUserId, item.Callbackurl, item.Callbackurl);
                }

                String callbackUrl = @"http://216.195.85.166:61080/FitTrackersSubscription/WithingsUpdates";
                String code = SubscriptionManager.Instance.AddNotification(client, WithingsUserId, callbackUrl, callbackUrl);
                LogErrorHelper.SaveError("Код от create notification: " + code);

                return RedirectToAction("Index", "Connectors", new
                {
                    accessToken = clientDb.AccessToken,
                    callbackUrl = CallbackUrlRemote
                });
            }
        }

        public ActionResult GetMeasures(String withingsUserId)
        {
            WithingsClient client = FitTrackerClients.GetWithingsClient(withingsUserId);

            try
            {
                var measureGroups = client.GetMeasures(withingsUserId, DateTime.Now.AddYears(-10), DateTime.Now.AddYears(10));
                return Json(measureGroups, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogErrorHelper.SaveError(ex.Message);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CreateNotification(String withingsUserId)
        {
            WithingsClient client = FitTrackerClients.GetWithingsClient(withingsUserId);

            List<NotificationProfile> notifications = client.GetNotifications(withingsUserId);
            foreach (var item in notifications)
            {
                SubscriptionManager.Instance.RevokeNotification(client, withingsUserId, item.Callbackurl, item.Callbackurl);
            }

            String callbackUrl = @"http://216.195.85.166:61080/FitTrackersSubscription/WithingsUpdates";
            String code = SubscriptionManager.Instance.AddNotification(client, withingsUserId, callbackUrl, callbackUrl);

            return Json(code, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RevokeNotification(String withingsUserId)
        {
            WithingsClient client = FitTrackerClients.GetWithingsClient(withingsUserId);

            List<NotificationProfile> notifications = client.GetNotifications(withingsUserId);

            foreach (var item in notifications)
            {
                SubscriptionManager.Instance.RevokeNotification(client, withingsUserId, item.Callbackurl, item.Callbackurl);
            }

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetNotifications(String withingsUserId)
        {
            WithingsClient client = FitTrackerClients.GetWithingsClient(withingsUserId);

            try
            {
                var results = client.GetNotifications(withingsUserId);
                return Json(results, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogErrorHelper.SaveError(ex.Message);
                throw ex;
            }
        }

        public ActionResult GetActivities(String withingsUserId)
        {
            WithingsClient client = FitTrackerClients.GetWithingsClient(withingsUserId);

            try
            {
                var results = client.GetActivity(withingsUserId, DateTime.Now.AddMonths(-1), DateTime.Now);
                return Json(results, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogErrorHelper.SaveError(ex.Message);
                throw ex;
            }
        }

        public ActionResult GetSleeps(String withingsUserId)
        {
            WithingsClient client = FitTrackerClients.GetWithingsClient(withingsUserId);

            try
            {
                var results = client.GetSleep(withingsUserId, DateTime.Now.AddMonths(-1), DateTime.Now);
                return Json(results, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogErrorHelper.SaveError(ex.Message);
                throw ex;
            }
        }

        public ActionResult GetSleepSummaries(String withingsUserId)
        {
            WithingsClient client = FitTrackerClients.GetWithingsClient(withingsUserId);

            try
            {
                var results = client.GetSleepSummary(withingsUserId, DateTime.Now.AddMonths(-1), DateTime.Now);
                return Json(results, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogErrorHelper.SaveError(ex.Message);
                throw ex;
            }
        }

        public ActionResult Disconnect(Int64 userId, String token)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                repo.ClearWithings(userId);

                return RedirectToAction("Index", "Connectors", new { accessToken = token });
            }
        }

        private String WithingsUserId
        {
            get
            {
                if (Session["WithingsUserId"] != null)
                {
                    return Session["WithingsUserId"].ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }
    }
}