﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Fitbit.Api;
using Fitbit.Api.Models;
using FitTracker.Web2.Core;
using FitTracker.Data;
using FitTracker.Data.Repo.Auth;
using FitTracker.Data.Entities.Auth;
using FitTracker.Data.Repo.Fitbit;
using FitTracker.Data.Helpers;

namespace FitTracker.Web2.Controllers
{
    public class FitbitController : BaseController
    {
        public ActionResult Authorize(Int64 userId, String callbackUrlRemote)
        {
            Fitbit.Api.Authenticator authenticator = new Fitbit.Api.Authenticator(SettingsHelper.FitBitConsumerKey,
                                                                                  SettingsHelper.FitBitConsumerSecret,
                                                                                    "http://api.fitbit.com/oauth/request_token",
                                                                                    "http://api.fitbit.com/oauth/access_token",
                                                                                    "http://api.fitbit.com/oauth/authorize");

            RequestToken token = authenticator.GetRequestToken();
            Session.Add("FitbitRequestTokenSecret", token.Secret.ToString());

            String authUrl = authenticator.GenerateAuthUrlFromRequestToken(token, true);

            CallbackUserId = userId;
            CallbackUrlRemote = callbackUrlRemote;

            return Redirect(authUrl);
        }

        //Final step. Take this authorization information and use it in the app
        public ActionResult Callback()
        {
            RequestToken token = new RequestToken();
            token.Token = Request.Params["oauth_token"];
            token.Secret = Session["FitbitRequestTokenSecret"].ToString();
            token.Verifier = Request.Params["oauth_verifier"];

            Fitbit.Api.Authenticator authenticator = new Fitbit.Api.Authenticator(SettingsHelper.FitBitConsumerKey,
                                                                                    SettingsHelper.FitBitConsumerSecret,
                                                                                    "http://api.fitbit.com/oauth/request_token",
                                                                                    "http://api.fitbit.com/oauth/access_token",
                                                                                    "http://api.fitbit.com/oauth/authorize");


            AuthCredential credential = authenticator.ProcessApprovedAuthCallback(token);

            Session["FitbitAuthToken"] = credential.AuthToken;
            Session["FitbitAuthTokenSecret"] = credential.AuthTokenSecret;
            Session["FitbitUserId"] = credential.UserId;
            //CurrentUser.FitbitId = "4444";

            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                var repoDevice = uow.GetRepo<IDeviceRepo>();

                Client clientDb = repo.Get(CallbackUserId);

                clientDb.FitbitId = credential.UserId;
                clientDb.FitbitAccessToken = credential.AuthToken;
                clientDb.FitbitSecret = credential.AuthTokenSecret;

                repo.Save(clientDb);

                uow.Commit();

                // here, we add subscription
                FitbitClient client = FitTrackerClients.GetFitbitClient(FitbitUserId);

                List<ApiSubscription> subscriptions = client.GetSubscriptions();

                foreach (var item in subscriptions.Where(x => x.SubscriberId == x.SubscriptionId))
                {
                    client.RemoveSubscription(APICollectionType.user, item.SubscriptionId);
                }

                subscriptions = client.GetSubscriptions();

                if (!subscriptions.Select(x => x.SubscriptionId).Contains(credential.UserId))
                {
                    SubscriptionManager.Instance.AddSubscription(client, credential.UserId);
                }

                // here, we add devices
                List<Device> devices = client.GetDevices();

                repoDevice.SaveDevices(credential.UserId, devices);

                return RedirectToAction("Index", "Connectors", new {
                    accessToken = clientDb.AccessToken,
                    callbackUrl = CallbackUrlRemote
                });
            }
        }

        public ActionResult GetUserInfo()
        {
            FitbitClient client = FitTrackerClients.GetFitbitClient(FitbitUserId);

            var results = client.GetUserProfile();

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBodyMeasurements()
        {
            FitbitClient client = FitTrackerClients.GetFitbitClient(FitbitUserId);

            var results = client.GetBodyMeasurements(DateTime.Now.AddMonths(-3));

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBodyWeight()
        {
            FitbitClient client = FitTrackerClients.GetFitbitClient(FitbitUserId);

            var results = client.GetWeight(DateTime.Now.AddMonths(-3));

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBodyFat()
        {
            FitbitClient client = FitTrackerClients.GetFitbitClient(FitbitUserId);

            var results = client.GetFat(DateTime.Now.AddMonths(-3));

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBodyWeightGoal()
        {
            FitbitClient client = FitTrackerClients.GetFitbitClient(FitbitUserId);

            var results = client.GetWeightGoal();

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBodyFatGoal()
        {
            FitbitClient client = FitTrackerClients.GetFitbitClient(FitbitUserId);

            var results = client.GetFatGoal();

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetActivities()
        {
            FitbitClient client = FitTrackerClients.GetFitbitClient(FitbitUserId);

            var results = client.GetActivities(new DateTime(2015, 2, 23));

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetActivityDailyGoals()
        {
            FitbitClient client = FitTrackerClients.GetFitbitClient(FitbitUserId);

            var results = client.GetActivityDailyGoals();

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetActivityWeeklyGoals()
        {
            FitbitClient client = FitTrackerClients.GetFitbitClient(FitbitUserId);

            var results = client.GetActivityWeeklyGoals();

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetFoodLogs()
        {
            FitbitClient client = FitTrackerClients.GetFitbitClient(FitbitUserId);

            var results = client.GetFood(DateTime.Now);

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetWaterLogs()
        {
            FitbitClient client = FitTrackerClients.GetFitbitClient(FitbitUserId);

            var results = client.GetWater(DateTime.Now);

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSleep()
        {
            FitbitClient client = FitTrackerClients.GetFitbitClient(FitbitUserId);

            var results = client.GetSleep(DateTime.Now, "-");

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSubscriptions()
        {
            FitbitClient client = FitTrackerClients.GetFitbitClient(FitbitUserId);

            var results = client.GetSubscriptions();

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Disconnect(Int64 userId, String token)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                repo.ClearFitbit(userId);

                return RedirectToAction("Index", "Connectors", new { accessToken = token });
            }
        }

        private String FitbitUserId
        {
            get
            {
                if (Session["FitbitUserId"] != null)
                {
                    return Session["FitbitUserId"].ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }
    }
}