﻿using FitTracker.Data;
using FitTracker.Data.Entities.Auth;
using FitTracker.Data.Helpers;
using FitTracker.Data.Repo.Auth;
using FitTracker.Data.Repo.Strava;
using FitTracker.Web2.Core;
using FitTracker.Web2.Models.Home;
using RestSharp;
using Strava.Api.Authentication;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FitTracker.Web2.Controllers
{
    public class StravaController : BaseController
    {
        public ActionResult Authorize(Int64 userId, String callbackUrlRemote)
        {
#if DEBUG
            String callbackUrl = @"https://localhost:44302/Strava/Callback";
#else
            String callbackUrl = @"https://216.195.85.166:61443/Strava/Callback";
#endif

            String authURL = "https://www.strava.com/oauth/authorize";

            RestClient client = new RestClient(authURL);

            var request = new RestRequest(
                    string.Format(
                    "?response_type=code&client_id={0}&redirect_uri={1}&scope=view_private,write",
                    SettingsHelper.StravaConsumerKey, callbackUrl
                    ), Method.GET);

            var url = client.BuildUri(request).ToString();

            CallbackUserId = userId;
            CallbackUrlRemote = callbackUrlRemote;

            return Redirect(url);
        }

        [RequireHttps]
        public ActionResult Callback()
        {
            String code = Request.Params["code"].ToString();

            String authURL = "https://www.strava.com/oauth/token";

            var request = (HttpWebRequest)WebRequest.Create(String.Format(
                    "{0}?client_id={1}&client_secret={2}&code={3}",
                    authURL, SettingsHelper.StravaConsumerKey, SettingsHelper.StravaConsumerSecret, code
                    ));
            request.Method = "POST";
            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            //// For demo, put this in the session managed by ASP.NET
            JavaScriptSerializer jss = new JavaScriptSerializer();
            StravaAccessTokenModel tokenModel = jss.Deserialize<StravaAccessTokenModel>(responseString);
            Session["StravaAccessToken"] = tokenModel.access_token;
            Session["StravaRefreshToken"] = tokenModel.access_token;
            Session["StravaClientId"] = tokenModel.athlete.id;

            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();

                Client clientDb = repo.Get(CallbackUserId);

                clientDb.StravaId = tokenModel.athlete.id.ToString();
                clientDb.StravaAccessToken = tokenModel.access_token;
                clientDb.StravaRefreshToken = tokenModel.access_token;

                repo.Save(clientDb);

                uow.Commit();

                return RedirectToAction("Index", "Connectors", new
                {
                    accessToken = clientDb.AccessToken,
                    callbackUrl = CallbackUrlRemote
                });
            }
        }


        public ActionResult Disconnect(Int64 userId, String token)
        {
            String deAuthURL = "https://www.strava.com/oauth/deauthorize";
            String accessToken;
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                Client clientDb = repo.GetByAccessToken(token);
                accessToken = clientDb.StravaAccessToken;
            }
            var request = (HttpWebRequest)WebRequest.Create(String.Format(@"{0}", deAuthURL));
            request.Method = "POST";
            request.Headers.Add("Authorization", "Bearer " + accessToken);
            request.GetResponse();

            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                repo.ClearStrava(userId);

                return RedirectToAction("Index", "Connectors", new { accessToken = token });
            }
        }

        public ActionResult GetData()
        {
            String accessToken;
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                Client clientDb = repo.GetByUserId(3)[0];
                accessToken = clientDb.StravaAccessToken;
            }

            Strava.Api.Clients.StravaClient client = FitTrackerClients.GetStravaClientByToken(accessToken);

            Strava.Api.Clients.AthleteClient athleteClient = client.Athletes;
            Strava.Api.Athletes.Athlete athleteInfo = athleteClient.GetAthlete();

            var athleteRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IAthleteRepo>();
            Int64 athletId = athleteRepo.SaveAthlete(athleteInfo).PkID;

            Strava.Api.Clients.GearClient gearClient = client.Gear;

            var bikeRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IBikeRepo>();
            foreach (Strava.Api.Gear.Bike bike in athleteInfo.Bikes)
            {
                Strava.Api.Gear.Bike bikeInfo = gearClient.GetGear(bike.Id);
                bikeRepo.SaveBike(athletId, bikeInfo);
            }

            var shoesRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IShoesRepo>();
            foreach (Strava.Api.Gear.Shoes shoes in athleteInfo.Shoes)
            {
                Strava.Api.Gear.Bike shoesInfo = gearClient.GetGear(shoes.Id);
                shoesRepo.SaveShoes(athletId, shoesInfo);
            }

            Strava.Api.Clients.ClubClient clubClient = client.Clubs;

            var clubRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IClubRepo>();
            foreach (Strava.Api.Clubs.Club club in athleteInfo.Clubs)
            {
                Strava.Api.Clubs.Club clubInfo = clubClient.GetClub(club.Id.ToString());
                clubRepo.SaveClub(athletId, clubInfo);
            }

            Strava.Api.Clients.ActivityClient activityInfo = client.Activities;
            List<Strava.Api.Activities.ActivitySummary> activitySummaries = activityInfo.GetAllActivities();
            var activityRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IActivityRepo>();
            foreach (Strava.Api.Activities.ActivitySummary activitySummary in activitySummaries)
            {
                Strava.Api.Activities.Activity activity = activityInfo.GetActivity(activitySummary.Id.ToString(), true);
                Int64 activityId = activityRepo.SaveActivity(athletId, activity).PkID;

                List<Strava.Api.Activities.Comment> comments = activityInfo.GetComments(activitySummary.Id.ToString());
                var commentRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<ICommentRepo>();
                commentRepo.SaveComments(activityId, comments);

                List<Strava.Api.Activities.Photo> photos = activityInfo.GetPhotos(activitySummary.Id.ToString());
                var photoRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IPhotoRepo>();
                photoRepo.SavePhotos(activityId, photos);

                List<Strava.Api.Activities.ActivityLap> activityLaps = activityInfo.GetActivityLaps(activitySummary.Id.ToString());
                var activityLapRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IActivityLapRepo>();
                activityLapRepo.SaveActivityLaps(activityId, activityLaps);

                Strava.Api.Clients.SegmentClient SegmentClient = client.Segments;
                Strava.Api.Clients.EffortClient EffortClient = client.Efforts;

                var segmentRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<ISegmentRepo>();
                foreach(Strava.Api.Segments.SegmentEffort segmentEffort in activity.SegmentEfforts)
                {
                    Strava.Api.Segments.Segment segment = SegmentClient.GetSegment(segmentEffort.Segment.Id.ToString());
                    segmentRepo.SaveSegment(segment);
                    List<Strava.Api.Segments.SegmentEffort> segmentEff = EffortClient.GetSegmentEffortsByAthlete(segmentEffort.Segment.Id.ToString(), athleteInfo.Id.ToString());
                }
            }

            return null;
        }

        private String _stravaUserId
        {
            get
            {
                if (Session["StravaClientId"] != null)
                {
                    return Session["StravaClientId"].ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }
    }
}