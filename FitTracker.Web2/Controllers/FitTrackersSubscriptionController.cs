﻿using FitbitApi = Fitbit.Api;
using WithingsApi = Withings.API;
using JawboneApi = Jawbone.Api;
using MisfitApi = Misfit.Api;

using FAM = Fitbit.Api.Models;
using WAM = Withings.API.Models;
using JAM = Jawbone.Api.Models;
using MAM = Misfit.Api.Models;

using RepoFitbit = FitTracker.Data.Repo.Fitbit;
using RepoWithings = FitTracker.Data.Repo.Withings;
using RepoJawbone = FitTracker.Data.Repo.Jawbone;

using FitTracker.Data;
using FitTracker.Web2.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using FitTracker.Data.Repo.Auth;
using FitTracker.Data.Entities.Auth;
using System.Threading;
using FitTracker.Core.Unity;
using System.Web.Script.Serialization;
using FitTracker.Data.Helpers;
using FitTracker.Web2.Enums;

namespace FitTracker.Web2.Controllers
{
    /// <summary>
    /// Receive and process subscription updates from Fitness Tracker APIs
    /// </summary>
    public class FitTrackersSubscriptionController : BaseController
    {
        /// <summary>
        /// Receive and respond to FitBit subscription updates
        /// </summary>
        /// <returns></returns>  
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> FitBitUpdates()
        {
            try
            {
                Request.InputStream.Seek(0, SeekOrigin.Begin);
                String jsonData = await new StreamReader(Request.InputStream).ReadToEndAsync();

                if (!String.IsNullOrEmpty(jsonData))
                {
                    List<FAM.UpdatedResource> data = FitbitApi.SubscriptionManager.Instance.ProcessUpdateReponseBody(jsonData);

                    Thread thread = new Thread(new ParameterizedThreadStart(FitbitThreadProc));
                    thread.Start(data);
                }

                return new HttpStatusCodeResult(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NoContent);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> WithingsUpdates(WAM.Appli appli, Int64 startdate, Int64 enddate, String userid)
        {
            try
            {
                //Request.InputStream.Seek(0, SeekOrigin.Begin);
                //String jsonData = await new StreamReader(Request.InputStream).ReadToEndAsync();

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                var jsonData = serializer.Serialize(new
                {
                    Appli = appli,
                    StartDate = DateHelper.FromUnixTime(startdate),
                    EndDate = DateHelper.FromUnixTime(enddate),
                    UserId = userid
                });

                if (!String.IsNullOrEmpty(jsonData))
                {
                    //LogErrorHelper.SaveError(WithingsClient.FromUnixTime(startdate).ToString() + " " + WithingsClient.FromUnixTime(enddate).ToString());
                    LogErrorHelper.SaveError(jsonData);

                    WAM.UpdatedResource data = WithingsApi.SubscriptionManager.Instance.ProcessUpdateReponseBody(jsonData);
                    
                    Thread thread = new Thread(new ParameterizedThreadStart(WithingsThreadProc));
                    thread.Start(data);
                }

                return new HttpStatusCodeResult(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NoContent);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> JawboneUpdates()
        {
            try
            {
                Request.InputStream.Seek(0, SeekOrigin.Begin);
                String jsonData = await new StreamReader(Request.InputStream).ReadToEndAsync();

                if (!String.IsNullOrEmpty(jsonData))
                {
                    LogErrorHelper.SaveError(jsonData);

                    JAM.UpdatedResource data = JawboneApi.SubscriptionManager.Instance.ProcessUpdateReponseBody(jsonData);

                    Thread thread = new Thread(new ParameterizedThreadStart(JawboneThreadProc));
                    thread.Start(data);
                }

                return new HttpStatusCodeResult(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NoContent);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> MisfitUpdates()
        {
            try
            {
                Request.InputStream.Seek(0, SeekOrigin.Begin);
                String jsonData = await new StreamReader(Request.InputStream).ReadToEndAsync();
                if (!String.IsNullOrEmpty(jsonData))
                {
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    String subscriptionType = jss.Deserialize<MAM.SubscriptionType>(jsonData).Type;
                    if (subscriptionType == "SubscriptionConfirmation")
                    {
                        MAM.SubscriptionConformition subscriptionConformition = jss.Deserialize<MAM.SubscriptionConformition>(jsonData);
                        LogErrorHelper.SaveError(subscriptionConformition.SubscribeURL);
                        var request = (HttpWebRequest)WebRequest.Create(String.Format(
                            "{0}",
                            subscriptionConformition.SubscribeURL));
                        request.Method = "GET";
                        var response = (HttpWebResponse)request.GetResponse();
                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                    LogErrorHelper.SaveError(jsonData);
                    MAM.SubscriptionNotification subscriptionModel = jss.Deserialize<MAM.SubscriptionNotification>(jsonData);
                    
                    MAM.UpdatedResource data = MisfitApi.SubscriptionManager.Instance.ProcessUpdateReponseBody(jsonData);

                    Thread thread = new Thread(new ParameterizedThreadStart(MisfitThreadProc));
                    thread.Start(data);
                }

                return new HttpStatusCodeResult(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NoContent);
            }
        }

        private static void FitbitThreadProc(Object obj)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repoBody = uow.GetRepo<RepoFitbit.IBodyRepo>();
                var repoBodyGoals = uow.GetRepo<RepoFitbit.IBodyGoalsRepo>();

                var repoActivityLog = uow.GetRepo<RepoFitbit.IActivityLogRepo>();
                var repoActivityDistance = uow.GetRepo<RepoFitbit.IActivityDistanceRepo>();
                var repoActivityGoals = uow.GetRepo<RepoFitbit.IActivityGoalsRepo>();
                var repoActivitySummary = uow.GetRepo<RepoFitbit.IActivitySummaryRepo>();
                
                var repoFoodLog = uow.GetRepo<RepoFitbit.IFoodLogRepo>();
                var repoFoodSummary = uow.GetRepo<RepoFitbit.IFoodSummaryRepo>();
                var repoFoodGoals = uow.GetRepo<RepoFitbit.IFoodGoalsRepo>();

                var repoSleepLog = uow.GetRepo<RepoFitbit.ISleepLogRepo>();
                var repoSleepSummary = uow.GetRepo<RepoFitbit.ISleepSummaryRepo>();

                try
                {
                    List<FAM.UpdatedResource> data = (List<FAM.UpdatedResource>)obj;

                    foreach (FAM.UpdatedResource subscrUpdate in data)
                    {
                        //Get the userObjectId
                        //Example subscriptionId "OgDnZLQo7M-activities"
                        String[] strArr = subscrUpdate.SubscriptionId.Split('-');

                        var userId = strArr[0];
                        var date = subscrUpdate.Date;

                        FitbitApi.FitbitClient client = FitTrackerClients.GetFitbitClient(userId);

                        if (client == null) continue;

                        if (subscrUpdate.CollectionType == FAM.APICollectionType.activities)
                        {
                            LogErrorHelper.SaveError("Fitbit - activities : " + (new JavaScriptSerializer()).Serialize(subscrUpdate));

                            FAM.Activity activity = client.GetActivities(date, userId);
                            
                            if (activity == null) continue;
                 
                            if (activity.Activities != null) repoActivityLog.SaveActivitiesLog(userId, date, activity.Activities);
                            if (activity.Goals != null) repoActivityGoals.SaveActivityGoals(userId, date, activity.Goals);
                            if (activity.Summary != null) repoActivitySummary.SaveActivitySummary(userId, date, activity.Summary);
                            
                            uow.Commit();

                            SendNotify(typeNotify: TypeNotify.Routine, date: date, fitbitUserId: userId);
                            SendNotify(typeNotify: TypeNotify.Fitness, date: date, fitbitUserId: userId);
                        }
                        else if (subscrUpdate.CollectionType == FAM.APICollectionType.body)
                        {
                            LogErrorHelper.SaveError("Fitbit - body : " + (new JavaScriptSerializer()).Serialize(subscrUpdate));

                            FAM.BodyMeasurements body = client.GetBodyMeasurements(date, userId);

                            if (body == null) continue;

                            if (body.Body != null) repoBody.SaveBody(userId, date, body.Body);
                            if (body.Goals != null) repoBodyGoals.SaveBodyGoals(userId, date, body.Goals);

                            uow.Commit();

                            SendNotify(typeNotify: TypeNotify.Weight, date: date, fitbitUserId: userId);
                        }
                        else if (subscrUpdate.CollectionType == FAM.APICollectionType.foods)
                        {
                            LogErrorHelper.SaveError("Fitbit - foods : " + (new JavaScriptSerializer()).Serialize(subscrUpdate));

                            FAM.Food body = client.GetFood(date, userId);

                            if (body == null) continue;

                            if (body.Foods != null) repoFoodLog.SaveData(userId, date, body.Foods);
                            if (body.Goals != null) repoFoodGoals.SaveData(userId, date, body.Goals);
                            if (body.Summary != null) repoFoodSummary.SaveData(userId, date, body.Summary);

                            uow.Commit();

                            SendNotify(typeNotify: TypeNotify.Nutrition, date: date, fitbitUserId: userId);
                        }
                        else if (subscrUpdate.CollectionType == FAM.APICollectionType.sleep)
                        {
                            LogErrorHelper.SaveError("Fitbit - sleep : " + (new JavaScriptSerializer()).Serialize(subscrUpdate));

                            FAM.SleepData body = client.GetSleep(date, userId);

                            if (body == null) continue;

                            if (body.Sleep != null) repoSleepLog.SaveData(userId, date, body.Sleep);
                            if (body.Summary != null) repoSleepSummary.SaveData(userId, date, body.Summary);

                            uow.Commit();

                            SendNotify(typeNotify: TypeNotify.Sleep, date: date, fitbitUserId: userId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogErrorHelper.SaveError(ex.Message);
                }
            }
        }

        private static void WithingsThreadProc(Object obj)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repoActivity = uow.GetRepo<RepoWithings.IActivityMeasureRepo>();
                var repoBodyMeasure = uow.GetRepo<RepoWithings.IBodyMeasureRepo>();
                var repoSleepMeasure = uow.GetRepo<RepoWithings.ISleepMeasureRepo>();
                var repoSleepSummary = uow.GetRepo<RepoWithings.ISleepSummaryRepo>();
                
                try
                {
                    WAM.UpdatedResource data = (WAM.UpdatedResource)obj;

                    WithingsApi.WithingsClient client = FitTrackerClients.GetWithingsClient(data.UserId);

                    if (client == null) return;

                    if (data.Appli == WAM.Appli.BodyScale)
                    {
                        LogErrorHelper.SaveError("Withings - BodyScale : " + (new JavaScriptSerializer()).Serialize(obj));

                        List<WAM.MeasureGroup> measureGroups = client.GetMeasures(data.UserId, data.StartDate, data.EndDate);

                        if (measureGroups == null) return;

                        repoBodyMeasure.SaveDataBodyScale(data.UserId, measureGroups, data.StartDate, data.EndDate);

                        uow.Commit();

                        SendNotify(typeNotify: TypeNotify.Weight, date: data.StartDate, withingsUserId: data.UserId);
                    }
                    else if (data.Appli == WAM.Appli.BloodPressure)
                    {
                        LogErrorHelper.SaveError("Withings - BloodPressure : " + (new JavaScriptSerializer()).Serialize(obj));

                        List<WAM.MeasureGroup> measureGroups = client.GetMeasures(data.UserId, data.StartDate, data.EndDate);

                        if (measureGroups == null) return;

                        repoBodyMeasure.SaveDataHeartRate(data.UserId, measureGroups, data.StartDate, data.EndDate);

                        uow.Commit();

                        //SendNotify(typeNotify: TypeNotify., withingsUserId: data.UserId);
                    }
                    else if (data.Appli == WAM.Appli.Pulse)
                    {
                        LogErrorHelper.SaveError("Withings - Pulse (Activity) : " + (new JavaScriptSerializer()).Serialize(obj));

                        List<WAM.Activity> activities = client.GetActivity(data.UserId, data.StartDate, data.EndDate);

                        if (activities == null) return;

                        repoActivity.SaveData(data.UserId, activities, data.StartDate, data.EndDate);
                        
                        uow.Commit();

                        //SendNotify(typeNotify: TypeNotify.Weight, withingsUserId: data.UserId);
                    }
                    else if (data.Appli == WAM.Appli.Sleep)
                    {
                        LogErrorHelper.SaveError("Withings - Sleep : " + (new JavaScriptSerializer()).Serialize(obj));

                        List<WAM.Sleep> sleeps = client.GetSleep(data.UserId, data.StartDate, data.EndDate);
                        List<WAM.SleepSummary> sleepSummaries = client.GetSleepSummary(data.UserId, data.StartDate, data.EndDate);

                        if (sleeps == null || sleepSummaries == null) return;

                        repoSleepMeasure.SaveData(data.UserId, sleeps, data.StartDate, data.EndDate);
                        repoSleepSummary.SaveData(data.UserId, sleepSummaries, data.StartDate, data.EndDate);

                        uow.Commit();

                        SendNotify(typeNotify: TypeNotify.Sleep, date: data.StartDate, withingsUserId: data.UserId);
                    }
                    else
                    {
                        LogErrorHelper.SaveError("What is it: " + (new JavaScriptSerializer()).Serialize(obj));
                    }
                }
                catch (Exception ex)
                {
                    LogErrorHelper.SaveError(ex.Message);
                }
            }
        }

        private static void JawboneThreadProc(Object obj)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                try
                {
                    JAM.UpdatedResource data = (JAM.UpdatedResource)obj;

                    foreach (var item in data.events)
                    {
                        JawboneApi.JawboneClient client = FitTrackerClients.GetJawboneClientByUserId(item.user_xid);

                        if (client == null) continue;

                        var date = DateHelper.FromUnixTime(item.timestamp);

                        if (item.type == "move")
                        {
                            LogErrorHelper.SaveError("Jawbone - move : " + (new JavaScriptSerializer()).Serialize(item));

                            JAM.Moves requestData = client.GetMoves(date);

                            var repo = uow.GetRepo<RepoJawbone.IMovesRepo>();
                            repo.SaveData(item.user_xid, date, requestData);

                            SendNotify(typeNotify: TypeNotify.Routine, date: date, jawboneUserId: item.user_xid);
                        }
                        else if (item.type == "sleep")
                        {
                            LogErrorHelper.SaveError("Jawbone - sleep : " + (new JavaScriptSerializer()).Serialize(item));

                            JAM.Sleeps requestData = client.GetSleeps(date);

                            var repo = uow.GetRepo<RepoJawbone.ISleepsRepo>();
                            repo.SaveData(item.user_xid, date, requestData);

                            SendNotify(typeNotify: TypeNotify.Sleep, date: date, jawboneUserId: item.user_xid);
                        }
                        else if (item.type == "body")
                        {
                            LogErrorHelper.SaveError("Jawbone - body : " + (new JavaScriptSerializer()).Serialize(item));

                            JAM.BodyEvent requestData = client.GetBodyEvent(date);

                            var repo = uow.GetRepo<RepoJawbone.IBodyEventRepo>();
                            repo.SaveData(item.user_xid, date, requestData);

                            SendNotify(typeNotify: TypeNotify.Weight, date: date, jawboneUserId: item.user_xid);
                        }
                        else if (item.type == "workout")
                        {
                            LogErrorHelper.SaveError("Jawbone - workout : " + (new JavaScriptSerializer()).Serialize(item));

                            JAM.Workouts requestData = client.GetWorkouts(date);

                            var repo = uow.GetRepo<RepoJawbone.IWorkoutsRepo>();
                            repo.SaveData(item.user_xid, date, requestData);

                            SendNotify(typeNotify: TypeNotify.Fitness, date: date, jawboneUserId: item.user_xid);
                        }
                        else if (item.type == "generic")
                        {
                            LogErrorHelper.SaveError("What is 'generic'? : " + (new JavaScriptSerializer()).Serialize(item));
                        }
                        else if (item.type == "meal")
                        {
                            LogErrorHelper.SaveError("Jawbone - meal : " + (new JavaScriptSerializer()).Serialize(item));
                            
                            JAM.Meals requestData = client.GetMeals(date);

                            var repo = uow.GetRepo<RepoJawbone.IMealsRepo>();
                            repo.SaveData(item.user_xid, date, requestData);

                            SendNotify(typeNotify: TypeNotify.Nutrition, date: date, jawboneUserId: item.user_xid);
                        }
                        else if (item.type == "mood")
                        {
                            LogErrorHelper.SaveError("Jawbone - mood : " + (new JavaScriptSerializer()).Serialize(item));

                            JAM.Mood requestData = client.GetMood(date);

                            var repo = uow.GetRepo<RepoJawbone.IMoodRepo>();
                            repo.SaveData(item.user_xid, date, requestData);
                        }
                        else if (item.type == "user_app_connection")
                        {

                        }
                        else if (item.type == "user_data_deletion")
                        {

                        }
                        else
                        {
                            LogErrorHelper.SaveError("What is it: " + (new JavaScriptSerializer()).Serialize(item));
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogErrorHelper.SaveError(ex.Message);
                }
            }
        }

        private static void MisfitThreadProc(Object obj)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                try
                {
                    MAM.UpdatedResource data = (MAM.UpdatedResource)obj;

                    foreach (var item in data.events)
                    {
                        MisfitApi.MisfitClient client = FitTrackerClients.GetMisfitClientByUserId(item.user_xid);

                        if (client == null) continue;

                        var date = DateHelper.FromUnixTime(item.timestamp);

                        if (item.type == "move")
                        {
                            LogErrorHelper.SaveError("Jawbone - move : " + (new JavaScriptSerializer()).Serialize(item));
                        }
                        else if (item.type == "sleep")
                        {
                            LogErrorHelper.SaveError("Jawbone - sleep : " + (new JavaScriptSerializer()).Serialize(item));
                        }
                        else if (item.type == "body")
                        {
                            LogErrorHelper.SaveError("Jawbone - body : " + (new JavaScriptSerializer()).Serialize(item));

                        }
                        else if (item.type == "workout")
                        {
                            LogErrorHelper.SaveError("Jawbone - workout : " + (new JavaScriptSerializer()).Serialize(item));


                            SendNotify(typeNotify: TypeNotify.Fitness, date: date, jawboneUserId: item.user_xid);
                        }
                        else if (item.type == "generic")
                        {
                            LogErrorHelper.SaveError("What is 'generic'? : " + (new JavaScriptSerializer()).Serialize(item));
                        }
                        else if (item.type == "meal")
                        {
                            LogErrorHelper.SaveError("Jawbone - meal : " + (new JavaScriptSerializer()).Serialize(item));
                        }
                        else if (item.type == "mood")
                        {
                            LogErrorHelper.SaveError("Jawbone - mood : " + (new JavaScriptSerializer()).Serialize(item));
                        }
                        else if (item.type == "user_app_connection")
                        {

                        }
                        else if (item.type == "user_data_deletion")
                        {

                        }
                        else
                        {
                            LogErrorHelper.SaveError("What is it: " + (new JavaScriptSerializer()).Serialize(item));
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogErrorHelper.SaveError(ex.Message);
                }
            }
        }
        
        private static void SendNotify(TypeNotify typeNotify, DateTime date, String fitbitUserId = "", String withingsUserId = "", String jawboneUserId = "", String runkeeperId = "")
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repoClient = uow.GetRepo<IClientRepo>();

                Client entity = new Client();

                if (!String.IsNullOrEmpty(fitbitUserId))
                {
                    entity = repoClient.GetByFitbitId(fitbitUserId);
                }
                else if (!String.IsNullOrEmpty(withingsUserId))
                {
                    entity = repoClient.GetByWithingsId(withingsUserId);
                }
                else if (!String.IsNullOrEmpty(jawboneUserId))
                {
                    entity = repoClient.GetByJawboneId(jawboneUserId);
                }
                else if (!String.IsNullOrEmpty(runkeeperId))
                {
                    entity = repoClient.GetByRunkeeperId(jawboneUserId);
                }

                String notificationUrl = entity.Applicant.NotificationUrl;

                if (String.IsNullOrEmpty(notificationUrl))
                {
                    return;
                }
                
                if (notificationUrl[notificationUrl.Length - 1] == '/')
                {
                    notificationUrl = notificationUrl.Substring(0, notificationUrl.Length - 1);
                }

                WebRequest myWebRequest = WebRequest.Create(String.Format(notificationUrl + "?user_id={0}&date_action={1}&type_action={2}", entity.ExternalId, date.ToString("yyyyMMdd"), (Int32)typeNotify));
                myWebRequest.Timeout = 10000;
                myWebRequest.Method = "POST";
                myWebRequest.ContentType = "application/json; charset=utf-8";
                myWebRequest.ContentLength = 0;

                using (WebResponse myWebResponse = myWebRequest.GetResponse())
                {

                }
            }
        }

        public ActionResult SendNotifyTest(TypeNotify typeNotify, DateTime date, String fitbitUserId = "", String withingsUserId = "", String jawboneUserId = "", String runkeeperId = "")
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repoClient = uow.GetRepo<IClientRepo>();

                Client entity = new Client();

                if (!String.IsNullOrEmpty(fitbitUserId))
                {
                    entity = repoClient.GetByFitbitId(fitbitUserId);
                }
                else if (!String.IsNullOrEmpty(withingsUserId))
                {
                    entity = repoClient.GetByWithingsId(withingsUserId);
                }
                else if (!String.IsNullOrEmpty(jawboneUserId))
                {
                    entity = repoClient.GetByJawboneId(jawboneUserId);
                }
                else if (!String.IsNullOrEmpty(runkeeperId))
                {
                    entity = repoClient.GetByRunkeeperId(runkeeperId);
                }

                String notificationUrl = entity.Applicant.NotificationUrl;

                if (String.IsNullOrEmpty(notificationUrl))
                {
                    return Json("Ok");
                }

                if (notificationUrl[notificationUrl.Length - 1] == '/')
                {
                    notificationUrl = notificationUrl.Substring(0, notificationUrl.Length - 1);
                }

                HttpWebRequest myWebRequest = WebRequest.Create(String.Format(notificationUrl + "?user_id={0}&date_action={1}&type_action={2}", entity.ExternalId, date.ToString("yyyyMMdd"), (Int32)typeNotify)) as HttpWebRequest;
                myWebRequest.Timeout = 10000;
                myWebRequest.Method = "POST";
                myWebRequest.ContentType = "application/json; charset=utf-8";
                myWebRequest.ContentLength = 0;

                using (WebResponse myWebResponse = myWebRequest.GetResponse())
                {

                }

                return Json("Ok");
            }
        }
        
        public ActionResult Test()
        {
            try
            {
                DateTime dt = new DateTime(2015, 6, 2);

                ////String jsonData = @"{'secret_hash': '073f473b2c3d99b0d881d4d8648649e28cc62fa8981e5afd01ffb778d727beda', 'events': [{'action': 'creation', 'timestamp': 1433234880, 'user_xid': 'pyfSz4Y4xoVcEx3Ii9dSrg', 'type': 'move', 'event_xid': 'd28LnnPxBUQ8qR5edl63f2mBoewRWd2d'}], 'notification_timestamp': 1433235384}";
                //List<String> jsonData = new List<String>();
                //jsonData.Add(@"{'secret_hash': '073f473b2c3d99b0d881d4d8648649e28cc62fa8981e5afd01ffb778d727beda', 'events': [{'action': 'creation', 'timestamp': " + DateHelper.ToUnixTime(dt).ToString() + ", 'user_xid': 'pyfSz4Y4xoVcEx3Ii9dSrg', 'type': 'move', 'event_xid': 'd28LnnPxBUQNZzfBo49k4DXtOs6UxUVN'}], 'notification_timestamp': 1433261757}");
                //jsonData.Add(@"{'secret_hash': '073f473b2c3d99b0d881d4d8648649e28cc62fa8981e5afd01ffb778d727beda', 'events': [{'action': 'creation', 'timestamp': " + DateHelper.ToUnixTime(dt).ToString() + ", 'user_xid': 'pyfSz4Y4xoVcEx3Ii9dSrg', 'type': 'sleep', 'event_xid': 'd28LnnPxBUQNZzfBo49k4DXtOs6UxUVN'}], 'notification_timestamp': 1433261757}");
                //jsonData.Add(@"{'secret_hash': '073f473b2c3d99b0d881d4d8648649e28cc62fa8981e5afd01ffb778d727beda', 'events': [{'action': 'creation', 'timestamp': " + DateHelper.ToUnixTime(dt).ToString() + ", 'user_xid': 'pyfSz4Y4xoVcEx3Ii9dSrg', 'type': 'body', 'event_xid': 'd28LnnPxBUQNZzfBo49k4DXtOs6UxUVN'}], 'notification_timestamp': 1433261757}");
                //jsonData.Add(@"{'secret_hash': '073f473b2c3d99b0d881d4d8648649e28cc62fa8981e5afd01ffb778d727beda', 'events': [{'action': 'creation', 'timestamp': " + DateHelper.ToUnixTime(dt).ToString() + ", 'user_xid': 'pyfSz4Y4xoVcEx3Ii9dSrg', 'type': 'workout', 'event_xid': 'd28LnnPxBUQNZzfBo49k4DXtOs6UxUVN'}], 'notification_timestamp': 1433261757}");
                //jsonData.Add(@"{'secret_hash': '073f473b2c3d99b0d881d4d8648649e28cc62fa8981e5afd01ffb778d727beda', 'events': [{'action': 'creation', 'timestamp': " + DateHelper.ToUnixTime(dt).ToString() + ", 'user_xid': 'pyfSz4Y4xoVcEx3Ii9dSrg', 'type': 'meal', 'event_xid': 'd28LnnPxBUQNZzfBo49k4DXtOs6UxUVN'}], 'notification_timestamp': 1433261757}");
                //jsonData.Add(@"{'secret_hash': '073f473b2c3d99b0d881d4d8648649e28cc62fa8981e5afd01ffb778d727beda', 'events': [{'action': 'creation', 'timestamp': " + DateHelper.ToUnixTime(dt).ToString() + ", 'user_xid': 'pyfSz4Y4xoVcEx3Ii9dSrg', 'type': 'mood', 'event_xid': 'd28LnnPxBUQNZzfBo49k4DXtOs6UxUVN'}], 'notification_timestamp': 1433261757}");

                //foreach (var item in jsonData)
                //{
                //    if (!String.IsNullOrEmpty(item))
                //    {
                //        JAM.UpdatedResource data = JawboneApi.SubscriptionManager.Instance.ProcessUpdateReponseBody(item);

                //        Thread thread = new Thread(new ParameterizedThreadStart(JawboneThreadProc));
                //        thread.Start(data);
                //    }
                //}

                //JavaScriptSerializer serializer = new JavaScriptSerializer();
                //DateTime startDate = new DateTime(2015, 6, 2);
                //DateTime endDate = new DateTime(2015, 6, 4);
                //String userid = "7140102";

                //jsonData = new List<String>();
                //jsonData.Add(serializer.Serialize(new { Appli = WAM.Appli.BodyScale, StartDate = startDate, EndDate = endDate, UserId = userid }));
                //jsonData.Add(serializer.Serialize(new { Appli = WAM.Appli.BloodPressure, StartDate = startDate, EndDate = endDate, UserId = userid }));
                //jsonData.Add(serializer.Serialize(new { Appli = WAM.Appli.Pulse, StartDate = startDate, EndDate = endDate, UserId = userid }));
                //jsonData.Add(serializer.Serialize(new { Appli = WAM.Appli.Sleep, StartDate = startDate, EndDate = endDate, UserId = userid }));

                //foreach (var item in jsonData)
                //{
                //    WAM.UpdatedResource data = WithingsApi.SubscriptionManager.Instance.ProcessUpdateReponseBody(item);

                //    Thread thread = new Thread(new ParameterizedThreadStart(WithingsThreadProc));
                //    thread.Start(data);
                //}

                //List<FAM.UpdatedResource> fitbitData = new List<FAM.UpdatedResource>();
                //fitbitData.Add(new FAM.UpdatedResource()
                //    {
                //        CollectionType = FAM.APICollectionType.foods,
                //        Date = DateTime.Now,
                //        OwnerId = "",
                //        OwnerType = FAM.ResourceOwnerType.User,
                //        SubscriptionId = "382H5K-bnm"
                //    });

                //fitbitData.Add(new FAM.UpdatedResource()
                //{
                //    CollectionType = FAM.APICollectionType.activities,
                //    Date = DateTime.Now,
                //    OwnerId = "",
                //    OwnerType = FAM.ResourceOwnerType.User,
                //    SubscriptionId = "382H5K-bnm"
                //});

                //fitbitData.Add(new FAM.UpdatedResource()
                //{
                //    CollectionType = FAM.APICollectionType.body,
                //    Date = DateTime.Now,
                //    OwnerId = "",
                //    OwnerType = FAM.ResourceOwnerType.User,
                //    SubscriptionId = "382H5K-bnm"
                //});

                //fitbitData.Add(new FAM.UpdatedResource()
                //{
                //    CollectionType = FAM.APICollectionType.sleep,
                //    Date = DateTime.Now,
                //    OwnerId = "",
                //    OwnerType = FAM.ResourceOwnerType.User,
                //    SubscriptionId = "382H5K-bnm"
                //});

                //Thread thread2 = new Thread(new ParameterizedThreadStart(FitbitThreadProc));
                //thread2.Start(fitbitData);

                return Json("Ok", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("No ok", JsonRequestBehavior.AllowGet);
            }
        }
    }
}