﻿using FitTracker.Data;
using FitTracker.Data.Entities.Auth;
using FitTracker.Data.Helpers;
using FitTracker.Data.Repo.Auth;
using FitTracker.Data.Repo.Runkeeper;
using FitTracker.Web2.Core;
using RestSharp;
using Runkeeper.API;
using Runkeeper.API.Endpoints;
using Runkeeper.API.Models;
using System;
using System.Web.Mvc;

namespace FitTracker.Web2.Controllers
{
    public class RunkeeperController : BaseController
    {
        public ActionResult Authorize(Int64 userId, String callbackUrlRemote)
        {
            #if DEBUG
            String callbackUrl = @"https://localhost:44302/Runkeeper/Callback";
            #else
            String callbackUrl = @"https://216.195.85.166:61443/Runkeeper/Callback";
            #endif

            String authURL = "https://runkeeper.com/apps/authorize";

            RestClient client = new RestClient(authURL);

            var request = new RestRequest(
                    string.Format(
                    "?response_type=code&client_id={0}&redirect_uri={1}",
                    SettingsHelper.RunkeeperConsumerKey, callbackUrl
                    ), Method.POST);

            var url = client.BuildUri(request).ToString();

            CallbackUserId = userId;
            CallbackUrlRemote = callbackUrlRemote;

            return Redirect(url);
        }

        [RequireHttps]
        public ActionResult Callback()
        {
            String code = Request.Params["code"].ToString();

#if DEBUG
            String callbackUrl = @"https://localhost:44302/Runkeeper/Callback";
#else
            String callbackUrl = @"https://216.195.85.166:61443/Runkeeper/Callback";
#endif

            Runkeeper.API.AccessTokenManager authenticator = new Runkeeper.API.AccessTokenManager(SettingsHelper.RunkeeperConsumerKey,
                                                                                    SettingsHelper.RunkeeperConsumerSecret,
                                                                                    callbackUrl);

            authenticator.InitAccessToken(code);

            Runkeeper.API.Endpoints.UsersEndpoint user = new Runkeeper.API.Endpoints.UsersEndpoint(authenticator);
            var userId = user.GetUser().UserID;

            // For demo, put this in the session managed by ASP.NET
            Session["RunkeeperAccessToken"] = authenticator.Token.AccessToken;
            Session["RunkeeperRefreshToken"] = authenticator.Token.AccessToken;
            Session["RunkeeperClientId"] = userId;

            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();

                Client clientDb = repo.Get(CallbackUserId);

                clientDb.RunkeeperId = userId.ToString();
                clientDb.RunkeeperAccessToken = authenticator.Token.AccessToken;
                clientDb.RunkeeperRefreshToken = authenticator.Token.AccessToken;

                repo.Save(clientDb);

                uow.Commit();

                return RedirectToAction("Index", "Connectors", new
                {
                    accessToken = clientDb.AccessToken,
                    callbackUrl = CallbackUrlRemote
                });
            }
        }

        public ActionResult Disconnect(Int64 userId, String token)
        {
            String deAuthURL = "https://runkeeper.com/apps/de-authorize";
            String accessToken;
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                Client clientDb = repo.GetByAccessToken(token);
                accessToken = clientDb.RunkeeperAccessToken;
            }
            RestClient client = new RestClient(deAuthURL);

            var request = new RestRequest(
                    string.Format(
                    "?access_token={0}", accessToken
                    ), Method.POST);

            var t = client.Execute(request);

            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                repo.ClearRunkeeper(userId);

                return RedirectToAction("Index", "Connectors", new { accessToken = token });
            }
        }

        public ActionResult GetData()
        {
#if DEBUG
            String callbackUrl = @"https://localhost:44302/Runkeeper/GetData";
#else
            String callbackUrl = @"https://216.195.85.166:61443/Runkeeper/GetData";
#endif
            String accessToken;
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                Client clientDb = repo.GetByUserId(3)[0];
                accessToken = clientDb.RunkeeperAccessToken;
            }
            AccessTokenManager authenticator = new AccessTokenManager(SettingsHelper.RunkeeperConsumerKey,
                                                                                    SettingsHelper.RunkeeperConsumerSecret,
                                                                                    callbackUrl,
                                                                                    accessToken);

            UsersEndpoint endPointUser = new UsersEndpoint(authenticator);
            var user = endPointUser.GetUser();

            FitnessActivitiesEndpoint fitnessActivitiesEndpoint = new FitnessActivitiesEndpoint(authenticator, user);
            FeedModel<FitnessActivitiesFeedItemModel> fitnessActivitiesFeedModel = fitnessActivitiesEndpoint.GetFeedPage();
            foreach (FitnessActivitiesFeedItemModel item in fitnessActivitiesFeedModel.Items)
            {
                FitnessActivitiesPastModel fitnessActivitiesModel = fitnessActivitiesEndpoint.GetActivity(item.Uri);

                var fitnessActivitiesRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IFitnessActivitiesRepo>();
                fitnessActivitiesRepo.SaveActivitySummary(user.UserID.ToString(), fitnessActivitiesModel.StartTime, fitnessActivitiesModel);
            }

            BackgroundActivitiesEndpoint backgroundActivitiesEndpoint = new BackgroundActivitiesEndpoint(authenticator, user);
            FeedModel<BackgroundActivitiesFeedItemModel> backgroundActivitiesFeedModel = backgroundActivitiesEndpoint.GetFeedPage();
            foreach (BackgroundActivitiesFeedItemModel item in backgroundActivitiesFeedModel.Items)
            {
                BackgroundActivitiesPastModel backgroundActivitiesModel = backgroundActivitiesEndpoint.GetActivity(item.Uri);

                var backgroundActivitiesRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IBackgroundActivitiesRepo>();
                backgroundActivitiesRepo.SaveBackgroundActivitie(user.UserID.ToString(), backgroundActivitiesModel.Timestamp, backgroundActivitiesModel);
            }

            DiabetesMeasurementsEndpoint diabetesMeasurementsEndpoint = new DiabetesMeasurementsEndpoint(authenticator, user);
            FeedModel<DiabetesMeasurementsFeedItemModel> diabetesMeasurementsFeedModel = diabetesMeasurementsEndpoint.GetFeedPage();
            foreach (DiabetesMeasurementsFeedItemModel item in diabetesMeasurementsFeedModel.Items)
            {
                DiabetesMeasurementsPastModel diabetesMeasurementsModel = diabetesMeasurementsEndpoint.GetMeasurement(item.Uri);

                var diabetesMeasurementsRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IDiabetesMeasurementsRepo>();
                diabetesMeasurementsRepo.SaveDiabetesMeasurement(user.UserID.ToString(), diabetesMeasurementsModel.Timestamp, diabetesMeasurementsModel);
            }

            StrengthTrainingActivitiesEndpoint strengthTrainingEndpoint = new StrengthTrainingActivitiesEndpoint(authenticator, user);
            FeedModel<StrengthTrainingActivitiesFeedItemModel> strengthTrainingFeedModel = strengthTrainingEndpoint.GetFeedPage();
            foreach (StrengthTrainingActivitiesFeedItemModel item in strengthTrainingFeedModel.Items)
            {
                StrengthTrainingActivitiesPastModel strengthTrainingModel = strengthTrainingEndpoint.GetActivity(item.Uri);

                var strengthTrainingRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IStrengthTrainingRepo>();
                strengthTrainingRepo.SaveStrengthTraining(user.UserID.ToString(), strengthTrainingModel.StartTime, strengthTrainingModel);
            }

            NutritionEndpoint nutritionEndpoint = new NutritionEndpoint(authenticator, user);
            FeedModel<NutritionFeedItemModel> nutritionFeedModel = nutritionEndpoint.GetFeedPage();
            foreach (NutritionFeedItemModel item in nutritionFeedModel.Items)
            {
                NutritionPastModel nutritionModel = nutritionEndpoint.GetNutrition(item.Uri);

                var nutritionRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<INutritionRepo>();
                nutritionRepo.SaveNutrition(user.UserID.ToString(), nutritionModel.Timestamp, nutritionModel);
            }

            GeneralMeasurementsEndpoint generalMeasurementsEndpoint = new GeneralMeasurementsEndpoint(authenticator, user);
            FeedModel<GeneralMeasurementsFeedItemModel> generalMeasurementsFeedModel = generalMeasurementsEndpoint.GetFeedPage();
            foreach (GeneralMeasurementsFeedItemModel item in generalMeasurementsFeedModel.Items)
            {
                GeneralMeasurementsPastModel generalMeasurementsModel = generalMeasurementsEndpoint.GetMeasurement(item.Uri);

                var generalMeasurementsRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IGeneralMeasurementsRepo>();
                generalMeasurementsRepo.SaveGeneralMeasurement(user.UserID.ToString(), generalMeasurementsModel.Timestamp, generalMeasurementsModel);
            }

            ProfileEndpoint profileEndpoint = new ProfileEndpoint(authenticator, user);
            ProfileModel profileModel =  profileEndpoint.GetProfile();

            var profileRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IProfileRepo>();
            profileRepo.SaveProfile(user.UserID.ToString(), profileModel);

            SleepEndpoint sleepEndpoint = new SleepEndpoint(authenticator, user);
            FeedModel<SleepFeedItemModel> sleepFeedModel = sleepEndpoint.GetFeedPage();
            foreach (SleepFeedItemModel item in sleepFeedModel.Items)
            {
                SleepPastModel sleepModel = sleepEndpoint.GetSleep(item.Uri);

                var sleepRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<ISleepRepo>();
                sleepRepo.SaveSleep(user.UserID.ToString(), sleepModel.Timestamp, sleepModel);
            }

            WeightEndpoint weightEndpoint = new WeightEndpoint(authenticator, user);
            FeedModel<WeightFeedItemModel> weightFeedModel = weightEndpoint.GetFeedPage();
            foreach (WeightFeedItemModel item in weightFeedModel.Items)
            {
                WeightPastModel weightModel = weightEndpoint.GetWeight(item.Uri);

                var weightRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IWeightRepo>();
                weightRepo.SaveWeight(user.UserID.ToString(), weightModel.Timestamp, weightModel);
            }

            return null;
        }

        private String _runkeeperUserId
        {
            get
            {
                if (Session["RunkeeperClientId"] != null)
                {
                    return Session["RunkeeperClientId"].ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }
    }
}