﻿using FitTracker.Data;
using FitTracker.Data.Entities.Auth;
using FitTracker.Data.Helpers;
using FitTracker.Data.Repo.Auth;
using FitTracker.Web2.Core;
using System;
using System.Web.Mvc;

namespace FitTracker.Web2.Controllers
{
    public class JawboneController : BaseController
    {
        public ActionResult Authorize(Int64 userId, String callbackUrlRemote)
        {
            Jawbone.Api.Authenticator authenticator = new Jawbone.Api.Authenticator(SettingsHelper.JawboneConsumerKey,
                                                                                    SettingsHelper.JawboneConsumerSecret,
                                                                                    "/auth/oauth2/auth",
                                                                                    "/auth/oauth2/token",
                                                                                    "/auth/oauth2/token");

            #if DEBUG
            String callbackUrl = @"https://localhost:44302/Jawbone/Callback";
            #else
            String callbackUrl = @"https://216.195.85.166:61443/Jawbone/Callback";
            #endif

            String codeUrl = authenticator.GetCodeUrl(callbackUrl);

            CallbackUserId = userId;
            CallbackUrlRemote = callbackUrlRemote;

            return Redirect(codeUrl);
        }

        [RequireHttps]
        public ActionResult Callback()
        {
            String code = Request.Params["code"].ToString();

            Jawbone.Api.Authenticator authenticator = new Jawbone.Api.Authenticator(SettingsHelper.JawboneConsumerKey,
                                                                                    SettingsHelper.JawboneConsumerSecret,
                                                                                    "/auth/oauth2/auth",
                                                                                    "/auth/oauth2/token",
                                                                                    "/auth/oauth2/token");

            Jawbone.Api.AccessToken accessTokenUrl = authenticator.GetAccessToken(code);

            Jawbone.Api.JawboneClient client = FitTrackerClients.GetJawboneClientByToken(accessTokenUrl.access_token);
            var accountInfo = client.GetAccountInfo();

            // For demo, put this in the session managed by ASP.NET
            Session["JawboneAccessToken"] = accessTokenUrl.access_token;
            Session["JawboneRefreshToken"] = accessTokenUrl.refresh_token;
            Session["JawboneClientId"] = accountInfo.data.xid;

            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();

                Client clientDb = repo.Get(CallbackUserId);

                clientDb.JawboneId = accountInfo.data.xid;
                clientDb.JawboneAccessToken = accessTokenUrl.access_token;
                clientDb.JawboneRefreshToken = accessTokenUrl.refresh_token;

                repo.Save(clientDb);

                uow.Commit();

                return RedirectToAction("Index", "Connectors", new
                {
                    accessToken = clientDb.AccessToken,
                    callbackUrl = CallbackUrlRemote
                });
            }
        }

        public ActionResult Disconnect(Int64 userId, String token)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                repo.ClearJawbone(userId);

                return RedirectToAction("Index", "Connectors", new { accessToken = token });
            }
        }

        private String _jawboneUserId
        {
            get
            {
                if (Session["JawboneClientId"] != null)
                {
                    return Session["JawboneClientId"].ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }
    }
}