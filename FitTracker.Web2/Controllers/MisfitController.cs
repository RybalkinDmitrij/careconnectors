﻿using FitTracker.Data;
using FitTracker.Data.Entities.Auth;
using FitTracker.Data.Helpers;
using FitTracker.Data.Repo.Auth;
using FitTracker.Data.Repo.Misfit;
using FitTracker.Data.Repo.Strava;
using FitTracker.Web2.Core;
using FitTracker.Web2.Models.Home;
using RestSharp;
using Strava.Api.Authentication;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FitTracker.Web2.Controllers
{
    public class MisfitController : BaseController
    {
        public ActionResult Authorize(Int64 userId, String callbackUrlRemote)
        {
            Misfit.Api.Authenticator authenticator = new Misfit.Api.Authenticator(SettingsHelper.MisfitConsumerKey,
                                                                                    SettingsHelper.MisfitConsumerSecret,
                                                                                    "/auth/dialog/authorize",
                                                                                    "/auth/tokens/exchange",
                                                                                    "/auth/tokens/exchange");

            #if DEBUG
            String callbackUrl = @"https://localhost:44302/Misfit/Callback";
            #else
            String callbackUrl = @"https://216.195.85.166:61443/Misfit/Callback";
            #endif

            String codeUrl = authenticator.GetCodeUrl(callbackUrl);

            CallbackUserId = userId;
            CallbackUrlRemote = callbackUrlRemote;

            return Redirect(codeUrl);
        }

        [RequireHttps]
        public ActionResult Callback()
        {
            String code = Request.Params["code"].ToString();

            Misfit.Api.Authenticator authenticator = new Misfit.Api.Authenticator(SettingsHelper.MisfitConsumerKey,
                                                                                   SettingsHelper.MisfitConsumerSecret,
                                                                                   "/auth/dialog/authorize",
                                                                                   "/auth/tokens/exchange",
                                                                                   "/auth/tokens/exchange");

            #if DEBUG
            String callbackUrl = @"https://localhost:44302/Misfit/Callback";
            #else
            String callbackUrl = @"https://216.195.85.166:61443/Misfit/Callback";
            #endif

            Misfit.Api.AccessToken accessTokenUrl = authenticator.GetAccessToken(code, callbackUrl);

            Misfit.Api.MisfitClient client = FitTrackerClients.GetMisfitClientByToken(accessTokenUrl.access_token);
            var accountInfo = client.GetProfileInfo();

            Session["MisfitAccessToken"] = accessTokenUrl.access_token;
            Session["MisfitRefreshToken"] = accessTokenUrl.access_token;
            Session["MisfitClientId"] = accountInfo.userId;

            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();

                Client clientDb = repo.Get(CallbackUserId);

                clientDb.MisfitId = accountInfo.userId;
                clientDb.MisfitAccessToken = accessTokenUrl.access_token;
                clientDb.MisfitRefreshToken = accessTokenUrl.access_token;

                repo.Save(clientDb);

                uow.Commit();

                return RedirectToAction("Index", "Connectors", new
                {
                    accessToken = clientDb.AccessToken,
                    callbackUrl = CallbackUrlRemote
                });
            }
        }


        public ActionResult Disconnect(Int64 userId, String token)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                repo.ClearMisfit(userId);

                return RedirectToAction("Index", "Connectors", new { accessToken = token });
            }
        }

        public ActionResult GetData()
        {
            String accessToken;
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IClientRepo>();
                Client clientDb = repo.GetByUserId(3)[0];
                accessToken = clientDb.MisfitAccessToken;
            }

            Misfit.Api.MisfitClient client = FitTrackerClients.GetMisfitClientByToken(accessToken);

            Misfit.Api.Models.ProfileInfo profileInfo = client.GetProfileInfo();
            var profileInfoRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IProfileInfoRepo>();
            var profile = profileInfoRepo.SaveProfile(profileInfo);

            Misfit.Api.Models.Device device = client.GetDevice();
            var deviceRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IDeviceRepo>();
            deviceRepo.SaveDevice(profile.UserId, device);

            Misfit.Api.Models.Goals goals = client.GetGoals(new DateTime(2015, 7, 26).ToString("yyyy-MM-dd"), new DateTime(2015, 7, 28).ToString("yyyy-MM-dd"));
            var goalRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<IGoalRepo>();
            goalRepo.SaveGoal(profile.UserId, goals);

            Misfit.Api.Models.Sessions sessions = client.GetSessions(new DateTime(2015, 7, 25).ToString("yyyy-MM-dd"), new DateTime(2015, 7, 29).ToString("yyyy-MM-dd"));
            var sessionRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<ISessionRepo>();
            sessionRepo.SaveSession(profile.UserId, sessions);

            Misfit.Api.Models.Sleeps sleeps = client.GetSleeps(new DateTime(2015, 7, 26).ToString("yyyy-MM-dd"), new DateTime(2015, 7, 28).ToString("yyyy-MM-dd"));
            var sleepRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<ISleepRepo>();
            sleepRepo.SaveSleep(profile.UserId, sleeps);

            Misfit.Api.Models.Summaries summaries = client.GetSummaries(new DateTime(2015, 7, 26).ToString("yyyy-MM-dd"), new DateTime(2015, 7, 28).ToString("yyyy-MM-dd"));
            var summaryRepo = UnityManager.Resolve<IUnitOfWork>().GetRepo<ISummaryRepo>();
            summaryRepo.SaveSummary(profile.UserId, summaries);

            return null;
        }

        private String _misfitUserId
        {
            get
            {
                if (Session["MisfitClientId"] != null)
                {
                    return Session["MisfitClientId"].ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }
    }
}