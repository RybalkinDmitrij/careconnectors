﻿using FitTracker.Data;
using FitTracker.Data.Repo.Auth;
using FitTracker.Web2.Core;
using FitTracker.Web2.Core.Attributes;
using FitTracker.Web2.Models.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using RepoFitbit = FitTracker.Data.Repo.Fitbit;
using RepoWithings = FitTracker.Data.Repo.Withings;
using RepoJawbone = FitTracker.Data.Repo.Jawbone;
using RepoRunkeeper = FitTracker.Data.Repo.Runkeeper;

namespace FitTracker.Web2.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                IndexModel result = new IndexModel();

                var repoClient = uow.GetRepo<IClientRepo>();

                var clients = repoClient.GetByUserId(AuthUser.UserId).ToList();

                if (clients == null) clients = new List<Data.Entities.Auth.Client>();

                result.Clients = clients
                    .Select(x => new ClientModel()
                    {
                        ApplicantId = x.ApplicantId,
                        FitbitId = x.FitbitId,
                        FitbitAccessToken = x.FitbitAccessToken,
                        FitbitSecret = x.FitbitSecret,
                        WithingsId = x.WithingsId,
                        WithingsAccessToken = x.WithingsAccessToken,
                        WithingsSecret = x.WithingsSecret,
                        JawboneId = x.JawboneId,
                        JawboneAccessToken = x.JawboneAccessToken,
                        JawboneRefreshToken = x.JawboneRefreshToken,
                        RunkeeperId = x.RunkeeperId,
                        RunkeeperAccessToken = x.RunkeeperAccessToken,
                        RunkeeperRefreshToken = x.RunkeeperRefreshToken,
                        StravaId = x.StravaId,
                        StravaAccessToken = x.StravaAccessToken,
                        StravaRefreshToken = x.RunkeeperRefreshToken,
                        MisfitId = x.MisfitId,
                        MisfitAccessToken = x.MisfitAccessToken,
                        MisfitRefreshToken = x.MisfitRefreshToken
                    })
                    .ToList();

                return View(result);
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult TotalFitbit(String userId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repoActiveLogs = uow.GetRepo<RepoFitbit.IActivityLogRepo>();
                var repoActivitySummary = uow.GetRepo<RepoFitbit.IActivitySummaryRepo>();
                var repoActivityGoals = uow.GetRepo<RepoFitbit.IActivityGoalsRepo>();
                var repoActivityDistances = uow.GetRepo<RepoFitbit.IActivityDistanceRepo>();

                var repoBody = uow.GetRepo<RepoFitbit.IBodyRepo>();
                var repoBodyGoals = uow.GetRepo<RepoFitbit.IBodyGoalsRepo>();

                var repoFoodLogs = uow.GetRepo<RepoFitbit.IFoodLogRepo>();
                var repoFoodSummary = uow.GetRepo<RepoFitbit.IFoodSummaryRepo>();
                var repoFoodGoals = uow.GetRepo<RepoFitbit.IFoodGoalsRepo>();

                TotalFitbitModel model = new TotalFitbitModel();
                model.Message = "";

                if (userId == null)
                {
                    model.Message = "You must login!";
                    if (model.ActivityLogs == null) model.ActivityLogs = new List<Data.Entities.Fitbit.ActivityLog>();
                    if (model.ActivitySummary == null) model.ActivitySummary = new List<Data.Entities.Fitbit.ActivitySummary>();
                    if (model.ActivityGoals == null) model.ActivityGoals = new List<Data.Entities.Fitbit.ActivityGoals>();
                    if (model.ActivityDistances == null) model.ActivityDistances = new List<Data.Entities.Fitbit.ActivityDistance>();

                    if (model.Body == null) model.Body = new List<Data.Entities.Fitbit.Body>();
                    if (model.BodyGoals == null) model.BodyGoals = new List<Data.Entities.Fitbit.BodyGoals>();

                    if (model.FoodLogs == null) model.FoodLogs = new List<Data.Entities.Fitbit.FoodLog>();
                    if (model.FoodSummary == null) model.FoodSummary = new Data.Entities.Fitbit.FoodSummary();
                    if (model.FoodGoals == null) model.FoodGoals = new List<Data.Entities.Fitbit.FoodGoals>();

                    return View(model);
                }

                model.ActivityLogs = repoActiveLogs.GetByUserId(userId);
                model.ActivitySummary = repoActivitySummary.GetByUserId(userId);
                model.ActivityGoals = repoActivityGoals.GetByUserId(userId);
                //model.ActivityDistances = model.ActivitySummary != null ?
                //    repoActivityDistances.GetByActivitySummaryId(model.ActivitySummary.PkID) : new List<Data.Entities.Fitbit.ActivityDistance>();

                model.Body = repoBody.GetByUserId(userId);
                model.BodyGoals = repoBodyGoals.GetByUserId(userId);

                model.FoodLogs = repoFoodLogs.GetByUserId(userId);
                model.FoodSummary = repoFoodSummary.GetByUserId(userId);
                model.FoodGoals = repoFoodGoals.GetByUserId(userId);

                if (model.ActivityLogs == null) model.ActivityLogs = new List<Data.Entities.Fitbit.ActivityLog>();
                if (model.ActivitySummary == null) model.ActivitySummary = new List<Data.Entities.Fitbit.ActivitySummary>();
                if (model.ActivityGoals == null) model.ActivityGoals = new List<Data.Entities.Fitbit.ActivityGoals>();
                if (model.ActivityDistances == null) model.ActivityDistances = new List<Data.Entities.Fitbit.ActivityDistance>();

                if (model.Body == null) model.Body = new List<Data.Entities.Fitbit.Body>();
                if (model.BodyGoals == null) model.BodyGoals = new List<Data.Entities.Fitbit.BodyGoals>();

                if (model.FoodLogs == null) model.FoodLogs = new List<Data.Entities.Fitbit.FoodLog>();
                if (model.FoodSummary == null) model.FoodSummary = new Data.Entities.Fitbit.FoodSummary();
                if (model.FoodGoals == null) model.FoodGoals = new List<Data.Entities.Fitbit.FoodGoals>();

                return View(model);
            }
        }

        public ActionResult TotalWithings(String userId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repoActivityMeasure = uow.GetRepo<RepoWithings.IActivityMeasureRepo>();
                var repoBodyMeasure = uow.GetRepo<RepoWithings.IBodyMeasureRepo>();
                var repoIntradayActivity = uow.GetRepo<RepoWithings.IIntradayActivityRepo>();
                var repoSleepMeasure = uow.GetRepo<RepoWithings.ISleepMeasureRepo>();
                var repoSleepSummary = uow.GetRepo<RepoWithings.ISleepSummaryRepo>();

                TotalWithingsModel model = new TotalWithingsModel();
                model.Message = "";

                if (userId == null)
                {
                    model.Message = "You must login!";

                    model.ActivityMeasures = new List<ActivityMeasure>();
                    model.BodyMeasures = new List<BodyMeasure>();
                    model.IntradayActivities = new List<IntradayActivity>();
                    model.SleepMeasures = new List<SleepMeasure>();
                    model.SleepSummaries = new List<SleepSummaryW>();

                    return View(model);
                }

                model.ActivityMeasures = repoActivityMeasure.GetByUserId(userId)
                    .Select(x => new ActivityMeasure()
                    {
                        Date = x.Date,
                        Steps = x.Steps,
                        Distance = x.Distance,
                        Calories = x.Calories,
                        Elevation = x.Elevation,
                        Soft = x.Soft,
                        Moderate = x.Moderate,
                        Intense = x.Intense,
                        Timezone = x.Timezone
                    })
                    .ToList();

                model.BodyMeasures = repoBodyMeasure.GetByUserId(userId)
                    .Select(x => new BodyMeasure()
                    {
                        GrpId = x.GrpId,
                        Attrib = x.Attrib,
                        Date = x.Date,
                        Category = x.Category,
                        Measures = x.Measures.Select(t => new BodyMeasureItem()
                        {
                            Value = Convert.ToDecimal(t.Value * Math.Pow(10, t.Unit)),
                            Type = t.Type == 1 ? "Weight (kg)" :
                                    t.Type == 4 ? "Height (meter)" :
                                    t.Type == 5 ? "Fat Free Mass (kg)" :
                                    t.Type == 6 ? "Fat Ratio (%)" :
                                    t.Type == 8 ? "Fat Mass Weight (kg)" :
                                    t.Type == 9 ? "Diastolic Blood Pressure (mmHg)" :
                                    t.Type == 10 ? "Systolic Blood Pressure (mmHg)" :
                                    t.Type == 11 ? "Heart Pulse (bpm)" :
                                    t.Type == 54 ? "SP02(%)" : String.Empty
                        })
                        .ToList()
                    })
                    .ToList();

                model.IntradayActivities = repoIntradayActivity.GetByUserId(userId)
                    .Select(x => new IntradayActivity()
                    {
                        Name = x.Name,
                        Calories = x.Calories,
                        Duration = x.Duration,
                        Steps = x.Steps,
                        Elevation = x.Elevation,
                        Distance = x.Distance
                    })
                    .ToList();

                model.SleepMeasures = repoSleepMeasure.GetByUserId(userId)
                    .Select(x => new SleepMeasure()
                    {
                        StartDate = x.StartDate,
                        EndDate = x.EndDate,
                        State = x.State
                    })
                    .ToList();

                model.SleepSummaries = repoSleepSummary.GetByUserId(userId)
                    .Select(x => new SleepSummaryW()
                    {
                        Id = x.Id,
                        Timezone = x.Timezone,
                        Model = x.Model,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate,
                        Date = x.Date,
                        WakeUpDuration = x.WakeUpDuration,
                        LightSleepDuration = x.LightSleepDuration,
                        DeepSleepDuration = x.DeepSleepDuration,
                        RemSleepDuration = x.RemSleepDuration,
                        DurationToSleep = x.DurationToSleep,
                        DurationToWakeup = x.DurationToWakeup,
                        WakeupCount = x.WakeupCount,
                        Modified = x.Modified
                    })
                    .ToList();

                if (model.ActivityMeasures == null) model.ActivityMeasures = new List<ActivityMeasure>();
                if (model.BodyMeasures == null) model.BodyMeasures = new List<BodyMeasure>();
                if (model.IntradayActivities == null) model.IntradayActivities = new List<IntradayActivity>();
                if (model.SleepMeasures == null) model.SleepMeasures = new List<SleepMeasure>();
                if (model.SleepSummaries == null) model.SleepSummaries = new List<SleepSummaryW>();

                return View(model);
            }
        }

        public ActionResult TotalJawbone(String userId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repoBodyEvent = uow.GetRepo<RepoJawbone.IBodyEventRepo>();
                var repoCustom = uow.GetRepo<RepoJawbone.ICustomRepo>();
                var repoGoals = uow.GetRepo<RepoJawbone.IGoalsRepo>();
                var repoHeartRate = uow.GetRepo<RepoJawbone.IHeartRateRepo>();
                var repoMeals = uow.GetRepo<RepoJawbone.IMealsRepo>();
                var repoMood = uow.GetRepo<RepoJawbone.IMoodRepo>();
                var repoMoves = uow.GetRepo<RepoJawbone.IMovesRepo>();
                var repoMovesTick = uow.GetRepo<RepoJawbone.IMovesTickRepo>();
                var repoSettings = uow.GetRepo<RepoJawbone.ISettingsRepo>();
                var repoSleeps = uow.GetRepo<RepoJawbone.ISleepsRepo>();
                var repoTimezone = uow.GetRepo<RepoJawbone.ITimezoneRepo>();
                var repoWorkouts = uow.GetRepo<RepoJawbone.IWorkoutsRepo>();

                TotalJawboneModel model = new TotalJawboneModel();

                model.BodyEvents = repoBodyEvent.GetByUserId(userId);
                model.Customs = repoCustom.GetByUserId(userId);
                model.Goals = repoGoals.GetByUserId(userId);
                model.HeartRates = repoHeartRate.GetByUserId(userId);
                model.Meals = repoMeals.GetByUserId(userId);
                model.Moods = repoMood.GetByUserId(userId);
                model.Moves = repoMoves.GetByUserId(userId);
                model.MovesTicks = repoMovesTick.GetByUserId(userId);
                model.Settings = repoSettings.GetByUserId(userId);
                model.Sleeps = repoSleeps.GetByUserId(userId);
                model.Timezones = repoTimezone.GetByUserId(userId);
                model.Workouts = repoWorkouts.GetByUserId(userId);

                return View(model);
            }
        }

        public ActionResult TotalRunkeeper(String userId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repoBackgroundActivities = uow.GetRepo<RepoRunkeeper.IBackgroundActivitiesRepo>();
                var repoCaloriesMod = uow.GetRepo<RepoRunkeeper.ICaloriesRepo>();
                var repoDiabetesMeasurements = uow.GetRepo<RepoRunkeeper.IDiabetesMeasurementsRepo>();
                var repoDistances = uow.GetRepo<RepoRunkeeper.IDistancesRepo>();
                var repoExercises = uow.GetRepo<RepoRunkeeper.IExercisesRepo>();
                var repoFitnessActivities = uow.GetRepo<RepoRunkeeper.IFitnessActivitiesRepo>();
                var repoGeneralMeasurements = uow.GetRepo<RepoRunkeeper.IGeneralMeasurementsRepo>();
                var repoHeartRates = uow.GetRepo<RepoRunkeeper.IHeartRatesRepo>();
                var repoNutrition = uow.GetRepo<RepoRunkeeper.INutritionRepo>();
                var repoPath = uow.GetRepo<RepoRunkeeper.IPathRepo>();
                var repoProfile = uow.GetRepo<RepoRunkeeper.IProfileRepo>();
                var repoSets = uow.GetRepo<RepoRunkeeper.ISetsRepo>();
                var repoSleep = uow.GetRepo<RepoRunkeeper.ISleepRepo>();
                var repoStrengthTraining = uow.GetRepo<RepoRunkeeper.IStrengthTrainingRepo>();
                var repoWeight = uow.GetRepo<RepoRunkeeper.IWeightRepo>();

                TotalRunkeeperModel model = new TotalRunkeeperModel();

                model.BackgroundActivities = repoBackgroundActivities.GetByUserId(userId);
                model.CaloriesMod = repoCaloriesMod.GetByUserId(userId);
                model.DiabetesMeasurements = repoDiabetesMeasurements.GetByUserId(userId);
                model.Distances = repoDistances.GetByUserId(userId);
                model.Exercises = repoExercises.GetByUserId(userId);
                model.FitnessActivities = repoFitnessActivities.GetByUserId(userId);
                model.GeneralMeasurements = repoGeneralMeasurements.GetByUserId(userId);
                model.HeartRates = repoHeartRates.GetByUserId(userId);
                model.Nutrition = repoNutrition.GetByUserId(userId);
                model.Path = repoPath.GetByUserId(userId);
                model.Profile = repoProfile.GetByUserId(userId);
                model.Sets = repoSets.GetByUserId(userId);
                model.Sleep = repoSleep.GetByUserId(userId);
                model.StrengthTraining = repoStrengthTraining.GetByUserId(userId);
                model.Weight = repoWeight.GetByUserId(userId);

                return View(model);
            }
        }
    }
}