﻿using FitTracker.Data;
using FitTracker.Data.Entities.Auth;
using FitTracker.Data.Helpers;
using FitTracker.Data.Repo.Auth;
using FitTracker.Web2.Core;
using FitTracker.Web2.Models.Applicant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FitTracker.Web2.Controllers
{
    [Authorize]
    public class ApplicantController : BaseController
    {
        public ActionResult Index()
        {
            Int64 userId = AuthUser.UserId;

            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                IndexModel result = new IndexModel();

                var repoApplicant = uow.GetRepo<IApplicantRepo>();

                var applicants = repoApplicant.GetByUserId(userId).ToList();

                if (applicants == null) applicants = new List<Data.Entities.Auth.Applicant>();

                result.UserId = userId;
                result.Applicants = applicants
                    .Select(x => new ApplicantModel()
                    {
                        AppId = x.PkID,
                        Name = x.UniqueId,
                        ConsumerKey = x.ConsumerKey,
                        ConsumerSecret = x.ConsumerSecret,
                        NotificationUrl = x.NotificationUrl
                    })
                    .ToList();

                return View(result);
            }
        }

        [HttpGet]
        public ActionResult Register(Int64 userId)
        {
            RegisterModel model = new RegisterModel();
            model.UserId = userId;

            return View(model);
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repoApplicant = uow.GetRepo<IApplicantRepo>();

                    var app = new Applicant();

                    app.UniqueId = model.AppName;
                    app.UserId = model.UserId;
                    app.ConsumerKey = Crypto.DoConsumerKey();
                    app.ConsumerSecret = Crypto.DoConsumerSecret();
                    app.NotificationUrl = model.NotificationUrl;

                    repoApplicant.Save(app);

                    uow.Commit();

                    return RedirectToAction("Index", new { userId = model.UserId });
                }
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Update(Int64 appId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repoApplicant = uow.GetRepo<IApplicantRepo>();

                var app = repoApplicant.Get(appId);
                
                RegisterModel model = new RegisterModel();
                model.UserId = app.UserId;
                model.AppName = app.UniqueId;
                model.NotificationUrl = app.NotificationUrl;

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Update(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repoApplicant = uow.GetRepo<IApplicantRepo>();

                    var app = repoApplicant.Get(model.AppId);

                    app.UniqueId = model.AppName;
                    app.NotificationUrl = model.NotificationUrl;

                    repoApplicant.Save(app);

                    uow.Commit();

                    return RedirectToAction("Index", new { userId = model.UserId });
                }
            }
            else
            {
                return View(model);
            }
        }

        public ActionResult Delete(Int64 appId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repoApplicant = uow.GetRepo<IApplicantRepo>();

                repoApplicant.Remove(appId);
                uow.Commit();

                return RedirectToAction("Index");
            }
        }
    }
}