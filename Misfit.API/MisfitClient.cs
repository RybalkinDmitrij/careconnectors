using System;
using Misfit.Api.Models;
using RestSharp;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace Misfit.Api
{
    public class MisfitClient : IMisfitClient
    {
        private readonly String _accessToken;
        private readonly IRestClient _restClient;

        private const String BaseApiUrl = "https://api.misfitwearables.com/move/resource/";
        private const String Version = "v1";

        public MisfitClient(IRestClient restClient)
        {
            this._restClient = restClient;
        }
        
        public MisfitClient(string accessToken)
        {
            this._accessToken = accessToken;
            this._restClient = new RestClient(BaseApiUrl);
        }

        public MisfitClient(string accessToken, IRestClient restClient)
        {
            this._accessToken = accessToken;
            this._restClient = restClient;
        }

        public ProfileInfo GetProfileInfo()
        {
            var request = new RestRequest(String.Format("/{0}/user/me/profile", Version), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute(request).Content;
            JavaScriptSerializer jss = new JavaScriptSerializer();
            ProfileInfo profileInfo = jss.Deserialize<ProfileInfo>(response);

            if (profileInfo.userId == null)
            {
                throw new Exception("Wrong request to Misfit API");
            }

            return profileInfo;
        }

        public Device GetDevice()
        {
            var request = new RestRequest(String.Format("/{0}/user/me/device", Version), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute(request);
            JavaScriptSerializer jss = new JavaScriptSerializer();
            Device device = jss.Deserialize<Device>(response.Content);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Misfit API");
            }

            return device;
        }

        public Goals GetGoals(String startDate, String endDate)
        {
            var request = new RestRequest(String.Format("/{0}/user/me/activity/goals?start_date={1}&end_date={2}", Version, startDate, endDate), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute(request);
            JavaScriptSerializer jss = new JavaScriptSerializer();
            Goals goals = jss.Deserialize<Goals>(response.Content);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Misfit API");
            }

            return goals;
        }

        public Sessions GetSessions(String startDate, String endDate)
        {
            var request = new RestRequest(String.Format("/{0}/user/me/activity/sessions?start_date={1}&end_date={2}", Version, startDate, endDate), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute(request);
            JavaScriptSerializer jss = new JavaScriptSerializer();
            Sessions sessions = jss.Deserialize<Sessions>(response.Content);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Misfit API");
            }

            return sessions;
        }

        public Sleeps GetSleeps(String startDate, String endDate)
        {
            var request = new RestRequest(String.Format("/{0}/user/me/activity/sleeps?start_date={1}&end_date={2}", Version, startDate, endDate), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute(request);
            JavaScriptSerializer jss = new JavaScriptSerializer();
            Sleeps sleeps = jss.Deserialize<Sleeps>(response.Content);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Misfit API");
            }

            return sleeps;
        }

        public Summaries GetSummaries(String startDate, String endDate)
        {
            var request = new RestRequest(String.Format("/{0}/user/me/activity/summary?start_date={1}&end_date={2}&detail=true", Version, startDate, endDate), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute(request);
            JavaScriptSerializer jss = new JavaScriptSerializer();
            Summaries summaries = jss.Deserialize<Summaries>(response.Content);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Misfit API");
            }

            return summaries;
        }
    }
}
