﻿using Misfit.Api.Models;
using System;
using System.Collections.Generic;

namespace Misfit.Api
{
    public interface IMisfitClient
    {
        ProfileInfo GetProfileInfo();

        Device GetDevice();

        Goals GetGoals(String startDate, String endDate);

        Sessions GetSessions(String startDate, String endDate);

        Sleeps GetSleeps(String startDate, String endDate);

        Summaries GetSummaries(String startDate, String endDate);
    }
}