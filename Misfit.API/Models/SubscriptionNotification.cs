﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Misfit.Api.Models
{
    public class SubscriptionNotification
    {
        public String Type { get; set; }

        public String MessageId { get; set; }

        public String TopicArn { get; set; }

        public List<Message> Message { get; set; }

        public String Timestamp { get; set; }

        public String SignatureVersion { get; set; }

        public String Signature { get; set; }

        public String SigningCertURL { get; set; }

        public String UnsubscribeURL { get; set; }
    }
}
