﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Misfit.Api.Models
{
    public class UpdatedResource
    {
        public Int64 notification_timestamp { get; set; }

        public List<UpdatedResourceEvent> events { get; set; }
    }

    public class UpdatedResourceEvent
    {
        public String user_xid { get; set; }

        public String event_xid { get; set; }

        public String type { get; set; }

        public String action { get; set; }

        public Int64 timestamp { get; set; }

        public String secret_hash { get; set; }
    }
}
