﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Misfit.Api.Models
{
    public class ProfileInfo
    {
        public String userId { get; set; }

        public String name { get; set; }

        public String birthday { get; set; }

        public String gender { get; set; }

        public String email { get; set; }
    }
}
