﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Misfit.Api.Models
{
    public class Summary
    {
        public DateTime date { get; set; }

        public Decimal points { get; set; }

        public Int32 steps { get; set; }

        public Decimal calories { get; set; }

        public Decimal activityCalories { get; set; }

        public Decimal distance { get; set; }
    }
}
