﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Misfit.Api.Models
{
    public class Goal
    {
        public String id { get; set; }

        public DateTime date { get; set; }

        public Decimal points { get; set; }

        public Int32 targetPoints { get; set; }
    }
}
