﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Misfit.Api.Models
{
    public class Session
    {
        public String id { get; set; }

        public String activityType { get; set; }

        public DateTime startTime { get; set; }

        public Int32 duration { get; set; }

        public Decimal points { get; set; }

        public Int32 steps { get; set; }

        public Decimal calories { get; set; }

        public Decimal distance { get; set; }
    }
}
