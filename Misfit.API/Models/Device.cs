﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Misfit.Api.Models
{
    public class Device
    {
        public String id { get; set; }

        public String deviceType { get; set; }

        public String serialNumber { get; set; }

        public String firmwareVersion { get; set; }

        public Int32 batteryLevel { get; set; }

        public Int32 lastSyncTime { get; set; }
    }
}
