﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Misfit.Api.Models
{
    public class Message
    {
        public String type { get; set; }

        public String action { get; set; }

        public String id { get; set; }

        public String ownerId { get; set; }

        public DateTime updatedAt { get; set; }
    }
}
