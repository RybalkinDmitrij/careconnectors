﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Misfit.Api.Models
{
    public class Sleeps
    {
        public List<Sleep> sleeps { get; set; }
    }
}
