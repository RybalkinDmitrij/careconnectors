﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Misfit.Api.Models
{
    public class Sleep
    {
        public String id { get; set; }

        public Boolean autoDetected { get; set; }

        public DateTime startTime { get; set; }

        public Int32 duration { get; set; }

        public List<SleepDetails> sleepDetails { get; set; }
    }

    public class SleepDetails
    {
        public DateTime datetime { get; set; }

        public Int32 value { get; set; }
    }
}
