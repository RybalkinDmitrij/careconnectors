﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Contrib;

namespace Misfit.Api
{
	public class Authenticator
	{
        const string MisfitBaseUrl = "https://api.misfitwearables.com";

		private string _consumerKey;
		private string _consumerSecret;
		private string _codeUrl;
		private string _accessTokenUrl;
		private string _refreshTokenUrl;
        		
        private readonly IRestClient client;

		public Authenticator(string consumerKey, string consumerSecret, string codeUrl, string accessTokenUrl,
		                     string refreshTokenUrl, IRestClient restClient = null)
		{
            this._consumerKey = consumerKey;
            this._consumerSecret = consumerSecret;
            this._codeUrl = codeUrl;
            this._accessTokenUrl = accessTokenUrl;
            this._refreshTokenUrl = refreshTokenUrl;
            client = restClient ?? new RestClient(MisfitBaseUrl);
		}
        
        public String GetCodeUrl(String callback)
        {
            var request = new RestRequest(
                    string.Format(
                    this._codeUrl + "?response_type=code&client_id={0}&redirect_uri={1}&scope=public,birthday,email",
                    this._consumerKey, callback
                    ), Method.GET);
            
            var url = client.BuildUri(request).ToString();

            return url;
        }

        public AccessToken GetAccessToken(String code, String callback)
        {
            var request = new RestRequest(this._accessTokenUrl, Method.POST);

            request.AddParameter("grant_type", "authorization_code");
            request.AddParameter("code", code);
            request.AddParameter("redirect_uri", callback);
            request.AddParameter("client_id", this._consumerKey);
            request.AddParameter("client_secret", this._consumerSecret);

            var response = client.Execute<AccessToken>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong auth to Jawbone API");
            }

            return response.Data;
        }

        public AccessToken GetRefreshToken(String refreshToken)
        {
            var request = new RestRequest(this._refreshTokenUrl, Method.POST);

            request.AddParameter("grant_type", "refresh_token");
            request.AddParameter("refresh_token", refreshToken);
            request.AddParameter("redirect_uri", this._codeUrl);
            request.AddParameter("client_id", this._consumerKey);
            request.AddParameter("client_secret", this._consumerSecret);

            var response = client.Execute<AccessToken>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong auth to Jawbone API");
            }

            return response.Data;
        }
	}

    public class AccessToken
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string token_type { get; set; }
        public string refresh_token { get; set; }
    }
}