﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jawbone.Api.Models;
using RestSharp;
using System.Web.Script.Serialization;

namespace Jawbone.Api
{
    public class SubscriptionManager
    {
        #region singleton

        public static SubscriptionManager Instance
        {
            get
            {
                if (_instance == null) _instance = new SubscriptionManager();
                return _instance;
            }
        }

        private static SubscriptionManager _instance = null;

        #endregion singleton

        public UpdatedResource ProcessUpdateReponseBody(string bodyContent)
        {
            //var deserializer = new RestSharp.Deserializers.JsonDeserializer();
            //UpdatedResource result = deserializer.Deserialize<UpdatedResource>(new RestResponse() { Content = bodyContent });
            //return result;

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            UpdatedResource result = serializer.Deserialize<UpdatedResource>(bodyContent);

            return result;
        }

        //public void AddSubscription(JawboneClient client, String subscriptionId)
        //{
        //    client.AddSubscription(subscriptionId);
        //}

        //private string StripSignatureString(string bodyContent)
        //{
        //    string sep = "<?xml";
        //    char[] sepChars = sep.ToCharArray();
        //    bodyContent = bodyContent.Substring(bodyContent.IndexOf(sep));

        //    string lastNodeCharacter = ">";
        //    int bodyEndPosition = bodyContent.LastIndexOf(lastNodeCharacter );

        //    bodyContent = bodyContent.Substring(0, bodyEndPosition + 1);

        //    return bodyContent;

        //}
    }
}
