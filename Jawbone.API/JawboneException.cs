﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Jawbone.Api
{
    /// <summary>
    /// Custom exception class to be used to gather response data from the API when there's an error
    /// </summary>
    public class JawboneException : Exception
    {
        public HttpStatusCode HttpStatusCode { get; set; }
    }
}