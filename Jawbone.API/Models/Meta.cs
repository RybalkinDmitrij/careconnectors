﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jawbone.Api.Models
{
    public class Meta
    {
        public String user_xid { get; set; }

        public String message { get; set; }

        public Int32 code { get; set; }

        public Int64 time { get; set; }
    }
}
