﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jawbone.Api.Models
{
    public class TimeZone
    {
        public Meta meta { get; set; }

        public TimeZoneData data { get; set; }
    }

    public class TimeZoneData
    {
        public List<TimeZoneDataItem> items { get; set; }

        public Int32 size { get; set; }
    }

    public class TimeZoneDataItem
    {
        public Int64 date { get; set; }

        public String tz { get; set; }

        public Int64 time { get; set; }
    }
}
