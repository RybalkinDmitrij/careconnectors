﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jawbone.Api.Models
{
    public class Trends
    {
        public Meta meta { get; set; }

        public TrendsData data { get; set; }
    }

    public class TrendsData
    {
        public Int64 earliest { get; set; }

        public List<TrendsDataItem> data { get; set; }

        public Links links { get; set; }
    }

    public class TrendsDataItem
    {
        public List<Object> objects { get; set; }
    }

    public class TrendsDataItemVal
    {
        public Decimal weight { get; set; }

        public Decimal height { get; set; }

        public Boolean gender { get; set; }

        public Decimal age { get; set; }

        public Decimal bmr { get; set; }

        public Int32 body_fat { get; set; }

        public Decimal goal_body_weight_intent { get; set; }

        public Decimal goal_body_weight { get; set; }

        public Int32 m_steps { get; set; }

        public Decimal m_calories { get; set; }

        public Decimal m_total_calories { get; set; }

        public Int32 m_active_time { get; set; }

        public Int32 m_workout_time { get; set; }

        public Int32 m_distance { get; set; }

        public Int32 e_calories { get; set; }

        public Decimal e_carbs { get; set; }

        public Int32 e_cholesterol { get; set; }

        public Decimal e_protein { get; set; }

        public Int32 e_calcium { get; set; }

        public Decimal e_unsat_fat { get; set; }

        public Int32 e_sat_fat { get; set; }

        public Int32 e_sodium { get; set; }

        public Int32 e_sugar { get; set; }

        public Int32 e_fiber { get; set; }

        public Int32 s_bedtime { get; set; }

        public Int32 s_asleep_time { get; set; }

        public Int32 s_awake { get; set; }

        public Int32 s_awake_time { get; set; }

        public Int32 s_awakenings { get; set; }

        public Int32 s_light { get; set; }

        public Int32 s_sound { get; set; }

        public Int32 s_duration { get; set; }

        public Int32 s_quality { get; set; }
    }
}
