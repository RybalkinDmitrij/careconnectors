﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jawbone.Api.Models
{
    public class Meals
    {
        public Meta meta { get; set; }

        public MealsData data { get; set; }
    }

    public class MealsData
    {
        public List<MealsDataItem> items { get; set; }

        public Links links { get; set; }

        public Int32 size { get; set; }
    }

    public class MealsDataItem
    {
        public String xid { get; set; }

        public String title { get; set; }

        public String note { get; set; }

        public String type { get; set; }

        public Int32 sub_type { get; set; }

        public Int64 time_created { get; set; }

        public Int64 time_updated { get; set; }

        public Int64 time_completed { get; set; }

        public Int64 date { get; set; }

        public String place_lat { get; set; }

        public String place_lon { get; set; }

        public String place_acc { get; set; }

        public String place_name { get; set; }

        public MealsDataItemDetails details { get; set; }
    }

    public class MealsDataItemDetails
    {
        public Int32 num_drinks { get; set; }

        public Int32 num_water { get; set; }

        public Int32 num_foods { get; set; }

        public Boolean only_waters { get; set; }

        public Int32 num_mealitems_green { get; set; }

        public Int32 num_mealitems_yellow { get; set; }

        public Int32 num_mealitems_red { get; set; }

        public Int32 num_mealitems_with_score { get; set; }

        public Int32 fiber { get; set; }

        public Decimal polyunsaturated_fat { get; set; }

        public Decimal potassium { get; set; }

        public Decimal fat { get; set; }

        public Decimal carbohydrate { get; set; }

        public Decimal saturated_fat { get; set; }

        public Decimal protein { get; set; }

        public Decimal monounsaturated_fat { get; set; }

        public Decimal sodium { get; set; }

        public Decimal vitamin_c { get; set; }

        public Decimal vitamin_a { get; set; }

        public Decimal calories { get; set; }

        public Decimal unsaturated_fat { get; set; }

        public Decimal sugar { get; set; }

        public Decimal calcium { get; set; }

        public Decimal iron { get; set; }

        public Decimal cholesterol { get; set; }

        public Decimal caffeine { get; set; }

        public Decimal accuracy { get; set; }

        public String tz { get; set; }
    }
}
