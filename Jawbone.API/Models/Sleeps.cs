﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jawbone.Api.Models
{
    public class Sleeps
    {
        public Meta meta { get; set; }

        public SleepsData data { get; set; }
    }

    public class SleepsData
    {
        public List<SleepsDataItem> items { get; set; }

        public Links links { get; set; }

        public Int32 size { get; set; }
    }

    public class SleepsDataItem
    {
        public String xid { get; set; }

        public String title { get; set; }

        public Int32 sub_type { get; set; }

        public Int64 time_created { get; set; }

        public Int64 time_completed { get; set; }

        public Int64 date { get; set; }

        public String place_lat { get; set; }

        public String place_lon { get; set; }

        public String place_acc { get; set; }

        public String place_name { get; set; }

        public String snapshot_image { get; set; }

        public SleepsDataItemDetails details { get; set; }
    }

    public class SleepsDataItemDetails
    {
        public Int64 smart_alarm_fire { get; set; }

        public Int64 awake_time { get; set; }

        public Int64 asleep_time { get; set; }

        public Int32 awakenings { get; set; }

        public Int32 rem { get; set; }

        public Int32 light { get; set; }

        public Int32 sound { get; set; }

        public Int32 awake { get; set; }

        public Int32 duration { get; set; }

        public Int32 quality { get; set; }

        public String tz { get; set; }
    }
}
