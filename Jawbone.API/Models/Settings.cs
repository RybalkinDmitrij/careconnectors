﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jawbone.Api.Models
{
    public class Settings
    {
        public Meta meta { get; set; }

        public SettingsData data { get; set; }
    }

    public class SettingsData
    {
        public String xid { get; set; }

        public Int32 metric { get; set; }

        public Boolean share_sleep { get; set; }

        public Boolean share_mood { get; set; }

        public Boolean share_body { get; set; }

        public Boolean share_eat { get; set; }

        public SettingsDataGoals goals { get; set; }
    }

    public class SettingsDataGoals
    {
        public SettingsDataGoalsMove move { get; set; }

        public SettingsDataGoalsSleep sleep { get; set; }
    }

    public class SettingsDataGoalsMove
    {
        public Int32 steps { get; set; }
    }

    public class SettingsDataGoalsSleep
    {
        public Int32 total { get; set; }
    }
}
