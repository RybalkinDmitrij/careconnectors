﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jawbone.Api.Models
{
    public class Workouts
    {
        public Meta meta { get; set; }

        public WorkoutsData data { get; set; }
    }

    public class WorkoutsData
    {
        public List<WorkoutsDataItem> items { get; set; }

        public Links links { get; set; }

        public Int32 size { get; set; }
    }

    public class WorkoutsDataItem
    {
        public String xid { get; set; }

        public String title { get; set; }

        public String type { get; set; }

        public Int32 sub_type { get; set; }

        public Int64 time_created { get; set; }

        public Int64 time_updated { get; set; }

        public Int64 time_completed { get; set; }

        public Int64 date { get; set; }

        public String place_lat { get; set; }

        public String place_lon { get; set; }

        public String place_acc { get; set; }

        public String place_name { get; set; }

        public String route { get; set; }

        public String image { get; set; }

        public String snapshot_image { get; set; }

        public WorkoutsDataItemDetails details { get; set; }
    }

    public class WorkoutsDataItemDetails
    {
        public Int32 steps { get; set; }

        public Int32 time { get; set; }

        public Int32 bg_active_time { get; set; }

        public Int32 meters { get; set; }

        public Decimal km { get; set; }

        public Int32 intensity { get; set; }

        public Int32 calories { get; set; }

        public Decimal bmr { get; set; }

        public Decimal bg_calories { get; set; }

        public Decimal bmr_calories { get; set; }

        public String tz { get; set; }
    }
}
