﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jawbone.Api.Models
{
    public class Moves
    {
        public Meta meta { get; set; }

        public MovesData data { get; set; }
    }

    public class MovesData
    {
        public List<MovesDataItem> items { get; set; }

        public Links links { get; set; }

        public Int32 size { get; set; }
    }

    public class MovesDataItem
    {
        public String xid { get; set; }

        public String title { get; set; }

        public String type { get; set; }

        public Int64 time_created { get; set; }

        public Int64 time_updated { get; set; }

        public Int64 time_completed { get; set; }

        public Int64 date { get; set; }

        public String snapshot_image { get; set; }

        public MovesDataItemDetails details { get; set; }
    }

    public class MovesDataItemDetails
    {
        public Int32 distance { get; set; }

        public Decimal km { get; set; }

        public Int32 steps { get; set; }

        public Int32 active_time { get; set; }

        public Int32 longest_active { get; set; }

        public Int32 inactive_time { get; set; }

        public Int32 longest_idle { get; set; }

        public Decimal calories { get; set; }

        public Decimal bmr_day { get; set; }

        public Decimal bmr { get; set; }

        public Decimal bg_calories { get; set; }

        public Decimal wo_calories { get; set; }

        public Int32 wo_time { get; set; }

        public Int32 wo_active_time { get; set; }

        public Int32 wo_count { get; set; }

        public Int32 wo_longest { get; set; }

        public Int64 sunrise { get; set; }

        public Int64 sunset { get; set; }

        public String tz { get; set; }

        public List<HourlyTotal> hourly_totals { get; set; }
    }

    public class HourlyTotal
    {
        public Int32 distance { get; set; }

        public Decimal calories { get; set; }

        public Int32 steps { get; set; }

        public Int32 active_time { get; set; }

        public Int32 inactive_time { get; set; }

        public Int32 longest_active_time { get; set; }

        public Int32 longest_idle_time { get; set; }
    }
}
