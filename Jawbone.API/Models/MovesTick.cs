﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jawbone.Api.Models
{
    public class MovesTick
    {
        public Meta meta { get; set; }

        public MovesTickData data { get; set; }
    }

    public class MovesTickData
    {
        public List<MovesTickDataItem> items { get; set; }

        public Int32 size { get; set; }
    }

    public class MovesTickDataItem
    {
        public Decimal distance { get; set; }

        public Int64 time_completed { get; set; }

        public Int32 active_time { get; set; }

        public Decimal calories { get; set; }

        public Int32 steps { get; set; }

        public Int64 time { get; set; }

        public Decimal speed { get; set; }
    }
}
