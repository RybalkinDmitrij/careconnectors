﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jawbone.Api.Models
{
    public class BodyEvent
    {
        public Meta meta { get; set; }

        public BodyEventData data { get; set; }
    }

    public class BodyEventData
    {
        public List<BodyEventItem> items { get; set; }

        public Links links { get; set; }

        public Int32 size { get; set; }
    }

    public class BodyEventItem
    {
        public String xid { get; set; }

        public String title { get; set; }

        public String type { get; set; }

        public Int64 time_created { get; set; }

        public Int64 time_updated { get; set; }

        public Int64 date { get; set; }

        public String place_lat { get; set; }

        public String place_lon { get; set; }

        public String place_acc { get; set; }

        public String place_name { get; set; }

        public String note { get; set; }

        public Int32 lean_mass { get; set; }

        public Int32 weight { get; set; }

        public Int32 body_fat { get; set; }

        public Int32 bmi { get; set; }

        public DetailsTZ details { get; set; }
    }
}
