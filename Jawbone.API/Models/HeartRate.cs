﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jawbone.Api.Models
{
    public class HeartRate
    {
        public Meta meta { get; set; }

        public HeartRateData data { get; set; }
    }

    public class HeartRateData
    {
        public List<HeartRateItem> items { get; set; }

        public Links links { get; set; }

        public Int32 size { get; set; }
    }

    public class HeartRateItem
    {
        public String xid { get; set; }

        public String title { get; set; }

        public Int64 time_created { get; set; }

        public Int64 time_updated { get; set; }

        public Int64 date { get; set; }

        public String place_lat { get; set; }

        public String place_lon { get; set; }

        public String place_acc { get; set; }

        public String place_name { get; set; }

        public Int32 resting_heartrate { get; set; }

        public DetailsTZ details { get; set; }
    }
}
