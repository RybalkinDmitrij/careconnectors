﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jawbone.Api.Models
{
    public class Goals
    {
        public Meta meta { get; set; }

        public GoalsData data { get; set; }
    }

    public class GoalsData
    {
        public Int32 move_steps { get; set; }

        public Int32 sleep_total { get; set; }

        public Decimal body_weight { get; set; }

        public Decimal body_weight_intent { get; set; }

        public GoalsDataRemainingForDay remaining_for_day { get; set; }
    }

    public class GoalsDataRemainingForDay
    {
        public Decimal intake_calories_remaining { get; set; }

        public Int32 move_steps_remaining { get; set; }

        public Int32 sleep_seconds_remaining { get; set; }
    }
}
