﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jawbone.Api.Models
{
    public class Custom
    {
        public Meta meta { get; set; }

        public CustomData data { get; set; }
    }

    public class CustomData
    {
        public List<CustomDataItem> items { get; set; }

        public Links links { get; set; }

        public Int32 size { get; set; }
    }

    public class CustomDataItem
    {
        public String xid { get; set; }

        public String title { get; set; }

        public String type { get; set; }

        public Int64 time_created { get; set; }

        public Int64 time_updated { get; set; }

        public Int64 date { get; set; }

        public String place_lat { get; set; }

        public String place_lon { get; set; }

        public String place_acc { get; set; }

        public String place_name { get; set; }

        public String image { get; set; }

        public CustomDataItemAttributes attributes { get; set; }

        public DetailsTZ details { get; set; }
    }

    public class CustomDataItemAttributes
    {
        public String description { get; set; }
    }
}
