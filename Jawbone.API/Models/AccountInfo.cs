﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jawbone.Api.Models
{
    public class AccountInfo
    {
        public Meta meta { get; set; }

        public AccountInfoData data { get; set; }
    }

    public class AccountInfoData
    {
        public String xid { get; set; }

        public String first { get; set; }

        public String last { get; set; }

        public String image { get; set; }

        public Decimal weight { get; set; }

        public Decimal height { get; set; }
    }
}
