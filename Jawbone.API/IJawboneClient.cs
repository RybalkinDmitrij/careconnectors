﻿using Jawbone.Api.Models;
using System;
using System.Collections.Generic;

namespace Jawbone.Api
{
    public interface IJawboneClient
    {
        AccountInfo GetAccountInfo();

        Moves GetMoves(DateTime date);

        Sleeps GetSleeps(DateTime date);

        BodyEvent GetBodyEvent(DateTime date);

        Workouts GetWorkouts(DateTime date);

        Meals GetMeals(DateTime date);

        Mood GetMood(DateTime date);
    }
}