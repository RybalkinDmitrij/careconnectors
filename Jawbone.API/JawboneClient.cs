using System;
using Jawbone.Api.Models;
using RestSharp;

namespace Jawbone.Api
{
    public class JawboneClient : IJawboneClient
    {
        private readonly String _accessToken;
        private readonly IRestClient _restClient;

        private const String BaseApiUrl = "https://jawbone.com/nudge/api";
        private const String Version = "v.1.1";

        public JawboneClient(IRestClient restClient)
        {
            this._restClient = restClient;
        }
        
        public JawboneClient(string accessToken)
        {
            this._accessToken = accessToken;
            this._restClient = new RestClient(BaseApiUrl);
        }

        public JawboneClient(string accessToken, IRestClient restClient)
        {
            this._accessToken = accessToken;
            this._restClient = restClient;
        }

        public AccountInfo GetAccountInfo()
        {
            var request = new RestRequest(String.Format("/{0}/users/@me", Version), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute<AccountInfo>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Jawbone API");
            }

            return response.Data;
        }

        public BodyEvent GetBodyEvent(DateTime date)
        {
            var request = new RestRequest(String.Format("/{0}/users/@me/body_events?date={1}", Version, date.ToString("yyyyMMdd")), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute<BodyEvent>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Jawbone API");
            }

            return response.Data;
        }

        public HeartRate GetHeartRate()
        {
            var request = new RestRequest(String.Format("/{0}/users/@me/heartrates", Version), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute<HeartRate>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Jawbone API");
            }

            return response.Data;
        }

        public Custom GetCustom()
        {
            var request = new RestRequest(String.Format("/{0}/users/@me/generic_events", Version), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute<Custom>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Jawbone API");
            }

            return response.Data;
        }

        public Goals GetGoals()
        {
            var request = new RestRequest(String.Format("/{0}/users/@me/goals", Version), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute<Goals>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Jawbone API");
            }

            return response.Data;
        }

        public Meals GetMeals(DateTime date)
        {
            var request = new RestRequest(String.Format("/{0}/users/@me/meals?date={1}", Version, date.ToString("yyyyMMdd")), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute<Meals>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Jawbone API");
            }

            return response.Data;
        }

        public Mood GetMood(DateTime date)
        {
            var request = new RestRequest(String.Format("/{0}/users/@me/mood?date={1}", Version, date.ToString("yyyyMMdd")), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute<Mood>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Jawbone API");
            }

            return response.Data;
        }

        public Moves GetMoves(DateTime date)
        {
            var request = new RestRequest(String.Format("/{0}/users/@me/moves?date={1}", Version, date.ToString("yyyyMMdd")), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute<Moves>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Jawbone API");
            }

            return response.Data;
        }

        public Settings GetSettings()
        {
            var request = new RestRequest(String.Format("/{0}/users/@me/settings", Version), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute<Settings>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Jawbone API");
            }

            return response.Data;
        }

        public Sleeps GetSleeps(DateTime date)
        {
            var request = new RestRequest(String.Format("/{0}/users/@me/sleeps?date={1}", Version, date.ToString("yyyyMMdd")), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute<Sleeps>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Jawbone API");
            }

            return response.Data;
        }

        public Jawbone.Api.Models.TimeZone GetTimeZone()
        {
            var request = new RestRequest(String.Format("/{0}/users/@me/timezone", Version), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute<Jawbone.Api.Models.TimeZone>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Jawbone API");
            }

            return response.Data;
        }

        public Trends GetTrends()
        {
            var request = new RestRequest(String.Format("/{0}/users/@me/trends", Version), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute<Trends>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Jawbone API");
            }

            return response.Data;
        }

        public Workouts GetWorkouts(DateTime date)
        {
            var request = new RestRequest(String.Format("/{0}/users/@me/workouts?date={1}", Version, date.ToString("yyyyMMdd")), Method.GET);

            request.AddParameter(
                "Authorization",
                String.Format("Bearer {0}", this._accessToken), ParameterType.HttpHeader);

            var response = _restClient.Execute<Workouts>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong request to Jawbone API");
            }

            return response.Data;
        }
    }
}
