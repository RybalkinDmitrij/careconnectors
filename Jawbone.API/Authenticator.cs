﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Contrib;
using Jawbone.Api.Models;

namespace Jawbone.Api
{
	public class Authenticator
	{
		const string JawboneBaseUrl = "https://jawbone.com";

		private string _consumerKey;
		private string _consumerSecret;
		private string _codeUrl;
		private string _accessTokenUrl;
		private string _refreshTokenUrl;
        		
        private readonly IRestClient client;

		public Authenticator(string consumerKey, string consumerSecret, string codeUrl, string accessTokenUrl,
		                     string refreshTokenUrl, IRestClient restClient = null)
		{
            this._consumerKey = consumerKey;
            this._consumerSecret = consumerSecret;
            this._codeUrl = codeUrl;
            this._accessTokenUrl = accessTokenUrl;
            this._refreshTokenUrl = refreshTokenUrl;
            client = restClient ?? new RestClient(JawboneBaseUrl);
		}
        
        public String GetCodeUrl(String callback)
        {
            var request = new RestRequest(
                    string.Format(
                    this._codeUrl + "?response_type=code&client_id={0}&scope=basic_read%20extended_read%20location_read%20friends_read%20mood_read%20move_read%20sleep_read%20meal_read%20weight_read%20generic_event_read%20heartrate_read&redirect_uri={1}",
                    this._consumerKey, callback
                    ), Method.POST);
            
            var url = client.BuildUri(request).ToString();

            return url;
        }

        public AccessToken GetAccessToken(String code)
        {
            var request = new RestRequest(this._accessTokenUrl, Method.POST);

            request.AddParameter("grant_type", "authorization_code");
            request.AddParameter("code", code);
            request.AddParameter("client_id", this._consumerKey);
            request.AddParameter("client_secret", this._consumerSecret);

            var response = client.Execute<AccessToken>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong auth to Jawbone API");
            }

            return response.Data;
        }

        public AccessToken GetRefreshToken(String refreshToken)
        {
            var request = new RestRequest(this._refreshTokenUrl, Method.POST);

            request.AddParameter("grant_type", "refresh_token");
            request.AddParameter("refresh_token", refreshToken);
            request.AddParameter("client_id", this._consumerKey);
            request.AddParameter("client_secret", this._consumerSecret);

            var response = client.Execute<AccessToken>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Wrong auth to Jawbone API");
            }

            return response.Data;
        }
	}

    public class AccessToken
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string token_type { get; set; }
        public string refresh_token { get; set; }
    }
}