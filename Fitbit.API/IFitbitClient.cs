﻿using System;
using System.Collections.Generic;

namespace Fitbit.Api
{
    public interface IFitbitClient
    {
        Fitbit.Api.Models.ApiSubscription AddSubscription(Fitbit.Api.Models.APICollectionType apiCollectionType, string uniqueSubscriptionId);
        Fitbit.Api.Models.ApiSubscription AddSubscription(Fitbit.Api.Models.APICollectionType apiCollectionType, string uniqueSubscriptionId, string subscriberId);
        DateTime? GetActivityTrackerFirstDay();
        List<Fitbit.Api.Models.TrackerAlarm> GetAlarms(string deviceId);
        Fitbit.Api.Models.BloodPressureData GetBloodPressure(DateTime date);
        Fitbit.Api.Models.BloodPressureData GetBloodPressure(DateTime date, string userId);
        Fitbit.Api.Models.BodyMeasurements GetBodyMeasurements(DateTime date);
        Fitbit.Api.Models.BodyMeasurements GetBodyMeasurements(DateTime date, string userId);
        Fitbit.Api.Models.Activity GetDayActivity(DateTime activityDate);
        Fitbit.Api.Models.ActivitySummary GetDayActivitySummary(DateTime activityDate);
        System.Collections.Generic.List<Fitbit.Api.Models.Device> GetDevices();
        Fitbit.Api.Models.Fat GetFat(DateTime startDate, Fitbit.Api.Models.DateRangePeriod period);
        Fitbit.Api.Models.Fat GetFat(DateTime startDate, DateTime? endDate = null);
        Fitbit.Api.Models.Food GetFood(DateTime date, string userId = null);
        System.Collections.Generic.List<Fitbit.Api.Models.UserProfile> GetFriends();
        Fitbit.Api.Models.IntradayData GetIntraDayTimeSeries(Fitbit.Api.Models.IntradayResourceType timeSeriesResourceType, DateTime dayAndStartTime, TimeSpan intraDayTimeSpan);
        Fitbit.Api.Models.SleepData GetSleep(DateTime sleepDate, String userId);
        Fitbit.Api.Models.ActivityGoals SetStepGoal(int newStepGoal);
        System.Collections.Generic.List<Fitbit.Api.Models.ApiSubscription> GetSubscriptions();
        Fitbit.Api.Models.TimeSeriesDataList GetTimeSeries(Fitbit.Api.Models.TimeSeriesResourceType timeSeriesResourceType, DateTime endDate, Fitbit.Api.Models.DateRangePeriod period);
        Fitbit.Api.Models.TimeSeriesDataList GetTimeSeries(Fitbit.Api.Models.TimeSeriesResourceType timeSeriesResourceType, DateTime endDate, Fitbit.Api.Models.DateRangePeriod period, string userId);
        Fitbit.Api.Models.TimeSeriesDataList GetTimeSeries(Fitbit.Api.Models.TimeSeriesResourceType timeSeriesResourceType, DateTime startDate, DateTime endDate);
        Fitbit.Api.Models.TimeSeriesDataList GetTimeSeries(Fitbit.Api.Models.TimeSeriesResourceType timeSeriesResourceType, DateTime startDate, DateTime endDate, string userId);
        Fitbit.Api.Models.TimeSeriesDataListInt GetTimeSeriesInt(Fitbit.Api.Models.TimeSeriesResourceType timeSeriesResourceType, DateTime baseDate, string endDateOrPeriod, string userId);
        Fitbit.Api.Models.TimeSeriesDataListInt GetTimeSeriesInt(Fitbit.Api.Models.TimeSeriesResourceType timeSeriesResourceType, DateTime endDate, Fitbit.Api.Models.DateRangePeriod period);
        Fitbit.Api.Models.TimeSeriesDataListInt GetTimeSeriesInt(Fitbit.Api.Models.TimeSeriesResourceType timeSeriesResourceType, DateTime endDate, Fitbit.Api.Models.DateRangePeriod period, string userId);
        Fitbit.Api.Models.TimeSeriesDataListInt GetTimeSeriesInt(Fitbit.Api.Models.TimeSeriesResourceType timeSeriesResourceType, DateTime startDate, DateTime endDate);
        Fitbit.Api.Models.TimeSeriesDataListInt GetTimeSeriesInt(Fitbit.Api.Models.TimeSeriesResourceType timeSeriesResourceType, DateTime startDate, DateTime endDate, string userId);
        Fitbit.Api.Models.UserProfile GetUserProfile();
        Fitbit.Api.Models.UserProfile GetUserProfile(string encodedUserId);
        Fitbit.Api.Models.Weight GetWeight(DateTime startDate, Fitbit.Api.Models.DateRangePeriod period);
        Fitbit.Api.Models.Weight GetWeight(DateTime startDate, DateTime? endDate = null);
        Fitbit.Api.Models.ApiSubscription RemoveSubscription(Fitbit.Api.Models.APICollectionType apiCollectionType, string uniqueSubscriptionId);
        Fitbit.Api.Models.HeartRateLog LogHeartRate(Fitbit.Api.Models.HeartRateLog log, string userId);
        Fitbit.Api.Models.HeartRates GetHeartRates(DateTime date);
        void DeleteHeartRateLog(int logId);
    }
}