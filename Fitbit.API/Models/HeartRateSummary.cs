﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitbit.Api.Models
{
    public class HeartRateSummary
    {
        public int HeartRate { get; set; }
        public string Tracker { get; set; }
    }
}
