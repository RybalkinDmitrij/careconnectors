﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fitbit.Api.Models
{
    public class ActivityDistance
    {
        public string Activity { get; set; }
        public float Distance { get; set; }
    }
}
