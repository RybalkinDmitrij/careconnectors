﻿using Fitbit.Api.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitbit.Api.Models
{
    public class FoodPlan
    {
        public Intensity Intensity { get; set; }

        public DateTime EstimatedDate { get; set; }

        public Boolean Personalized { get; set; }
    }
}
