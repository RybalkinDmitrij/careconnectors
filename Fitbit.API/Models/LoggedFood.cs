﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fitbit.Api.Models
{
    public class LoggedFood
    {
        //Todo: Enumerate access level
        public string AccessLevel { get; set; }
        public float Amount { get; set; }
        public string Brand { get; set; }
        public float Calories { get; set; }
        public long FoodId { get; set; }
        public long MealTypeId { get; set; }
        //Todo: Map to a locale object
        public string Locale { get; set; }
        public string Name { get; set; }
        //Todo: Add unit and units
        public FoodLogUnit Unit { get; set; }
    }
}
