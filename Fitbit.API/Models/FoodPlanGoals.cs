﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fitbit.Api.Models
{
    public class FoodPlanGoals
    {
        public FoodPlanGoal Goals { get; set; }

        public FoodPlan FoodPlan { get; set; }
    }
}
