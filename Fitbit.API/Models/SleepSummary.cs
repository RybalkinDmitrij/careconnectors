﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fitbit.Api.Models
{
    public class SleepSummary
    {
        public int TotalSleepRecords { get; set; }
        public int TotalMinutesAsleep { get; set; }
        public int TotalTimeInBed { get; set; }
    }
}
