﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fitbit.Api.Models
{
    public class WeightGoal
    {
        public DateTime StartDate { get; set; }

        public float StartWeight { get; set; }

        public float Weight { get; set; }
    }
}
