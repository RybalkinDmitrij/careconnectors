﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitbit.Api.Models
{
    public class FoodPlanGoal
    {
        public Int32 Calories { get; set; }
    }
}
