﻿using System.Collections.Generic;

namespace Fitbit.Api.Models
{
    public class SleepData
    {
        public List<SleepLog> Sleep { get; set; }
        public SleepSummary Summary { get; set; }
    }
}