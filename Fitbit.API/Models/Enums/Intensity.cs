﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitbit.Api.Models.Enums
{
    public enum Intensity
    {
        Maintenance = 1,
        Easier = 2,
        Medium = 3,
        Kindahard = 4,
        Harder = 5
    }
}
