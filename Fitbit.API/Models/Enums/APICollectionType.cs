﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fitbit.Api.Models
{
    public enum APICollectionType
    {
        activities,
        foods,
        meals,
        sleep,
        body,
        user,
        weight    
    }
}
