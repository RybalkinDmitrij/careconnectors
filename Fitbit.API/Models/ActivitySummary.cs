﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Fitbit.Api.Models
{
    public class ActivitySummary
    {
        // removed from Fitbit API:  https://groups.google.com/forum/#!topic/fitbit-api/8IRaX6RW7g4
        //public int ActiveScore{ get; set; }
        public int ActivityCalories { get; set; }
        public int CaloriesBMR { get; set; }
        public float Elevation { get; set; }
        public int Floors { get; set; }
        public int MarginalCalories { get; set; }
        public int VeryActiveMinutes { get; set; }
        public int CaloriesOut { get; set; }
        public List<ActivityDistance> Distances { get; set; }
        public float FairlyActiveMinutes { get; set; }
        public float LightlyActiveMinutes { get; set; }
        public float SedentaryMinutes { get; set; }
        public int Steps { get; set; }

        public Dictionary<string, float> GetDistancesAsDictionary()
        {
            Dictionary<string, float> activity = new Dictionary<string, float>();
            foreach (ActivityDistance ad in this.Distances)
            {
                activity.Add(ad.Activity, ad.Distance);
            }
            return activity;
        }

    }

}