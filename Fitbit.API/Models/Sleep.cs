﻿using System.Collections.Generic;

namespace Fitbit.Api.Models
{
    public class Sleep
    {
        public List<SleepLog> SleepLog { get; set; }
    }
}
