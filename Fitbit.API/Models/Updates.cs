﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fitbit.Api.Models
{
    public class Updates
    {
        public List<UpdatedResource> UpdatesList { get; set; }
    }
}
