﻿using System;

namespace Fitbit.Api.Models
{
    public class MinuteData
    {
        public DateTime DateTime { get; set; }
        public int Value { get; set; }
    }
}