﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fitbit.Api.Models
{
    public class Water
    {
        public Int32 LogId { get; set; }

        public Int32 Amount { get; set; }
    }
}
